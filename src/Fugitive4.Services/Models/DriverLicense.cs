using System;
using System.Collections.Generic;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Services.Migrations;


namespace Fugitive4.Services.Models
{
	public class DriverLicense : AuditableEntityBase, IAddress
    {
		public int Id { get; set; }
		public string DLNumber { get; set; }
		public string FullName { get; set; }
		public string IssuingState { get; set; }
		public Nullable<System.DateTime> IssueDate { get; set; }
		public Nullable<System.DateTime> ExpirationDate { get; set; }
		public string Notes { get; set; }
		public int SubjectId { get; set; }
		public string Address1 { get; set; }
		public string Address2 { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string Zipcode { get; set; }

		
        public Subject Subject { get; set; } //Parent
    }

}
