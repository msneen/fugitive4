using System;
using System.Collections.Generic;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Services.Migrations;


namespace Fugitive4.Services.Models
{
	public class Video : AuditableEntityBase
    {
		public int Id { get; set; }
		public string Url { get; set; }
		public string Notes { get; set; }
		public int SubjectId { get; set; }

		
        public Subject Subject { get; set; } //Parent
    }

}
