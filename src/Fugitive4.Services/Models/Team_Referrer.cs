using System;
using System.Collections.Generic;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Services.Migrations;


namespace Fugitive4.Services.Models
{
	public class Team_Referrer : AuditableEntityBase
    {
		public int Id { get; set; }
		public int ReferrerId { get; set; }
		public int TeamId { get; set; }

		
        public Referrer Referrer { get; set; } //Parent
		
        public Team Team { get; set; } //Parent
    }

}
