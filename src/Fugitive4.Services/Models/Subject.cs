using System;
using System.Collections.Generic;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Services.Migrations;
using Fugitive4.Services.ModelsUnmapped;


namespace Fugitive4.Services.Models
{
	public class Subject : AuditableEntityBase
    {
		public int Id { get; set; }
		public Nullable<System.DateTime> DOB { get; set; }
		public string EyeColor { get; set; }
		public string Firstname { get; set; }
		public string HairColor { get; set; }
		public string Height { get; set; }
		public string Lastname { get; set; }
		public string Middlename { get; set; }
		public string SSN { get; set; }
        public int PerceivedThreatLevel { get; set; }
		public Nullable<int> Weight { get; set; }
		public int ReferrerId { get; set; }
		public int GenderId { get; set; }
		public string Notes { get; set; }
		public int RaceId { get; set; }

		
        public Referrer Referrer { get; set; } //Parent
		public ICollection<Address> Addresses { get; set; }//Child
		public ICollection<BailBond> BailBonds { get; set; }//Child
		public ICollection<EmailAddress> EmailAddresses { get; set; }//Child
		
        public Gender Gender { get; set; } //Parent
		public ICollection<PhoneNumber> PhoneNumbers { get; set; }//Child
		public ICollection<Vehicle> Vehicles { get; set; }//Child
		public ICollection<ActivityNote> ActivityNotes { get; set; }//Child
		public ICollection<DriverLicense> DriverLicenses { get; set; }//Child
		public ICollection<StoredFileCondensed> StoredFiles { get; set; }//Child
		public ICollection<Hyperlink> Hyperlinks { get; set; }//Child
		public ICollection<SearchResult> SearchResults { get; set; }//Child
		public ICollection<Video> Videos { get; set; }//Child
        public ICollection<AKA> AKAs { get; set; }//Child
		
        public Race Race { get; set; } //Parent
		public ICollection<SubjectRelationship> SubjectRelationships { get; set; }//Child
		public ICollection<SubjectRelationship> OtherSubjectRelationships { get; set; }//Child

		public ICollection<SubjectSearchRecipe> SubjectSearchRecipes { get; set; }//Child
		public ICollection<SubjectSearchRecipeItem> SubjectSearchRecipeItems { get; set; }//Child
		public ICollection<Employer> Employers { get; set; }//Child
		public ICollection<Todo> Todos { get; set; }//Child
    }

}
