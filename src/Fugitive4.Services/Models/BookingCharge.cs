using System;
using System.Collections.Generic;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Services.Migrations;


namespace Fugitive4.Services.Models
{
	public class BookingCharge : AuditableEntityBase, IAddress
    {
		public int Id { get; set; }
		public string BookingNumber { get; set; }
		public string ChargeName { get; set; }
		public string CodeNumber { get; set; }
		public bool Felony { get; set; }
		public Nullable<System.DateTime> DateOccurred { get; set; }
		public string Jurisdiction { get; set; }
		public string PoliceAgency { get; set; }
		public string Address1 { get; set; }
		public string Address2 { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string Zipcode { get; set; }
		public string Detectives { get; set; }
		public string Victim { get; set; }
		public string Details { get; set; }
		public string ReportNumber { get; set; }
		public string Description { get; set; }
		public string Notes { get; set; }
		public int BailBondId { get; set; }

		
        public BailBond BailBond { get; set; } //Parent
    }

}
