using System;
using System.Collections.Generic;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Services.Migrations;


namespace Fugitive4.Services.Models
{
	public class SubjectSearchRecipeItem : AuditableEntityBase
    {
		public int Id { get; set; }
		public int SearchRecipeItemId { get; set; }
		public int SubjectId { get; set; }

		
        public SearchRecipeItem SearchRecipeItem { get; set; } //Parent
		public ICollection<SearchResult> SearchResults { get; set; }//Child
		
        public Subject Subject { get; set; } //Parent
    }

}
