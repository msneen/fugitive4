using System;
using System.Collections.Generic;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Services.Migrations;


namespace Fugitive4.Services.Models
{
	public class BailBond : AuditableEntityBase
    {
		public int Id { get; set; }
		public Nullable<System.DateTime> OriginalArrestDate { get; set; }
		public Nullable<System.DateTime> IssueDate { get; set; }
		public Nullable<System.DateTime> DefaultDate { get; set; }
		public string IssuingBondsman { get; set; }
		public string IssuingBondCompany { get; set; }
		public string Court { get; set; }
		public string Description { get; set; }
		public int SubjectId { get; set; }
		public string BookingNumber { get; set; }
		public Nullable<decimal> BondAmount { get; set; }
		public Nullable<decimal> PaymentAmount { get; set; }
		public Nullable<System.DateTime> PaymentDate { get; set; }
		public bool? Done { get; set; }

		
        public Subject Subject { get; set; } //Parent
		public ICollection<BookingCharge> BookingCharges { get; set; }//Child
    }

}
