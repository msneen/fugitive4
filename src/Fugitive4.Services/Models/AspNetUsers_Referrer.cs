using System;
using System.Collections.Generic;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Services.Migrations;
using System.ComponentModel.DataAnnotations;

namespace Fugitive4.Services.Models
{
	public class AspNetUsers_Referrer : AuditableEntityBase
    {
		public int Id { get; set; }
		public int ReferrerId { get; set; }
        
        [MaxLength(450)]
		public string AspNetUsersId { get; set; }

		
        public ApplicationUser ApplicationUser { get; set; } //Parent
		
        public Referrer Referrer { get; set; } //Parent
    }

}
