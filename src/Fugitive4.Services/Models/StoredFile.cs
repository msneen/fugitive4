using System;
using System.Collections.Generic;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Services.Migrations;


namespace Fugitive4.Services.Models
{
	public class StoredFile : AuditableEntityBase
    {
		public int Id { get; set; }
		public string FileName { get; set; }
		public string ContentType { get; set; }
		public byte[] FileContent { get; set; }
		public string FileType { get; set; }
		public string Notes { get; set; }
		public int SubjectId { get; set; }
		public bool IsSubject { get; set; }
		public bool ShowOnDetail { get; set; }

		
        public Subject Subject { get; set; } //Parent
    }

}
