﻿using System.Collections.Generic;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Services.Models;

namespace Fugitive4.Services.Models
{
    public class Referrer : AuditableEntityBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string AdditionalInfo { get; set; }

        public ICollection<Subject> Subjects { get; set; }

        public ICollection<Team_Referrer> Team_Referrers { get; set; }//Child
        public ICollection<AspNetUsers_Referrer> AspNetUser_Referrers { get; set; }//Child
    }
}
