using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Services.Migrations;


namespace Fugitive4.Services.Models
{
	public class Team : AuditableEntityBase
    {
		public int Id { get; set; }
        [MaxLength(50)]
		public string Name { get; set; }
		public string Description { get; set; }


		public ICollection<AspNetUsers_Team> AspNetUsers_Teams { get; set; }//Child
		public ICollection<Team_Referrer> Team_Referrers { get; set; }//Child
    }

}
