﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Fugitive4.Services.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public ICollection<AspNetUsers_Team> ApplicationUser_Teams { get; set; }//Child
        public ICollection<AspNetUsers_Referrer> ApplicationUser_Referrers { get; set; }//Child
    }
}
