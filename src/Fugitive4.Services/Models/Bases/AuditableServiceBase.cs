﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fugitive4.Services.Models.Bases
{
    public class AuditableServiceBase
    {
        public void SetCreateModify(IAuditableEntityBase poco, string userId)
        {
            if (poco.CreatedDate == DateTime.Parse("1/1/0001").Date)
            {
                poco.CreatedDate = DateTime.Now;
                poco.CreatedBy = userId;
            }

            poco.ModifiedBy = userId;
            poco.ModifiedDate = DateTime.Now;
        }
    }
}
