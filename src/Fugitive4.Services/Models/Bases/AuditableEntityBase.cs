﻿using System;

namespace Fugitive4.Services.Models.Bases
{
    public interface IAuditableEntityBase
    {
        string CreatedBy { get; set; }
        DateTime CreatedDate { get; set; }
        string ModifiedBy { get; set; }
        DateTime? ModifiedDate { get; set; }
        bool Disabled { get; set; }
    }
    public class AuditableEntityBase : IAuditableEntityBase
    {
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public bool Disabled { get; set; }
    }
}
