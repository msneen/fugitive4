using System;
using System.Collections.Generic;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Services.Migrations;


namespace Fugitive4.Services.Models
{
	public class Todo : AuditableEntityBase
    {
		public int Id { get; set; }
		public string Synopsis { get; set; }
		public string Description { get; set; }
		public string Notes { get; set; }
		public string AssignedTo { get; set; }
		public bool Done { get; set; }
		public int TodoStatusId { get; set; }
		public Nullable<int> SubjectId { get; set; }
		public Nullable<int> SortOrder { get; set; }

		
        public Subject Subject { get; set; } //Parent
		
        public TodoStatus TodoStatus { get; set; } //Parent
    }

}
