using System;
using System.Collections.Generic;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Services.Migrations;


namespace Fugitive4.Services.Models
{
	public class AspNetUsers_Team : AuditableEntityBase
    {
		public int Id { get; set; }
		public string ApplicationUserId { get; set; }
		public int TeamId { get; set; }
	
        public ApplicationUser ApplicationUser { get; set; } //Parent		
        public Team Team { get; set; } //Parent
    }

}
