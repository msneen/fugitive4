using System;
using System.Collections.Generic;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Services.Migrations;


namespace Fugitive4.Services.Models
{
	public class SearchRecipeItem : AuditableEntityBase
    {
		public int Id { get; set; }
		public string Name { get; set; }
		public string UrlOrSource { get; set; }
		public string Description { get; set; }
		public int SearchRecipeId { get; set; }
		public bool Disabled { get; set; }
		public string CreatedBy { get; set; }
		public System.DateTime CreatedDate { get; set; }
		public string ModifiedBy { get; set; }
		public Nullable<System.DateTime> ModifiedDate { get; set; }
		public int SortOrder { get; set; }

		
        public SearchRecipe SearchRecipe { get; set; } //Parent
    }

}
