using System;
using System.Collections.Generic;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Services.Migrations;


namespace Fugitive4.Services.Models
{
	public class SubjectRelationship : AuditableEntityBase
    {
		public int Id { get; set; }
		public int SubjectId { get; set; }
		public int RelatedSubjectId { get; set; }
		public int RelationshipTypeId { get; set; }
		public string Notes { get; set; }
		public string Description { get; set; }
		public Nullable<int> EmployerId { get; set; }
		public Nullable<int> BailBondId { get; set; }
		public Nullable<int> SortOrder { get; set; }

		
        public RelationshipType RelationshipType { get; set; } //Parent
		
        public Subject Subject { get; set; } //Parent
		
        public Subject RelatedSubject { get; set; } //Parent
		
        public BailBond BailBond { get; set; } //Parent
		
        public Employer Employer { get; set; } //Parent
    }

}
