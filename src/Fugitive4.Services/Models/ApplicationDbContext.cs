﻿using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Data.Entity;
using Microsoft.Extensions.Configuration;
using Microsoft.Data.Entity.Metadata;
using Fugitive4.Services.Models;

namespace Fugitive4.Services.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public IConfigurationRoot Configuration { get; set; }

        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Referrer> Referrers { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<AddressType> AddressTypes { get; set; }
        public DbSet<Gender> Genders { get; set; }
        public DbSet<PhoneType> PhoneTypes { get; set; }
        public DbSet<PhoneNumber> PhoneNumbers { get; set; }
        public DbSet<EmailType> EmailTypes { get; set; }
        public DbSet<EmailAddress> EmailAddresses { get; set; }
        public DbSet<BailBond> BailBonds { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<ActivityNote> ActivityNotes { get; set; }
        public DbSet<DriverLicense> DriverLicenses { get; set; }
        public DbSet<StoredFile> StoredFiles { get; set; }
        public DbSet<Hyperlink> Hyperlinks { get; set; }
        public DbSet<SearchResult> SearchResults { get; set; }
        public DbSet<Video> Videos { get; set; }
        public DbSet<Race> Races { get; set; }
        public DbSet<RelationshipType> RelationshipTypes { get; set; }
        public DbSet<SubjectRelationship> SubjectRelationships { get; set; }
        public DbSet<BookingCharge> BookingCharges { get; set; }
        public DbSet<AKA> AKAS { get; set; }
        public DbSet<SearchRecipe> SearchRecipes { get; set; }
        public DbSet<SearchRecipeItem> SearchRecipeItems { get; set; }
        public DbSet<SubjectSearchRecipe> SubjectSearchRecipes { get; set; }
        public DbSet<SubjectSearchRecipeItem> SubjectSearchRecipeItems { get; set; }
        public DbSet<Employer> Employers { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<Team_Referrer> Team_Referrers { get; set; }
        public DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public DbSet<AspNetUsers_Team> AspNetUsers_Teams { get; set; }
        public DbSet<AspNetUsers_Referrer> AspNetUsers_Referrers { get; set; }
        public DbSet<TodoStatus> TodoStatuses { get; set; }
        public DbSet<Todo> Todos { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<Subject>()
                .Property(p => p.PerceivedThreatLevel)
                .HasDefaultValue(0);


            builder.Entity<Subject>().HasKey(q => q.Id);
            builder.Entity<SubjectRelationship>().HasKey(q => q.Id);
            builder.Entity<SubjectSearchRecipe>().HasKey(q => q.Id);

            builder.Entity<SubjectRelationship>()
                .HasOne(q => q.Subject)
                .WithMany(r=>r.SubjectRelationships)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<SubjectRelationship>()
                .HasOne(q => q.RelatedSubject)
                .WithMany(r => r.OtherSubjectRelationships)
                .OnDelete(DeleteBehavior.Restrict);


            builder.Entity<SubjectSearchRecipe>()
                .HasOne(q => q.Subject)
                .WithMany(r => r.SubjectSearchRecipes)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<SubjectSearchRecipe>()
                .HasOne(q => q.SearchRecipe)
                .WithMany(r => r.SubjectSearchRecipes)
                .OnDelete(DeleteBehavior.Restrict);


            builder.Entity<ApplicationUser>().HasKey(q => q.Id);
            builder.Entity<Team>().HasKey(q => q.Id);
            builder.Entity<AspNetUsers_Team>().HasKey(q => q.Id);
            builder.Entity<Team_Referrer>().HasKey(q => q.Id);
            builder.Entity<Referrer>().HasKey(q => q.Id);
            builder.Entity<AspNetUsers_Referrer>().Property(q => q.AspNetUsersId).HasMaxLength(450);



            builder.Entity<AspNetUsers_Team>()
                .HasOne(q => q.ApplicationUser)
                .WithMany(r => r.ApplicationUser_Teams)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<AspNetUsers_Team>()
                .HasOne(q => q.Team)
                .WithMany(r => r.AspNetUsers_Teams)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Team_Referrer>()
                .HasOne(q => q.Team)
                .WithMany(r => r.Team_Referrers)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Team_Referrer>()
                .HasOne(q => q.Referrer)
                .WithMany(r => r.Team_Referrers)
                .OnDelete(DeleteBehavior.Restrict);



            builder.Entity<AspNetUsers_Referrer>()
                .HasOne(q => q.ApplicationUser)
                .WithMany(r => r.ApplicationUser_Referrers)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<AspNetUsers_Referrer>()
                .HasOne(q => q.Referrer)
                .WithMany(r => r.AspNetUser_Referrers)
                .OnDelete(DeleteBehavior.Restrict)
                ;

            //Unique constraint
            builder
                .Entity<AspNetUsers_Referrer>()
                .HasIndex(u => new { u.ReferrerId, u.AspNetUsersId })
                .IsUnique();

            builder
                .Entity<SubjectRelationship>()
                .HasIndex(u => new { u.SubjectId, u.RelatedSubjectId })
                .IsUnique();

            builder
                .Entity<Team>()
                .HasIndex(u => u.Name)
                .IsUnique();

            builder
                .Entity<AddressType>()
                .HasIndex(u => u.Name)
                .IsUnique();

            builder
                .Entity<EmailType>()
                .HasIndex(u => u.Name)
                .IsUnique();

            builder
                .Entity<PhoneType>()
                .HasIndex(u => u.Name)
                .IsUnique();

            builder
                .Entity<SearchRecipe>()
                .HasIndex(u => u.Name)
                .IsUnique();


        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json");
            Configuration = builder.Build();

            optionsBuilder.UseSqlServer(Configuration["Data:DefaultConnection:ConnectionString"]);
            

        }
    }
}
