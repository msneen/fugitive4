using System;
using System.Collections.Generic;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Services.Migrations;


namespace Fugitive4.Services.Models
{
	public class Employer : AuditableEntityBase, IAddress
    {
		public int Id { get; set; }
		public string Name { get; set; }
		public string JobTitle { get; set; }
		public string PhoneNumber { get; set; }
		public string Address1 { get; set; }
		public string Address2 { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string Zipcode { get; set; }
		public int SubjectId { get; set; }
		public string Notes { get; set; }
		public Nullable<System.DateTime> StartDate { get; set; }
		public Nullable<System.DateTime> EndDate { get; set; }

		
        public Subject Subject { get; set; } //Parent
		public ICollection<SubjectRelationship> SubjectRelationships { get; set; }//Child
    }

}
