using System;
using System.Collections.Generic;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Services.Migrations;


namespace Fugitive4.Services.Models
{
	public class SubjectSearchRecipe : AuditableEntityBase
    {
		public int Id { get; set; }
		public int SubjectId { get; set; }
		public int SearchRecipeId { get; set; }
		public string Description { get; set; }
		public string Notes { get; set; }

		
        public SearchRecipe SearchRecipe { get; set; } //Parent
		
        public Subject Subject { get; set; } //Parent
    }

}
