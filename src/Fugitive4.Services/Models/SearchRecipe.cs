using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Services.Migrations;


namespace Fugitive4.Services.Models
{
	public class SearchRecipe : AuditableEntityBase
    {
		public int Id { get; set; }

        [MaxLength(150)]
        public string Name { get; set; }
		public string Description { get; set; }

		public ICollection<SearchRecipeItem> SearchRecipeItems { get; set; }//Child
		public ICollection<SubjectSearchRecipe> SubjectSearchRecipes { get; set; }//Child
    }

}
