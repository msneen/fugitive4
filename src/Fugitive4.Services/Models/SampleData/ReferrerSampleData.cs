﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace Fugitive4.Services.Models.SampleData
{
    public class ReferrerSampleData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetService<ApplicationDbContext>();
            if (!context.Referrers.Any(a => a.Name == "Happy Bail Bonds"))
            {
                context.Referrers.AddRange(
                    new Referrer
                    {
                        Name = "Happy Bail Bonds",
                        Description = "Test Bail Bond Company",
                        AdditionalInfo = "Test Company"
                    }
                );
                context.SaveChanges();
            }
            if (!context.Referrers.Any(a=>a.Name == "Golden Boy Bail Bonds"))
            {
                context.Referrers.AddRange(
                    new Referrer
                    {
                        Name = "Golden Boy Bail Bonds",
                        Description = "Golden Boy Bail Bonds",
                        AdditionalInfo = "Roy and Abel"
                    }
                );
                context.SaveChanges();
            }
            if (!context.Referrers.Any(a => a.Name == "Bail Hotline"))
            {
                context.Referrers.AddRange(
                    new Referrer
                    {
                        Name = "Bail Hotline",
                        Description = "Bail Hotline in Riverside",
                        AdditionalInfo = "Jaime"
                    }
                );
                context.SaveChanges();
            }
        }
    }
}
