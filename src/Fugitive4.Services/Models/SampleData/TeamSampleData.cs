﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace Fugitive4.Services.Models.SampleData
{
    public class TeamSampleData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetService<ApplicationDbContext>();
            if (!context.Teams.Any(t => t.Name =="Fugitive4"))
            {
                context.Teams.AddRange(
                    new Team
                    {
                        Name = "Fugitive4"
                    }
                );
                context.SaveChanges();
            }
        }
    }
}


