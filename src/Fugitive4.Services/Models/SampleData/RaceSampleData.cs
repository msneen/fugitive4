﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace Fugitive4.Services.Models.SampleData
{
    public class RaceSampleData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetService<ApplicationDbContext>();
            if (!context.Races.Any())
            {
                context.Races.AddRange(
                    new Race
                    {
                        Name = "White"
                    },
                    new Race
                    {
                        Name = "Latino"
                    },
                    new Race
                    {
                        Name = "Black"
                    },
                    new Race
                    {
                        Name = "Asian"
                    },
                    new Race
                    {
                        Name = "Middle Eastern"
                    },
                    new Race
                    {
                        Name = "Native American"
                    },
                    new Race
                    {
                        Name = "Other"
                    }
                );
                context.SaveChanges();
            }
        }
    }
}

