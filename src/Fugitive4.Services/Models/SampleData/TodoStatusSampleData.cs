﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace Fugitive4.Services.Models.SampleData
{
    public class TodoStatusSampleData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetService<ApplicationDbContext>();
            if (!context.TodoStatuses.Any())
            {
                context.TodoStatuses.AddRange(
                    new TodoStatus()
                    {
                        Name = "Idea Only"
                    },
                    new TodoStatus
                    {
                        Name = "Not Started"
                    },

                    new TodoStatus
                    {
                        Name = "In Progress"
                    },
                                        new TodoStatus
                    {
                        Name = "On Hold"
                    },
                    new TodoStatus
                    {
                        Name = "Done"
                    }
                );
                context.SaveChanges();
            }
        }
    }
}
