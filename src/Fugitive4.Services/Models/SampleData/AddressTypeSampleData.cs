﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace Fugitive4.Services.Models.SampleData
{
    public class AddressTypeSampleData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetService<ApplicationDbContext>();
            if (!context.AddressTypes.Any())
            {
                context.AddressTypes.AddRange(
                    new AddressType
                    {
                        Name = "Home"
                    },
                    new AddressType
                    {
                        Name = "Work"
                    },
                    new AddressType
                    {
                        Name = "Parent"
                    },
                    new AddressType
                    {
                        Name = "Significant Other"
                    },
                    new AddressType
                    {
                        Name = "Friend"
                    },
                    new AddressType
                    {
                        Name = "Unknown"
                    }
                );
                context.SaveChanges();
            }
        }
    }
}

