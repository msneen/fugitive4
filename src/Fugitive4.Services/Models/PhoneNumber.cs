using System;
using System.Collections.Generic;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Services.Migrations;


namespace Fugitive4.Services.Models
{
	public class PhoneNumber : AuditableEntityBase
    {
		public int Id { get; set; }
		public string PhoneNumber1 { get; set; }
		public string BelongsTo { get; set; }
		public int SubjectId { get; set; }
		public int PhoneTypeId { get; set; }
		public string Notes { get; set; }

		
        public PhoneType PhoneType { get; set; } //Parent
		
        public Subject Subject { get; set; } //Parent
    }

}
