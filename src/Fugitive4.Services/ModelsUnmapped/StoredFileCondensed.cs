﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fugitive4.Services.ModelsUnmapped
{
    public class StoredFileCondensed
    {
        public int Id { get; set; }
        public string FileName { get; set; }       
        public string Notes { get; set; }
        public int SubjectId { get; set; }
        public bool IsSubject { get; set; }
    }
}
