﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Fugitive4.Services.Enums
{
    public enum AddressSource
    {
        SubjectAddress,
        BailBond,
        Employer
    }

    public enum RoleName
    {
        [Description("MEMBER")]
        Member = 1,
        [Description("ADMINISTRATOR")]
        Admin,
        [Description("BAILBONDSMAN")]
        BailBondsman
    }

    public enum ClaimName
    {
        [Description("TeamAdmin")]
        TeamAdmin = 1,
        [Description("SearchRecipeAdmin")]
        SearchRecipeAdmin
    }

}
