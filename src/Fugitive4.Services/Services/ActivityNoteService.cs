using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface IActivityNoteService 	{
		IEnumerable<ActivityNote> GetAll(bool disabled = false);
		Task<ICollection<ActivityNote>> GetAllAsync(bool disabled = false);
		ActivityNote GetById(int id);
		Task<ActivityNote> GetByIdAsync(int id);
		Task InsertAsync(ActivityNote activityNote, string userId);
        void Update(ActivityNote activityNote, string userId);
        Task UpdateAsync(ActivityNote activityNote, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);
		List<ActivityNote> GetBySubjectId(int subjectId);
	}														

	public partial class ActivityNoteService : AuditableServiceBase, IActivityNoteService, IDisposable  
	{
		private readonly ApplicationDbContext _context;

		public ActivityNoteService(ApplicationDbContext context)
		{
			_context = context;
		}

		public IEnumerable<ActivityNote> GetAll(bool disabled = false) 
		{
			return _context.ActivityNotes.Where(x => x.Disabled == false || x.Disabled == disabled).ToList();
		}

		public async Task<ICollection<ActivityNote>> GetAllAsync(bool disabled = false)
        {
            return await _context.ActivityNotes.Where(x => x.Disabled == false || x.Disabled == disabled).ToListAsync();
        }

		public ActivityNote GetById(int id) 
		{	
			return _context.ActivityNotes.SingleOrDefault(e => e.Id == id);
		}

		public async Task<ActivityNote> GetByIdAsync(int id)
        {
            return await _context.ActivityNotes.SingleAsync(m => m.Id == id);
        }

		public void Insert(ActivityNote activityNote, string userId)
        {
			SetCreateModify(activityNote, userId);
            _context.ActivityNotes.Add(activityNote);
            _context.SaveChanges();
        }

		public async Task InsertAsync(ActivityNote activityNote, string userId)
        {
			SetCreateModify(activityNote, userId);
            _context.ActivityNotes.Add(activityNote);
            await _context.SaveChangesAsync();
        }

		public void Update(ActivityNote activityNote, string userId)
		{
			SetCreateModify(activityNote, userId);

			_context.Entry(activityNote).State = activityNote.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(ActivityNote activityNote, string userId)
        {
            SetCreateModify(activityNote, userId);

			_context.Entry(activityNote).State = activityNote.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			ActivityNote activityNote = _context.ActivityNotes.SingleOrDefault(e => e.Id == id);
			            if (activityNote == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(activityNote, userId);

            activityNote.Disabled = true;
            Update(activityNote, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var activityNote = await GetByIdAsync(id);
            SetCreateModify(activityNote, userId);

            activityNote.Disabled = true;

            await UpdateAsync(activityNote, userId);
        }

		private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }



        public List<ActivityNote> GetBySubjectId(int subjectId)
        {
            return _context.ActivityNotes.Where(x => x.SubjectId == subjectId).ToList();
        }
	}
}
