using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface ISubjectSearchRecipeService 	{
		IEnumerable<SubjectSearchRecipe> GetAll(bool disabled = false);
		Task<ICollection<SubjectSearchRecipe>> GetAllAsync(bool disabled = false);
		SubjectSearchRecipe GetById(int id);
		Task<SubjectSearchRecipe> GetByIdAsync(int id);
		Task InsertAsync(SubjectSearchRecipe subjectSearchRecipe, string userId);
        void Update(SubjectSearchRecipe subjectSearchRecipe, string userId);
        Task UpdateAsync(SubjectSearchRecipe subjectSearchRecipe, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);
		List<SubjectSearchRecipe> GetBySearchRecipeId(int searchRecipeId);
		List<SubjectSearchRecipe> GetBySubjectId(int subjectId);
	}														

	public partial class SubjectSearchRecipeService : AuditableServiceBase, ISubjectSearchRecipeService, IDisposable  
	{
		private readonly ApplicationDbContext _context;

		public SubjectSearchRecipeService(ApplicationDbContext context)
		{
			_context = context;
		}

		public IEnumerable<SubjectSearchRecipe> GetAll(bool disabled = false) 
		{
			return _context.SubjectSearchRecipes.Where(x => x.Disabled == false || x.Disabled == disabled).ToList();
		}

		public async Task<ICollection<SubjectSearchRecipe>> GetAllAsync(bool disabled = false)
        {
            return await _context
                .SubjectSearchRecipes
                .Where(x => x.Disabled == false || x.Disabled == disabled)
                .Include(s=>s.SearchRecipe).ThenInclude(t=>t.SearchRecipeItems)
                .ToListAsync();
        }

		public SubjectSearchRecipe GetById(int id) 
		{	
			return _context.SubjectSearchRecipes.SingleOrDefault(e => e.Id == id);
		}

		public async Task<SubjectSearchRecipe> GetByIdAsync(int id)
        {
            return await _context.SubjectSearchRecipes.SingleAsync(m => m.Id == id);
        }

		public void Insert(SubjectSearchRecipe subjectSearchRecipe, string userId)
        {
			SetCreateModify(subjectSearchRecipe, userId);
            _context.SubjectSearchRecipes.Add(subjectSearchRecipe);
            _context.SaveChanges();
        }

		public async Task InsertAsync(SubjectSearchRecipe subjectSearchRecipe, string userId)
        {
			SetCreateModify(subjectSearchRecipe, userId);
            _context.SubjectSearchRecipes.Add(subjectSearchRecipe);
            await _context.SaveChangesAsync();
        }

		public void Update(SubjectSearchRecipe subjectSearchRecipe, string userId)
		{
			SetCreateModify(subjectSearchRecipe, userId);

			_context.Entry(subjectSearchRecipe).State = subjectSearchRecipe.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(SubjectSearchRecipe subjectSearchRecipe, string userId)
        {
            SetCreateModify(subjectSearchRecipe, userId);

			_context.Entry(subjectSearchRecipe).State = subjectSearchRecipe.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			SubjectSearchRecipe subjectSearchRecipe = _context.SubjectSearchRecipes.SingleOrDefault(e => e.Id == id);
			            if (subjectSearchRecipe == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(subjectSearchRecipe, userId);

            subjectSearchRecipe.Disabled = true;
            Update(subjectSearchRecipe, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var subjectSearchRecipe = await GetByIdAsync(id);
            SetCreateModify(subjectSearchRecipe, userId);

            subjectSearchRecipe.Disabled = true;

            await UpdateAsync(subjectSearchRecipe, userId);
        }

		private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }



        public List<SubjectSearchRecipe> GetBySearchRecipeId(int searchRecipeId)
        {
            return _context.SubjectSearchRecipes.Where(x => x.SearchRecipeId == searchRecipeId).ToList();
        }

        public List<SubjectSearchRecipe> GetBySubjectId(int subjectId)
        {
            return _context.SubjectSearchRecipes.Where(x => x.SubjectId == subjectId).ToList();
        }
	}
}
