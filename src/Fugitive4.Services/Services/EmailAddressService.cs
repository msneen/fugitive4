using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface IEmailAddressService 	{
		IEnumerable<EmailAddress> GetAll(bool disabled = false);
		Task<ICollection<EmailAddress>> GetAllAsync(bool disabled = false);
		EmailAddress GetById(int id);
		Task<EmailAddress> GetByIdAsync(int id);
		Task InsertAsync(EmailAddress emailAddress, string userId);
        void Update(EmailAddress emailAddress, string userId);
        Task UpdateAsync(EmailAddress emailAddress, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);
		List<EmailAddress> GetByEmailTypeId(int emailTypeId);
		List<EmailAddress> GetBySubjectId(int subjectId);
	}														

	public partial class EmailAddressService : AuditableServiceBase, IEmailAddressService, IDisposable  
	{
		private readonly ApplicationDbContext _context;

		public EmailAddressService(ApplicationDbContext context)
		{
			_context = context;
		}

		public IEnumerable<EmailAddress> GetAll(bool disabled = false) 
		{
			return _context.EmailAddresses.Where(x => x.Disabled == false || x.Disabled == disabled).ToList();
		}

		public async Task<ICollection<EmailAddress>> GetAllAsync(bool disabled = false)
        {
            return await _context.EmailAddresses.Where(x => x.Disabled == false || x.Disabled == disabled).ToListAsync();
        }

		public EmailAddress GetById(int id) 
		{	
			return _context.EmailAddresses.SingleOrDefault(e => e.Id == id);
		}

		public async Task<EmailAddress> GetByIdAsync(int id)
        {
            return await _context.EmailAddresses.SingleAsync(m => m.Id == id);
        }

		public void Insert(EmailAddress emailAddress, string userId)
        {
			SetCreateModify(emailAddress, userId);
            _context.EmailAddresses.Add(emailAddress);
            _context.SaveChanges();
        }

		public async Task InsertAsync(EmailAddress emailAddress, string userId)
        {
			SetCreateModify(emailAddress, userId);
            _context.EmailAddresses.Add(emailAddress);
            await _context.SaveChangesAsync();
        }

		public void Update(EmailAddress emailAddress, string userId)
		{
			SetCreateModify(emailAddress, userId);

			_context.Entry(emailAddress).State = emailAddress.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(EmailAddress emailAddress, string userId)
        {
            SetCreateModify(emailAddress, userId);

			_context.Entry(emailAddress).State = emailAddress.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			EmailAddress emailAddress = _context.EmailAddresses.SingleOrDefault(e => e.Id == id);
			            if (emailAddress == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(emailAddress, userId);

            emailAddress.Disabled = true;
            Update(emailAddress, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var emailAddress = await GetByIdAsync(id);
            SetCreateModify(emailAddress, userId);

            emailAddress.Disabled = true;

            await UpdateAsync(emailAddress, userId);
        }

		private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }



        public List<EmailAddress> GetByEmailTypeId(int emailTypeId)
        {
            return _context.EmailAddresses.Where(x => x.EmailTypeId == emailTypeId).ToList();
        }

        public List<EmailAddress> GetBySubjectId(int subjectId)
        {
            return _context.EmailAddresses.Where(x => x.SubjectId == subjectId).ToList();
        }
	}
}
