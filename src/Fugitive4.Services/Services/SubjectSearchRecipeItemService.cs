using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface ISubjectSearchRecipeItemService 	{
		IEnumerable<SubjectSearchRecipeItem> GetAll(bool disabled = false);
		Task<ICollection<SubjectSearchRecipeItem>> GetAllAsync(bool disabled = false);
		SubjectSearchRecipeItem GetById(int id);
		Task<SubjectSearchRecipeItem> GetByIdAsync(int id);
		Task InsertAsync(SubjectSearchRecipeItem subjectSearchRecipeItem, string userId);
        void Update(SubjectSearchRecipeItem subjectSearchRecipeItem, string userId);
        Task UpdateAsync(SubjectSearchRecipeItem subjectSearchRecipeItem, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);
		List<SubjectSearchRecipeItem> GetBySearchRecipeItemId(int searchRecipeItemId);
		List<SubjectSearchRecipeItem> GetBySubjectId(int subjectId);
	}														

	public partial class SubjectSearchRecipeItemService : AuditableServiceBase, ISubjectSearchRecipeItemService, IDisposable  
	{
		private readonly ApplicationDbContext _context;

		public SubjectSearchRecipeItemService(ApplicationDbContext context)
		{
			_context = context;
		}

		public IEnumerable<SubjectSearchRecipeItem> GetAll(bool disabled = false) 
		{
			return _context.SubjectSearchRecipeItems
                .Where(x => x.Disabled == false || x.Disabled == disabled)
                .Include(x => x.SearchRecipeItem).ThenInclude(y => y.SearchRecipe)
                .ToList();
		}

		public async Task<ICollection<SubjectSearchRecipeItem>> GetAllAsync(bool disabled = false)
        {
            return await _context
                .SubjectSearchRecipeItems
                .Where(x => x.Disabled == false || x.Disabled == disabled)
                .Include(x=>x.SearchRecipeItem).ThenInclude(y=>y.SearchRecipe)
                .ToListAsync();
        }

		public SubjectSearchRecipeItem GetById(int id) 
		{	
			return _context.SubjectSearchRecipeItems.SingleOrDefault(e => e.Id == id);
		}

		public async Task<SubjectSearchRecipeItem> GetByIdAsync(int id)
        {
            return await _context.SubjectSearchRecipeItems.SingleAsync(m => m.Id == id);
        }

		public void Insert(SubjectSearchRecipeItem subjectSearchRecipeItem, string userId)
        {
			SetCreateModify(subjectSearchRecipeItem, userId);
            _context.SubjectSearchRecipeItems.Add(subjectSearchRecipeItem);
            _context.SaveChanges();
        }

		public async Task InsertAsync(SubjectSearchRecipeItem subjectSearchRecipeItem, string userId)
        {
			SetCreateModify(subjectSearchRecipeItem, userId);
            _context.SubjectSearchRecipeItems.Add(subjectSearchRecipeItem);
            await _context.SaveChangesAsync();
        }

		public void Update(SubjectSearchRecipeItem subjectSearchRecipeItem, string userId)
		{
			SetCreateModify(subjectSearchRecipeItem, userId);

			_context.Entry(subjectSearchRecipeItem).State = subjectSearchRecipeItem.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(SubjectSearchRecipeItem subjectSearchRecipeItem, string userId)
        {
            SetCreateModify(subjectSearchRecipeItem, userId);

			_context.Entry(subjectSearchRecipeItem).State = subjectSearchRecipeItem.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			SubjectSearchRecipeItem subjectSearchRecipeItem = _context.SubjectSearchRecipeItems.SingleOrDefault(e => e.Id == id);
			            if (subjectSearchRecipeItem == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(subjectSearchRecipeItem, userId);

            subjectSearchRecipeItem.Disabled = true;
            Update(subjectSearchRecipeItem, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var subjectSearchRecipeItem = await GetByIdAsync(id);
            SetCreateModify(subjectSearchRecipeItem, userId);

            subjectSearchRecipeItem.Disabled = true;

            await UpdateAsync(subjectSearchRecipeItem, userId);
        }

		private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }



        public List<SubjectSearchRecipeItem> GetBySearchRecipeItemId(int searchRecipeItemId)
        {
            return _context.SubjectSearchRecipeItems.Where(x => x.SearchRecipeItemId == searchRecipeItemId).ToList();
        }
    //

        public List<SubjectSearchRecipeItem> GetBySubjectId(int subjectId)
        {
            return _context.SubjectSearchRecipeItems.Where(x => x.SubjectId == subjectId).ToList();
        }
	}
}
