using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface IPhoneNumberService 	{
		IEnumerable<PhoneNumber> GetAll(bool disabled = false);
		Task<ICollection<PhoneNumber>> GetAllAsync(bool disabled = false);
		PhoneNumber GetById(int id);
		Task<PhoneNumber> GetByIdAsync(int id);
		Task InsertAsync(PhoneNumber phoneNumber, string userId);
        void Update(PhoneNumber phoneNumber, string userId);
        Task UpdateAsync(PhoneNumber phoneNumber, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);
		List<PhoneNumber> GetByPhoneTypeId(int phoneTypeId);
		List<PhoneNumber> GetBySubjectId(int subjectId);
	}														

	public partial class PhoneNumberService : AuditableServiceBase, IPhoneNumberService, IDisposable  
	{
		private readonly ApplicationDbContext _context;

		public PhoneNumberService(ApplicationDbContext context)
		{
			_context = context;
		}

		public IEnumerable<PhoneNumber> GetAll(bool disabled = false) 
		{
			return _context.PhoneNumbers.Where(x => x.Disabled == false || x.Disabled == disabled).ToList();
		}

		public async Task<ICollection<PhoneNumber>> GetAllAsync(bool disabled = false)
        {
            return await _context.PhoneNumbers.Where(x => x.Disabled == false || x.Disabled == disabled).ToListAsync();
        }

		public PhoneNumber GetById(int id) 
		{	
			return _context.PhoneNumbers.SingleOrDefault(e => e.Id == id);
		}

		public async Task<PhoneNumber> GetByIdAsync(int id)
        {
            return await _context.PhoneNumbers.SingleAsync(m => m.Id == id);
        }

		public void Insert(PhoneNumber phoneNumber, string userId)
        {
			SetCreateModify(phoneNumber, userId);
            _context.PhoneNumbers.Add(phoneNumber);
            _context.SaveChanges();
        }

		public async Task InsertAsync(PhoneNumber phoneNumber, string userId)
        {
			SetCreateModify(phoneNumber, userId);
            _context.PhoneNumbers.Add(phoneNumber);
            await _context.SaveChangesAsync();
        }

		public void Update(PhoneNumber phoneNumber, string userId)
		{
			SetCreateModify(phoneNumber, userId);

			_context.Entry(phoneNumber).State = phoneNumber.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(PhoneNumber phoneNumber, string userId)
        {
            SetCreateModify(phoneNumber, userId);

			_context.Entry(phoneNumber).State = phoneNumber.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			PhoneNumber phoneNumber = _context.PhoneNumbers.SingleOrDefault(e => e.Id == id);
			            if (phoneNumber == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(phoneNumber, userId);

            phoneNumber.Disabled = true;
            Update(phoneNumber, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var phoneNumber = await GetByIdAsync(id);
            SetCreateModify(phoneNumber, userId);

            phoneNumber.Disabled = true;

            await UpdateAsync(phoneNumber, userId);
        }

		private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }



        public List<PhoneNumber> GetByPhoneTypeId(int phoneTypeId)
        {
            return _context.PhoneNumbers.Where(x => x.PhoneTypeId == phoneTypeId).ToList();
        }

        public List<PhoneNumber> GetBySubjectId(int subjectId)
        {
            return _context.PhoneNumbers.Where(x => x.SubjectId == subjectId).ToList();
        }
	}
}
