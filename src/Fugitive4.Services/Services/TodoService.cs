using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface ITodoService 	{
		IEnumerable<Todo> GetAll(bool disabled = false);
		Task<ICollection<Todo>> GetAllAsync(bool disabled = false);
		Todo GetById(int id);
		Task<Todo> GetByIdAsync(int id);
		Task InsertAsync(Todo todo, string userId);
        void Update(Todo todo, string userId);
        Task UpdateAsync(Todo todo, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);
		List<Todo> GetBySubjectId(int subjectId);
		List<Todo> GetByTodoStatuId(int todoStatuId);
        IEnumerable<Todo> GetAllByUser(string userId, bool disabled = false);
	    Task<ICollection<Todo>> GetAllByUserAsync(string userId, bool disabled = false);
	}														

	public partial class TodoService : AuditableServiceBase, ITodoService, IDisposable  
	{
		private readonly ApplicationDbContext _context;
	    private readonly ITeamService _teamService;

	    public TodoService(ApplicationDbContext context, ITeamService teamService)
	    {
	        _context = context;
	        _teamService = teamService;
	    }

	    public IEnumerable<Todo> GetAll(bool disabled = false) 
		{
			return _context.Todos.Where(x => x.Disabled == false || x.Disabled == disabled).ToList();
		}

		public async Task<ICollection<Todo>> GetAllAsync(bool disabled = false)
        {
            return await _context.Todos.Where(x => x.Disabled == false || x.Disabled == disabled).ToListAsync();
        }

		public Todo GetById(int id) 
		{	
			return _context.Todos.SingleOrDefault(e => e.Id == id);
		}

		public async Task<Todo> GetByIdAsync(int id)
        {
            return await _context.Todos.SingleAsync(m => m.Id == id);
        }

		public void Insert(Todo todo, string userId)
        {
			SetCreateModify(todo, userId);
            _context.Todos.Add(todo);
            _context.SaveChanges();
        }

		public async Task InsertAsync(Todo todo, string userId)
        {
			SetCreateModify(todo, userId);
            _context.Todos.Add(todo);
            await _context.SaveChangesAsync();
        }

		public void Update(Todo todo, string userId)
		{
			SetCreateModify(todo, userId);

			_context.Entry(todo).State = todo.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(Todo todo, string userId)
        {
            SetCreateModify(todo, userId);

			_context.Entry(todo).State = todo.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			Todo todo = _context.Todos.SingleOrDefault(e => e.Id == id);
			            if (todo == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(todo, userId);

            todo.Disabled = true;
            Update(todo, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var todo = await GetByIdAsync(id);
            SetCreateModify(todo, userId);

            todo.Disabled = true;

            await UpdateAsync(todo, userId);
        }

		private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }



        public List<Todo> GetBySubjectId(int subjectId)
        {
            return _context.Todos.Where(x => x.SubjectId == subjectId).ToList();
        }

        public List<Todo> GetByTodoStatuId(int todoStatuId)
        {
            return _context.Todos.Where(x => x.TodoStatusId == todoStatuId).ToList();
        }

        public IEnumerable<Todo> GetAllByUser(string userId, bool disabled = false)
	    {
            var userTeams = _teamService.GetAspNetUsersTeams(userId);
            var userIdsInTeam = userTeams.Select(y => y.ApplicationUserId);
            return _context
                .Todos
                .Where(x => (x.Disabled == false || x.Disabled == disabled) &&
                               userIdsInTeam.Contains(x.CreatedBy))
                .ToList();
	    }

        public async Task<ICollection<Todo>> GetAllByUserAsync(string userId, bool disabled = false)
        {
            var userTeams = _teamService.GetAspNetUsersTeams(userId);
            var userIdsInTeam = userTeams.Select(y => y.ApplicationUser.UserName);
            return await _context
                .Todos
                .Where(x => (x.Disabled == false || x.Disabled == disabled) &&
                               userIdsInTeam.Contains(x.CreatedBy))
                .ToListAsync();
        }
    }
}
