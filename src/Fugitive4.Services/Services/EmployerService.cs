using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface IEmployerService 	{
		IEnumerable<Employer> GetAll(bool disabled = false);
		Task<ICollection<Employer>> GetAllAsync(bool disabled = false);
		Employer GetById(int id);
		Task<Employer> GetByIdAsync(int id);
		Task InsertAsync(Employer employer, string userId);
        void Update(Employer employer, string userId);
        Task UpdateAsync(Employer employer, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);
		List<Employer> GetBySubjectId(int subjectId);
	}														

	public partial class EmployerService : AuditableServiceBase, IEmployerService, IDisposable  
	{
		private readonly ApplicationDbContext _context;

		public EmployerService(ApplicationDbContext context)
		{
			_context = context;
		}

		public IEnumerable<Employer> GetAll(bool disabled = false) 
		{
			return _context.Employers.Where(x => x.Disabled == false || x.Disabled == disabled).ToList();
		}

		public async Task<ICollection<Employer>> GetAllAsync(bool disabled = false)
        {
            return await _context.Employers.Where(x => x.Disabled == false || x.Disabled == disabled).ToListAsync();
        }

		public Employer GetById(int id) 
		{	
			return _context.Employers.SingleOrDefault(e => e.Id == id);
		}

		public async Task<Employer> GetByIdAsync(int id)
        {
            return await _context.Employers.SingleAsync(m => m.Id == id);
        }

		public void Insert(Employer employer, string userId)
        {
			SetCreateModify(employer, userId);
            _context.Employers.Add(employer);
            _context.SaveChanges();
        }

		public async Task InsertAsync(Employer employer, string userId)
        {
			SetCreateModify(employer, userId);
            _context.Employers.Add(employer);
            await _context.SaveChangesAsync();
        }

		public void Update(Employer employer, string userId)
		{
			SetCreateModify(employer, userId);

			_context.Entry(employer).State = employer.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(Employer employer, string userId)
        {
            SetCreateModify(employer, userId);

			_context.Entry(employer).State = employer.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			Employer employer = _context.Employers.SingleOrDefault(e => e.Id == id);
			            if (employer == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(employer, userId);

            employer.Disabled = true;
            Update(employer, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var employer = await GetByIdAsync(id);
            SetCreateModify(employer, userId);

            employer.Disabled = true;

            await UpdateAsync(employer, userId);
        }

		private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }



        public List<Employer> GetBySubjectId(int subjectId)
        {
            return _context.Employers.Where(x => x.SubjectId == subjectId).ToList();
        }
    //
	}
}
