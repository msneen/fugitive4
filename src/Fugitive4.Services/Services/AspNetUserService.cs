using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface IAspNetUserService 	{
		IEnumerable<ApplicationUser> GetAll(bool disabled = false);
		Task<ICollection<ApplicationUser>> GetAllAsync(bool disabled = false);
        ApplicationUser GetById(string id);
		Task<ApplicationUser> GetByIdAsync(string id);
	    void Delete(string id);
	}														

	public partial class AspNetUserService : IAspNetUserService,  IDisposable  
	{
		private readonly ApplicationDbContext _context;

		public AspNetUserService(ApplicationDbContext context)
		{
			_context = context;
		}

		public IEnumerable<ApplicationUser> GetAll(bool disabled = false) 
		{
			return _context.ApplicationUsers.ToList();
		}

		public async Task<ICollection<ApplicationUser>> GetAllAsync(bool disabled = false)
        {
            return await _context.ApplicationUsers.ToListAsync();
        }

		public ApplicationUser GetById(string id) 
		{	
			return _context.ApplicationUsers.SingleOrDefault(e => e.Id == id);
		}

		public async Task<ApplicationUser> GetByIdAsync(string id)
        {
            return await _context.ApplicationUsers.SingleAsync(m => m.Id == id);
        }

        public void Delete(string id)
        {
            var user = _context.ApplicationUsers.SingleOrDefault(e => e.Id == id);
            _context.ApplicationUsers.Remove(user);
            _context.SaveChanges();
        }



        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


    //
	}
}
