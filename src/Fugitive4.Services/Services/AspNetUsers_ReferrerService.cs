using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface IAspNetUsers_ReferrerService 	{
		IEnumerable<AspNetUsers_Referrer> GetAll(bool disabled = false);
		Task<ICollection<AspNetUsers_Referrer>> GetAllAsync(bool disabled = false);
		AspNetUsers_Referrer GetById(int id);
		Task<AspNetUsers_Referrer> GetByIdAsync(int id);
		Task InsertAsync(AspNetUsers_Referrer aspNetUsers_Referrer, string userId);
        void Update(AspNetUsers_Referrer aspNetUsers_Referrer, string userId);
        Task UpdateAsync(AspNetUsers_Referrer aspNetUsers_Referrer, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);
		List<AspNetUsers_Referrer> GetByAspNetUserId(string aspNetUserId);
		List<AspNetUsers_Referrer> GetByReferrerId(int referrerId);
	}														

	public partial class AspNetUsers_ReferrerService : AuditableServiceBase, IAspNetUsers_ReferrerService, IDisposable  
	{
		private readonly ApplicationDbContext _context;

		public AspNetUsers_ReferrerService(ApplicationDbContext context)
		{
			_context = context;
		}

		public IEnumerable<AspNetUsers_Referrer> GetAll(bool disabled = false) 
		{
			return _context.AspNetUsers_Referrers.Where(x => x.Disabled == false || x.Disabled == disabled).ToList();
		}

		public async Task<ICollection<AspNetUsers_Referrer>> GetAllAsync(bool disabled = false)
        {
            return await _context
                .AspNetUsers_Referrers
                .Where(x => x.Disabled == false || x.Disabled == disabled)
                .Include(u=>u.ApplicationUser)
                .Include(u=>u.Referrer)
                .ToListAsync();
        }

		public AspNetUsers_Referrer GetById(int id) 
		{	
			return _context.AspNetUsers_Referrers.SingleOrDefault(e => e.Id == id);
		}

		public async Task<AspNetUsers_Referrer> GetByIdAsync(int id)
        {
            return await _context.AspNetUsers_Referrers.SingleAsync(m => m.Id == id);
        }

		public void Insert(AspNetUsers_Referrer aspNetUsers_Referrer, string userId)
        {
			SetCreateModify(aspNetUsers_Referrer, userId);
            _context.AspNetUsers_Referrers.Add(aspNetUsers_Referrer);
            _context.SaveChanges();
        }

		public async Task InsertAsync(AspNetUsers_Referrer aspNetUsers_Referrer, string userId)
        {
			SetCreateModify(aspNetUsers_Referrer, userId);
            _context.AspNetUsers_Referrers.Add(aspNetUsers_Referrer);
            await _context.SaveChangesAsync();
        }

		public void Update(AspNetUsers_Referrer aspNetUsers_Referrer, string userId)
		{
			SetCreateModify(aspNetUsers_Referrer, userId);

			_context.Entry(aspNetUsers_Referrer).State = aspNetUsers_Referrer.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(AspNetUsers_Referrer aspNetUsers_Referrer, string userId)
        {
            SetCreateModify(aspNetUsers_Referrer, userId);

			_context.Entry(aspNetUsers_Referrer).State = aspNetUsers_Referrer.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			AspNetUsers_Referrer aspNetUsers_Referrer = _context.AspNetUsers_Referrers.SingleOrDefault(e => e.Id == id);
			            if (aspNetUsers_Referrer == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(aspNetUsers_Referrer, userId);

            aspNetUsers_Referrer.Disabled = true;
            Update(aspNetUsers_Referrer, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var aspNetUsers_Referrer = await GetByIdAsync(id);
            SetCreateModify(aspNetUsers_Referrer, userId);

            aspNetUsers_Referrer.Disabled = true;

            await UpdateAsync(aspNetUsers_Referrer, userId);
        }

		private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }



        public List<AspNetUsers_Referrer> GetByAspNetUserId(string aspNetUserId)
        {
            return _context.AspNetUsers_Referrers.Where(x => x.AspNetUsersId == aspNetUserId).ToList();
        }

        public List<AspNetUsers_Referrer> GetByReferrerId(int referrerId)
        {
            return _context.AspNetUsers_Referrers.Where(x => x.ReferrerId == referrerId).ToList();
        }
	}
}
