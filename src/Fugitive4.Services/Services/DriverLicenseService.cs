using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface IDriverLicenseService 	{
		IEnumerable<DriverLicense> GetAll(bool disabled = false);
		Task<ICollection<DriverLicense>> GetAllAsync(bool disabled = false);
		DriverLicense GetById(int id);
		Task<DriverLicense> GetByIdAsync(int id);
		Task InsertAsync(DriverLicense driverLicense, string userId);
        void Update(DriverLicense driverLicense, string userId);
        Task UpdateAsync(DriverLicense driverLicense, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);
		List<DriverLicense> GetBySubjectId(int subjectId);
	}														

	public partial class DriverLicenseService : AuditableServiceBase, IDriverLicenseService, IDisposable  
	{
		private readonly ApplicationDbContext _context;

		public DriverLicenseService(ApplicationDbContext context)
		{
			_context = context;
		}

		public IEnumerable<DriverLicense> GetAll(bool disabled = false) 
		{
			return _context.DriverLicenses.Where(x => x.Disabled == false || x.Disabled == disabled).ToList();
		}

		public async Task<ICollection<DriverLicense>> GetAllAsync(bool disabled = false)
        {
            return await _context.DriverLicenses.Where(x => x.Disabled == false || x.Disabled == disabled).ToListAsync();
        }

		public DriverLicense GetById(int id) 
		{	
			return _context.DriverLicenses.SingleOrDefault(e => e.Id == id);
		}

		public async Task<DriverLicense> GetByIdAsync(int id)
        {
            return await _context.DriverLicenses.SingleAsync(m => m.Id == id);
        }

		public void Insert(DriverLicense driverLicense, string userId)
        {
			SetCreateModify(driverLicense, userId);
            _context.DriverLicenses.Add(driverLicense);
            _context.SaveChanges();
        }

		public async Task InsertAsync(DriverLicense driverLicense, string userId)
        {
			SetCreateModify(driverLicense, userId);
            _context.DriverLicenses.Add(driverLicense);
            await _context.SaveChangesAsync();
        }

		public void Update(DriverLicense driverLicense, string userId)
		{
			SetCreateModify(driverLicense, userId);

			_context.Entry(driverLicense).State = driverLicense.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(DriverLicense driverLicense, string userId)
        {
            SetCreateModify(driverLicense, userId);

			_context.Entry(driverLicense).State = driverLicense.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			DriverLicense driverLicense = _context.DriverLicenses.SingleOrDefault(e => e.Id == id);
			            if (driverLicense == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(driverLicense, userId);

            driverLicense.Disabled = true;
            Update(driverLicense, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var driverLicense = await GetByIdAsync(id);
            SetCreateModify(driverLicense, userId);

            driverLicense.Disabled = true;

            await UpdateAsync(driverLicense, userId);
        }

		private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }



        public List<DriverLicense> GetBySubjectId(int subjectId)
        {
            return _context.DriverLicenses.Where(x => x.SubjectId == subjectId).ToList();
        }
	}
}
