using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface ITeam_ReferrerService 	{
		IEnumerable<Team_Referrer> GetAll(bool disabled = false);
		Task<ICollection<Team_Referrer>> GetAllAsync(bool disabled = false);
		Team_Referrer GetById(int id);
		Task<Team_Referrer> GetByIdAsync(int id);
        void Insert(Team_Referrer team_Referrer, string userId);
        Task InsertAsync(Team_Referrer team_Referrer, string userId);
        void Update(Team_Referrer team_Referrer, string userId);
        Task UpdateAsync(Team_Referrer team_Referrer, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);
		List<Team_Referrer> GetByReferrerId(int referrerId);
		List<Team_Referrer> GetByTeamId(int teamId);
	}														

	public partial class Team_ReferrerService : AuditableServiceBase, ITeam_ReferrerService, IDisposable  
	{
		private readonly ApplicationDbContext _context;

		public Team_ReferrerService(ApplicationDbContext context)
		{
			_context = context;
		}

		public IEnumerable<Team_Referrer> GetAll(bool disabled = false) 
		{
			return _context.Team_Referrers.Where(x => x.Disabled == false || x.Disabled == disabled).ToList();
		}

		public async Task<ICollection<Team_Referrer>> GetAllAsync(bool disabled = false)
        {
            return await _context.Team_Referrers.Where(x => x.Disabled == false || x.Disabled == disabled).ToListAsync();
        }

		public Team_Referrer GetById(int id) 
		{	
			return _context.Team_Referrers.SingleOrDefault(e => e.Id == id);
		}

		public async Task<Team_Referrer> GetByIdAsync(int id)
        {
            return await _context.Team_Referrers.SingleAsync(m => m.Id == id);
        }

		public void Insert(Team_Referrer team_Referrer, string userId)
        {
			SetCreateModify(team_Referrer, userId);
            _context.Team_Referrers.Add(team_Referrer);
            _context.SaveChanges();
        }

		public async Task InsertAsync(Team_Referrer team_Referrer, string userId)
        {
			SetCreateModify(team_Referrer, userId);
            _context.Team_Referrers.Add(team_Referrer);
            await _context.SaveChangesAsync();
        }

		public void Update(Team_Referrer team_Referrer, string userId)
		{
			SetCreateModify(team_Referrer, userId);

			_context.Entry(team_Referrer).State = team_Referrer.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(Team_Referrer team_Referrer, string userId)
        {
            SetCreateModify(team_Referrer, userId);

			_context.Entry(team_Referrer).State = team_Referrer.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			Team_Referrer team_Referrer = _context.Team_Referrers.SingleOrDefault(e => e.Id == id);
			            if (team_Referrer == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(team_Referrer, userId);

            team_Referrer.Disabled = true;
            Update(team_Referrer, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var team_Referrer = await GetByIdAsync(id);
            SetCreateModify(team_Referrer, userId);

            team_Referrer.Disabled = true;

            await UpdateAsync(team_Referrer, userId);
        }

		private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }



        public List<Team_Referrer> GetByReferrerId(int referrerId)
        {
            return _context.Team_Referrers.Where(x => x.ReferrerId == referrerId).ToList();
        }

        public List<Team_Referrer> GetByTeamId(int teamId)
        {
            return _context.Team_Referrers.Where(x => x.TeamId == teamId).ToList();
        }
	}
}
