using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface ITeamService 	{
		IEnumerable<Team> GetAll(bool disabled = false);
		Task<ICollection<Team>> GetAllAsync(bool disabled = false);
        Team GetById(int id);
		Task<Team> GetByIdAsync(int id);
        void Insert(Team team, string userId);
        Task InsertAsync(Team team, string userId);
        void Update(Team team, string userId);
        Task UpdateAsync(Team team, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);
	    List<int> GetTeamIds(string userId);
	    List<AspNetUsers_Team> GetAspNetUsersTeams(string userId);
	}														

	public partial class TeamService : AuditableServiceBase, ITeamService, IDisposable  
	{
		private readonly ApplicationDbContext _context;

		public TeamService(ApplicationDbContext context)
		{
			_context = context;
		}

		public IEnumerable<Team> GetAll(bool disabled = false) 
		{
			return _context.Teams.Where(x => x.Disabled == false || x.Disabled == disabled).ToList();
		}

		public async Task<ICollection<Team>> GetAllAsync(bool disabled = false)
        {
            return await _context.Teams.Where(x => x.Disabled == false || x.Disabled == disabled).ToListAsync();
        }

		public Team GetById(int id) 
		{	
			return _context.Teams.SingleOrDefault(e => e.Id == id);
		}

		public async Task<Team> GetByIdAsync(int id)
        {
            return await _context.Teams.SingleAsync(m => m.Id == id);
        }

		public void Insert(Team team, string userId)
        {
			SetCreateModify(team, userId);
            _context.Teams.Add(team);
            _context.SaveChanges();
        }

		public async Task InsertAsync(Team team, string userId)
        {
			SetCreateModify(team, userId);
            _context.Teams.Add(team);
            await _context.SaveChangesAsync();
        }

		public void Update(Team team, string userId)
		{
			SetCreateModify(team, userId);

			_context.Entry(team).State = team.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(Team team, string userId)
        {
            SetCreateModify(team, userId);

			_context.Entry(team).State = team.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			Team team = _context.Teams.SingleOrDefault(e => e.Id == id);
			            if (team == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(team, userId);

            team.Disabled = true;
            Update(team, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var team = await GetByIdAsync(id);
            SetCreateModify(team, userId);

            team.Disabled = true;

            await UpdateAsync(team, userId);
        }

        public List<int> GetTeamIds(string userId)
        {
            //Get the UserTeams
            var listOfUsers = GetAspNetUsersTeams(userId);
            var teamIds = listOfUsers.Select(s => s.TeamId).Distinct().ToList();
            return teamIds;
        }

	    public List<AspNetUsers_Team> GetAspNetUsersTeams(string userId)
	    {
	        var listOfUsers = new List<AspNetUsers_Team>();

	        var applicationUserId = _context.ApplicationUsers.FirstOrDefault(a => a.UserName == userId).Id;
	        var userTeams = _context.AspNetUsers_Teams.Where(t => t.ApplicationUserId == applicationUserId);
	        foreach (var userTeam in userTeams)
	        {
	            var users = _context
	                .AspNetUsers_Teams
	                .Where(t => t.TeamId == userTeam.TeamId)
	                .Include(s => s.ApplicationUser);

	            listOfUsers.AddRange(users);
	        }
	        return listOfUsers;
	    }

	    private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


    //
    //
	}
}
