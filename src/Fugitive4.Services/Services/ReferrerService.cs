﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Fugitive4.Services.Models;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface IReferrerService 	{
		IEnumerable<Referrer> GetAll(bool disabled = false);
		Task<ICollection<Referrer>> GetAllAsync(bool disabled = false);
		Referrer GetById(int id);
		Task<Referrer> GetByIdAsync(int id);
		Task InsertAsync(Referrer referrer, string userId);
        void Update(Referrer referrer, string userId);
        Task UpdateAsync(Referrer referrer, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);

        Task<ICollection<Referrer>> GetAllByUserAsync(string userId, bool disabled = false);
        IEnumerable<Referrer> GetAllByUser(string userId, bool disabled = false);
    }														

	public partial class ReferrerService : AuditableServiceBase, IReferrerService, IDisposable  
	{
		private readonly ApplicationDbContext _context;
	    private readonly ITeamService _teamService;

	    public ReferrerService(ApplicationDbContext context, ITeamService teamService)
	    {
	        _context = context;
	        _teamService = teamService;
	    }


	    public List<int> UserIdsInReferrerTeam(string userId)
        {
            var teamIds = _teamService.GetTeamIds(userId);

            //Get the TeamReferrers
            var referrerIds = _context
                                    .Team_Referrers
                                    .Where(tr => teamIds.Contains(tr.TeamId))
                                    .Select(t=>t.ReferrerId)
                                    .Distinct()
                                    .ToList();
            return referrerIds;
        }



	    public IEnumerable<Referrer> GetAllByUser(string userId, bool disabled = false) 
		{
            var referrerIds = UserIdsInReferrerTeam(userId);
			return _context
                .Referrers
                .Where(x => (x.Disabled == false || x.Disabled == disabled) &&
                    referrerIds.Contains(x.Id))
                .ToList();
		}

		public async Task<ICollection<Referrer>> GetAllByUserAsync(string userId, bool disabled = false)
        {
            var referrerIds = UserIdsInReferrerTeam(userId);
            return await _context
                            .Referrers
                            .Where(x => (x.Disabled == false || x.Disabled == disabled) &&
                                referrerIds.Contains(x.Id))
                            .ToListAsync();
        }

        public IEnumerable<Referrer> GetAll(bool disabled = false)
        {

            return _context
                .Referrers
                .Where(x => (x.Disabled == false || x.Disabled == disabled))
                .ToList();
        }

        public async Task<ICollection<Referrer>> GetAllAsync(bool disabled = false)
        {
            return await _context
                            .Referrers
                            .Where(x => (x.Disabled == false || x.Disabled == disabled))
                            .ToListAsync();
        }

        public Referrer GetById(int id) 
		{	
			return _context.Referrers.SingleOrDefault(e => e.Id == id);
		}

		public async Task<Referrer> GetByIdAsync(int id)
        {
            return await _context.Referrers.SingleAsync(m => m.Id == id);
        }

		public void Insert(Referrer referrer, string userId)
        {
			SetCreateModify(referrer, userId);
            _context.Referrers.Add(referrer);
            _context.SaveChanges();
        }

		public async Task InsertAsync(Referrer referrer, string userId)
        {
			SetCreateModify(referrer, userId);
            _context.Referrers.Add(referrer);
            await _context.SaveChangesAsync();
        }

		public void Update(Referrer referrer, string userId)
		{
			SetCreateModify(referrer, userId);

			_context.Entry(referrer).State = referrer.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(Referrer referrer, string userId)
        {
            SetCreateModify(referrer, userId);

			_context.Entry(referrer).State = referrer.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			Referrer referrer = _context.Referrers.SingleOrDefault(e => e.Id == id);
			            if (referrer == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(referrer, userId);

            referrer.Disabled = true;
            Update(referrer, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var referrer = await GetByIdAsync(id);
            SetCreateModify(referrer, userId);

            referrer.Disabled = true;

            await UpdateAsync(referrer, userId);
        }

		private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


    //
	}
}
