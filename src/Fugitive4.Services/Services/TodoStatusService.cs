using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface ITodoStatusService 	{
		IEnumerable<TodoStatus> GetAll(bool disabled = false);
		Task<ICollection<TodoStatus>> GetAllAsync(bool disabled = false);
		TodoStatus GetById(int id);
		Task<TodoStatus> GetByIdAsync(int id);
		Task InsertAsync(TodoStatus todoStatus, string userId);
        void Update(TodoStatus todoStatus, string userId);
        Task UpdateAsync(TodoStatus todoStatus, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);
	}														

	public partial class TodoStatusService : AuditableServiceBase, ITodoStatusService, IDisposable  
	{
		private readonly ApplicationDbContext _context;

		public TodoStatusService(ApplicationDbContext context)
		{
			_context = context;
		}

		public IEnumerable<TodoStatus> GetAll(bool disabled = false) 
		{
			return _context.TodoStatuses.Where(x => x.Disabled == false || x.Disabled == disabled).ToList();
		}

		public async Task<ICollection<TodoStatus>> GetAllAsync(bool disabled = false)
        {
            return await _context.TodoStatuses.Where(x => x.Disabled == false || x.Disabled == disabled).ToListAsync();
        }

		public TodoStatus GetById(int id) 
		{	
			return _context.TodoStatuses.SingleOrDefault(e => e.Id == id);
		}

		public async Task<TodoStatus> GetByIdAsync(int id)
        {
            return await _context.TodoStatuses.SingleAsync(m => m.Id == id);
        }

		public void Insert(TodoStatus todoStatus, string userId)
        {
			SetCreateModify(todoStatus, userId);
            _context.TodoStatuses.Add(todoStatus);
            _context.SaveChanges();
        }

		public async Task InsertAsync(TodoStatus todoStatus, string userId)
        {
			SetCreateModify(todoStatus, userId);
            _context.TodoStatuses.Add(todoStatus);
            await _context.SaveChangesAsync();
        }

		public void Update(TodoStatus todoStatus, string userId)
		{
			SetCreateModify(todoStatus, userId);

			_context.Entry(todoStatus).State = todoStatus.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(TodoStatus todoStatus, string userId)
        {
            SetCreateModify(todoStatus, userId);

			_context.Entry(todoStatus).State = todoStatus.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			TodoStatus todoStatus = _context.TodoStatuses.SingleOrDefault(e => e.Id == id);
			            if (todoStatus == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(todoStatus, userId);

            todoStatus.Disabled = true;
            Update(todoStatus, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var todoStatus = await GetByIdAsync(id);
            SetCreateModify(todoStatus, userId);

            todoStatus.Disabled = true;

            await UpdateAsync(todoStatus, userId);
        }

		private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


    //
	}
}
