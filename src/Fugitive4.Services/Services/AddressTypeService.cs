using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface IAddressTypeService 	{
		IEnumerable<AddressType> GetAll(bool disabled = false);
		Task<ICollection<AddressType>> GetAllAsync(bool disabled = false);
		AddressType GetById(int id);
		Task<AddressType> GetByIdAsync(int id);
		Task InsertAsync(AddressType addressType, string userId);
        void Update(AddressType addressType, string userId);
        Task UpdateAsync(AddressType addressType, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);
	}														

	public partial class AddressTypeService : AuditableServiceBase, IAddressTypeService, IDisposable  
	{
		private readonly ApplicationDbContext _context;

		public AddressTypeService(ApplicationDbContext context)
		{
			_context = context;
		}

		public IEnumerable<AddressType> GetAll(bool disabled = false) 
		{
			return _context.AddressTypes.Where(x => x.Disabled == false || x.Disabled == disabled).ToList();
		}

		public async Task<ICollection<AddressType>> GetAllAsync(bool disabled = false)
        {
            return await _context.AddressTypes.Where(x => x.Disabled == false || x.Disabled == disabled).ToListAsync();
        }

		public AddressType GetById(int id) 
		{	
			return _context.AddressTypes.SingleOrDefault(e => e.Id == id);
		}

		public async Task<AddressType> GetByIdAsync(int id)
        {
            return await _context.AddressTypes.SingleAsync(m => m.Id == id);
        }

		public void Insert(AddressType addressType, string userId)
        {
			SetCreateModify(addressType, userId);
            _context.AddressTypes.Add(addressType);
            _context.SaveChanges();
        }

		public async Task InsertAsync(AddressType addressType, string userId)
        {
			SetCreateModify(addressType, userId);
            _context.AddressTypes.Add(addressType);
            await _context.SaveChangesAsync();
        }

		public void Update(AddressType addressType, string userId)
		{
			SetCreateModify(addressType, userId);

			_context.Entry(addressType).State = addressType.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(AddressType addressType, string userId)
        {
            SetCreateModify(addressType, userId);

			_context.Entry(addressType).State = addressType.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			AddressType addressType = _context.AddressTypes.SingleOrDefault(e => e.Id == id);
			            if (addressType == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(addressType, userId);

            addressType.Disabled = true;
            Update(addressType, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var addressType = await GetByIdAsync(id);
            SetCreateModify(addressType, userId);

            addressType.Disabled = true;

            await UpdateAsync(addressType, userId);
        }

		private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


    //
	}
}
