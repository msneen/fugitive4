using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface ISubjectService 	{
		IEnumerable<Subject> GetAll(string userId, bool disabled = false);
		Task<ICollection<Subject>> GetAllAsync(string userId, bool disabled = false);
		Subject GetById(int id, string userId);
        Task<Subject> GetByIdAsync(int id, string userId);
        Task<Subject> GetByIdMediumAsync(int id, string userId);
        Task<Subject> GetByIdAllAsync(int id, string userId);
	    Task<Subject> GetByIdFourthColumnAsync(int id, string userId);
        Task InsertAsync(Subject subject, string userId);
        void Update(Subject subject, string userId);
        Task UpdateAsync(Subject subject, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);
		List<Subject> GetByReferrerId(int referrerId);
		List<Subject> GetByGenderId(int genderId);
		List<Subject> GetByRaceId(int raceId);

        Task<ICollection<Subject>> GetAllAsyncForBondsman(string userId, bool disabled = false);

    }														

	public partial class SubjectService : AuditableServiceBase, ISubjectService, IDisposable  
	{
		private readonly ApplicationDbContext _context;

		public SubjectService(ApplicationDbContext context)
		{
			_context = context;
		}

        public List<string> UsersIdsInUserTeam(string userId)
        {
            var listOfUsers = new List<AspNetUsers_Team>();

            var applicationUserId = _context.ApplicationUsers.FirstOrDefault(a => a.UserName == userId).Id;
            var userTeams = _context.AspNetUsers_Teams.Where(t => t.ApplicationUserId == applicationUserId);
            foreach(var userTeam in userTeams)
            {
                var users = _context
                    .AspNetUsers_Teams
                    .Where(t => t.TeamId == userTeam.TeamId)
                    .Include(s=>s.ApplicationUser);

                listOfUsers.AddRange(users);
            }
            var ids = listOfUsers.Select(s => s.ApplicationUser.UserName).Distinct().ToList();
            return ids;
        }

		public IEnumerable<Subject> GetAll(string userId, bool disabled = false) 
		{
            var otherTeamUsers = UsersIdsInUserTeam(userId);
			return _context
                .Subjects
                .Where(x => (x.Disabled == false || x.Disabled == disabled) &&
                    otherTeamUsers.Contains(x.CreatedBy))
                .Include(s => s.BailBonds)
                .ToList();
		}

        public async Task<ICollection<Subject>> GetAllAsync(string userId, bool disabled = false)
        {
            var otherTeamUsers = UsersIdsInUserTeam(userId);
            return await _context
                .Subjects
                .Where(x => (x.Disabled == false || x.Disabled == disabled) &&
                    otherTeamUsers.Contains(x.CreatedBy))
                .Include(s => s.BailBonds)
                .Include(r=>r.SubjectRelationships)
                .Include(r=>r.OtherSubjectRelationships)
                .Include(s => s.Addresses)//.ThenInclude(a => a.AddressType)
                .Include(s => s.PhoneNumbers)//.ThenInclude(a => a.PhoneType)
                .Include(s => s.Employers)
                .ToListAsync();
        }

        public async Task<ICollection<Subject>> GetAllAsyncForBondsman(string userId, bool disabled = false)
        {
            //var otherTeamUsers = UsersIdsInUserTeam(userId);
            var applicationUserId = _context.ApplicationUsers.FirstOrDefault(a => a.UserName == userId).Id;
            var referrersForUser = _context.AspNetUsers_Referrers.Where(ar => ar.AspNetUsersId == applicationUserId).Select(y=>y.ReferrerId).ToList<int>();

            return await _context
                .Subjects
                .Where(x => (x.Disabled == false || x.Disabled == disabled) &&
                    referrersForUser.Contains(x.ReferrerId))
                .Include(s => s.BailBonds)
                .Include(r => r.SubjectRelationships)
                .ToListAsync();
        }



        public Subject GetById(int id, string userId) 
		{
            var otherTeamUsers = UsersIdsInUserTeam(userId);
            var subject = _context
                .Subjects
                .Include(s=>s.Race)
                .Include(s=>s.Gender)
                //.Include(s=>s.StoredFiles)
                .Include(s=>s.Addresses).ThenInclude(a=>a.AddressType)
                .Include(s => s.PhoneNumbers).ThenInclude(a => a.PhoneType)
                .Include(s=>s.BailBonds).ThenInclude(b=>b.BookingCharges)
                .Include(s=>s.ActivityNotes).OrderByDescending(t=>t.CreatedDate)
                .Include(s=>s.Vehicles)
                .Include(s => s.DriverLicenses)
                .Include(s => s.EmailAddresses)
                .Include(s => s.Hyperlinks)
                .Include(s => s.SearchResults)
                .Include(s => s.Videos)
                .Include(s => s.AKAs)
                .Include(s=>s.SubjectSearchRecipes).ThenInclude(b=>b.SearchRecipe)
                .Where(x => otherTeamUsers.Contains(x.CreatedBy))
                .FirstOrDefault(e => e.Id == id);
            return subject;
		}

        public async Task<Subject> GetByIdAsync(int id, string userId)
        {
            var otherTeamUsers = UsersIdsInUserTeam(userId);
            return await _context
                .Subjects
                .Include(s => s.Race)
                .Include(s => s.Gender)               
                .Include(s => s.AKAs)
                .Where(x => otherTeamUsers.Contains(x.CreatedBy))
                .FirstOrDefaultAsync(m => m.Id == id);
        }

        public async Task<Subject> GetByIdMediumAsync(int id, string userId)
        {
            var otherTeamUsers = UsersIdsInUserTeam(userId);
            return await _context
                .Subjects
                .Include(s => s.Race)
                .Include(s => s.Gender)
                //.Include(s => s.StoredFiles)
                .Include(s => s.Addresses).ThenInclude(a => a.AddressType)
                .Include(s=>s.PhoneNumbers).ThenInclude(a=>a.PhoneType)
                .Include(s => s.BailBonds).ThenInclude(b => b.BookingCharges)
                .Include(s => s.Employers)
                .Include(s => s.Vehicles)
                .Include(s => s.AKAs)
                .Where(x => otherTeamUsers.Contains(x.CreatedBy))
                .FirstOrDefaultAsync(m => m.Id == id);
        }

        public async Task<Subject> GetByIdAllAsync(int id, string userId)
        {
            var otherTeamUsers = UsersIdsInUserTeam(userId);
            return await _context
                .Subjects
                //.Include(s => s.Race)
                //.Include(s => s.Gender)
                //.Include(s => s.StoredFiles)
                //.Include(s => s.Addresses).ThenInclude(a => a.AddressType)
                //.Include(s => s.PhoneNumbers).ThenInclude(a => a.PhoneType)
                //.Include(s => s.BailBonds).ThenInclude(b => b.BookingCharges)
                .Include(s => s.ActivityNotes).OrderByDescending(t => t.CreatedDate)
                //.Include(s => s.Vehicles)
                .Include(s => s.DriverLicenses)
                //.Include(s => s.EmailAddresses).ThenInclude(a => a.EmailType)
                //.Include(s => s.Hyperlinks)
                //.Include(s => s.SearchResults)
                //.Include(s => s.Videos)
                //.Include(s => s.AKAs)
                //.Include(s => s.Employers)
                //.Include(s => s.SubjectSearchRecipes).ThenInclude(b => b.SearchRecipe)
                .Include(s => s.Todos).ThenInclude(t=>t.TodoStatus)
                .Where(x => otherTeamUsers.Contains(x.CreatedBy))
                .FirstOrDefaultAsync(m => m.Id == id);
        }

        public async Task<Subject> GetByIdFourthColumnAsync(int id, string userId)
        {
            var otherTeamUsers = UsersIdsInUserTeam(userId);
            return await _context
                .Subjects
                //.Include(s => s.Race)
                //.Include(s => s.Gender)
                /////.Include(s => s.StoredFiles)//This errors if used
                //.Include(s => s.Addresses).ThenInclude(a => a.AddressType)
                //.Include(s => s.PhoneNumbers).ThenInclude(a => a.PhoneType)
                .Include(s => s.BailBonds).ThenInclude(b => b.BookingCharges)
                //.Include(s => s.ActivityNotes).OrderByDescending(t => t.CreatedDate)
                //.Include(s => s.Vehicles)
                .Include(s => s.DriverLicenses)
                //.Include(s => s.EmailAddresses).ThenInclude(a => a.EmailType)
                .Include(s => s.Hyperlinks)
                .Include(s => s.SearchResults)
                .Include(s => s.Videos)
                //.Include(s => s.AKAs)
                //.Include(s => s.Employers)
                //.Include(s => s.SubjectSearchRecipes).ThenInclude(b => b.SearchRecipe)
                //.Include(s => s.Todos).ThenInclude(t => t.TodoStatus)
                .Where(x => otherTeamUsers.Contains(x.CreatedBy))
                .FirstOrDefaultAsync(m => m.Id == id);
        }

        public void Insert(Subject subject, string userId)
        {
			SetCreateModify(subject, userId);
            _context.Subjects.Add(subject);
            _context.SaveChanges();
        }

		public async Task InsertAsync(Subject subject, string userId)
        {
			SetCreateModify(subject, userId);
            _context.Subjects.Add(subject);
            await _context.SaveChangesAsync();
        }

		public void Update(Subject subject, string userId)
		{
			SetCreateModify(subject, userId);

			_context.Entry(subject).State = subject.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(Subject subject, string userId)
        {
            SetCreateModify(subject, userId);

			_context.Entry(subject).State = subject.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			Subject subject = _context.Subjects.SingleOrDefault(e => e.Id == id);
			            if (subject == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(subject, userId);

            subject.Disabled = true;
            Update(subject, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var subject = await GetByIdMediumAsync(id, userId);
            SetCreateModify(subject, userId);

            subject.Disabled = true;

            await UpdateAsync(subject, userId);
        }

		private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }



        public List<Subject> GetByReferrerId(int referrerId)
        {
            return _context.Subjects.Where(x => x.ReferrerId == referrerId).ToList();
        }
    //
    //
    //

        public List<Subject> GetByGenderId(int genderId)
        {
            return _context.Subjects.Where(x => x.GenderId == genderId).ToList();
        }
    //
    //
    //
    //
    //
    //
    //
    //

        public List<Subject> GetByRaceId(int raceId)
        {
            return _context.Subjects.Where(x => x.RaceId == raceId).ToList();
        }
	}
}
