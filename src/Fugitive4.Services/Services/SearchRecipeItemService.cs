using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface ISearchRecipeItemService 	{
		IEnumerable<SearchRecipeItem> GetAll(bool disabled = false);
		Task<ICollection<SearchRecipeItem>> GetAllAsync(bool disabled = false);
		SearchRecipeItem GetById(int id);
		Task<SearchRecipeItem> GetByIdAsync(int id);
		Task InsertAsync(SearchRecipeItem searchRecipeItem, string userId);
        void Update(SearchRecipeItem searchRecipeItem, string userId);
        Task UpdateAsync(SearchRecipeItem searchRecipeItem, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);
		List<SearchRecipeItem> GetBySearchRecipeId(int searchRecipeId);
	}														

	public partial class SearchRecipeItemService : AuditableServiceBase, ISearchRecipeItemService, IDisposable  
	{
		private readonly ApplicationDbContext _context;

		public SearchRecipeItemService(ApplicationDbContext context)
		{
			_context = context;
		}

		public IEnumerable<SearchRecipeItem> GetAll(bool disabled = false) 
		{
			return _context.SearchRecipeItems.Where(x => x.Disabled == false || x.Disabled == disabled).ToList();
		}

		public async Task<ICollection<SearchRecipeItem>> GetAllAsync(bool disabled = false)
        {
            return await _context.SearchRecipeItems.Where(x => x.Disabled == false || x.Disabled == disabled).ToListAsync();
        }

		public SearchRecipeItem GetById(int id) 
		{	
			return _context.SearchRecipeItems.SingleOrDefault(e => e.Id == id);
		}

		public async Task<SearchRecipeItem> GetByIdAsync(int id)
        {
            return await _context.SearchRecipeItems.SingleAsync(m => m.Id == id);
        }

		public void Insert(SearchRecipeItem searchRecipeItem, string userId)
        {
			SetCreateModify(searchRecipeItem, userId);
            _context.SearchRecipeItems.Add(searchRecipeItem);
            _context.SaveChanges();
        }

		public async Task InsertAsync(SearchRecipeItem searchRecipeItem, string userId)
        {
			SetCreateModify(searchRecipeItem, userId);
            _context.SearchRecipeItems.Add(searchRecipeItem);
            await _context.SaveChangesAsync();
        }

		public void Update(SearchRecipeItem searchRecipeItem, string userId)
		{
			SetCreateModify(searchRecipeItem, userId);

			_context.Entry(searchRecipeItem).State = searchRecipeItem.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(SearchRecipeItem searchRecipeItem, string userId)
        {
            SetCreateModify(searchRecipeItem, userId);

			_context.Entry(searchRecipeItem).State = searchRecipeItem.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			SearchRecipeItem searchRecipeItem = _context.SearchRecipeItems.SingleOrDefault(e => e.Id == id);
			            if (searchRecipeItem == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(searchRecipeItem, userId);

            searchRecipeItem.Disabled = true;
            Update(searchRecipeItem, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var searchRecipeItem = await GetByIdAsync(id);
            SetCreateModify(searchRecipeItem, userId);

            searchRecipeItem.Disabled = true;

            await UpdateAsync(searchRecipeItem, userId);
        }

		private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }



        public List<SearchRecipeItem> GetBySearchRecipeId(int searchRecipeId)
        {
            return _context.SearchRecipeItems.Where(x => x.SearchRecipeId == searchRecipeId).ToList();
        }
	}
}
