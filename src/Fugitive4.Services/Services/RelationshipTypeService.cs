using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface IRelationshipTypeService 	{
		IEnumerable<RelationshipType> GetAll(bool disabled = false);
		Task<ICollection<RelationshipType>> GetAllAsync(bool disabled = false);
		RelationshipType GetById(int id);
		Task<RelationshipType> GetByIdAsync(int id);
		Task InsertAsync(RelationshipType relationshipType, string userId);
        void Update(RelationshipType relationshipType, string userId);
        Task UpdateAsync(RelationshipType relationshipType, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);
	}														

	public partial class RelationshipTypeService : AuditableServiceBase, IRelationshipTypeService, IDisposable  
	{
		private readonly ApplicationDbContext _context;

		public RelationshipTypeService(ApplicationDbContext context)
		{
			_context = context;
		}

		public IEnumerable<RelationshipType> GetAll(bool disabled = false) 
		{
			return _context.RelationshipTypes.Where(x => x.Disabled == false || x.Disabled == disabled).ToList();
		}

		public async Task<ICollection<RelationshipType>> GetAllAsync(bool disabled = false)
        {
            return await _context.RelationshipTypes.Where(x => x.Disabled == false || x.Disabled == disabled).ToListAsync();
        }

		public RelationshipType GetById(int id) 
		{	
			return _context.RelationshipTypes.SingleOrDefault(e => e.Id == id);
		}

		public async Task<RelationshipType> GetByIdAsync(int id)
        {
            return await _context.RelationshipTypes.SingleAsync(m => m.Id == id);
        }

		public void Insert(RelationshipType relationshipType, string userId)
        {
			SetCreateModify(relationshipType, userId);
            _context.RelationshipTypes.Add(relationshipType);
            _context.SaveChanges();
        }

		public async Task InsertAsync(RelationshipType relationshipType, string userId)
        {
			SetCreateModify(relationshipType, userId);
            _context.RelationshipTypes.Add(relationshipType);
            await _context.SaveChangesAsync();
        }

		public void Update(RelationshipType relationshipType, string userId)
		{
			SetCreateModify(relationshipType, userId);

			_context.Entry(relationshipType).State = relationshipType.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(RelationshipType relationshipType, string userId)
        {
            SetCreateModify(relationshipType, userId);

			_context.Entry(relationshipType).State = relationshipType.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			RelationshipType relationshipType = _context.RelationshipTypes.SingleOrDefault(e => e.Id == id);
			            if (relationshipType == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(relationshipType, userId);

            relationshipType.Disabled = true;
            Update(relationshipType, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var relationshipType = await GetByIdAsync(id);
            SetCreateModify(relationshipType, userId);

            relationshipType.Disabled = true;

            await UpdateAsync(relationshipType, userId);
        }

		private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


    //
	}
}
