using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface ISearchRecipeService 	{
		IEnumerable<SearchRecipe> GetAll(bool disabled = false);
		Task<ICollection<SearchRecipe>> GetAllAsync(bool disabled = false);
		SearchRecipe GetById(int id);
		Task<SearchRecipe> GetByIdAsync(int id);
		Task InsertAsync(SearchRecipe searchRecipe, string userId);
        void Update(SearchRecipe searchRecipe, string userId);
        Task UpdateAsync(SearchRecipe searchRecipe, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);
	}														

	public partial class SearchRecipeService : AuditableServiceBase, ISearchRecipeService, IDisposable  
	{
		private readonly ApplicationDbContext _context;

		public SearchRecipeService(ApplicationDbContext context)
		{
			_context = context;
		}

		public IEnumerable<SearchRecipe> GetAll(bool disabled = false) 
		{
			return _context.SearchRecipes.Where(x => x.Disabled == false || x.Disabled == disabled).ToList();
		}

		public async Task<ICollection<SearchRecipe>> GetAllAsync(bool disabled = false)
        {
            return await _context.SearchRecipes.Where(x => x.Disabled == false || x.Disabled == disabled).ToListAsync();
        }

		public SearchRecipe GetById(int id) 
		{	
			return _context.SearchRecipes.SingleOrDefault(e => e.Id == id);
		}

		public async Task<SearchRecipe> GetByIdAsync(int id)
        {
            return await _context.SearchRecipes.SingleAsync(m => m.Id == id);
        }

		public void Insert(SearchRecipe searchRecipe, string userId)
        {
			SetCreateModify(searchRecipe, userId);
            _context.SearchRecipes.Add(searchRecipe);
            _context.SaveChanges();
        }

		public async Task InsertAsync(SearchRecipe searchRecipe, string userId)
        {
			SetCreateModify(searchRecipe, userId);
            _context.SearchRecipes.Add(searchRecipe);
            await _context.SaveChangesAsync();
        }

		public void Update(SearchRecipe searchRecipe, string userId)
		{
			SetCreateModify(searchRecipe, userId);

			_context.Entry(searchRecipe).State = searchRecipe.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(SearchRecipe searchRecipe, string userId)
        {
            SetCreateModify(searchRecipe, userId);

			_context.Entry(searchRecipe).State = searchRecipe.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			SearchRecipe searchRecipe = _context.SearchRecipes.SingleOrDefault(e => e.Id == id);
			            if (searchRecipe == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(searchRecipe, userId);

            searchRecipe.Disabled = true;
            Update(searchRecipe, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var searchRecipe = await GetByIdAsync(id);
            SetCreateModify(searchRecipe, userId);

            searchRecipe.Disabled = true;

            await UpdateAsync(searchRecipe, userId);
        }

		private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


    //
    //
	}
}
