using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface IBookingChargeService 	{
		IEnumerable<BookingCharge> GetAll(bool disabled = false);
		Task<ICollection<BookingCharge>> GetAllAsync(bool disabled = false);
		BookingCharge GetById(int id);
		Task<BookingCharge> GetByIdAsync(int id);
		Task InsertAsync(BookingCharge bookingCharge, string userId);
        void Update(BookingCharge bookingCharge, string userId);
        Task UpdateAsync(BookingCharge bookingCharge, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);
		List<BookingCharge> GetByBailBondId(int bailBondId);
	}														

	public partial class BookingChargeService : AuditableServiceBase, IBookingChargeService, IDisposable  
	{
		private readonly ApplicationDbContext _context;

		public BookingChargeService(ApplicationDbContext context)
		{
			_context = context;
		}

		public IEnumerable<BookingCharge> GetAll(bool disabled = false) 
		{
			return _context.BookingCharges.Where(x => x.Disabled == false || x.Disabled == disabled).ToList();
		}

		public async Task<ICollection<BookingCharge>> GetAllAsync(bool disabled = false)
        {
            return await _context.BookingCharges.Where(x => x.Disabled == false || x.Disabled == disabled).ToListAsync();
        }

		public BookingCharge GetById(int id) 
		{	
			return _context.BookingCharges.SingleOrDefault(e => e.Id == id);
		}

		public async Task<BookingCharge> GetByIdAsync(int id)
        {
            return await _context.BookingCharges.SingleAsync(m => m.Id == id);
        }

		public void Insert(BookingCharge bookingCharge, string userId)
        {
			SetCreateModify(bookingCharge, userId);
            _context.BookingCharges.Add(bookingCharge);
            _context.SaveChanges();
        }

		public async Task InsertAsync(BookingCharge bookingCharge, string userId)
        {
			SetCreateModify(bookingCharge, userId);
            _context.BookingCharges.Add(bookingCharge);
            await _context.SaveChangesAsync();
        }

		public void Update(BookingCharge bookingCharge, string userId)
		{
			SetCreateModify(bookingCharge, userId);

			_context.Entry(bookingCharge).State = bookingCharge.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(BookingCharge bookingCharge, string userId)
        {
            SetCreateModify(bookingCharge, userId);

			_context.Entry(bookingCharge).State = bookingCharge.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			BookingCharge bookingCharge = _context.BookingCharges.SingleOrDefault(e => e.Id == id);
			            if (bookingCharge == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(bookingCharge, userId);

            bookingCharge.Disabled = true;
            Update(bookingCharge, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var bookingCharge = await GetByIdAsync(id);
            SetCreateModify(bookingCharge, userId);

            bookingCharge.Disabled = true;

            await UpdateAsync(bookingCharge, userId);
        }

		private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }



        public List<BookingCharge> GetByBailBondId(int bailBondId)
        {
            return _context.BookingCharges.Where(x => x.BailBondId == bailBondId).ToList();
        }
	}
}
