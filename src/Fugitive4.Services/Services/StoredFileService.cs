using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Services.ModelsUnmapped;

namespace Fugitive4.Services.Services
{
	public interface IStoredFileService 	{
		IEnumerable<StoredFile> GetAll(bool disabled = false);
		Task<ICollection<StoredFile>> GetAllAsync(bool disabled = false);
		StoredFile GetById(int id);
		Task<StoredFile> GetByIdAsync(int id);
		Task InsertAsync(StoredFile storedFile, string userId);
        void Update(StoredFile storedFile, string userId);
        Task UpdateAsync(StoredFile storedFile, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);
		List<StoredFile> GetBySubjectId(int subjectId);
	    Task<ICollection<StoredFileCondensed>> GetBySubjectIdAsync(int subjectId);

	}														

	public partial class StoredFileService : AuditableServiceBase, IStoredFileService, IDisposable  
	{
		private readonly ApplicationDbContext _context;

		public StoredFileService(ApplicationDbContext context)
		{
			_context = context;
		}

		public IEnumerable<StoredFile> GetAll(bool disabled = false) 
		{
			return _context.StoredFiles.Where(x => x.Disabled == false || x.Disabled == disabled).ToList();
		}

		public async Task<ICollection<StoredFile>> GetAllAsync(bool disabled = false)
        {
            return await _context.StoredFiles.Where(x => x.Disabled == false || x.Disabled == disabled).ToListAsync();
        }

		public StoredFile GetById(int id) 
		{	
			return _context.StoredFiles.SingleOrDefault(e => e.Id == id);
		}

		public async Task<StoredFile> GetByIdAsync(int id)
        {
            return await _context.StoredFiles.SingleAsync(m => m.Id == id);
        }

		public void Insert(StoredFile storedFile, string userId)
		{
		    var exists =
		        _context.StoredFiles.Any(x => x.SubjectId == storedFile.SubjectId && x.FileName == storedFile.FileName);

		    if (!exists)
		    {
		        SetCreateModify(storedFile, userId);
		        _context.StoredFiles.Add(storedFile);
		        _context.SaveChanges();
		    }
		}

		public async Task InsertAsync(StoredFile storedFile, string userId)
        {
            var exists =
                    _context.StoredFiles.Any(x => x.SubjectId == storedFile.SubjectId && x.FileName == storedFile.FileName);

		    if (!exists)
		    {
		        SetCreateModify(storedFile, userId);
		        _context.StoredFiles.Add(storedFile);
		        await _context.SaveChangesAsync();
		    }
        }

		public void Update(StoredFile storedFile, string userId)
		{
			SetCreateModify(storedFile, userId);

			_context.Entry(storedFile).State = storedFile.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(StoredFile storedFile, string userId)
        {
            SetCreateModify(storedFile, userId);

			_context.Entry(storedFile).State = storedFile.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			StoredFile storedFile = _context.StoredFiles.SingleOrDefault(e => e.Id == id);
			            if (storedFile == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(storedFile, userId);

            storedFile.Disabled = true;
            Update(storedFile, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var storedFile = await GetByIdAsync(id);
            SetCreateModify(storedFile, userId);

            storedFile.Disabled = true;

            await UpdateAsync(storedFile, userId);
        }

		private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }



        public List<StoredFile> GetBySubjectId(int subjectId)
        {
            return _context.StoredFiles.Where(x => x.SubjectId == subjectId).ToList();
        }

        public async Task<ICollection<StoredFileCondensed>> GetBySubjectIdAsync(int subjectId)
        {        
            return await _context
                            .StoredFiles
                            .Where(x => x.SubjectId == subjectId)
                            .Select(x => new StoredFileCondensed
                            {
                                Id = x.Id,
                                FileName = x.FileName,
                                IsSubject = x.IsSubject,
                                Notes = x.Notes,
                                SubjectId = subjectId,
                            })
                            .ToListAsync();
        }
    }
}
