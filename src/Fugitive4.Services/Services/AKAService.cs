using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface IAKAService 	{
		IEnumerable<AKA> GetAll(bool disabled = false);
		Task<ICollection<AKA>> GetAllAsync(bool disabled = false);
		AKA GetById(int id);
		Task<AKA> GetByIdAsync(int id);
		Task InsertAsync(AKA aKA, string userId);
        void Update(AKA aKA, string userId);
        Task UpdateAsync(AKA aKA, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);
		List<AKA> GetBySubjectId(int subjectId);
	}														

	public partial class AKAService : AuditableServiceBase, IAKAService, IDisposable  
	{
		private readonly ApplicationDbContext _context;

		public AKAService(ApplicationDbContext context)
		{
			_context = context;
		}

		public IEnumerable<AKA> GetAll(bool disabled = false) 
		{
			return _context.AKAS.Where(x => x.Disabled == false || x.Disabled == disabled).ToList();
		}

		public async Task<ICollection<AKA>> GetAllAsync(bool disabled = false)
        {
            return await _context.AKAS.Where(x => x.Disabled == false || x.Disabled == disabled).ToListAsync();
        }

		public AKA GetById(int id) 
		{	
			return _context.AKAS.SingleOrDefault(e => e.Id == id);
		}

		public async Task<AKA> GetByIdAsync(int id)
        {
            return await _context.AKAS.SingleAsync(m => m.Id == id);
        }

		public void Insert(AKA aKA, string userId)
        {
			SetCreateModify(aKA, userId);
            _context.AKAS.Add(aKA);
            _context.SaveChanges();
        }

		public async Task InsertAsync(AKA aKA, string userId)
        {
			SetCreateModify(aKA, userId);
            _context.AKAS.Add(aKA);
            await _context.SaveChangesAsync();
        }

		public void Update(AKA aKA, string userId)
		{
			SetCreateModify(aKA, userId);

			_context.Entry(aKA).State = aKA.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(AKA aKA, string userId)
        {
            SetCreateModify(aKA, userId);

			_context.Entry(aKA).State = aKA.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			AKA aKA = _context.AKAS.SingleOrDefault(e => e.Id == id);
			            if (aKA == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(aKA, userId);

            aKA.Disabled = true;
            Update(aKA, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var aKA = await GetByIdAsync(id);
            SetCreateModify(aKA, userId);

            aKA.Disabled = true;

            await UpdateAsync(aKA, userId);
        }

		private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }



        public List<AKA> GetBySubjectId(int subjectId)
        {
            return _context.AKAS.Where(x => x.SubjectId == subjectId).ToList();
        }
	}
}
