using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface IHyperlinkService 	{
		IEnumerable<Hyperlink> GetAll(bool disabled = false);
		Task<ICollection<Hyperlink>> GetAllAsync(bool disabled = false);
		Hyperlink GetById(int id);
		Task<Hyperlink> GetByIdAsync(int id);
		Task InsertAsync(Hyperlink hyperlink, string userId);
        void Update(Hyperlink hyperlink, string userId);
        Task UpdateAsync(Hyperlink hyperlink, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);
		List<Hyperlink> GetBySubjectId(int subjectId);
	}														

	public partial class HyperlinkService : AuditableServiceBase, IHyperlinkService, IDisposable  
	{
		private readonly ApplicationDbContext _context;

		public HyperlinkService(ApplicationDbContext context)
		{
			_context = context;
		}

		public IEnumerable<Hyperlink> GetAll(bool disabled = false) 
		{
			return _context.Hyperlinks.Where(x => x.Disabled == false || x.Disabled == disabled).ToList();
		}

		public async Task<ICollection<Hyperlink>> GetAllAsync(bool disabled = false)
        {
            return await _context.Hyperlinks.Where(x => x.Disabled == false || x.Disabled == disabled).ToListAsync();
        }

		public Hyperlink GetById(int id) 
		{	
			return _context.Hyperlinks.SingleOrDefault(e => e.Id == id);
		}

		public async Task<Hyperlink> GetByIdAsync(int id)
        {
            return await _context.Hyperlinks.SingleAsync(m => m.Id == id);
        }

		public void Insert(Hyperlink hyperlink, string userId)
        {
			SetCreateModify(hyperlink, userId);
            _context.Hyperlinks.Add(hyperlink);
            _context.SaveChanges();
        }

		public async Task InsertAsync(Hyperlink hyperlink, string userId)
        {
			SetCreateModify(hyperlink, userId);
            _context.Hyperlinks.Add(hyperlink);
            await _context.SaveChangesAsync();
        }

		public void Update(Hyperlink hyperlink, string userId)
		{
			SetCreateModify(hyperlink, userId);

			_context.Entry(hyperlink).State = hyperlink.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(Hyperlink hyperlink, string userId)
        {
            SetCreateModify(hyperlink, userId);

			_context.Entry(hyperlink).State = hyperlink.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			Hyperlink hyperlink = _context.Hyperlinks.SingleOrDefault(e => e.Id == id);
			            if (hyperlink == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(hyperlink, userId);

            hyperlink.Disabled = true;
            Update(hyperlink, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var hyperlink = await GetByIdAsync(id);
            SetCreateModify(hyperlink, userId);

            hyperlink.Disabled = true;

            await UpdateAsync(hyperlink, userId);
        }

		private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }



        public List<Hyperlink> GetBySubjectId(int subjectId)
        {
            return _context.Hyperlinks.Where(x => x.SubjectId == subjectId).ToList();
        }
	}
}
