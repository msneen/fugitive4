using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface IAspNetUsers_TeamService 	{
		IEnumerable<AspNetUsers_Team> GetAll(bool disabled = false);
		Task<ICollection<AspNetUsers_Team>> GetAllAsync(bool disabled = false);
		AspNetUsers_Team GetById(int id);
		Task<AspNetUsers_Team> GetByIdAsync(int id);
        void Insert(AspNetUsers_Team aspNetUsers_Team, string userId);
        Task InsertAsync(AspNetUsers_Team aspNetUsers_Team, string userId);
        void Update(AspNetUsers_Team aspNetUsers_Team, string userId);
        Task UpdateAsync(AspNetUsers_Team aspNetUsers_Team, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);
		List<AspNetUsers_Team> GetByAspNetUserId(string aspNetUserId);
		List<AspNetUsers_Team> GetByTeamId(int teamId);
	    void SaveTeamUser(int teamId, string userId, string logonUserId);
	    void DeleteUserTeam(string userIdToDelete, string userId);

	}														

	public partial class AspNetUsers_TeamService : AuditableServiceBase, IAspNetUsers_TeamService, IDisposable  
	{
		private readonly ApplicationDbContext _context;

		public AspNetUsers_TeamService(ApplicationDbContext context)
		{
			_context = context;
		}

		public IEnumerable<AspNetUsers_Team> GetAll(bool disabled = false) 
		{
			return _context.AspNetUsers_Teams.Where(x => x.Disabled == false || x.Disabled == disabled).ToList();
		}

		public async Task<ICollection<AspNetUsers_Team>> GetAllAsync(bool disabled = false)
        {
            return await _context.AspNetUsers_Teams.Where(x => x.Disabled == false || x.Disabled == disabled).ToListAsync();
        }

		public AspNetUsers_Team GetById(int id) 
		{	
			return _context.AspNetUsers_Teams.SingleOrDefault(e => e.Id == id);
		}

		public async Task<AspNetUsers_Team> GetByIdAsync(int id)
        {
            return await _context.AspNetUsers_Teams.SingleAsync(m => m.Id == id);
        }

		public void Insert(AspNetUsers_Team aspNetUsers_Team, string userId)
        {
			SetCreateModify(aspNetUsers_Team, userId);
            _context.AspNetUsers_Teams.Add(aspNetUsers_Team);
            _context.SaveChanges();
        }

		public async Task InsertAsync(AspNetUsers_Team aspNetUsers_Team, string userId)
        {
			SetCreateModify(aspNetUsers_Team, userId);
            _context.AspNetUsers_Teams.Add(aspNetUsers_Team);
            await _context.SaveChangesAsync();
        }

		public void Update(AspNetUsers_Team aspNetUsers_Team, string userId)
		{
			SetCreateModify(aspNetUsers_Team, userId);

			_context.Entry(aspNetUsers_Team).State = aspNetUsers_Team.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(AspNetUsers_Team aspNetUsers_Team, string userId)
        {
            SetCreateModify(aspNetUsers_Team, userId);

			_context.Entry(aspNetUsers_Team).State = aspNetUsers_Team.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			AspNetUsers_Team aspNetUsers_Team = _context.AspNetUsers_Teams.SingleOrDefault(e => e.Id == id);
			            if (aspNetUsers_Team == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(aspNetUsers_Team, userId);

            aspNetUsers_Team.Disabled = true;
            Update(aspNetUsers_Team, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var aspNetUsers_Team = await GetByIdAsync(id);
            SetCreateModify(aspNetUsers_Team, userId);

            aspNetUsers_Team.Disabled = true;

            await UpdateAsync(aspNetUsers_Team, userId);
        }

        public void DeleteUserTeam(string userIdToDelete, string userId)
        {
            var aspNetUsers_Team = _context.AspNetUsers_Teams.Where(x => x.ApplicationUserId == userIdToDelete);

            foreach (var userTeam in aspNetUsers_Team)
            {
                _context.AspNetUsers_Teams.Remove(userTeam);
            }
            
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }



        public List<AspNetUsers_Team> GetByAspNetUserId(string aspNetUserId)
        {
            return _context.AspNetUsers_Teams.Where(x => x.ApplicationUserId == aspNetUserId).ToList();
        }

        public List<AspNetUsers_Team> GetByTeamId(int teamId)
        {
            return _context.AspNetUsers_Teams.Where(x => x.TeamId == teamId).ToList();
        }

        public void SaveTeamUser(int teamId, string userId, string logonUserId)
        {
            var existingTeamUser = GetByTeamId(teamId)
                                    .FirstOrDefault(b => b.ApplicationUserId == userId);
            //Add any users that aren't already there
            if (existingTeamUser == null)
            {
                AspNetUsers_Team aspNetUserTeam = new AspNetUsers_Team
                {
                    ApplicationUserId = userId,
                    TeamId = teamId
                };
                Insert(aspNetUserTeam, logonUserId);
            }
        }
    }
}
