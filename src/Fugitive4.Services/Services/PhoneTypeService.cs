using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface IPhoneTypeService 	{
		IEnumerable<PhoneType> GetAll(bool disabled = false);
		Task<ICollection<PhoneType>> GetAllAsync(bool disabled = false);
		PhoneType GetById(int id);
		Task<PhoneType> GetByIdAsync(int id);
		Task InsertAsync(PhoneType phoneType, string userId);
        void Update(PhoneType phoneType, string userId);
        Task UpdateAsync(PhoneType phoneType, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);
	}														

	public partial class PhoneTypeService : AuditableServiceBase, IPhoneTypeService, IDisposable  
	{
		private readonly ApplicationDbContext _context;

		public PhoneTypeService(ApplicationDbContext context)
		{
			_context = context;
		}

		public IEnumerable<PhoneType> GetAll(bool disabled = false) 
		{
			return _context.PhoneTypes.Where(x => x.Disabled == false || x.Disabled == disabled).ToList();
		}

		public async Task<ICollection<PhoneType>> GetAllAsync(bool disabled = false)
        {
            return await _context.PhoneTypes.Where(x => x.Disabled == false || x.Disabled == disabled).ToListAsync();
        }

		public PhoneType GetById(int id) 
		{	
			return _context.PhoneTypes.SingleOrDefault(e => e.Id == id);
		}

		public async Task<PhoneType> GetByIdAsync(int id)
        {
            return await _context.PhoneTypes.SingleAsync(m => m.Id == id);
        }

		public void Insert(PhoneType phoneType, string userId)
        {
			SetCreateModify(phoneType, userId);
            _context.PhoneTypes.Add(phoneType);
            _context.SaveChanges();
        }

		public async Task InsertAsync(PhoneType phoneType, string userId)
        {
			SetCreateModify(phoneType, userId);
            _context.PhoneTypes.Add(phoneType);
            await _context.SaveChangesAsync();
        }

		public void Update(PhoneType phoneType, string userId)
		{
			SetCreateModify(phoneType, userId);

			_context.Entry(phoneType).State = phoneType.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(PhoneType phoneType, string userId)
        {
            SetCreateModify(phoneType, userId);

			_context.Entry(phoneType).State = phoneType.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			PhoneType phoneType = _context.PhoneTypes.SingleOrDefault(e => e.Id == id);
			            if (phoneType == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(phoneType, userId);

            phoneType.Disabled = true;
            Update(phoneType, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var phoneType = await GetByIdAsync(id);
            SetCreateModify(phoneType, userId);

            phoneType.Disabled = true;

            await UpdateAsync(phoneType, userId);
        }

		private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


    //
	}
}
