using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface ISearchResultService 	{
		IEnumerable<SearchResult> GetAll(bool disabled = false);
		Task<ICollection<SearchResult>> GetAllAsync(bool disabled = false);
		SearchResult GetById(int id);
		Task<SearchResult> GetByIdAsync(int id);
		Task InsertAsync(SearchResult searchResult, string userId);
        void Update(SearchResult searchResult, string userId);
        Task UpdateAsync(SearchResult searchResult, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);
		List<SearchResult> GetBySubjectId(int subjectId);
		List<SearchResult> GetBySubjectSearchRecipeItemId(int subjectSearchRecipeItemId);
	}														

	public partial class SearchResultService : AuditableServiceBase, ISearchResultService, IDisposable  
	{
		private readonly ApplicationDbContext _context;

		public SearchResultService(ApplicationDbContext context)
		{
			_context = context;
		}

		public IEnumerable<SearchResult> GetAll(bool disabled = false) 
		{
			return _context.SearchResults.Where(x => x.Disabled == false || x.Disabled == disabled).ToList();
		}

		public async Task<ICollection<SearchResult>> GetAllAsync(bool disabled = false)
        {
            return await _context.SearchResults.Where(x => x.Disabled == false || x.Disabled == disabled).ToListAsync();
        }

		public SearchResult GetById(int id) 
		{	
			return _context.SearchResults.SingleOrDefault(e => e.Id == id);
		}

		public async Task<SearchResult> GetByIdAsync(int id)
        {
            return await _context.SearchResults.SingleAsync(m => m.Id == id);
        }

		public void Insert(SearchResult searchResult, string userId)
        {
			SetCreateModify(searchResult, userId);
            _context.SearchResults.Add(searchResult);
            _context.SaveChanges();
        }

		public async Task InsertAsync(SearchResult searchResult, string userId)
        {
			SetCreateModify(searchResult, userId);
            _context.SearchResults.Add(searchResult);
            await _context.SaveChangesAsync();
        }

		public void Update(SearchResult searchResult, string userId)
		{
			SetCreateModify(searchResult, userId);

			_context.Entry(searchResult).State = searchResult.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(SearchResult searchResult, string userId)
        {
            SetCreateModify(searchResult, userId);

			_context.Entry(searchResult).State = searchResult.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			SearchResult searchResult = _context.SearchResults.SingleOrDefault(e => e.Id == id);
			            if (searchResult == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(searchResult, userId);

            searchResult.Disabled = true;
            Update(searchResult, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var searchResult = await GetByIdAsync(id);
            SetCreateModify(searchResult, userId);

            searchResult.Disabled = true;

            await UpdateAsync(searchResult, userId);
        }

		private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }



        public List<SearchResult> GetBySubjectId(int subjectId)
        {
            return _context.SearchResults.Where(x => x.SubjectId == subjectId).ToList();
        }

        public List<SearchResult> GetBySubjectSearchRecipeItemId(int subjectSearchRecipeItemId)
        {
            return _context.SearchResults.Where(x => x.SubjectSearchRecipeItemId == subjectSearchRecipeItemId).ToList();
        }
	}
}
