using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface IRaceService 	{
		IEnumerable<Race> GetAll(bool disabled = false);
		Task<ICollection<Race>> GetAllAsync(bool disabled = false);
		Race GetById(int id);
		Task<Race> GetByIdAsync(int id);
		Task InsertAsync(Race race, string userId);
        void Update(Race race, string userId);
        Task UpdateAsync(Race race, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);
	}														

	public partial class RaceService : AuditableServiceBase, IRaceService, IDisposable  
	{
		private readonly ApplicationDbContext _context;

		public RaceService(ApplicationDbContext context)
		{
			_context = context;
		}

		public IEnumerable<Race> GetAll(bool disabled = false) 
		{
			return _context.Races.Where(x => x.Disabled == false || x.Disabled == disabled).ToList();
		}

		public async Task<ICollection<Race>> GetAllAsync(bool disabled = false)
        {
            return await _context.Races.Where(x => x.Disabled == false || x.Disabled == disabled).ToListAsync();
        }

		public Race GetById(int id) 
		{	
			return _context.Races.SingleOrDefault(e => e.Id == id);
		}

		public async Task<Race> GetByIdAsync(int id)
        {
            return await _context.Races.SingleAsync(m => m.Id == id);
        }

		public void Insert(Race race, string userId)
        {
			SetCreateModify(race, userId);
            _context.Races.Add(race);
            _context.SaveChanges();
        }

		public async Task InsertAsync(Race race, string userId)
        {
			SetCreateModify(race, userId);
            _context.Races.Add(race);
            await _context.SaveChangesAsync();
        }

		public void Update(Race race, string userId)
		{
			SetCreateModify(race, userId);

			_context.Entry(race).State = race.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(Race race, string userId)
        {
            SetCreateModify(race, userId);

			_context.Entry(race).State = race.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			Race race = _context.Races.SingleOrDefault(e => e.Id == id);
			            if (race == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(race, userId);

            race.Disabled = true;
            Update(race, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var race = await GetByIdAsync(id);
            SetCreateModify(race, userId);

            race.Disabled = true;

            await UpdateAsync(race, userId);
        }

		private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


    //
	}
}
