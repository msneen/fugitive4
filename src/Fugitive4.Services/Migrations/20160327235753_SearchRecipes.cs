using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;
using Microsoft.Data.Entity.Metadata;

namespace Fugitive4.Services.Migrations
{
    public partial class SearchRecipes : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_ActivityNote_Subject_SubjectId", table: "ActivityNote");
            migrationBuilder.DropForeignKey(name: "FK_Address_AddressType_AddressTypeId", table: "Address");
            migrationBuilder.DropForeignKey(name: "FK_Address_Subject_SubjectId", table: "Address");
            migrationBuilder.DropForeignKey(name: "FK_BailBond_Subject_SubjectId", table: "BailBond");
            migrationBuilder.DropForeignKey(name: "FK_BookingCharge_BailBond_BailBondId", table: "BookingCharge");
            migrationBuilder.DropForeignKey(name: "FK_DriverLicense_Subject_SubjectId", table: "DriverLicense");
            migrationBuilder.DropForeignKey(name: "FK_EmailAddress_EmailType_EmailTypeId", table: "EmailAddress");
            migrationBuilder.DropForeignKey(name: "FK_EmailAddress_Subject_SubjectId", table: "EmailAddress");
            migrationBuilder.DropForeignKey(name: "FK_Hyperlink_Subject_SubjectId", table: "Hyperlink");
            migrationBuilder.DropForeignKey(name: "FK_PhoneNumber_PhoneType_PhoneTypeId", table: "PhoneNumber");
            migrationBuilder.DropForeignKey(name: "FK_PhoneNumber_Subject_SubjectId", table: "PhoneNumber");
            migrationBuilder.DropForeignKey(name: "FK_SearchResult_Subject_SubjectId", table: "SearchResult");
            migrationBuilder.DropForeignKey(name: "FK_StoredFile_Subject_SubjectId", table: "StoredFile");
            migrationBuilder.DropForeignKey(name: "FK_Subject_Gender_GenderId", table: "Subject");
            migrationBuilder.DropForeignKey(name: "FK_Subject_Race_RaceId", table: "Subject");
            migrationBuilder.DropForeignKey(name: "FK_Subject_Referrer_ReferrerId", table: "Subject");
            migrationBuilder.DropForeignKey(name: "FK_SubjectRelationship_RelationshipType_RelationshipTypeId", table: "SubjectRelationship");
            migrationBuilder.DropForeignKey(name: "FK_Vehicle_Subject_SubjectId", table: "Vehicle");
            migrationBuilder.DropForeignKey(name: "FK_Video_Subject_SubjectId", table: "Video");
            migrationBuilder.DropForeignKey(name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId", table: "AspNetRoleClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId", table: "AspNetUserClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId", table: "AspNetUserLogins");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_IdentityRole_RoleId", table: "AspNetUserRoles");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_ApplicationUser_UserId", table: "AspNetUserRoles");
            migrationBuilder.CreateTable(
                name: "AKA",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    Disabled = table.Column<bool>(nullable: false),
                    Firstname = table.Column<string>(nullable: true),
                    Lastname = table.Column<string>(nullable: true),
                    Middlename = table.Column<string>(nullable: true),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    SubjectId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AKA", x => x.Id);
                    table.ForeignKey(
                        name: "FK_AKA_Subject_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "Subject",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
            migrationBuilder.CreateTable(
                name: "SearchRecipe",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Disabled = table.Column<bool>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SearchRecipe", x => x.Id);
                });
            migrationBuilder.CreateTable(
                name: "SearchRecipeItem",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Disabled = table.Column<bool>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    Name = table.Column<string>(nullable: true),
                    SearchRecipeId = table.Column<int>(nullable: false),
                    UrlOrSource = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SearchRecipeItem", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SearchRecipeItem_SearchRecipe_SearchRecipeId",
                        column: x => x.SearchRecipeId,
                        principalTable: "SearchRecipe",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });
            migrationBuilder.CreateTable(
                name: "SubjectSearchRecipe",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CreatedBy = table.Column<string>(nullable: true),
                    CreatedDate = table.Column<DateTime>(nullable: false),
                    Description = table.Column<string>(nullable: true),
                    Disabled = table.Column<bool>(nullable: false),
                    ModifiedBy = table.Column<string>(nullable: true),
                    ModifiedDate = table.Column<DateTime>(nullable: true),
                    Notes = table.Column<string>(nullable: true),
                    SearchRecipeId = table.Column<int>(nullable: false),
                    SubjectId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SubjectSearchRecipe", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SubjectSearchRecipe_SearchRecipe_SearchRecipeId",
                        column: x => x.SearchRecipeId,
                        principalTable: "SearchRecipe",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SubjectSearchRecipe_Subject_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "Subject",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });
            migrationBuilder.AddColumn<int>(
                name: "PerceivedThreatLevel",
                table: "Subject",
                nullable: false,
                defaultValue: 0);
            migrationBuilder.AddForeignKey(
                name: "FK_ActivityNote_Subject_SubjectId",
                table: "ActivityNote",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Address_AddressType_AddressTypeId",
                table: "Address",
                column: "AddressTypeId",
                principalTable: "AddressType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Address_Subject_SubjectId",
                table: "Address",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_BailBond_Subject_SubjectId",
                table: "BailBond",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_BookingCharge_BailBond_BailBondId",
                table: "BookingCharge",
                column: "BailBondId",
                principalTable: "BailBond",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_DriverLicense_Subject_SubjectId",
                table: "DriverLicense",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_EmailAddress_EmailType_EmailTypeId",
                table: "EmailAddress",
                column: "EmailTypeId",
                principalTable: "EmailType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_EmailAddress_Subject_SubjectId",
                table: "EmailAddress",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Hyperlink_Subject_SubjectId",
                table: "Hyperlink",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_PhoneNumber_PhoneType_PhoneTypeId",
                table: "PhoneNumber",
                column: "PhoneTypeId",
                principalTable: "PhoneType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_PhoneNumber_Subject_SubjectId",
                table: "PhoneNumber",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_SearchResult_Subject_SubjectId",
                table: "SearchResult",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_StoredFile_Subject_SubjectId",
                table: "StoredFile",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Subject_Gender_GenderId",
                table: "Subject",
                column: "GenderId",
                principalTable: "Gender",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Subject_Race_RaceId",
                table: "Subject",
                column: "RaceId",
                principalTable: "Race",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Subject_Referrer_ReferrerId",
                table: "Subject",
                column: "ReferrerId",
                principalTable: "Referrer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_SubjectRelationship_RelationshipType_RelationshipTypeId",
                table: "SubjectRelationship",
                column: "RelationshipTypeId",
                principalTable: "RelationshipType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Vehicle_Subject_SubjectId",
                table: "Vehicle",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Video_Subject_SubjectId",
                table: "Video",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId",
                table: "AspNetUserClaims",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId",
                table: "AspNetUserLogins",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_IdentityRole_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_ApplicationUser_UserId",
                table: "AspNetUserRoles",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_ActivityNote_Subject_SubjectId", table: "ActivityNote");
            migrationBuilder.DropForeignKey(name: "FK_Address_AddressType_AddressTypeId", table: "Address");
            migrationBuilder.DropForeignKey(name: "FK_Address_Subject_SubjectId", table: "Address");
            migrationBuilder.DropForeignKey(name: "FK_BailBond_Subject_SubjectId", table: "BailBond");
            migrationBuilder.DropForeignKey(name: "FK_BookingCharge_BailBond_BailBondId", table: "BookingCharge");
            migrationBuilder.DropForeignKey(name: "FK_DriverLicense_Subject_SubjectId", table: "DriverLicense");
            migrationBuilder.DropForeignKey(name: "FK_EmailAddress_EmailType_EmailTypeId", table: "EmailAddress");
            migrationBuilder.DropForeignKey(name: "FK_EmailAddress_Subject_SubjectId", table: "EmailAddress");
            migrationBuilder.DropForeignKey(name: "FK_Hyperlink_Subject_SubjectId", table: "Hyperlink");
            migrationBuilder.DropForeignKey(name: "FK_PhoneNumber_PhoneType_PhoneTypeId", table: "PhoneNumber");
            migrationBuilder.DropForeignKey(name: "FK_PhoneNumber_Subject_SubjectId", table: "PhoneNumber");
            migrationBuilder.DropForeignKey(name: "FK_SearchResult_Subject_SubjectId", table: "SearchResult");
            migrationBuilder.DropForeignKey(name: "FK_StoredFile_Subject_SubjectId", table: "StoredFile");
            migrationBuilder.DropForeignKey(name: "FK_Subject_Gender_GenderId", table: "Subject");
            migrationBuilder.DropForeignKey(name: "FK_Subject_Race_RaceId", table: "Subject");
            migrationBuilder.DropForeignKey(name: "FK_Subject_Referrer_ReferrerId", table: "Subject");
            migrationBuilder.DropForeignKey(name: "FK_SubjectRelationship_RelationshipType_RelationshipTypeId", table: "SubjectRelationship");
            migrationBuilder.DropForeignKey(name: "FK_Vehicle_Subject_SubjectId", table: "Vehicle");
            migrationBuilder.DropForeignKey(name: "FK_Video_Subject_SubjectId", table: "Video");
            migrationBuilder.DropForeignKey(name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId", table: "AspNetRoleClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId", table: "AspNetUserClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId", table: "AspNetUserLogins");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_IdentityRole_RoleId", table: "AspNetUserRoles");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_ApplicationUser_UserId", table: "AspNetUserRoles");
            migrationBuilder.DropColumn(name: "PerceivedThreatLevel", table: "Subject");
            migrationBuilder.DropTable("AKA");
            migrationBuilder.DropTable("SearchRecipeItem");
            migrationBuilder.DropTable("SubjectSearchRecipe");
            migrationBuilder.DropTable("SearchRecipe");
            migrationBuilder.AddForeignKey(
                name: "FK_ActivityNote_Subject_SubjectId",
                table: "ActivityNote",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Address_AddressType_AddressTypeId",
                table: "Address",
                column: "AddressTypeId",
                principalTable: "AddressType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Address_Subject_SubjectId",
                table: "Address",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_BailBond_Subject_SubjectId",
                table: "BailBond",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_BookingCharge_BailBond_BailBondId",
                table: "BookingCharge",
                column: "BailBondId",
                principalTable: "BailBond",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_DriverLicense_Subject_SubjectId",
                table: "DriverLicense",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_EmailAddress_EmailType_EmailTypeId",
                table: "EmailAddress",
                column: "EmailTypeId",
                principalTable: "EmailType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_EmailAddress_Subject_SubjectId",
                table: "EmailAddress",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Hyperlink_Subject_SubjectId",
                table: "Hyperlink",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_PhoneNumber_PhoneType_PhoneTypeId",
                table: "PhoneNumber",
                column: "PhoneTypeId",
                principalTable: "PhoneType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_PhoneNumber_Subject_SubjectId",
                table: "PhoneNumber",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_SearchResult_Subject_SubjectId",
                table: "SearchResult",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_StoredFile_Subject_SubjectId",
                table: "StoredFile",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Subject_Gender_GenderId",
                table: "Subject",
                column: "GenderId",
                principalTable: "Gender",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Subject_Race_RaceId",
                table: "Subject",
                column: "RaceId",
                principalTable: "Race",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Subject_Referrer_ReferrerId",
                table: "Subject",
                column: "ReferrerId",
                principalTable: "Referrer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_SubjectRelationship_RelationshipType_RelationshipTypeId",
                table: "SubjectRelationship",
                column: "RelationshipTypeId",
                principalTable: "RelationshipType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Vehicle_Subject_SubjectId",
                table: "Vehicle",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Video_Subject_SubjectId",
                table: "Video",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId",
                table: "AspNetUserClaims",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId",
                table: "AspNetUserLogins",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_IdentityRole_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_ApplicationUser_UserId",
                table: "AspNetUserRoles",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
