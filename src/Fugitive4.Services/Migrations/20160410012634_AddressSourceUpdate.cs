using System;
using System.Collections.Generic;
using Microsoft.Data.Entity.Migrations;
using Fugitive4.Services.Enums;

namespace Fugitive4.Services.Migrations
{
    public partial class AddressSourceUpdate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_ActivityNote_Subject_SubjectId", table: "ActivityNote");
            migrationBuilder.DropForeignKey(name: "FK_Address_AddressType_AddressTypeId", table: "Address");
            migrationBuilder.DropForeignKey(name: "FK_Address_Subject_SubjectId", table: "Address");
            migrationBuilder.DropForeignKey(name: "FK_AKA_Subject_SubjectId", table: "AKA");
            migrationBuilder.DropForeignKey(name: "FK_BailBond_Subject_SubjectId", table: "BailBond");
            migrationBuilder.DropForeignKey(name: "FK_BookingCharge_BailBond_BailBondId", table: "BookingCharge");
            migrationBuilder.DropForeignKey(name: "FK_DriverLicense_Subject_SubjectId", table: "DriverLicense");
            migrationBuilder.DropForeignKey(name: "FK_EmailAddress_EmailType_EmailTypeId", table: "EmailAddress");
            migrationBuilder.DropForeignKey(name: "FK_EmailAddress_Subject_SubjectId", table: "EmailAddress");
            migrationBuilder.DropForeignKey(name: "FK_Employer_Subject_SubjectId", table: "Employer");
            migrationBuilder.DropForeignKey(name: "FK_Hyperlink_Subject_SubjectId", table: "Hyperlink");
            migrationBuilder.DropForeignKey(name: "FK_PhoneNumber_PhoneType_PhoneTypeId", table: "PhoneNumber");
            migrationBuilder.DropForeignKey(name: "FK_PhoneNumber_Subject_SubjectId", table: "PhoneNumber");
            migrationBuilder.DropForeignKey(name: "FK_SearchRecipeItem_SearchRecipe_SearchRecipeId", table: "SearchRecipeItem");
            migrationBuilder.DropForeignKey(name: "FK_SearchResult_Subject_SubjectId", table: "SearchResult");
            migrationBuilder.DropForeignKey(name: "FK_StoredFile_Subject_SubjectId", table: "StoredFile");
            migrationBuilder.DropForeignKey(name: "FK_Subject_Gender_GenderId", table: "Subject");
            migrationBuilder.DropForeignKey(name: "FK_Subject_Race_RaceId", table: "Subject");
            migrationBuilder.DropForeignKey(name: "FK_Subject_Referrer_ReferrerId", table: "Subject");
            migrationBuilder.DropForeignKey(name: "FK_SubjectRelationship_RelationshipType_RelationshipTypeId", table: "SubjectRelationship");
            migrationBuilder.DropForeignKey(name: "FK_SubjectSearchRecipeItem_SearchRecipeItem_SearchRecipeItemId", table: "SubjectSearchRecipeItem");
            migrationBuilder.DropForeignKey(name: "FK_SubjectSearchRecipeItem_Subject_SubjectId", table: "SubjectSearchRecipeItem");
            migrationBuilder.DropForeignKey(name: "FK_Vehicle_Subject_SubjectId", table: "Vehicle");
            migrationBuilder.DropForeignKey(name: "FK_Video_Subject_SubjectId", table: "Video");
            migrationBuilder.DropForeignKey(name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId", table: "AspNetRoleClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId", table: "AspNetUserClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId", table: "AspNetUserLogins");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_IdentityRole_RoleId", table: "AspNetUserRoles");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_ApplicationUser_UserId", table: "AspNetUserRoles");
            migrationBuilder.AddColumn<int>(
                name: "RelatedAddressSource",
                table: "ActivityNote",
                nullable: false,
                defaultValue: AddressSource.SubjectAddress);
            migrationBuilder.AddForeignKey(
                name: "FK_ActivityNote_Subject_SubjectId",
                table: "ActivityNote",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Address_AddressType_AddressTypeId",
                table: "Address",
                column: "AddressTypeId",
                principalTable: "AddressType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Address_Subject_SubjectId",
                table: "Address",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_AKA_Subject_SubjectId",
                table: "AKA",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_BailBond_Subject_SubjectId",
                table: "BailBond",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_BookingCharge_BailBond_BailBondId",
                table: "BookingCharge",
                column: "BailBondId",
                principalTable: "BailBond",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_DriverLicense_Subject_SubjectId",
                table: "DriverLicense",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_EmailAddress_EmailType_EmailTypeId",
                table: "EmailAddress",
                column: "EmailTypeId",
                principalTable: "EmailType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_EmailAddress_Subject_SubjectId",
                table: "EmailAddress",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Employer_Subject_SubjectId",
                table: "Employer",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Hyperlink_Subject_SubjectId",
                table: "Hyperlink",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_PhoneNumber_PhoneType_PhoneTypeId",
                table: "PhoneNumber",
                column: "PhoneTypeId",
                principalTable: "PhoneType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_PhoneNumber_Subject_SubjectId",
                table: "PhoneNumber",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_SearchRecipeItem_SearchRecipe_SearchRecipeId",
                table: "SearchRecipeItem",
                column: "SearchRecipeId",
                principalTable: "SearchRecipe",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_SearchResult_Subject_SubjectId",
                table: "SearchResult",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_StoredFile_Subject_SubjectId",
                table: "StoredFile",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Subject_Gender_GenderId",
                table: "Subject",
                column: "GenderId",
                principalTable: "Gender",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Subject_Race_RaceId",
                table: "Subject",
                column: "RaceId",
                principalTable: "Race",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Subject_Referrer_ReferrerId",
                table: "Subject",
                column: "ReferrerId",
                principalTable: "Referrer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_SubjectRelationship_RelationshipType_RelationshipTypeId",
                table: "SubjectRelationship",
                column: "RelationshipTypeId",
                principalTable: "RelationshipType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_SubjectSearchRecipeItem_SearchRecipeItem_SearchRecipeItemId",
                table: "SubjectSearchRecipeItem",
                column: "SearchRecipeItemId",
                principalTable: "SearchRecipeItem",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_SubjectSearchRecipeItem_Subject_SubjectId",
                table: "SubjectSearchRecipeItem",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Vehicle_Subject_SubjectId",
                table: "Vehicle",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_Video_Subject_SubjectId",
                table: "Video",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId",
                table: "AspNetUserClaims",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId",
                table: "AspNetUserLogins",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_IdentityRole_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_ApplicationUser_UserId",
                table: "AspNetUserRoles",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(name: "FK_ActivityNote_Subject_SubjectId", table: "ActivityNote");
            migrationBuilder.DropForeignKey(name: "FK_Address_AddressType_AddressTypeId", table: "Address");
            migrationBuilder.DropForeignKey(name: "FK_Address_Subject_SubjectId", table: "Address");
            migrationBuilder.DropForeignKey(name: "FK_AKA_Subject_SubjectId", table: "AKA");
            migrationBuilder.DropForeignKey(name: "FK_BailBond_Subject_SubjectId", table: "BailBond");
            migrationBuilder.DropForeignKey(name: "FK_BookingCharge_BailBond_BailBondId", table: "BookingCharge");
            migrationBuilder.DropForeignKey(name: "FK_DriverLicense_Subject_SubjectId", table: "DriverLicense");
            migrationBuilder.DropForeignKey(name: "FK_EmailAddress_EmailType_EmailTypeId", table: "EmailAddress");
            migrationBuilder.DropForeignKey(name: "FK_EmailAddress_Subject_SubjectId", table: "EmailAddress");
            migrationBuilder.DropForeignKey(name: "FK_Employer_Subject_SubjectId", table: "Employer");
            migrationBuilder.DropForeignKey(name: "FK_Hyperlink_Subject_SubjectId", table: "Hyperlink");
            migrationBuilder.DropForeignKey(name: "FK_PhoneNumber_PhoneType_PhoneTypeId", table: "PhoneNumber");
            migrationBuilder.DropForeignKey(name: "FK_PhoneNumber_Subject_SubjectId", table: "PhoneNumber");
            migrationBuilder.DropForeignKey(name: "FK_SearchRecipeItem_SearchRecipe_SearchRecipeId", table: "SearchRecipeItem");
            migrationBuilder.DropForeignKey(name: "FK_SearchResult_Subject_SubjectId", table: "SearchResult");
            migrationBuilder.DropForeignKey(name: "FK_StoredFile_Subject_SubjectId", table: "StoredFile");
            migrationBuilder.DropForeignKey(name: "FK_Subject_Gender_GenderId", table: "Subject");
            migrationBuilder.DropForeignKey(name: "FK_Subject_Race_RaceId", table: "Subject");
            migrationBuilder.DropForeignKey(name: "FK_Subject_Referrer_ReferrerId", table: "Subject");
            migrationBuilder.DropForeignKey(name: "FK_SubjectRelationship_RelationshipType_RelationshipTypeId", table: "SubjectRelationship");
            migrationBuilder.DropForeignKey(name: "FK_SubjectSearchRecipeItem_SearchRecipeItem_SearchRecipeItemId", table: "SubjectSearchRecipeItem");
            migrationBuilder.DropForeignKey(name: "FK_SubjectSearchRecipeItem_Subject_SubjectId", table: "SubjectSearchRecipeItem");
            migrationBuilder.DropForeignKey(name: "FK_Vehicle_Subject_SubjectId", table: "Vehicle");
            migrationBuilder.DropForeignKey(name: "FK_Video_Subject_SubjectId", table: "Video");
            migrationBuilder.DropForeignKey(name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId", table: "AspNetRoleClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId", table: "AspNetUserClaims");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId", table: "AspNetUserLogins");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_IdentityRole_RoleId", table: "AspNetUserRoles");
            migrationBuilder.DropForeignKey(name: "FK_IdentityUserRole<string>_ApplicationUser_UserId", table: "AspNetUserRoles");
            migrationBuilder.DropColumn(name: "RelatedAddressSource", table: "ActivityNote");
            migrationBuilder.AddForeignKey(
                name: "FK_ActivityNote_Subject_SubjectId",
                table: "ActivityNote",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Address_AddressType_AddressTypeId",
                table: "Address",
                column: "AddressTypeId",
                principalTable: "AddressType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Address_Subject_SubjectId",
                table: "Address",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_AKA_Subject_SubjectId",
                table: "AKA",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_BailBond_Subject_SubjectId",
                table: "BailBond",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_BookingCharge_BailBond_BailBondId",
                table: "BookingCharge",
                column: "BailBondId",
                principalTable: "BailBond",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_DriverLicense_Subject_SubjectId",
                table: "DriverLicense",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_EmailAddress_EmailType_EmailTypeId",
                table: "EmailAddress",
                column: "EmailTypeId",
                principalTable: "EmailType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_EmailAddress_Subject_SubjectId",
                table: "EmailAddress",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Employer_Subject_SubjectId",
                table: "Employer",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Hyperlink_Subject_SubjectId",
                table: "Hyperlink",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_PhoneNumber_PhoneType_PhoneTypeId",
                table: "PhoneNumber",
                column: "PhoneTypeId",
                principalTable: "PhoneType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_PhoneNumber_Subject_SubjectId",
                table: "PhoneNumber",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_SearchRecipeItem_SearchRecipe_SearchRecipeId",
                table: "SearchRecipeItem",
                column: "SearchRecipeId",
                principalTable: "SearchRecipe",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_SearchResult_Subject_SubjectId",
                table: "SearchResult",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_StoredFile_Subject_SubjectId",
                table: "StoredFile",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Subject_Gender_GenderId",
                table: "Subject",
                column: "GenderId",
                principalTable: "Gender",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Subject_Race_RaceId",
                table: "Subject",
                column: "RaceId",
                principalTable: "Race",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Subject_Referrer_ReferrerId",
                table: "Subject",
                column: "ReferrerId",
                principalTable: "Referrer",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_SubjectRelationship_RelationshipType_RelationshipTypeId",
                table: "SubjectRelationship",
                column: "RelationshipTypeId",
                principalTable: "RelationshipType",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_SubjectSearchRecipeItem_SearchRecipeItem_SearchRecipeItemId",
                table: "SubjectSearchRecipeItem",
                column: "SearchRecipeItemId",
                principalTable: "SearchRecipeItem",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_SubjectSearchRecipeItem_Subject_SubjectId",
                table: "SubjectSearchRecipeItem",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Vehicle_Subject_SubjectId",
                table: "Vehicle",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_Video_Subject_SubjectId",
                table: "Video",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityRoleClaim<string>_IdentityRole_RoleId",
                table: "AspNetRoleClaims",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserClaim<string>_ApplicationUser_UserId",
                table: "AspNetUserClaims",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserLogin<string>_ApplicationUser_UserId",
                table: "AspNetUserLogins",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_IdentityRole_RoleId",
                table: "AspNetUserRoles",
                column: "RoleId",
                principalTable: "AspNetRoles",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
            migrationBuilder.AddForeignKey(
                name: "FK_IdentityUserRole<string>_ApplicationUser_UserId",
                table: "AspNetUserRoles",
                column: "UserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
