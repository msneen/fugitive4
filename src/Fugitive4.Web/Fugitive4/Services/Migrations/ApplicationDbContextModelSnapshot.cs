using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using Fugitive4.Services.Models;

namespace Fugitive4.Services.Migrations
{
    [DbContext(typeof(ApplicationDbContext))]
    partial class ApplicationDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Fugitive4.Services.Models.ActivityNote", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<DateTime>("ActivityDate");

                    b.Property<string>("Address");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<string>("Description");

                    b.Property<bool>("Disabled");

                    b.Property<string>("Latitude");

                    b.Property<string>("Longitude");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("PerformedBy");

                    b.Property<int?>("RelatedAddressId");

                    b.Property<int>("RelatedAddressSource");

                    b.Property<int?>("RelatedSubjectId");

                    b.Property<int>("SubjectId");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.Address", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address1");

                    b.Property<string>("Address2");

                    b.Property<int>("AddressTypeId");

                    b.Property<bool>("BelongsToSubject");

                    b.Property<string>("City");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<bool>("Disabled");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Notes");

                    b.Property<string>("State");

                    b.Property<int>("SubjectId");

                    b.Property<string>("Zipcode");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.AddressType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<bool>("Disabled");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Name")
                        .HasAnnotation("MaxLength", 150);

                    b.HasKey("Id");

                    b.HasIndex("Name")
                        .IsUnique();
                });

            modelBuilder.Entity("Fugitive4.Services.Models.AKA", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<bool>("Disabled");

                    b.Property<string>("Firstname");

                    b.Property<string>("Lastname");

                    b.Property<string>("Middlename");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<int>("SubjectId");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.ApplicationUser", b =>
                {
                    b.Property<string>("Id");

                    b.Property<int>("AccessFailedCount");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Email")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<bool>("EmailConfirmed");

                    b.Property<bool>("LockoutEnabled");

                    b.Property<DateTimeOffset?>("LockoutEnd");

                    b.Property<string>("NormalizedEmail")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedUserName")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("PasswordHash");

                    b.Property<string>("PhoneNumber");

                    b.Property<bool>("PhoneNumberConfirmed");

                    b.Property<string>("SecurityStamp");

                    b.Property<bool>("TwoFactorEnabled");

                    b.Property<string>("UserName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasAnnotation("Relational:Name", "EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .HasAnnotation("Relational:Name", "UserNameIndex");

                    b.HasAnnotation("Relational:TableName", "AspNetUsers");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.AspNetUsers_Referrer", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ApplicationUserId");

                    b.Property<string>("AspNetUsersId")
                        .HasAnnotation("MaxLength", 450);

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<bool>("Disabled");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<int>("ReferrerId");

                    b.HasKey("Id");

                    b.HasIndex("ReferrerId", "AspNetUsersId")
                        .IsUnique();
                });

            modelBuilder.Entity("Fugitive4.Services.Models.AspNetUsers_Team", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ApplicationUserId");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<bool>("Disabled");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<int>("TeamId");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.BailBond", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<decimal?>("BondAmount");

                    b.Property<string>("BookingNumber");

                    b.Property<string>("Court");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<DateTime?>("DefaultDate");

                    b.Property<string>("Description");

                    b.Property<bool>("Disabled");

                    b.Property<DateTime?>("IssueDate");

                    b.Property<string>("IssuingBondCompany");

                    b.Property<string>("IssuingBondsman");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<DateTime?>("OriginalArrestDate");

                    b.Property<int>("SubjectId");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.BookingCharge", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address1");

                    b.Property<string>("Address2");

                    b.Property<int>("BailBondId");

                    b.Property<string>("BookingNumber");

                    b.Property<string>("ChargeName");

                    b.Property<string>("City");

                    b.Property<string>("CodeNumber");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<DateTime?>("DateOccurred");

                    b.Property<string>("Description");

                    b.Property<string>("Details");

                    b.Property<string>("Detectives");

                    b.Property<bool>("Disabled");

                    b.Property<bool>("Felony");

                    b.Property<string>("Jurisdiction");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Notes");

                    b.Property<string>("PoliceAgency");

                    b.Property<string>("ReportNumber");

                    b.Property<string>("State");

                    b.Property<string>("Victim");

                    b.Property<string>("Zipcode");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.DriverLicense", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address1");

                    b.Property<string>("Address2");

                    b.Property<string>("City");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<string>("DLNumber");

                    b.Property<bool>("Disabled");

                    b.Property<DateTime?>("ExpirationDate");

                    b.Property<string>("FullName");

                    b.Property<DateTime?>("IssueDate");

                    b.Property<string>("IssuingState");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Notes");

                    b.Property<string>("State");

                    b.Property<int>("SubjectId");

                    b.Property<string>("Zipcode");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.EmailAddress", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BelongsTo");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<bool>("Disabled");

                    b.Property<string>("EmailAddress1");

                    b.Property<int>("EmailTypeId");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Notes");

                    b.Property<int>("SubjectId");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.EmailType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<bool>("Disabled");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Name")
                        .HasAnnotation("MaxLength", 150);

                    b.HasKey("Id");

                    b.HasIndex("Name")
                        .IsUnique();
                });

            modelBuilder.Entity("Fugitive4.Services.Models.Employer", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Address1");

                    b.Property<string>("Address2");

                    b.Property<string>("City");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<bool>("Disabled");

                    b.Property<DateTime?>("EndDate");

                    b.Property<string>("JobTitle");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Name");

                    b.Property<string>("Notes");

                    b.Property<string>("PhoneNumber");

                    b.Property<DateTime?>("StartDate");

                    b.Property<string>("State");

                    b.Property<int>("SubjectId");

                    b.Property<string>("Zipcode");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.Gender", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<bool>("Disabled");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Name");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.Hyperlink", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<bool>("Disabled");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Notes");

                    b.Property<int>("SubjectId");

                    b.Property<string>("Url");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.PhoneNumber", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BelongsTo");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<bool>("Disabled");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Notes");

                    b.Property<string>("PhoneNumber1");

                    b.Property<int>("PhoneTypeId");

                    b.Property<int>("SubjectId");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.PhoneType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<bool>("Disabled");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Name")
                        .HasAnnotation("MaxLength", 150);

                    b.HasKey("Id");

                    b.HasIndex("Name")
                        .IsUnique();
                });

            modelBuilder.Entity("Fugitive4.Services.Models.Race", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<bool>("Disabled");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Name");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.Referrer", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("AdditionalInfo");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<string>("Description");

                    b.Property<bool>("Disabled");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Name");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.RelationshipType", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<bool>("Disabled");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Name");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.SearchRecipe", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<string>("Description");

                    b.Property<bool>("Disabled");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Name")
                        .HasAnnotation("MaxLength", 150);

                    b.HasKey("Id");

                    b.HasIndex("Name")
                        .IsUnique();
                });

            modelBuilder.Entity("Fugitive4.Services.Models.SearchRecipeItem", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<string>("Description");

                    b.Property<bool>("Disabled");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Name");

                    b.Property<int>("SearchRecipeId");

                    b.Property<int>("SortOrder");

                    b.Property<string>("UrlOrSource");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.SearchResult", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<bool>("Disabled");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Notes");

                    b.Property<string>("ResultText");

                    b.Property<int>("SubjectId");

                    b.Property<int?>("SubjectSearchRecipeItemId");

                    b.Property<string>("UrlOrSource");

                    b.Property<string>("Username");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.StoredFile", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ContentType");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<bool>("Disabled");

                    b.Property<byte[]>("FileContent");

                    b.Property<string>("FileName");

                    b.Property<string>("FileType");

                    b.Property<bool>("IsSubject");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Notes");

                    b.Property<bool>("ShowOnDetail");

                    b.Property<int>("SubjectId");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.Subject", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<DateTime?>("DOB");

                    b.Property<bool>("Disabled");

                    b.Property<string>("EyeColor");

                    b.Property<string>("Firstname");

                    b.Property<int>("GenderId");

                    b.Property<string>("HairColor");

                    b.Property<string>("Height");

                    b.Property<string>("Lastname");

                    b.Property<string>("Middlename");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Notes");

                    b.Property<int>("PerceivedThreatLevel")
                        .HasAnnotation("Relational:DefaultValue", "0")
                        .HasAnnotation("Relational:DefaultValueType", "System.Int32");

                    b.Property<int>("RaceId");

                    b.Property<int>("ReferrerId");

                    b.Property<string>("SSN");

                    b.Property<int?>("Weight");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.SubjectRelationship", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<int?>("BailBondId");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<string>("Description");

                    b.Property<bool>("Disabled");

                    b.Property<int?>("EmployerId");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Notes");

                    b.Property<int>("RelatedSubjectId");

                    b.Property<int>("RelationshipTypeId");

                    b.Property<int>("SubjectId");

                    b.HasKey("Id");

                    b.HasIndex("SubjectId", "RelatedSubjectId")
                        .IsUnique();
                });

            modelBuilder.Entity("Fugitive4.Services.Models.SubjectSearchRecipe", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<string>("Description");

                    b.Property<bool>("Disabled");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Notes");

                    b.Property<int>("SearchRecipeId");

                    b.Property<int>("SubjectId");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.SubjectSearchRecipeItem", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<bool>("Disabled");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<int>("SearchRecipeItemId");

                    b.Property<int>("SubjectId");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.Team", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<string>("Description");

                    b.Property<bool>("Disabled");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Name")
                        .HasAnnotation("MaxLength", 50);

                    b.HasKey("Id");

                    b.HasIndex("Name")
                        .IsUnique();
                });

            modelBuilder.Entity("Fugitive4.Services.Models.Team_Referrer", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<bool>("Disabled");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<int>("ReferrerId");

                    b.Property<int>("TeamId");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.Vehicle", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("BelongsTo");

                    b.Property<string>("Color");

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<string>("Description");

                    b.Property<bool>("Disabled");

                    b.Property<string>("Make");

                    b.Property<string>("Model");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("PlateNumber");

                    b.Property<int>("SubjectId");

                    b.Property<string>("Year");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.Video", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("CreatedBy");

                    b.Property<DateTime>("CreatedDate");

                    b.Property<bool>("Disabled");

                    b.Property<string>("ModifiedBy");

                    b.Property<DateTime?>("ModifiedDate");

                    b.Property<string>("Notes");

                    b.Property<int>("SubjectId");

                    b.Property<string>("Url");

                    b.HasKey("Id");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityRole", b =>
                {
                    b.Property<string>("Id");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken();

                    b.Property<string>("Name")
                        .HasAnnotation("MaxLength", 256);

                    b.Property<string>("NormalizedName")
                        .HasAnnotation("MaxLength", 256);

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .HasAnnotation("Relational:Name", "RoleNameIndex");

                    b.HasAnnotation("Relational:TableName", "AspNetRoles");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("RoleId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "AspNetRoleClaims");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("ClaimType");

                    b.Property<string>("ClaimValue");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("Id");

                    b.HasAnnotation("Relational:TableName", "AspNetUserClaims");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider");

                    b.Property<string>("ProviderKey");

                    b.Property<string>("ProviderDisplayName");

                    b.Property<string>("UserId")
                        .IsRequired();

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasAnnotation("Relational:TableName", "AspNetUserLogins");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId");

                    b.Property<string>("RoleId");

                    b.HasKey("UserId", "RoleId");

                    b.HasAnnotation("Relational:TableName", "AspNetUserRoles");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.ActivityNote", b =>
                {
                    b.HasOne("Fugitive4.Services.Models.Subject")
                        .WithMany()
                        .HasForeignKey("SubjectId");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.Address", b =>
                {
                    b.HasOne("Fugitive4.Services.Models.AddressType")
                        .WithMany()
                        .HasForeignKey("AddressTypeId");

                    b.HasOne("Fugitive4.Services.Models.Subject")
                        .WithMany()
                        .HasForeignKey("SubjectId");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.AKA", b =>
                {
                    b.HasOne("Fugitive4.Services.Models.Subject")
                        .WithMany()
                        .HasForeignKey("SubjectId");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.AspNetUsers_Referrer", b =>
                {
                    b.HasOne("Fugitive4.Services.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("ApplicationUserId");

                    b.HasOne("Fugitive4.Services.Models.Referrer")
                        .WithMany()
                        .HasForeignKey("ReferrerId");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.AspNetUsers_Team", b =>
                {
                    b.HasOne("Fugitive4.Services.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("ApplicationUserId");

                    b.HasOne("Fugitive4.Services.Models.Team")
                        .WithMany()
                        .HasForeignKey("TeamId");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.BailBond", b =>
                {
                    b.HasOne("Fugitive4.Services.Models.Subject")
                        .WithMany()
                        .HasForeignKey("SubjectId");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.BookingCharge", b =>
                {
                    b.HasOne("Fugitive4.Services.Models.BailBond")
                        .WithMany()
                        .HasForeignKey("BailBondId");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.DriverLicense", b =>
                {
                    b.HasOne("Fugitive4.Services.Models.Subject")
                        .WithMany()
                        .HasForeignKey("SubjectId");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.EmailAddress", b =>
                {
                    b.HasOne("Fugitive4.Services.Models.EmailType")
                        .WithMany()
                        .HasForeignKey("EmailTypeId");

                    b.HasOne("Fugitive4.Services.Models.Subject")
                        .WithMany()
                        .HasForeignKey("SubjectId");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.Employer", b =>
                {
                    b.HasOne("Fugitive4.Services.Models.Subject")
                        .WithMany()
                        .HasForeignKey("SubjectId");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.Hyperlink", b =>
                {
                    b.HasOne("Fugitive4.Services.Models.Subject")
                        .WithMany()
                        .HasForeignKey("SubjectId");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.PhoneNumber", b =>
                {
                    b.HasOne("Fugitive4.Services.Models.PhoneType")
                        .WithMany()
                        .HasForeignKey("PhoneTypeId");

                    b.HasOne("Fugitive4.Services.Models.Subject")
                        .WithMany()
                        .HasForeignKey("SubjectId");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.SearchRecipeItem", b =>
                {
                    b.HasOne("Fugitive4.Services.Models.SearchRecipe")
                        .WithMany()
                        .HasForeignKey("SearchRecipeId");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.SearchResult", b =>
                {
                    b.HasOne("Fugitive4.Services.Models.Subject")
                        .WithMany()
                        .HasForeignKey("SubjectId");

                    b.HasOne("Fugitive4.Services.Models.SubjectSearchRecipeItem")
                        .WithMany()
                        .HasForeignKey("SubjectSearchRecipeItemId");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.StoredFile", b =>
                {
                    b.HasOne("Fugitive4.Services.Models.Subject")
                        .WithMany()
                        .HasForeignKey("SubjectId");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.Subject", b =>
                {
                    b.HasOne("Fugitive4.Services.Models.Gender")
                        .WithMany()
                        .HasForeignKey("GenderId");

                    b.HasOne("Fugitive4.Services.Models.Race")
                        .WithMany()
                        .HasForeignKey("RaceId");

                    b.HasOne("Fugitive4.Services.Models.Referrer")
                        .WithMany()
                        .HasForeignKey("ReferrerId");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.SubjectRelationship", b =>
                {
                    b.HasOne("Fugitive4.Services.Models.BailBond")
                        .WithMany()
                        .HasForeignKey("BailBondId");

                    b.HasOne("Fugitive4.Services.Models.Employer")
                        .WithMany()
                        .HasForeignKey("EmployerId");

                    b.HasOne("Fugitive4.Services.Models.Subject")
                        .WithMany()
                        .HasForeignKey("RelatedSubjectId");

                    b.HasOne("Fugitive4.Services.Models.RelationshipType")
                        .WithMany()
                        .HasForeignKey("RelationshipTypeId");

                    b.HasOne("Fugitive4.Services.Models.Subject")
                        .WithMany()
                        .HasForeignKey("SubjectId");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.SubjectSearchRecipe", b =>
                {
                    b.HasOne("Fugitive4.Services.Models.SearchRecipe")
                        .WithMany()
                        .HasForeignKey("SearchRecipeId");

                    b.HasOne("Fugitive4.Services.Models.Subject")
                        .WithMany()
                        .HasForeignKey("SubjectId");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.SubjectSearchRecipeItem", b =>
                {
                    b.HasOne("Fugitive4.Services.Models.SearchRecipeItem")
                        .WithMany()
                        .HasForeignKey("SearchRecipeItemId");

                    b.HasOne("Fugitive4.Services.Models.Subject")
                        .WithMany()
                        .HasForeignKey("SubjectId");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.Team_Referrer", b =>
                {
                    b.HasOne("Fugitive4.Services.Models.Referrer")
                        .WithMany()
                        .HasForeignKey("ReferrerId");

                    b.HasOne("Fugitive4.Services.Models.Team")
                        .WithMany()
                        .HasForeignKey("TeamId");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.Vehicle", b =>
                {
                    b.HasOne("Fugitive4.Services.Models.Subject")
                        .WithMany()
                        .HasForeignKey("SubjectId");
                });

            modelBuilder.Entity("Fugitive4.Services.Models.Video", b =>
                {
                    b.HasOne("Fugitive4.Services.Models.Subject")
                        .WithMany()
                        .HasForeignKey("SubjectId");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNet.Identity.EntityFramework.IdentityRole")
                        .WithMany()
                        .HasForeignKey("RoleId");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("Fugitive4.Services.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("Fugitive4.Services.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });

            modelBuilder.Entity("Microsoft.AspNet.Identity.EntityFramework.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNet.Identity.EntityFramework.IdentityRole")
                        .WithMany()
                        .HasForeignKey("RoleId");

                    b.HasOne("Fugitive4.Services.Models.ApplicationUser")
                        .WithMany()
                        .HasForeignKey("UserId");
                });
        }
    }
}
