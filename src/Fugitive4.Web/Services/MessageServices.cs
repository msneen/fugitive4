﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.OptionsModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Twilio;  //not available for dnx 5.0 yet

namespace Fugitive4.Web.Services
{
    // This class is used by the application to send Email and SMS
    // when you turn on two-factor authentication in ASP.NET Identity.
    // For more details see this link http://go.microsoft.com/fwlink/?LinkID=532713
    public class AuthMessageSender : IEmailSender, ISmsSender
    {
        private AuthMessageSMSSenderOptions _authMessageSMSSenderOptions;
        public AuthMessageSender(IOptions<AuthMessageSMSSenderOptions> twilioOptions)
        {
            _authMessageSMSSenderOptions = twilioOptions.Value;
        }

        public Task SendEmailAsync(string email, string subject, string message)
        {
            // Plug in your email service here to send an email.
            return Task.FromResult(0);
        }

        public Task SendSmsAsync(string number, string message)
        {
            // Plug in your SMS service here to send a text message.

            string AccountSid = _authMessageSMSSenderOptions.SID;
            string AuthToken = _authMessageSMSSenderOptions.AuthToken;

            var twilio = new TwilioRestClient(AccountSid, AuthToken);
            var returnMessage = twilio.SendMessage(
                _authMessageSMSSenderOptions.SendNumber, number,
                message);


            return Task.FromResult(0);
        }




    }
}
