using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.Employer;
using Microsoft.AspNet.Authorization;

namespace Fugitive4.Web.Controllers
{
    [Authorize(Roles = "Member,Admin")]
    public class EmployerController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly IEmployerService _employerService;					
		private readonly ISubjectService _subjectService;

		public EmployerController(IEmployerService employerService,
								ISubjectService subjectService,
								IMapper mapper		)
        {
            _employerService = employerService;
			_mapper = mapper;
			_subjectService = subjectService;

        }
		[Route("Subject/{subjectId}/Employer/Index")]
        public async Task<IActionResult> Index(int subjectId , string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";

            var employers = await _employerService.GetAllAsync();
            var employerList = new EmployerListVm
            {
                Employers = _mapper.Map<IEnumerable<Employer>, IEnumerable<EmployerVm>>(employers.ToList())
            };

            return View("Index", employerList);
        }

		public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            Employer employer = await _employerService.GetByIdAsync(id.Value);
            if (employer == null)
            {
                return HttpNotFound();
            }

            var employerVm = _mapper.Map<Employer, EmployerVm>(employer);

            return View(employerVm);
        }


		[Route("Subject/{subjectId}/Employer/Create", Name="EmployerForSubject")]
		public ActionResult Create(int subjectId , string returnUrl = "")
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";
			ViewBag.ReturnUrl = returnUrl;

            var employerVm = new EmployerVm();

			LoadLookups(employerVm);

            return View(employerVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
		[Route("Subject/{subjectId}/Employer/Create", Name = "EmployerForSubject")]
        public async Task<IActionResult> Create(EmployerVm employerVm, string returnUrl = "")
        {
			ViewBag.ReturnUrl = returnUrl;
            try
            {
                if (ModelState.IsValid)
                {
                    var employer = _mapper.Map<EmployerVm, Employer>(employerVm);
                    await _employerService.InsertAsync(employer, GetLogonUser());
					return RedirectToIndexOrSubjectDetail(returnUrl);
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(employerVm);

                return View(employerVm);
            }
        }

		[Route("Subject/{subjectId}/Employer/Edit/{id}", Name = "EmployerForSubjectEdit")]
		public async Task<IActionResult> Edit(int subjectId , int? id, string returnUrl = "")
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";
			ViewBag.ReturnUrl = returnUrl;

			if (id == null)
            {
                return HttpNotFound();
            }

            var employer = await _employerService.GetByIdAsync(id.Value);
            if (employer == null)
            {
                return HttpNotFound();
            }            
			
			var employerVm = _mapper.Map<Employer, EmployerVm>(employer);

			LoadLookups(employerVm);

            return View(employerVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
		[Route("Subject/{subjectId}/Employer/Edit/{id}", Name = "EmployerForSubjectEdit")]
        public async Task<IActionResult> Edit(EmployerVm employerVm, string returnUrl = "")
        {
			ViewBag.ReturnUrl = returnUrl;
            try
            {
                if (ModelState.IsValid)
                {
                    var employer = _mapper.Map<EmployerVm, Employer>(employerVm); 
                    await _employerService.UpdateAsync(employer, GetLogonUser());
					return RedirectToIndexOrSubjectDetail(returnUrl);
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(employerVm);

                return View(employerVm);
            }
        }

		[ActionName("Delete")]
		[Route("Subject/{subjectId}/Employer/Delete/{id}")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var employer = await _employerService.GetByIdAsync(id.Value);
			if (employer == null)
            {
                return HttpNotFound();
            }

            var employerVm = _mapper.Map<Employer, EmployerVm>(employer);

            return View(employerVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
		[Route("Subject/{subjectId}/Employer/Delete/{id}")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _employerService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(EmployerVm employerVm)
        {
			employerVm.SubjectList = _subjectService.GetAll(GetLogonUser()).ToSelectList(s => s.Id.ToString(), s => s.EyeColor).ToList();
		}

	}
}
