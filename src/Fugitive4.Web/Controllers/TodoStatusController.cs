using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.TodoStatus;
using Microsoft.AspNet.Authorization;

namespace Fugitive4.Web.Controllers
{
	[Authorize]
    public class TodoStatusController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly ITodoStatusService _todoStatusService;

		public TodoStatusController(ITodoStatusService todoStatusService,
								IMapper mapper		)
        {
            _todoStatusService = todoStatusService;
			_mapper = mapper;

        }
        public async Task<IActionResult> Index( string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";

            var todoStatuses = await _todoStatusService.GetAllAsync();
            var todoStatusList = new TodoStatusListVm
            {
                TodoStatuses = _mapper.Map<IEnumerable<TodoStatus>, IEnumerable<TodoStatusVm>>(todoStatuses.ToList())
            };

            return View("Index", todoStatusList);
        }

		public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            TodoStatus todoStatus = await _todoStatusService.GetByIdAsync(id.Value);
            if (todoStatus == null)
            {
                return HttpNotFound();
            }

            var todoStatusVm = _mapper.Map<TodoStatus, TodoStatusVm>(todoStatus);

            return View(todoStatusVm);
        }


		public ActionResult Create( string returnUrl = "")
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";
			ViewBag.ReturnUrl = returnUrl;

            var todoStatusVm = new TodoStatusVm();

			LoadLookups(todoStatusVm);

            return View(todoStatusVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(TodoStatusVm todoStatusVm, string returnUrl = "")
        {
					ViewBag.ReturnUrl = returnUrl;
            try
            {
                if (ModelState.IsValid)
                {
                    var todoStatus = _mapper.Map<TodoStatusVm, TodoStatus>(todoStatusVm);
                    await _todoStatusService.InsertAsync(todoStatus, GetLogonUser());
					return RedirectToIndexOrSubjectDetail(returnUrl);
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(todoStatusVm);

                return View(todoStatusVm);
            }
        }

		public async Task<IActionResult> Edit( int? id, string returnUrl = "")
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";
			ViewBag.ReturnUrl = returnUrl;

			if (id == null)
            {
                return HttpNotFound();
            }

            var todoStatus = await _todoStatusService.GetByIdAsync(id.Value);
            if (todoStatus == null)
            {
                return HttpNotFound();
            }            
			
			var todoStatusVm = _mapper.Map<TodoStatus, TodoStatusVm>(todoStatus);

			LoadLookups(todoStatusVm);

            return View(todoStatusVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(TodoStatusVm todoStatusVm, string returnUrl = "")
        {
			ViewBag.ReturnUrl = returnUrl;
            try
            {
                if (ModelState.IsValid)
                {
                    var todoStatus = _mapper.Map<TodoStatusVm, TodoStatus>(todoStatusVm); 
                    await _todoStatusService.UpdateAsync(todoStatus, GetLogonUser());
					return RedirectToIndexOrSubjectDetail(returnUrl);
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(todoStatusVm);

                return View(todoStatusVm);
            }
        }

		[ActionName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var todoStatus = await _todoStatusService.GetByIdAsync(id.Value);
			if (todoStatus == null)
            {
                return HttpNotFound();
            }

            var todoStatusVm = _mapper.Map<TodoStatus, TodoStatusVm>(todoStatus);

            return View(todoStatusVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _todoStatusService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(TodoStatusVm todoStatusVm)
        {
		}

	}
}
