using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.Referrer;
using Microsoft.AspNet.Authorization;

namespace Fugitive4.Web.Controllers
{
    [Authorize(Roles = "Member,Admin")]
    public class ReferrerController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly IReferrerService _referrerService;

		public ReferrerController(IReferrerService referrerService,
								IMapper mapper		)
        {
            _referrerService = referrerService;
			_mapper = mapper;

        }

        public async Task<IActionResult> Index(string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";

            var referrers = await _referrerService.GetAllAsync();
            var referrerList = new ReferrerListVm
            {
                Referrers = _mapper.Map<IEnumerable<Referrer>, IEnumerable<ReferrerVm>>(referrers.ToList())
            };

            return View("Index", referrerList);
        }

		public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            Referrer referrer = await _referrerService.GetByIdAsync(id.Value);
            if (referrer == null)
            {
                return HttpNotFound();
            }

            var referrerVm = _mapper.Map<Referrer, ReferrerVm>(referrer);

            return View(referrerVm);
        }




		public ActionResult Create()
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";

            var referrerVm = new ReferrerVm();

			LoadLookups(referrerVm);

            return View(referrerVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ReferrerVm referrerVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var referrer = _mapper.Map<ReferrerVm, Referrer>(referrerVm);
                    await _referrerService.InsertAsync(referrer, GetLogonUser());
					return RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(referrerVm);

                return View(referrerVm);
            }
        }

		public async Task<IActionResult> Edit(int? id)
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";

			if (id == null)
            {
                return HttpNotFound();
            }

            var referrer = await _referrerService.GetByIdAsync(id.Value);
            if (referrer == null)
            {
                return HttpNotFound();
            }            
			
			var referrerVm = _mapper.Map<Referrer, ReferrerVm>(referrer);

			LoadLookups(referrerVm);

            return View(referrerVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(ReferrerVm referrerVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var referrer = _mapper.Map<ReferrerVm, Referrer>(referrerVm); 
                    await _referrerService.UpdateAsync(referrer, GetLogonUser());
					return RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(referrerVm);

                return View(referrerVm);
            }
        }

		[ActionName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var referrer = await _referrerService.GetByIdAsync(id.Value);
			if (referrer == null)
            {
                return HttpNotFound();
            }

            var referrerVm = _mapper.Map<Referrer, ReferrerVm>(referrer);

            return View(referrerVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _referrerService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(ReferrerVm referrerVm)
        {
		}

	}
}
