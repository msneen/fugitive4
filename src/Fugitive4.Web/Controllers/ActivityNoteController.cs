using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Services.Enums;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.ActivityNote;
using Microsoft.AspNet.Authorization;
using Fugitive4.Web.ViewModels.Subject;
using Fugitive4.Web.ViewModels.Address;
using Fugitive4.Web.Helpers;
using Fugitive4.Web.ViewModels._Interfaces;



namespace Fugitive4.Web.Controllers
{
    [Authorize(Roles = "Member,Admin")]
    public class ActivityNoteController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly IActivityNoteService _activityNoteService;					
		private readonly ISubjectService _subjectService;
        private readonly IAddressService _addressService;
        private readonly IAddressTypeService _addressTypeService;
        private readonly ISubjectRelationshipService _subjectRelationshipService;
        private readonly IGenderService _genderService;
        private readonly IRaceService _raceService;
        private readonly IReferrerService _referrerService;
        private readonly IRelationshipTypeService _relationshipTypeService;
        private readonly IEmployerService _employerService;

        public ActivityNoteController(IActivityNoteService activityNoteService,
								ISubjectService subjectService,
                                IAddressService addressService,
                                IAddressTypeService addressTypeService,
                                ISubjectRelationshipService subjectRelationshipService,
                                IGenderService genderService,
                                IRaceService raceService,
                                IReferrerService referrerService,
                                IRelationshipTypeService relationshipTypeService,
                                IEmployerService employerService,
                                IMapper mapper		)
        {
            _activityNoteService = activityNoteService;
			_mapper = mapper;
			_subjectService = subjectService;
            _addressService = addressService;
            _addressTypeService = addressTypeService;
            _subjectRelationshipService = subjectRelationshipService;
            _genderService = genderService;
            _raceService = raceService;
            _referrerService = referrerService;
            _relationshipTypeService = relationshipTypeService;
            _employerService = employerService;
        }
[Route("Subject/{subjectId}/ActivityNote/Index")]
        public async Task<IActionResult> Index(int subjectId , string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";

            var activityNotes = await _activityNoteService.GetAllAsync();
            var activityNoteList = new ActivityNoteListVm
            {
                ActivityNotes = _mapper.Map<IEnumerable<ActivityNote>, IEnumerable<ActivityNoteVm>>(activityNotes.Where(s=>s.SubjectId==subjectId).OrderByDescending(an=>an.ActivityDate).ToList())
            };

            return View("Index", activityNoteList);
        }

		public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            ActivityNote activityNote = await _activityNoteService.GetByIdAsync(id.Value);
            if (activityNote == null)
            {
                return HttpNotFound();
            }

            var activityNoteVm = _mapper.Map<ActivityNote, ActivityNoteVm>(activityNote);

            if (activityNote.RelatedSubjectId.HasValue && activityNote.RelatedSubjectId.Value > 0)
            {
                var subject = await _subjectService.GetByIdMediumAsync(activityNote.RelatedSubjectId.Value, GetLogonUser());
                activityNoteVm.RelatedSubject = _mapper.Map<Subject, SubjectVm>(subject);
            }
            if(activityNote.RelatedAddressId.HasValue && activityNote.RelatedAddressId.Value > 0)
            {
                var address = await _addressService.GetByIdAsync(activityNote.RelatedAddressId.Value);
                activityNoteVm.RelatedAddress = _mapper.Map<Address, AddressVm>(address);
            }

            return View(activityNoteVm);
        }


[Route("Subject/{subjectId}/ActivityNote/Create", Name= "ActivityNoteForSubject")]
		public ActionResult Create(int subjectId, string returnUrl = "")
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";
            ViewBag.ReturnUrl = returnUrl;

            var activityNoteVm = new ActivityNoteVm
            {
                SubjectId = subjectId,
                ActivityDate = DateTime.Now,
                PerformedBy = GetLogonUser(),                
            };

			LoadLookups(activityNoteVm);

            return View(activityNoteVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
[Route("Subject/{subjectId}/ActivityNote/Create", Name = "ActivityNoteForSubject")]
        public async Task<IActionResult> Create(ActivityNoteVm activityNoteVm, string returnUrl = "")
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var activityNote = _mapper.Map<ActivityNoteVm, ActivityNote>(activityNoteVm);
                    activityNote.PerformedBy = GetLogonUser();
                    activityNote.RelatedSubjectId = activityNoteVm.RelatedSubjectId.HasValue ? activityNoteVm.RelatedSubjectId.Value : 0;
                    activityNote.RelatedAddressId = activityNoteVm.RelatedAddressId.HasValue ? activityNoteVm.RelatedAddressId.Value : 0;

                    if(activityNoteVm.RelatedAddressId.HasValue && activityNoteVm.RelatedAddressId.Value > 0)
                    {
                        if(activityNoteVm.AddressList == null || activityNoteVm.AddressList.Count < 1)
                        {
                            LoadLookups(activityNoteVm);
                        }
                        var selectedAddress = activityNoteVm.AddressList.Find(a => a.Value == activityNote.RelatedAddressId.ToString());
                        if(selectedAddress != null)
                        {
                            activityNote.RelatedAddressSource = (AddressSource)Enum.Parse(typeof(AddressSource),selectedAddress.Group.Name);
                        }
                    }

                    //Save the new subject if they added one
                    if(activityNoteVm.RelatedSubjectId == 0)
                    {
                        if(!string.IsNullOrWhiteSpace(activityNoteVm.Lastname) && !string.IsNullOrWhiteSpace(activityNoteVm.Firstname))
                        {
                            var existingSubject = await _subjectService.GetByIdMediumAsync(activityNoteVm.SubjectId, GetLogonUser());
                            var newSubject = new Subject
                            {
                                Lastname = activityNoteVm.Lastname,
                                Firstname=activityNoteVm.Firstname,
                                GenderId = activityNoteVm.GenderId,
                                RaceId = activityNoteVm.RaceId,
                                ReferrerId = existingSubject.ReferrerId
                            };
                            await _subjectService.InsertAsync(newSubject, GetLogonUser());
                            var subjectRelationship = new SubjectRelationship
                            {
                                SubjectId = activityNoteVm.SubjectId,
                                RelatedSubjectId = newSubject.Id,
                                RelationshipTypeId = (await _relationshipTypeService.GetAllAsync()).Last().Id
                            };
                            await _subjectRelationshipService.InsertAsync(subjectRelationship, GetLogonUser());
                            activityNote.RelatedSubjectId = newSubject.Id;
                        }
                    }

                    await SaveAddress(activityNote);
                    await _activityNoteService.InsertAsync(activityNote, GetLogonUser());
                    return RedirectToIndexOrSubjectDetail(returnUrl);
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(activityNoteVm);

                return View(activityNoteVm);
            }
        }

        private async Task SaveAddress(ActivityNote activityNote)
        {
            //Parse the string address and add it to the subject, if there is one
            //123 Main St, ste 201, San Diego, Ca  92108
            //before first comma is address line 1
            //After Last Comma is State and Zip
            //Second to last is City
            //any left over is Addresss Line 2
            try
            {
                if (!string.IsNullOrWhiteSpace(activityNote.Address) && activityNote.Address.Trim().Count() > 15)
                {
                    var addressLines = activityNote.Address.Split(',');
                    if (activityNote.RelatedSubjectId.HasValue && activityNote.RelatedSubjectId.Value > 0 &&
                        addressLines.Count() > 0)
                    {
                        var address = AddressHelper.ParseAddress(addressLines);
                        address.SubjectId = activityNote.RelatedSubjectId.Value;
                        address.AddressTypeId = (await _addressTypeService.GetAllAsync()).FirstOrDefault().Id;
                        //save subject address here
                        await _addressService.InsertAsync(address, GetLogonUser());
                    }
                }
            }
            catch(Exception ex) { }//just ignore the exception. The user will be on a phone, and we
            //don't want to waste their time playing with a parsing exception

        }



        [Route("Subject/{subjectId}/ActivityNote/Edit/{id}", Name = "ActivityNoteForSubjectEdit")]
		public async Task<IActionResult> Edit(int subjectId , int? id, string returnUrl = "")
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";
            ViewBag.ReturnUrl = returnUrl;

            if (id == null)
            {
                return HttpNotFound();
            }

            var activityNote = await _activityNoteService.GetByIdAsync(id.Value);
            if (activityNote == null)
            {
                return HttpNotFound();
            }            
			
			var activityNoteVm = _mapper.Map<ActivityNote, ActivityNoteVm>(activityNote);

			LoadLookups(activityNoteVm);

            return View(activityNoteVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Subject/{subjectId}/ActivityNote/Edit/{id}", Name = "ActivityNoteForSubjectEdit")]
        public async Task<IActionResult> Edit(ActivityNoteVm activityNoteVm, string returnUrl = "")
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var activityNote = _mapper.Map<ActivityNoteVm, ActivityNote>(activityNoteVm); 
                    await _activityNoteService.UpdateAsync(activityNote, GetLogonUser());
                    return RedirectToIndexOrSubjectDetail(returnUrl);
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(activityNoteVm);

                return View(activityNoteVm);
            }
        }

		[ActionName("Delete")]
[Route("Subject/{subjectId}/ActivityNote/Delete/{id}")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var activityNote = await _activityNoteService.GetByIdAsync(id.Value);
			if (activityNote == null)
            {
                return HttpNotFound();
            }

            var activityNoteVm = _mapper.Map<ActivityNote, ActivityNoteVm>(activityNote);

            return View(activityNoteVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
[Route("Subject/{subjectId}/ActivityNote/Delete/{id}")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _activityNoteService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(ActivityNoteVm activityNoteVm)
        {
            activityNoteVm.AddressList = new List<SelectListItem>();
            List<Subject> subjects = new List<Subject>();
            List<IAddress> addresses = new List<IAddress>();
            var subject = _subjectService.GetById(activityNoteVm.SubjectId, GetLogonUser());
            subjects.Add(subject);


            var subjectAddressSelectList = subject
                                            .Addresses
                                            .ToSelectList(a => a.Id.ToString(), t => t.Address1 + " - " + t.City, "SubjectAddress")
                                            .GroupBy(g => g.Value).Select(h => h.First())
                                            .ToList();
            activityNoteVm.AddressList.AddRange(subjectAddressSelectList);

            var subjectRelationships = _subjectRelationshipService.GetBySubjectId(activityNoteVm.SubjectId).ToList();
            foreach(var subjectRelationship in subjectRelationships)
            {
                if(subjectRelationship.Subject.Id != activityNoteVm.SubjectId)
                {
                    subjects.Add(subjectRelationship.Subject);
                    addresses.AddRange(subjectRelationship.Subject.Addresses);
                }
                else if (subjectRelationship.RelatedSubject.Id != activityNoteVm.SubjectId)
                {
                    subjects.Add(subjectRelationship.RelatedSubject);
                    addresses.AddRange(subjectRelationship.RelatedSubject.Addresses);
                }
            }

            var relatedSubjectAddressSelectList = addresses
                                .ToSelectList(a => a.Id.ToString(), t => t.Address1 + " - " + t.City, "SubjectAddress")
                                .GroupBy(g => g.Value).Select(h => h.First())
                                .ToList();
            activityNoteVm.AddressList.AddRange(relatedSubjectAddressSelectList);
            addresses.Clear();

            //addresses in bail charges
            var addressCharges = subject.BailBonds.SelectMany(b => b.BookingCharges).Where(bc => !string.IsNullOrEmpty(bc.Address1) && !string.IsNullOrEmpty(bc.City));
            foreach(IAddress bookingCharge in addressCharges)
            {
                addresses.Add(bookingCharge);
            }
            var bookingChargeAddressSelectList = addresses
                    .ToSelectList(a => a.Id.ToString(), t => t.Address1 + " - " + t.City, "BailBond")
                    .GroupBy(g => g.Value).Select(h => h.First())
                    .ToList();
            activityNoteVm.AddressList.AddRange(bookingChargeAddressSelectList);
            addresses.Clear();

            var subjectEmployers = _employerService.GetBySubjectId(subject.Id);
            foreach(IAddress employer in subjectEmployers)
            {
                addresses.Add(employer);
            }
            var employerAddressSelectList = addresses
                                            .ToSelectList(a => a.Id.ToString(), t => t.Address1 + " - " + t.City, "Employer")
                                            .GroupBy(g => g.Value).Select(h => h.First())
                                            .ToList();
            activityNoteVm.AddressList.AddRange(employerAddressSelectList);
            addresses.Clear();

            activityNoteVm.SubjectList = subjects
                                        .ToSelectList(s => s.Id.ToString(), s => s.Lastname + ", " + s.Firstname)
                                        .GroupBy(g=>g.Value).Select(h=>h.First())
                                        .ToList();
            activityNoteVm.SubjectList.Insert(0, new SelectListItem { Text = "New", Value = "0", Selected = true });
            activityNoteVm.SubjectList.Insert(0, new SelectListItem {Text="None", Value = "-1", Selected = true});

            //activityNoteVm.AddressList = addresses
            //                            .ToSelectList(a => a.Id.ToString(), t => t.Address1 + " - " + t.City)
            //                            .GroupBy(g => g.Value).Select(h => h.First())
            //                            .ToList();
            activityNoteVm.AddressList.Insert(0, new SelectListItem { Text = "None", Value = "0", Selected = true });


            activityNoteVm.GenderList = _genderService.GetAll().ToSelectList(s => s.Id.ToString(), s => s.Name).ToList();
            activityNoteVm.RaceList = _raceService.GetAll().ToSelectList(s => s.Id.ToString(), s => s.Name).ToList();

        }



    }
}
