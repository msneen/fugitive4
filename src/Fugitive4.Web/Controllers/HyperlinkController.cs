using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.Hyperlink;
using Microsoft.AspNet.Authorization;

namespace Fugitive4.Web.Controllers
{
    [Authorize(Roles = "Member,Admin")]
    public class HyperlinkController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly IHyperlinkService _hyperlinkService;					
		private readonly ISubjectService _subjectService;

		public HyperlinkController(IHyperlinkService hyperlinkService,
								ISubjectService subjectService
,
								IMapper mapper		)
        {
            _hyperlinkService = hyperlinkService;
			_mapper = mapper;
			_subjectService = subjectService;

        }
        [Route("Subject/{subjectId}/Hyperlink/Index")]
        public async Task<IActionResult> Index(int subjectId , string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";

            var hyperlinks = await _hyperlinkService.GetAllAsync();
            var hyperlinkList = new HyperlinkListVm
            {
                Hyperlinks = _mapper.Map<IEnumerable<Hyperlink>, IEnumerable<HyperlinkVm>>(hyperlinks.Where(s=>s.SubjectId==subjectId).ToList())
            };

            return View("Index", hyperlinkList);
        }

		public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            Hyperlink hyperlink = await _hyperlinkService.GetByIdAsync(id.Value);
            if (hyperlink == null)
            {
                return HttpNotFound();
            }

            var hyperlinkVm = _mapper.Map<Hyperlink, HyperlinkVm>(hyperlink);

            return View(hyperlinkVm);
        }


        [Route("Subject/{subjectId}/Hyperlink/Create", Name = "HyperlinksForSubject")]
		public ActionResult Create(int subjectId, string returnUrl = "")
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";
            ViewBag.ReturnUrl = returnUrl;

            var hyperlinkVm = new HyperlinkVm { SubjectId = subjectId };
            

			LoadLookups(hyperlinkVm);

            return View(hyperlinkVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Subject/{subjectId}/Hyperlink/Create", Name = "HyperlinksForSubject")]
        public async Task<IActionResult> Create(HyperlinkVm hyperlinkVm, string returnUrl = "")
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var hyperlink = _mapper.Map<HyperlinkVm, Hyperlink>(hyperlinkVm);
                    await _hyperlinkService.InsertAsync(hyperlink, GetLogonUser());
                    return RedirectToIndexOrSubjectDetail(returnUrl); //RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(hyperlinkVm);

                return View(hyperlinkVm);
            }
        }

        [Route("Subject/{subjectId}/Hyperlink/Edit/{id}", Name = "HyperlinksForSubjectEdit")]
		public async Task<IActionResult> Edit(int subjectId , int? id, string returnUrl = "")
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";
            ViewBag.ReturnUrl = returnUrl;

            if (id == null)
            {
                return HttpNotFound();
            }

            var hyperlink = await _hyperlinkService.GetByIdAsync(id.Value);
            if (hyperlink == null)
            {
                return HttpNotFound();
            }            
			
			var hyperlinkVm = _mapper.Map<Hyperlink, HyperlinkVm>(hyperlink);

			LoadLookups(hyperlinkVm);

            return View(hyperlinkVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
[Route("Subject/{subjectId}/Hyperlink/Edit/{id}", Name = "HyperlinksForSubjectEdit")]
        public async Task<IActionResult> Edit(HyperlinkVm hyperlinkVm, string returnUrl = "")
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var hyperlink = _mapper.Map<HyperlinkVm, Hyperlink>(hyperlinkVm); 
                    await _hyperlinkService.UpdateAsync(hyperlink, GetLogonUser());
                    return RedirectToIndexOrSubjectDetail(returnUrl);
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(hyperlinkVm);

                return View(hyperlinkVm);
            }
        }

		[ActionName("Delete")]
[Route("Subject/{subjectId}/Hyperlink/Delete/{id}")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var hyperlink = await _hyperlinkService.GetByIdAsync(id.Value);
			if (hyperlink == null)
            {
                return HttpNotFound();
            }

            var hyperlinkVm = _mapper.Map<Hyperlink, HyperlinkVm>(hyperlink);

            return View(hyperlinkVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
[Route("Subject/{subjectId}/Hyperlink/Delete/{id}")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _hyperlinkService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(HyperlinkVm hyperlinkVm)
        {
			hyperlinkVm.SubjectList = _subjectService.GetAll(GetLogonUser()).ToSelectList(s => s.Id.ToString(), s => s.EyeColor).ToList();
		}

	}
}
