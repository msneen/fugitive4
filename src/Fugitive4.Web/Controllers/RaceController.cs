using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.Race;
using Microsoft.AspNet.Authorization;

namespace Fugitive4.Web.Controllers
{
    [Authorize(Roles = "Member,Admin")]
    public class RaceController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly IRaceService _raceService;

		public RaceController(IRaceService raceService,
								IMapper mapper		)
        {
            _raceService = raceService;
			_mapper = mapper;

        }
        public async Task<IActionResult> Index( string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";

            var races = await _raceService.GetAllAsync();
            var raceList = new RaceListVm
            {
                Races = _mapper.Map<IEnumerable<Race>, IEnumerable<RaceVm>>(races.ToList())
            };

            return View("Index", raceList);
        }

		public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            Race race = await _raceService.GetByIdAsync(id.Value);
            if (race == null)
            {
                return HttpNotFound();
            }

            var raceVm = _mapper.Map<Race, RaceVm>(race);

            return View(raceVm);
        }


		public ActionResult Create()
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";

            var raceVm = new RaceVm();

			LoadLookups(raceVm);

            return View(raceVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(RaceVm raceVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var race = _mapper.Map<RaceVm, Race>(raceVm);
                    await _raceService.InsertAsync(race, GetLogonUser());
					return RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(raceVm);

                return View(raceVm);
            }
        }

		public async Task<IActionResult> Edit( int? id)
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";

			if (id == null)
            {
                return HttpNotFound();
            }

            var race = await _raceService.GetByIdAsync(id.Value);
            if (race == null)
            {
                return HttpNotFound();
            }            
			
			var raceVm = _mapper.Map<Race, RaceVm>(race);

			LoadLookups(raceVm);

            return View(raceVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(RaceVm raceVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var race = _mapper.Map<RaceVm, Race>(raceVm); 
                    await _raceService.UpdateAsync(race, GetLogonUser());
					return RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(raceVm);

                return View(raceVm);
            }
        }

		[ActionName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var race = await _raceService.GetByIdAsync(id.Value);
			if (race == null)
            {
                return HttpNotFound();
            }

            var raceVm = _mapper.Map<Race, RaceVm>(race);

            return View(raceVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _raceService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(RaceVm raceVm)
        {
		}

	}
}
