using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.AKA;
using Microsoft.AspNet.Authorization;

namespace Fugitive4.Web.Controllers
{
    [Authorize(Roles = "Member,Admin")]
    public class AKAController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly IAKAService _aKAService;					
		private readonly ISubjectService _subjectService;

		public AKAController(IAKAService aKAService,
								ISubjectService subjectService
,
								IMapper mapper		)
        {
            _aKAService = aKAService;
			_mapper = mapper;
			_subjectService = subjectService;

        }
		[Route("Subject/{subjectId}/AKA/Index")]
        public async Task<IActionResult> Index(int subjectId , string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";

            var aKAS = await _aKAService.GetAllAsync();
            var aKAList = new AKAListVm
            {
                AKAS = _mapper.Map<IEnumerable<AKA>, IEnumerable<AKAVm>>(aKAS.ToList())
            };

            return View("Index", aKAList);
        }

		public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            AKA aKA = await _aKAService.GetByIdAsync(id.Value);
            if (aKA == null)
            {
                return HttpNotFound();
            }

            var aKAVm = _mapper.Map<AKA, AKAVm>(aKA);

            return View(aKAVm);
        }


		[Route("Subject/{subjectId}/AKA/Create", Name="AKAForSubject")]
		public ActionResult Create(int subjectId , string returnUrl = "")
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";
			ViewBag.ReturnUrl = returnUrl;

            var aKAVm = new AKAVm();

			LoadLookups(aKAVm);

            return View(aKAVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
		[Route("Subject/{subjectId}/AKA/Create", Name = "AKAForSubject")]
        public async Task<IActionResult> Create(AKAVm aKAVm, string returnUrl = "")
        {
					ViewBag.ReturnUrl = returnUrl;
            try
            {
                if (ModelState.IsValid)
                {
                    var aKA = _mapper.Map<AKAVm, AKA>(aKAVm);
                    await _aKAService.InsertAsync(aKA, GetLogonUser());
					return RedirectToIndexOrSubjectDetail(returnUrl);
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(aKAVm);

                return View(aKAVm);
            }
        }

		[Route("Subject/{subjectId}/AKA/Edit/{id}", Name = "AKAForSubjectEdit")]
		public async Task<IActionResult> Edit(int subjectId , int? id, string returnUrl = "")
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";
			ViewBag.ReturnUrl = returnUrl;

			if (id == null)
            {
                return HttpNotFound();
            }

            var aKA = await _aKAService.GetByIdAsync(id.Value);
            if (aKA == null)
            {
                return HttpNotFound();
            }            
			
			var aKAVm = _mapper.Map<AKA, AKAVm>(aKA);

			LoadLookups(aKAVm);

            return View(aKAVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
		[Route("Subject/{subjectId}/AKA/Edit/{id}", Name = "AKAForSubjectEdit")]
        public async Task<IActionResult> Edit(AKAVm aKAVm, string returnUrl = "")
        {
			ViewBag.ReturnUrl = returnUrl;
            try
            {
                if (ModelState.IsValid)
                {
                    var aKA = _mapper.Map<AKAVm, AKA>(aKAVm); 
                    await _aKAService.UpdateAsync(aKA, GetLogonUser());
					return RedirectToIndexOrSubjectDetail(returnUrl);
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(aKAVm);

                return View(aKAVm);
            }
        }

		[ActionName("Delete")]
		[Route("Subject/{subjectId}/AKA/Delete/{id}")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var aKA = await _aKAService.GetByIdAsync(id.Value);
			if (aKA == null)
            {
                return HttpNotFound();
            }

            var aKAVm = _mapper.Map<AKA, AKAVm>(aKA);

            return View(aKAVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
		[Route("Subject/{subjectId}/AKA/Delete/{id}")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _aKAService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(AKAVm aKAVm)
        {
			aKAVm.SubjectList = _subjectService.GetAll(GetLogonUser()).ToSelectList(s => s.Id.ToString(), s => s.EyeColor).ToList();
		}

	}
}
