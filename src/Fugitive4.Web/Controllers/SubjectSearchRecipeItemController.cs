using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.SubjectSearchRecipeItem;
using Microsoft.AspNet.Authorization;

namespace Fugitive4.Web.Controllers
{
    [Authorize(Roles = "Member,Admin")]
    public class SubjectSearchRecipeItemController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly ISubjectSearchRecipeItemService _subjectSearchRecipeItemService;					
		private readonly ISearchRecipeItemService _searchRecipeItemService;					
		private readonly ISubjectService _subjectService;

		public SubjectSearchRecipeItemController(ISubjectSearchRecipeItemService subjectSearchRecipeItemService,
								ISearchRecipeItemService searchRecipeItemService,
								ISubjectService subjectService
,
								IMapper mapper		)
        {
            _subjectSearchRecipeItemService = subjectSearchRecipeItemService;
			_mapper = mapper;
			_searchRecipeItemService = searchRecipeItemService;
			_subjectService = subjectService;

        }

        [Route("Subject/{subjectId}/SubjectSearchRecipeItem/Index")]
        public async Task<IActionResult> Index(int subjectId , string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";

            var subjectSearchRecipeItems = (await _subjectSearchRecipeItemService.GetAllAsync()).Where(s=>s.SubjectId == subjectId)
                .OrderBy(s=>s.SearchRecipeItem.SearchRecipe.Name).ThenBy(t=>t.SearchRecipeItem.SortOrder);


            var subjectSearchRecipeItemList = new SubjectSearchRecipeItemListVm
            {
                SubjectSearchRecipeItems = _mapper.Map<IEnumerable<SubjectSearchRecipeItem>, IEnumerable<SubjectSearchRecipeItemVm>>(subjectSearchRecipeItems.ToList())
            };

            return View("Index", subjectSearchRecipeItemList);
        }

		public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            SubjectSearchRecipeItem subjectSearchRecipeItem = await _subjectSearchRecipeItemService.GetByIdAsync(id.Value);
            if (subjectSearchRecipeItem == null)
            {
                return HttpNotFound();
            }

            var subjectSearchRecipeItemVm = _mapper.Map<SubjectSearchRecipeItem, SubjectSearchRecipeItemVm>(subjectSearchRecipeItem);

            return View(subjectSearchRecipeItemVm);
        }


		[Route("Subject/{subjectId}/{searchRecipeId}/SubjectSearchRecipeItem/Create", Name = "SubjectSearchRecipeItemForSubject")]
		public ActionResult Create(int subjectId, int? searchRecipeId = null, string returnUrl = "")
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";
			ViewBag.ReturnUrl = returnUrl;

            var subjectSearchRecipeItemVm = new SubjectSearchRecipeItemVm
            {
                SubjectId = subjectId,
                //SearchRecipeId = searchRecipeId.HasValue ? searchRecipeId.Value : 0
            };

			LoadLookups(subjectSearchRecipeItemVm);

            return View(subjectSearchRecipeItemVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
		[Route("Subject/{subjectId}/SubjectSearchRecipeItem/Create")]
        public async Task<IActionResult> Create(SubjectSearchRecipeItemVm subjectSearchRecipeItemVm, string returnUrl = "")
        {
					ViewBag.ReturnUrl = returnUrl;
            try
            {
                if (ModelState.IsValid)
                {
                    var subjectSearchRecipeItem = _mapper.Map<SubjectSearchRecipeItemVm, SubjectSearchRecipeItem>(subjectSearchRecipeItemVm);
                    await _subjectSearchRecipeItemService.InsertAsync(subjectSearchRecipeItem, GetLogonUser());
					return RedirectToIndexOrSubjectDetail(returnUrl);
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(subjectSearchRecipeItemVm);

                return View(subjectSearchRecipeItemVm);
            }
        }

		[Route("Subject/{subjectId}/SubjectSearchRecipeItem/Edit/{id}")]
		public async Task<IActionResult> Edit(int subjectId , int? id, string returnUrl = "")
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";
			ViewBag.ReturnUrl = returnUrl;

			if (id == null)
            {
                return HttpNotFound();
            }

            var subjectSearchRecipeItem = await _subjectSearchRecipeItemService.GetByIdAsync(id.Value);
            if (subjectSearchRecipeItem == null)
            {
                return HttpNotFound();
            }            
			
			var subjectSearchRecipeItemVm = _mapper.Map<SubjectSearchRecipeItem, SubjectSearchRecipeItemVm>(subjectSearchRecipeItem);

			LoadLookups(subjectSearchRecipeItemVm);

            return View(subjectSearchRecipeItemVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
		[Route("Subject/{searchRecipeItemId}/SubjectSearchRecipeItem/Edit/{id}")]
        public async Task<IActionResult> Edit(SubjectSearchRecipeItemVm subjectSearchRecipeItemVm, string returnUrl = "")
        {
			ViewBag.ReturnUrl = returnUrl;
            try
            {
                if (ModelState.IsValid)
                {
                    var subjectSearchRecipeItem = _mapper.Map<SubjectSearchRecipeItemVm, SubjectSearchRecipeItem>(subjectSearchRecipeItemVm); 
                    await _subjectSearchRecipeItemService.UpdateAsync(subjectSearchRecipeItem, GetLogonUser());
					return RedirectToIndexOrSubjectDetail(returnUrl);
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(subjectSearchRecipeItemVm);

                return View(subjectSearchRecipeItemVm);
            }
        }

		[ActionName("Delete")]
		[Route("Subject/{searchRecipeItemId}/SubjectSearchRecipeItem/Delete/{id}")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var subjectSearchRecipeItem = await _subjectSearchRecipeItemService.GetByIdAsync(id.Value);
			if (subjectSearchRecipeItem == null)
            {
                return HttpNotFound();
            }

            var subjectSearchRecipeItemVm = _mapper.Map<SubjectSearchRecipeItem, SubjectSearchRecipeItemVm>(subjectSearchRecipeItem);

            return View(subjectSearchRecipeItemVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
		[Route("Subject/{searchRecipeItemId}/SubjectSearchRecipeItem/Delete/{id}")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _subjectSearchRecipeItemService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(SubjectSearchRecipeItemVm subjectSearchRecipeItemVm)
        {
			subjectSearchRecipeItemVm.SearchRecipeItemList = _searchRecipeItemService.GetAll().ToSelectList(s => s.Id.ToString(), s => s.Name).ToList();
			subjectSearchRecipeItemVm.SubjectList = _subjectService.GetAll(GetLogonUser()).ToSelectList(s => s.Id.ToString(), s => s.EyeColor).ToList();
		}

	}
}
