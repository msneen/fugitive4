using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.SubjectSearchRecipe;
using Microsoft.AspNet.Authorization;

namespace Fugitive4.Web.Controllers
{
    [Authorize(Roles = "Member,Admin")]
    public class SubjectSearchRecipeController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly ISubjectSearchRecipeService _subjectSearchRecipeService;					
		private readonly ISearchRecipeService _searchRecipeService;
	    private readonly ISubjectSearchRecipeItemService _subjectSearchRecipeItemService;
	    private readonly ISearchRecipeItemService _searchRecipeItemService;
	    private readonly ISubjectService _subjectService;

		public SubjectSearchRecipeController(ISubjectSearchRecipeService subjectSearchRecipeService,
								             ISearchRecipeService searchRecipeService,
                                             ISubjectSearchRecipeItemService subjectSearchRecipeItemService,
                                             ISearchRecipeItemService searchRecipeItemService,
                                             ISubjectService subjectService,
								             IMapper mapper		)
        {
            _subjectSearchRecipeService = subjectSearchRecipeService;
			_mapper = mapper;
			_searchRecipeService = searchRecipeService;
		    _subjectSearchRecipeItemService = subjectSearchRecipeItemService;
		    _searchRecipeItemService = searchRecipeItemService;
		    _subjectService = subjectService;

        }
		[Route("SearchRecipe/{subjectId}/SubjectSearchRecipe/Index")]
        public async Task<IActionResult> Index(int subjectId , string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";

            var subjectSearchRecipes = (await _subjectSearchRecipeService.GetAllAsync()).Where(s=>s.SubjectId ==subjectId);
            var subjectSearchRecipeList = new SubjectSearchRecipeListVm
            {
                SubjectSearchRecipes = _mapper.Map<IEnumerable<SubjectSearchRecipe>, IEnumerable<SubjectSearchRecipeVm>>(subjectSearchRecipes.ToList())
            };

            return View("Index", subjectSearchRecipeList);
        }

		public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            SubjectSearchRecipe subjectSearchRecipe = await _subjectSearchRecipeService.GetByIdAsync(id.Value);
            if (subjectSearchRecipe == null)
            {
                return HttpNotFound();
            }

            var subjectSearchRecipeVm = _mapper.Map<SubjectSearchRecipe, SubjectSearchRecipeVm>(subjectSearchRecipe);

            return View(subjectSearchRecipeVm);
        }


		[Route("SearchRecipe/{subjectId}/SubjectSearchRecipe/Create", Name= "SubjectSearchRecipeForSubject")]
		public ActionResult Create(int subjectId , string returnUrl = "")
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";
			ViewBag.ReturnUrl = returnUrl;

            var subjectSearchRecipeVm = new SubjectSearchRecipeVm
            {
                SubjectId =  subjectId
            };

			LoadLookups(subjectSearchRecipeVm);

            return View(subjectSearchRecipeVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
		[Route("SearchRecipe/{subjectId}/SubjectSearchRecipe/Create", Name = "SubjectSearchRecipeForSubject")]
        public async Task<IActionResult> Create(SubjectSearchRecipeVm subjectSearchRecipeVm, string returnUrl = "")
        {
					ViewBag.ReturnUrl = returnUrl;
            try
            {
                if (ModelState.IsValid)
                {
                    var subjectSearchRecipe = _mapper.Map<SubjectSearchRecipeVm, SubjectSearchRecipe>(subjectSearchRecipeVm);
                    await _subjectSearchRecipeService.InsertAsync(subjectSearchRecipe, GetLogonUser());

                    //Create the SubjectSearchRecipeItem Children
                    var searchRecipeId = subjectSearchRecipe.SearchRecipeId;
                    var SearchRecipeItems = _searchRecipeItemService.GetBySearchRecipeId(searchRecipeId);
                    foreach(var searchRecipeItem in SearchRecipeItems)
                    {
                        var subjectSearchRecipeItem = new SubjectSearchRecipeItem
                        {
                            SubjectId = subjectSearchRecipe.SubjectId,
                            SearchRecipeItemId = searchRecipeItem.Id
                        };
                        await _subjectSearchRecipeItemService.InsertAsync(subjectSearchRecipeItem, GetLogonUser());
                    }



                    return RedirectToIndexOrSubjectDetail(returnUrl);
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(subjectSearchRecipeVm);

                return View(subjectSearchRecipeVm);
            }
        }

		[Route("SearchRecipe/{subjectId}/SubjectSearchRecipe/Edit/{id}")]
		public async Task<IActionResult> Edit(int subjectId , int? id, string returnUrl = "")
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";
			ViewBag.ReturnUrl = returnUrl;

			if (id == null)
            {
                return HttpNotFound();
            }

            var subjectSearchRecipe = await _subjectSearchRecipeService.GetByIdAsync(id.Value);
            if (subjectSearchRecipe == null)
            {
                return HttpNotFound();
            }            
			
			var subjectSearchRecipeVm = _mapper.Map<SubjectSearchRecipe, SubjectSearchRecipeVm>(subjectSearchRecipe);

			LoadLookups(subjectSearchRecipeVm);

            return View(subjectSearchRecipeVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
		[Route("SearchRecipe/{subjectId}/SubjectSearchRecipe/Edit/{id}")]
        public async Task<IActionResult> Edit(SubjectSearchRecipeVm subjectSearchRecipeVm, string returnUrl = "")
        {
			ViewBag.ReturnUrl = returnUrl;
            try
            {
                if (ModelState.IsValid)
                {
                    var subjectSearchRecipe = _mapper.Map<SubjectSearchRecipeVm, SubjectSearchRecipe>(subjectSearchRecipeVm); 
                    await _subjectSearchRecipeService.UpdateAsync(subjectSearchRecipe, GetLogonUser());
					return RedirectToIndexOrSubjectDetail(returnUrl);
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(subjectSearchRecipeVm);

                return View(subjectSearchRecipeVm);
            }
        }

		[ActionName("Delete")]
		[Route("SearchRecipe/{searchRecipeId}/SubjectSearchRecipe/Delete/{id}")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var subjectSearchRecipe = await _subjectSearchRecipeService.GetByIdAsync(id.Value);
			if (subjectSearchRecipe == null)
            {
                return HttpNotFound();
            }

            var subjectSearchRecipeVm = _mapper.Map<SubjectSearchRecipe, SubjectSearchRecipeVm>(subjectSearchRecipe);

            return View(subjectSearchRecipeVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
		[Route("SearchRecipe/{searchRecipeId}/SubjectSearchRecipe/Delete/{id}")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _subjectSearchRecipeService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(SubjectSearchRecipeVm subjectSearchRecipeVm)
        {
			subjectSearchRecipeVm.SearchRecipeList = _searchRecipeService.GetAll().ToSelectList(s => s.Id.ToString(), s => s.Name).ToList();
			subjectSearchRecipeVm.SubjectList = _subjectService.GetAll(GetLogonUser()).ToSelectList(s => s.Id.ToString(), s => s.EyeColor).ToList();
		}

	}
}
