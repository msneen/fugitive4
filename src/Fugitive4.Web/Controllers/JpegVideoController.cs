using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Http.Internal;
using Microsoft.AspNet.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System.Net.Http.Headers;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Fugitive4.Web.Helpers;
using Fugitive4.Web.ViewModels.JpegVideo;
using Microsoft.ApplicationInsights;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Server.Kestrel;
using Microsoft.AspNet.WebUtilities;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.PlatformAbstractions;
using Newtonsoft.Json;


namespace Fugitive4.Web.Controllers
{
    public class JpegVideoController : ControllerBase
    {
        private readonly IJpegCache _jpegCache;
        private readonly IMemoryCache _jpegVideoMemoryCache;
        private readonly IStoredFileService _storedFileService;
        private readonly IApplicationEnvironment _applicationEnvironment;
        private readonly IActivityNoteService _activityNoteService;
        //private Bitmap _lastBitmap;
        //private int _previousFrame = 0;
        private TelemetryClient _telemetry;


        public JpegVideoController( IJpegCache jpegCache,
                                    IMemoryCache jpegVideoMemoryCache,
                                    IStoredFileService storedFileService,
                                    IApplicationEnvironment applicationEnvironment,
                                    IActivityNoteService activityNoteService) // for testing only
        {
            _jpegCache = jpegCache;
            _jpegVideoMemoryCache = jpegVideoMemoryCache;
            _storedFileService = storedFileService;
            _applicationEnvironment = applicationEnvironment;
            _activityNoteService = activityNoteService;

            _telemetry = new Microsoft.ApplicationInsights.TelemetryClient();
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult CameraList()
        {
            return View(_jpegCache.GetActiveCameraList());
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id">Camera Name</param>
        /// <param name="increment"></param>
        /// <returns></returns>
        [HttpPost]
        public JsonResult AddFeed(string id, int frame = 0)
        {
            
            bool increment = frame !=0 ;
          
            try
            {
                var contentLength = HttpContext.Request.ContentLength;

                var previousFrame = GetPreviousFromCache(id);

                _telemetry.TrackTrace($"AddFeed Frame={frame} PreviousFrame={previousFrame} Increment={increment}");

                if (contentLength.GetValueOrDefault() > 0 && ( frame > previousFrame || frame == 0))
                {
                    
                    var bytes = ReadRequestBody(Request.Body);

                    bytes = SetOrUpdateLastBitmap(id, increment, bytes);

                    //_jpegVideoMemoryCache.Set(id, bytes);//This was my original Code
                    //SetPreviousInCache(id, frame);
                    _jpegCache.PutNextFrame(id, bytes, frame);
                }
                


            }
            catch (Exception ex)
            {
                _telemetry.TrackException(ex);
                RecordInfo(0, 0, ex.Message + ex.StackTrace); 
            }
            var jsonString = GetReturnJsonString(id);
            return new JsonResult(jsonString); 
        }


        [HttpGet]
        public async Task<ActionResult> GetFeed(string id)
        {
            //var image = new byte[20];
            //if (_jpegVideoMemoryCache.TryGetValue(id, out image))//This was my original Code
            //{
            //    return File(image, "image/jpeg", id + ".jpg");

            //}
            try
            {
                byte[] nextFrame = _jpegCache.GetNextFrame(id);
                if (nextFrame != null)
                {
                    return File(nextFrame, "image/jpeg", id + ".jpg");
                }
            }
            catch (Exception ex)
            {
                _telemetry.TrackException(ex);
            }
            return View();
        }

        [HttpGet]
        [Authorize(Roles = "Admin")]
        public ActionResult CameraProperties(string id)
        {
            var jpegVideoCameraProperties = GetJpegVideoCameraProperties(id);

            return View(jpegVideoCameraProperties);
        }

        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult CameraProperties(JpegVideoCameraProperties jpegVideoCameraProperties)
        {
            _jpegVideoMemoryCache.Set(jpegVideoCameraProperties.CameraId + "_Props", jpegVideoCameraProperties);
            return RedirectToAction("CameraProperties",  new {id = jpegVideoCameraProperties .CameraId});
        }

        private string GetReturnJsonString(string cameraId)
        {

            var settings = GetJpegVideoCameraProperties(cameraId);
            using (var stringWriter = new StringWriter())
            {
                using (var writer = new JsonTextWriter(stringWriter) { QuoteName = false })
                    new JsonSerializer().Serialize(writer, settings);
                var stringWithBackslashes = stringWriter.ToString();
                var stringWithoutBackslashes = stringWithBackslashes.Replace("\"", "").Replace("\\", "");
                return stringWithoutBackslashes;
            }

            //return jsonSettings;           
        }


        private byte[] SetOrUpdateLastBitmap(string id, bool increment, byte[] bytes)
        {
            Bitmap lastBitmap = GetLastBitmapFromCache(id);

            var lastBitmapDebugString = lastBitmap == null ? "null" : "populated";
            _telemetry.TrackTrace($"SetOrUpdateLastBitmap. Increment={increment}  LastBitmap={lastBitmapDebugString}");


            if (increment && lastBitmap != null)
            {
                try
                {
                    _telemetry.TrackTrace("Trying to update bitmap");
                    var diffsOnlyBitmap = FromBytes(bytes);
                    var bitmap = Difference(diffsOnlyBitmap, lastBitmap);
                    if (bitmap != null)
                    {
                        _jpegVideoMemoryCache.Set($"LastBitmap{id}", bitmap);
                        var compositBytes = FromBitmap(bitmap);
                        _telemetry.TrackTrace($"SetOrUpdateLastBitmap. Bitmap Saved. CompositeLength={compositBytes.Length}  OriginalLength = {bytes.Length}");
                        return compositBytes;
                       
                    }

                }
                catch (Exception ex)
                {
                    _telemetry.TrackException(ex);
                }

            }
            else if (!increment)
            {
                //this is a full image               
                var bitmap = FromBytes(bytes);
                _jpegVideoMemoryCache.Set($"LastBitmap{id}", bitmap);

                lastBitmapDebugString = bitmap == null ? "null" : "populated";
                _telemetry.TrackTrace($"updated full bitmap.  LastBitmap={lastBitmapDebugString}");
            }
            else
            {
                _telemetry.TrackTrace("This was an Incremental Map, but _lastBitmap is null");
            }
            return bytes;
        }



        public static byte[] ReadRequestBody(Stream input)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                input.CopyTo(ms);
                return ms.ToArray();
            }
        }

        private void RecordInfo(long fileCount, int fileLength, string error)
        {
            //var fileLength = fileCount > 0 ? files.FirstOrDefault().Length : 0;
            var activityLog = new ActivityNote
            {
                Description = $"count = {fileCount}, length = {fileLength}, {error}",
                SubjectId = 22
            };
            _activityNoteService.Update(activityLog, "Mike");
            ViewBag.FileCount = fileCount;
        }

        private JpegVideoCameraProperties GetJpegVideoCameraProperties(string id)
        {
            var jpegVideoCameraProperties = GetInitializedJpegVideoCameraProperties(id);

            if (!_jpegVideoMemoryCache.TryGetValue(id + "_Props", out jpegVideoCameraProperties))
            {
                jpegVideoCameraProperties = GetInitializedJpegVideoCameraProperties(id);
            }
            return jpegVideoCameraProperties;
        }

        private JpegVideoCameraProperties GetInitializedJpegVideoCameraProperties(string cameraId)
        {
            var jpegVideoCameraProperties = new JpegVideoCameraProperties
            {
                CameraId = cameraId,
                preview = false,
                differenceThreshold = 6,
                pixelThreshold = 50,
                transmitSeconds = 5,
                focus = 0,
                zoom = 0,
                zoomXOffset = 0,
                zoomYOffset = 0,
                maxTemp = 150,
                minBatteryPercent = 15,
                afterDarkHour = 20,
                beforeSunriseHour = 6,
                quality = 20,
                movement = new Coordinates
                {
                    x = 0,
                    y = 0,
                    w = 1280,
                    h = 720
                },
                view = new Coordinates
                {
                    x = 0,
                    y = 0,
                    w = 1280,
                    h = 720
                }
            };
            return jpegVideoCameraProperties;
            
        }

        private Bitmap FromBytes(byte[] byteBitmap)
        {
            try
            {
                using (var ms = new MemoryStream(byteBitmap))
                {

                    //JpegBitmapDecoder decoder = new JpegBitmapDecoder(ms, BitmapCreateOptions.PreservePixelFormat, BitmapCacheOption.Default);
                    //BitmapSource bitmapSource = decoder.Frames[0];

                    //int width = bitmapSource.PixelWidth;
                    //int height = bitmapSource.PixelHeight;
                    //int stride = width * ((bitmapSource.Format.BitsPerPixel + 7) / 8);
                    //IntPtr ptr = IntPtr.Zero;
                    //try
                    //{
                    //    ptr = Marshal.AllocHGlobal(height * stride);
                    //    bitmapSource.CopyPixels(new Int32Rect(0, 0, width, height), ptr, height * stride, stride);
                    //    using (var btm = new System.Drawing.Bitmap(width, height, stride, System.Drawing.Imaging.PixelFormat.Alpha, ptr))
                    //    {
                    //        // Clone the bitmap so that we can dispose it and
                    //        // release the unmanaged memory at ptr
                    //        return new System.Drawing.Bitmap(btm);
                    //    }
                    //}
                    //finally
                    //{
                    //    if (ptr != IntPtr.Zero)
                    //        Marshal.FreeHGlobal(ptr);
                    //}

                    var image = Image.FromStream(ms);
                    var bmp = new Bitmap(image);

                    _telemetry.TrackTrace($"Array Converted to Bitmap {bmp.Width} x {bmp.Height}");

                    return bmp;
                }
            }
            catch (Exception ex)
            {
                _telemetry.TrackException(ex);
            }
            return null;
        }

        private byte[] FromBitmap(Bitmap bitmap)
        {
            try
            {
                using (Image newImage = new Bitmap(bitmap))
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        newImage.Save(ms, ImageFormat.Png);
                        var result = ms.ToArray();
                        return result;
                    }
                }
            }
            catch (Exception ex)
            {
                _telemetry.TrackException(ex);
            }
            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="diffsOnlyBitmap">The new bitmap</param>
        /// <param name="originalBitmap">The Original Bitmap</param>
        /// <param name="restore"></param>
        /// <returns></returns>
        public Bitmap Difference(Bitmap diffsOnlyBitmap, Bitmap originalBitmap)
        {
            try
            {
                int Bpp = 32;  // assuming an effective pixelformat of 32bpp
                var diffsOnlyData = diffsOnlyBitmap.LockBits(
                                new Rectangle(0, 0, diffsOnlyBitmap.Width, diffsOnlyBitmap.Height),
                                ImageLockMode.ReadWrite, diffsOnlyBitmap.PixelFormat);
                var originalData = originalBitmap.LockBits(
                                new Rectangle(0, 0, originalBitmap.Width, originalBitmap.Height),
                                ImageLockMode.ReadOnly, originalBitmap.PixelFormat);
               

                int len = diffsOnlyData.Height * diffsOnlyData.Stride;
                byte[] data0 = new byte[len];
                byte[] originalBytes = new byte[len];
                Marshal.Copy(diffsOnlyData.Scan0, data0, 0, len);
                Marshal.Copy(originalData.Scan0, originalBytes, 0, len);

                int diffCount = 0;
                for (int i = 0; i < len; i += Bpp)
                {
                    bool toberestored = (originalBytes[i] != 2 && originalBytes[i + 1] != 3 &&
                                            originalBytes[i + 2] != 7 && originalBytes[i + 3] != 42);
                    if (toberestored)
                    {
                        for (int j = 0; j < 32; j++)
                        {
                            data0[i + j] = originalBytes[i + j];//bgrA (backwards) * 8 =32bpp
                        }
                    }
                    diffCount++;

                }

                Marshal.Copy(data0, 0, diffsOnlyData.Scan0, len);
                diffsOnlyBitmap.UnlockBits(diffsOnlyData);
                originalBitmap.UnlockBits(originalData);

                var properties = new Dictionary<string, string>
                {
                    {"OriginalPixelFormat", originalBitmap.PixelFormat.ToString() },
                    {"DiffPixelFormat",  diffsOnlyBitmap.PixelFormat.ToString()},
                    {"DifferencesFound", diffCount.ToString()}

                };
                _telemetry.TrackTrace("JpegVideo:Difference", properties);

                return diffsOnlyBitmap;
            }
            catch (Exception ex)
            {
                _telemetry.TrackException(ex);
            }
            return null;
        }

        private Bitmap GetLastBitmapFromCache(string id)
        {
            //Get the last bitmap from the cache
            Bitmap bitmap;
            if (_jpegVideoMemoryCache.TryGetValue($"LastBitmap{id}", out bitmap))
            {
                return bitmap;
            }
            return null;
        }

        private int GetPreviousFromCache(string id)
        {
            //Get the last bitmap from the cache
            int previousFrame = -1;
            if (_jpegVideoMemoryCache.TryGetValue($"PreviousFrame{id}", out previousFrame))
            {
                return previousFrame;
            }
            return -1;
        }

        private void SetPreviousInCache(string id, int frame)
        {
            _jpegVideoMemoryCache.Set($"PreviousFrame{id}", frame);
        }

    }
}