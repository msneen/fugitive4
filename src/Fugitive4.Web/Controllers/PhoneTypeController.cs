using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.PhoneType;
using Microsoft.AspNet.Authorization;

namespace Fugitive4.Web.Controllers
{
    [Authorize(Roles = "Member,Admin")]
    public class PhoneTypeController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly IPhoneTypeService _phoneTypeService;

		public PhoneTypeController(IPhoneTypeService phoneTypeService,
								IMapper mapper		)
        {
            _phoneTypeService = phoneTypeService;
			_mapper = mapper;

        }
        public async Task<IActionResult> Index( string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";

            var phoneTypes = await _phoneTypeService.GetAllAsync();
            var phoneTypeList = new PhoneTypeListVm
            {
                PhoneTypes = _mapper.Map<IEnumerable<PhoneType>, IEnumerable<PhoneTypeVm>>(phoneTypes.ToList())
            };

            return View("Index", phoneTypeList);
        }

		public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            PhoneType phoneType = await _phoneTypeService.GetByIdAsync(id.Value);
            if (phoneType == null)
            {
                return HttpNotFound();
            }

            var phoneTypeVm = _mapper.Map<PhoneType, PhoneTypeVm>(phoneType);

            return View(phoneTypeVm);
        }


		public ActionResult Create()
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";

            var phoneTypeVm = new PhoneTypeVm();

			LoadLookups(phoneTypeVm);

            return View(phoneTypeVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(PhoneTypeVm phoneTypeVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var phoneType = _mapper.Map<PhoneTypeVm, PhoneType>(phoneTypeVm);
                    await _phoneTypeService.InsertAsync(phoneType, GetLogonUser());
					return RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(phoneTypeVm);

                return View(phoneTypeVm);
            }
        }

		public async Task<IActionResult> Edit( int? id)
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";

			if (id == null)
            {
                return HttpNotFound();
            }

            var phoneType = await _phoneTypeService.GetByIdAsync(id.Value);
            if (phoneType == null)
            {
                return HttpNotFound();
            }            
			
			var phoneTypeVm = _mapper.Map<PhoneType, PhoneTypeVm>(phoneType);

			LoadLookups(phoneTypeVm);

            return View(phoneTypeVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(PhoneTypeVm phoneTypeVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var phoneType = _mapper.Map<PhoneTypeVm, PhoneType>(phoneTypeVm); 
                    await _phoneTypeService.UpdateAsync(phoneType, GetLogonUser());
					return RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(phoneTypeVm);

                return View(phoneTypeVm);
            }
        }

		[ActionName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var phoneType = await _phoneTypeService.GetByIdAsync(id.Value);
			if (phoneType == null)
            {
                return HttpNotFound();
            }

            var phoneTypeVm = _mapper.Map<PhoneType, PhoneTypeVm>(phoneType);

            return View(phoneTypeVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _phoneTypeService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(PhoneTypeVm phoneTypeVm)
        {
		}

	}
}
