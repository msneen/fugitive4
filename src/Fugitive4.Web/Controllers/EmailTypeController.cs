using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.EmailType;
using Microsoft.AspNet.Authorization;

namespace Fugitive4.Web.Controllers
{
    [Authorize(Roles = "Member,Admin")]
    public class EmailTypeController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly IEmailTypeService _emailTypeService;

		public EmailTypeController(IEmailTypeService emailTypeService,
								IMapper mapper		)
        {
            _emailTypeService = emailTypeService;
			_mapper = mapper;

        }
        public async Task<IActionResult> Index( string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";

            var emailTypes = await _emailTypeService.GetAllAsync();
            var emailTypeList = new EmailTypeListVm
            {
                EmailTypes = _mapper.Map<IEnumerable<EmailType>, IEnumerable<EmailTypeVm>>(emailTypes.ToList())
            };

            return View("Index", emailTypeList);
        }

		public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            EmailType emailType = await _emailTypeService.GetByIdAsync(id.Value);
            if (emailType == null)
            {
                return HttpNotFound();
            }

            var emailTypeVm = _mapper.Map<EmailType, EmailTypeVm>(emailType);

            return View(emailTypeVm);
        }


		public ActionResult Create()
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";

            var emailTypeVm = new EmailTypeVm();

			LoadLookups(emailTypeVm);

            return View(emailTypeVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(EmailTypeVm emailTypeVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var emailType = _mapper.Map<EmailTypeVm, EmailType>(emailTypeVm);
                    await _emailTypeService.InsertAsync(emailType, GetLogonUser());
					return RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(emailTypeVm);

                return View(emailTypeVm);
            }
        }

		public async Task<IActionResult> Edit( int? id)
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";

			if (id == null)
            {
                return HttpNotFound();
            }

            var emailType = await _emailTypeService.GetByIdAsync(id.Value);
            if (emailType == null)
            {
                return HttpNotFound();
            }            
			
			var emailTypeVm = _mapper.Map<EmailType, EmailTypeVm>(emailType);

			LoadLookups(emailTypeVm);

            return View(emailTypeVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(EmailTypeVm emailTypeVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var emailType = _mapper.Map<EmailTypeVm, EmailType>(emailTypeVm); 
                    await _emailTypeService.UpdateAsync(emailType, GetLogonUser());
					return RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(emailTypeVm);

                return View(emailTypeVm);
            }
        }

		[ActionName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var emailType = await _emailTypeService.GetByIdAsync(id.Value);
			if (emailType == null)
            {
                return HttpNotFound();
            }

            var emailTypeVm = _mapper.Map<EmailType, EmailTypeVm>(emailType);

            return View(emailTypeVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _emailTypeService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(EmailTypeVm emailTypeVm)
        {
		}

	}
}
