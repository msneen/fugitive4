using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.AspNetUser;
using Fugitive4.Web.ViewModels.Team;
using Microsoft.AspNet.Authorization;

namespace Fugitive4.Web.Controllers
{
	[Authorize(Policy = "ManageTeams")]
    public class TeamController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly ITeamService _teamService;
	    private readonly IAspNetUsers_TeamService _aspNetUsersTeamService;
	    private readonly IAspNetUserService _aspNetUserService;
	    private readonly ITeam_ReferrerService _teamReferrerService;
	    private readonly IReferrerService _referrerService;

	    public TeamController(ITeamService teamService,
                                IAspNetUsers_TeamService aspNetUsersTeamService,
                                IAspNetUserService aspNetUserService,
                                ITeam_ReferrerService teamReferrerService,
                                IReferrerService referrerService,
								IMapper mapper		)
        {
            _teamService = teamService;
	        _aspNetUsersTeamService = aspNetUsersTeamService;
	        _aspNetUserService = aspNetUserService;
	        _teamReferrerService = teamReferrerService;
	        _referrerService = referrerService;
	        _mapper = mapper;

        }
        public async Task<IActionResult> Index( string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";

            var teams = await _teamService.GetAllAsync();
            var teamList = new TeamListVm
            {
                Teams = _mapper.Map<IEnumerable<Team>, IEnumerable<TeamVm>>(teams.ToList())
            };

            return View("Index", teamList);
        }

		public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            Team team = await _teamService.GetByIdAsync(id.Value);
            if (team == null)
            {
                return HttpNotFound();
            }

            var teamVm = _mapper.Map<Team, TeamVm>(team);

            return View(teamVm);
        }


		public ActionResult Create( string returnUrl = "")
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";
			ViewBag.ReturnUrl = returnUrl;

            var teamVm = new TeamVm();

			LoadLookups(teamVm);
            LoadTeamUsers(teamVm);

            return View(teamVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(TeamVm teamVm, string returnUrl = "")
        {
			ViewBag.ReturnUrl = returnUrl;
            try
            {
                if (ModelState.IsValid)
                {
                    var team = _mapper.Map<TeamVm, Team>(teamVm);
                    await _teamService.InsertAsync(team, GetLogonUser());
                    SaveUsers(teamVm);
                    SaveReferrers(teamVm);
                    return RedirectToIndexOrSubjectDetail(returnUrl);
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(teamVm);
                LoadTeamUsers(teamVm);

                return View(teamVm);
            }
        }

		public async Task<IActionResult> Edit( int? id, string returnUrl = "")
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";
			ViewBag.ReturnUrl = returnUrl;

			if (id == null)
            {
                return HttpNotFound();
            }

            var team = await _teamService.GetByIdAsync(id.Value);
            if (team == null)
            {
                return HttpNotFound();
            }            
			
			var teamVm = _mapper.Map<Team, TeamVm>(team);

            LoadTeamUsers(teamVm);
			LoadLookups(teamVm);

            return View(teamVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(TeamVm teamVm, string returnUrl = "")
        {
			ViewBag.ReturnUrl = returnUrl;
            try
            {
                if (ModelState.IsValid)
                {
                    var team = _mapper.Map<TeamVm, Team>(teamVm); 
                    await _teamService.UpdateAsync(team, GetLogonUser());
                    SaveUsers(teamVm);
                    SaveReferrers(teamVm);
                    return RedirectToIndexOrSubjectDetail(returnUrl);
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(teamVm);
                LoadTeamUsers(teamVm);

                return View(teamVm);
            }
        }

        [ActionName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var team = await _teamService.GetByIdAsync(id.Value);
			if (team == null)
            {
                return HttpNotFound();
            }

            var teamVm = _mapper.Map<Team, TeamVm>(team);

            return View(teamVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _teamService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(TeamVm teamVm)
        {

            teamVm.UserList = _aspNetUserService.GetAll().ToSelectList(s => s.Id, s => s.UserName).ToList();
            teamVm.ReferrerList =_referrerService.GetAll().ToSelectList(s => s.Id.ToString(), s => s.Name).ToList();

        }

        private void LoadTeamUsers(TeamVm teamVm)
        {
            var teamUsers = _aspNetUsersTeamService.GetByTeamId(teamVm.Id).Select(i => i.ApplicationUserId).ToArray<string>();
            teamVm.UserIds = teamUsers;

            var teamReferrers = _teamReferrerService.GetByTeamId(teamVm.Id).Select(i => i.ReferrerId).ToArray<int>();
            teamVm.ReferrerIds = teamReferrers;
        }

        private void SaveUsers(TeamVm teamVm)
        {
            var teamId = teamVm.Id;
            var teamUsers = _aspNetUsersTeamService.GetByTeamId(teamId);

            foreach (var userId in teamVm.UserIds)
            {
                _aspNetUsersTeamService.SaveTeamUser(teamId, userId, GetLogonUser());
            }

            //Delete any users that weren't selected
            foreach(var teamUser in teamUsers)
            {
                if (!teamVm.UserIds.Contains(teamUser.ApplicationUserId))
                {
                    _aspNetUsersTeamService.Delete(teamUser.Id, GetLogonUser());
                }
            }
        }



	    //SAVE referrers
        private void SaveReferrers(TeamVm teamVm)
        {
            var teamReferrers = _teamReferrerService.GetByTeamId(teamVm.Id);
            foreach(var referrerId in teamVm.ReferrerIds)
            {
                var existingTeamReferrer = teamReferrers.FirstOrDefault(b => b.ReferrerId == referrerId);
                if(existingTeamReferrer == null)
                {
                    Team_Referrer teamReferrer = new Team_Referrer
                    {
                        ReferrerId = referrerId,
                        TeamId = teamVm.Id
                    };
                    _teamReferrerService.Insert(teamReferrer, GetLogonUser());
                }
            }
            //Delete any Referrers that weren't selected
            foreach (var teamReferrer in teamReferrers)
            {
                if (!teamVm.ReferrerIds.Contains(teamReferrer.ReferrerId))
                {
                    _teamReferrerService.Delete(teamReferrer.Id, GetLogonUser());
                }
            }
        }
    }
}
