using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.AspNetUsers_Referrer;
using Microsoft.AspNet.Authorization;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Fugitive4.Web.Controllers
{
	[Authorize(Roles = "Admin")]
    public class AspNetUsers_ReferrerController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly IAspNetUsers_ReferrerService _aspNetUsers_ReferrerService;					
		private readonly IAspNetUserService _aspNetUserService;					
		private readonly IReferrerService _referrerService;
	    private readonly IServiceProvider _serviceProvider;

	    public AspNetUsers_ReferrerController(IAspNetUsers_ReferrerService aspNetUsers_ReferrerService,
								IAspNetUserService aspNetUserService,
								IReferrerService referrerService,
                                IServiceProvider serviceProvider,
                                IMapper mapper		)
        {
            _aspNetUsers_ReferrerService = aspNetUsers_ReferrerService;
			_mapper = mapper;
			_aspNetUserService = aspNetUserService;
			_referrerService = referrerService;
	        _serviceProvider = serviceProvider;
        }
		//[Route("AspNetUser/{aspNetUserId}/AspNetUsers_Referrer/Index")]
        public async Task<IActionResult> Index(int? aspNetUserId = null , string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";
            

            var aspNetUsers_Referrers = await _aspNetUsers_ReferrerService.GetAllAsync();

            var aspNetUsers_ReferrerVms = MapToVm(aspNetUsers_Referrers);

            var aspNetUsers_ReferrerList = new AspNetUsers_ReferrerListVm
            {
                AspNetUsers_Referrers = aspNetUsers_ReferrerVms.OrderBy(o=>o.Referrer.Name).ThenBy(p=>p.ApplicationUser.UserName)  //_mapper.Map<IEnumerable<AspNetUsers_Referrer>, IEnumerable<AspNetUsers_ReferrerVm>>(aspNetUsers_Referrers.ToList())
            };

            return View("Index", aspNetUsers_ReferrerList);
        }


	    public async Task<IActionResult> Details(int? id)
        {
            return HttpNotFound();
            //ViewBag.TestValue = "Details";
            //if (id == null)
            //{
            //    return HttpNotFound();
            //}

            //AspNetUsers_Referrer aspNetUsers_Referrer = await _aspNetUsers_ReferrerService.GetByIdAsync(id.Value);
            //if (aspNetUsers_Referrer == null)
            //{
            //    return HttpNotFound();
            //}

            //var aspNetUsers_ReferrerVm = _mapper.Map<AspNetUsers_Referrer, AspNetUsers_ReferrerVm>(aspNetUsers_Referrer);

            //return View(aspNetUsers_ReferrerVm);
        }


		//[Route("AspNetUser/{aspNetUserId}/AspNetUsers_Referrer/Create", Name="AspNetUsers_ReferrerForAspNetUser")]
		public ActionResult Create(int? aspNetUserId = null, string returnUrl = "")
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";
			ViewBag.ReturnUrl = returnUrl;

            var aspNetUsers_ReferrerVm = new AspNetUsers_ReferrerVm();

			LoadLookups(aspNetUsers_ReferrerVm);

            return View(aspNetUsers_ReferrerVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
		//[Route("AspNetUser/{aspNetUserId}/AspNetUsers_Referrer/Create", Name="AspNetUsers_ReferrerForAspNetUser")]
        public async Task<IActionResult> Create(AspNetUsers_ReferrerVm aspNetUsers_ReferrerVm, string returnUrl = "")
        {
					ViewBag.ReturnUrl = returnUrl;
            try
            {
                if (ModelState.IsValid)
                {
                    //Manually Map the vm to the domain class

                    var aspNetUsers_Referrer = new AspNetUsers_Referrer
                    {
                        AspNetUsersId = aspNetUsers_ReferrerVm.AspNetUsersId,
                        ReferrerId = aspNetUsers_ReferrerVm.ReferrerId
                    };

                    await _aspNetUsers_ReferrerService.InsertAsync(aspNetUsers_Referrer, GetLogonUser());

                    await AddBondsmanRoleToUser(aspNetUsers_ReferrerVm);

                    return RedirectToIndexOrSubjectDetail(returnUrl);
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(aspNetUsers_ReferrerVm);

                return View(aspNetUsers_ReferrerVm);
            }
        }



	    [Route("AspNetUser/{aspNetUserId}/AspNetUsers_Referrer/Edit/{id}")]
		public async Task<IActionResult> Edit(int aspNetUserId , int? id, string returnUrl = "")
        {
            return HttpNotFound();
   //         ViewBag.Title = "Edit"; 
			//ViewBag.TestValue = "Edit";
			//ViewBag.ReturnUrl = returnUrl;

			//if (id == null)
   //         {
   //             return HttpNotFound();
   //         }

   //         var aspNetUsers_Referrer = await _aspNetUsers_ReferrerService.GetByIdAsync(id.Value);
   //         if (aspNetUsers_Referrer == null)
   //         {
   //             return HttpNotFound();
   //         }            
			
			//var aspNetUsers_ReferrerVm = _mapper.Map<AspNetUsers_Referrer, AspNetUsers_ReferrerVm>(aspNetUsers_Referrer);

			//LoadLookups(aspNetUsers_ReferrerVm);

   //         return View(aspNetUsers_ReferrerVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
		[Route("AspNetUser/{aspNetUserId}/AspNetUsers_Referrer/Edit/{id}")]
        public async Task<IActionResult> Edit(AspNetUsers_ReferrerVm aspNetUsers_ReferrerVm, string returnUrl = "")
        {
            return HttpNotFound();
    //        ViewBag.ReturnUrl = returnUrl;
    //        try
    //        {
    //            if (ModelState.IsValid)
    //            {
    //                var aspNetUsers_Referrer = _mapper.Map<AspNetUsers_ReferrerVm, AspNetUsers_Referrer>(aspNetUsers_ReferrerVm); 
    //                await _aspNetUsers_ReferrerService.UpdateAsync(aspNetUsers_Referrer, GetLogonUser());
				//	return RedirectToIndexOrSubjectDetail(returnUrl);
    //            }
				//else
				//{
    //                var message = string.Join(" | ", ModelState.Values
    //                    .SelectMany(v => v.Errors)
    //                    .Select(e => e.ErrorMessage));
    //                throw new Exception(message);
    //            }                 
    //        }
    //        catch(Exception ex)
    //        {
				//LogError(ex);
				////Add any dropdown values back to the VM Here
				//LoadLookups(aspNetUsers_ReferrerVm);

    //            return View(aspNetUsers_ReferrerVm);
    //        }
        }

		[ActionName("Delete")]
		//[Route("AspNetUser/{aspNetUserId}/AspNetUsers_Referrer/Delete/{id}")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var aspNetUsers_Referrer = await _aspNetUsers_ReferrerService.GetByIdAsync(id.Value);
			if (aspNetUsers_Referrer == null)
            {
                return HttpNotFound();
            }

            var aspNetUsers_ReferrerVm = _mapper.Map<AspNetUsers_Referrer, AspNetUsers_ReferrerVm>(aspNetUsers_Referrer);

            return View(aspNetUsers_ReferrerVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
		//[Route("AspNetUser/{aspNetUserId}/AspNetUsers_Referrer/Delete/{id}")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _aspNetUsers_ReferrerService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(AspNetUsers_ReferrerVm aspNetUsers_ReferrerVm)
        {
			aspNetUsers_ReferrerVm.AspNetUserList = _aspNetUserService.GetAll().ToSelectList(s => s.Id.ToString(), s => s.UserName).ToList();
			aspNetUsers_ReferrerVm.ReferrerList = _referrerService.GetAll().ToSelectList(s => s.Id.ToString(), s => s.Name).ToList();
		}

        private List<AspNetUsers_ReferrerVm> MapToVm(ICollection<AspNetUsers_Referrer> aspNetUsers_Referrers)
        {
            var aspNetUsers_ReferrerVms = new List<AspNetUsers_ReferrerVm>();
            foreach (var aspnetUserReferrer in aspNetUsers_Referrers.Where(ar => ar.ApplicationUser == null))
            {
                aspnetUserReferrer.ApplicationUser = _aspNetUserService.GetById(aspnetUserReferrer.AspNetUsersId);
                var aspNetUsers_ReferrerVm = new AspNetUsers_ReferrerVm
                {
                    AspNetUsersId = aspnetUserReferrer.AspNetUsersId,
                    ApplicationUser = new ViewModels.AspNetUser.ApplicationUserVm
                    {
                        Id = aspnetUserReferrer.ApplicationUser.Id,
                        UserName = aspnetUserReferrer.ApplicationUser.UserName
                    },
                    ReferrerId = aspnetUserReferrer.ReferrerId,
                    Referrer = new ViewModels.Referrer.ReferrerVm
                    {
                        Id = aspnetUserReferrer.Referrer.Id,
                        Name = aspnetUserReferrer.Referrer.Name
                    }
                };
                aspNetUsers_ReferrerVms.Add(aspNetUsers_ReferrerVm);
            }
            return aspNetUsers_ReferrerVms;
        }

        private async Task AddBondsmanRoleToUser(AspNetUsers_ReferrerVm aspNetUsers_ReferrerVm)
        {
            var userManager = _serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            var roleManager = _serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();

            var applicationUser = _aspNetUserService.GetById(aspNetUsers_ReferrerVm.AspNetUsersId);

            var bondsmanUser = await userManager.FindByEmailAsync(applicationUser.UserName);
            if (bondsmanUser != null)
            {
                var isBondsman = (await userManager.IsInRoleAsync(bondsmanUser, "BailBondsman"));
                if (!isBondsman)
                {
                    var adminAdded = await userManager.AddToRoleAsync(bondsmanUser, "BailBondsman");
                }
            }
        }

    }
}
