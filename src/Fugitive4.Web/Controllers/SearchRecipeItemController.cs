using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.SearchRecipeItem;
using Microsoft.AspNet.Authorization;

namespace Fugitive4.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class SearchRecipeItemController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly ISearchRecipeItemService _searchRecipeItemService;					
		private readonly ISearchRecipeService _searchRecipeService;

		public SearchRecipeItemController(ISearchRecipeItemService searchRecipeItemService,
								ISearchRecipeService searchRecipeService
,
								IMapper mapper		)
        {
            _searchRecipeItemService = searchRecipeItemService;
			_mapper = mapper;
			_searchRecipeService = searchRecipeService;

        }
		[Route("SearchRecipe/{searchRecipeId}/SearchRecipeItem/Index")]
        public async Task<IActionResult> Index(int searchRecipeId , string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";

            var searchRecipeItems = (await _searchRecipeItemService.GetAllAsync()).Where(s=>s.SearchRecipeId == searchRecipeId).ToList();
            var searchRecipeItemList = new SearchRecipeItemListVm
            {
                SearchRecipeName = (await _searchRecipeService.GetByIdAsync(searchRecipeId)).Name,
                SearchRecipeItems = _mapper.Map<IEnumerable<SearchRecipeItem>, IEnumerable<SearchRecipeItemVm>>(searchRecipeItems.ToList())
            };

            return View("Index", searchRecipeItemList);
        }

		public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            SearchRecipeItem searchRecipeItem = await _searchRecipeItemService.GetByIdAsync(id.Value);
            if (searchRecipeItem == null)
            {
                return HttpNotFound();
            }

            var searchRecipeItemVm = _mapper.Map<SearchRecipeItem, SearchRecipeItemVm>(searchRecipeItem);

            return View(searchRecipeItemVm);
        }


		[Route("SearchRecipe/{searchRecipeId}/SearchRecipeItem/Create")]
		public ActionResult Create(int searchRecipeId , string returnUrl = "")
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";
			ViewBag.ReturnUrl = returnUrl;

            var searchRecipeItemVm = new SearchRecipeItemVm();

			LoadLookups(searchRecipeItemVm);

            return View(searchRecipeItemVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
		[Route("SearchRecipe/{searchRecipeId}/SearchRecipeItem/Create")]
        public async Task<IActionResult> Create(SearchRecipeItemVm searchRecipeItemVm, string returnUrl = "")
        {
					ViewBag.ReturnUrl = returnUrl;
            try
            {
                if (ModelState.IsValid)
                {
                    var searchRecipeItem = _mapper.Map<SearchRecipeItemVm, SearchRecipeItem>(searchRecipeItemVm);
                    await _searchRecipeItemService.InsertAsync(searchRecipeItem, GetLogonUser());
					return RedirectToIndexOrSubjectDetail(returnUrl);
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(searchRecipeItemVm);

                return View(searchRecipeItemVm);
            }
        }

		[Route("SearchRecipe/{searchRecipeId}/SearchRecipeItem/Edit/{id}")]
		public async Task<IActionResult> Edit(int searchRecipeId , int? id, string returnUrl = "")
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";
			ViewBag.ReturnUrl = returnUrl;

			if (id == null)
            {
                return HttpNotFound();
            }

            var searchRecipeItem = await _searchRecipeItemService.GetByIdAsync(id.Value);
            if (searchRecipeItem == null)
            {
                return HttpNotFound();
            }            
			
			var searchRecipeItemVm = _mapper.Map<SearchRecipeItem, SearchRecipeItemVm>(searchRecipeItem);

			LoadLookups(searchRecipeItemVm);

            return View(searchRecipeItemVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
		[Route("SearchRecipe/{searchRecipeId}/SearchRecipeItem/Edit/{id}")]
        public async Task<IActionResult> Edit(SearchRecipeItemVm searchRecipeItemVm, string returnUrl = "")
        {
			ViewBag.ReturnUrl = returnUrl;
            try
            {
                if (ModelState.IsValid)
                {
                    var searchRecipeItem = _mapper.Map<SearchRecipeItemVm, SearchRecipeItem>(searchRecipeItemVm); 
                    await _searchRecipeItemService.UpdateAsync(searchRecipeItem, GetLogonUser());
					return RedirectToIndexOrSubjectDetail(returnUrl);
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(searchRecipeItemVm);

                return View(searchRecipeItemVm);
            }
        }

		[ActionName("Delete")]
		[Route("SearchRecipe/{searchRecipeId}/SearchRecipeItem/Delete/{id}")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var searchRecipeItem = await _searchRecipeItemService.GetByIdAsync(id.Value);
			if (searchRecipeItem == null)
            {
                return HttpNotFound();
            }

            var searchRecipeItemVm = _mapper.Map<SearchRecipeItem, SearchRecipeItemVm>(searchRecipeItem);

            return View(searchRecipeItemVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
		[Route("SearchRecipe/{searchRecipeId}/SearchRecipeItem/Delete/{id}")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _searchRecipeItemService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(SearchRecipeItemVm searchRecipeItemVm)
        {
			searchRecipeItemVm.SearchRecipeList = _searchRecipeService.GetAll().ToSelectList(s => s.Id.ToString(), s => s.Name).ToList();
		}

	}
}
