using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.SearchRecipe;
using Microsoft.AspNet.Authorization;

namespace Fugitive4.Web.Controllers
{
    [Authorize(Roles = "Admin")]
    public class SearchRecipeController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly ISearchRecipeService _searchRecipeService;

		public SearchRecipeController(ISearchRecipeService searchRecipeService,
								IMapper mapper		)
        {
            _searchRecipeService = searchRecipeService;
			_mapper = mapper;

        }
        public async Task<IActionResult> Index( string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";

            var searchRecipes = await _searchRecipeService.GetAllAsync();
            var searchRecipeList = new SearchRecipeListVm
            {
                SearchRecipes = _mapper.Map<IEnumerable<SearchRecipe>, IEnumerable<SearchRecipeVm>>(searchRecipes.ToList())
            };

            return View("Index", searchRecipeList);
        }

		public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            SearchRecipe searchRecipe = await _searchRecipeService.GetByIdAsync(id.Value);
            if (searchRecipe == null)
            {
                return HttpNotFound();
            }

            var searchRecipeVm = _mapper.Map<SearchRecipe, SearchRecipeVm>(searchRecipe);

            return View(searchRecipeVm);
        }


		public ActionResult Create( string returnUrl = "")
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";
			ViewBag.ReturnUrl = returnUrl;

            var searchRecipeVm = new SearchRecipeVm();

			LoadLookups(searchRecipeVm);

            return View(searchRecipeVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(SearchRecipeVm searchRecipeVm, string returnUrl = "")
        {
					ViewBag.ReturnUrl = returnUrl;
            try
            {
                if (ModelState.IsValid)
                {
                    var searchRecipe = _mapper.Map<SearchRecipeVm, SearchRecipe>(searchRecipeVm);
                    await _searchRecipeService.InsertAsync(searchRecipe, GetLogonUser());
					return RedirectToIndexOrSubjectDetail(returnUrl);
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(searchRecipeVm);

                return View(searchRecipeVm);
            }
        }

		public async Task<IActionResult> Edit( int? id, string returnUrl = "")
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";
			ViewBag.ReturnUrl = returnUrl;

			if (id == null)
            {
                return HttpNotFound();
            }

            var searchRecipe = await _searchRecipeService.GetByIdAsync(id.Value);
            if (searchRecipe == null)
            {
                return HttpNotFound();
            }            
			
			var searchRecipeVm = _mapper.Map<SearchRecipe, SearchRecipeVm>(searchRecipe);

			LoadLookups(searchRecipeVm);

            return View(searchRecipeVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(SearchRecipeVm searchRecipeVm, string returnUrl = "")
        {
			ViewBag.ReturnUrl = returnUrl;
            try
            {
                if (ModelState.IsValid)
                {
                    var searchRecipe = _mapper.Map<SearchRecipeVm, SearchRecipe>(searchRecipeVm); 
                    await _searchRecipeService.UpdateAsync(searchRecipe, GetLogonUser());
					return RedirectToIndexOrSubjectDetail(returnUrl);
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(searchRecipeVm);

                return View(searchRecipeVm);
            }
        }

		[ActionName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var searchRecipe = await _searchRecipeService.GetByIdAsync(id.Value);
			if (searchRecipe == null)
            {
                return HttpNotFound();
            }

            var searchRecipeVm = _mapper.Map<SearchRecipe, SearchRecipeVm>(searchRecipe);

            return View(searchRecipeVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _searchRecipeService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(SearchRecipeVm searchRecipeVm)
        {
		}

	}
}
