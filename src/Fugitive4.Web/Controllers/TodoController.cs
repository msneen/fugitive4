using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.Todo;
using Microsoft.AspNet.Authorization;
using Microsoft.AspNet.Http.Internal;
using Newtonsoft.Json;

namespace Fugitive4.Web.Controllers
{
	[Authorize]
    public class TodoController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly ITodoService _todoService;					
		private readonly ISubjectService _subjectService;					
		private readonly ITodoStatusService _todoStatusService;

		public TodoController(ITodoService todoService,
								ISubjectService subjectService,
								ITodoStatusService todoStatusService
,
								IMapper mapper		)
        {
            _todoService = todoService;
			_mapper = mapper;
			_subjectService = subjectService;
			_todoStatusService = todoStatusService;

        }

        [HttpPost]
        [Produces("application/jsonp")]
        public ActionResult Reorder([FromBody] dynamic reorderView)
        {
            var json = reorderView.ToString();
            ReorderVm reorderVm = JsonConvert.DeserializeObject<ReorderVm>(json);

            var todos = _todoService.GetBySubjectId(reorderVm.SubjectId);


            for (var i = 0; i < reorderVm.TodoIds.Length; i++)
            {
                var todo = todos.FirstOrDefault(t => t.Id == reorderVm.TodoIds[i]);
                if (todo != null)
                {
                    todo.SortOrder = i;
                    _todoService.Update(todo, GetLogonUser());
                }
                
            }

            return Json(new { success = true, responseText = "Items Reordered Successfully" });
        }


		[Route("Subject/{subjectId}/Todo/Index", Name="TodoForSubjectIndex")]
        [Route("Todo/Index")]
        public async Task<IActionResult> Index(int? subjectId , string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";
		    ViewBag.SubjectName = "All Todos for the Team";

            var todos = (await _todoService.GetAllByUserAsync(GetLogonUser())).Where(t=>t.Done ==false);
		    if (subjectId.HasValue && subjectId.Value > 0)
		    {
		        todos = todos.Where(t => t.SubjectId == subjectId.Value);
		        var subject = _subjectService.GetById(subjectId.Value, GetLogonUser());
		        ViewBag.SubjectName = $"Todo List For {subject.Lastname}, {subject.Firstname}";
		    }
            var todoList = new TodoListVm
            {
                Todos = _mapper.Map<IEnumerable<Todo>, IEnumerable<TodoVm>>(todos.OrderBy(x=>x.ModifiedDate).ToList())
            };

            return View("Index", todoList);
        }

		public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            Todo todo = await _todoService.GetByIdAsync(id.Value);
            if (todo == null)
            {
                return HttpNotFound();
            }

            var todoVm = _mapper.Map<Todo, TodoVm>(todo);

            return View(todoVm);
        }


		[Route("Subject/{subjectId}/Todo/Create", Name="TodoForSubject")]
		public ActionResult Create(int subjectId , string returnUrl = "")
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";
			ViewBag.ReturnUrl = returnUrl;

            var todoVm = new TodoVm();

			LoadLookups(todoVm);

            return View(todoVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
		[Route("Subject/{subjectId}/Todo/Create", Name="TodoForSubject")]
        public async Task<IActionResult> Create(TodoVm todoVm, string returnUrl = "")
        {
					ViewBag.ReturnUrl = returnUrl;
            try
            {
                if (ModelState.IsValid)
                {
                    var todo = _mapper.Map<TodoVm, Todo>(todoVm);
                    await _todoService.InsertAsync(todo, GetLogonUser());
					return RedirectToIndexOrSubjectDetail(returnUrl);
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(todoVm);

                return View(todoVm);
            }
        }

		[Route("Subject/{subjectId}/Todo/Edit/{id}", Name="TodoForSubjectEdit")]
        [Route("Todo/Edit/{id}")]
        public async Task<IActionResult> Edit(int? subjectId , int? id, string returnUrl = "")
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";
			ViewBag.ReturnUrl = returnUrl;

			if (id == null)
            {
                return HttpNotFound();
            }

            var todo = await _todoService.GetByIdAsync(id.Value);
            if (todo == null)
            {
                return HttpNotFound();
            }            
			
			var todoVm = _mapper.Map<Todo, TodoVm>(todo);

			LoadLookups(todoVm);

            return View(todoVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
		[Route("Subject/{subjectId}/Todo/Edit/{id}")]
        [Route("Todo/Edit/{id}")]
        public async Task<IActionResult> Edit(TodoVm todoVm, string returnUrl = "")
        {
			ViewBag.ReturnUrl = returnUrl;
            try
            {
                if (ModelState.IsValid)
                {
                    var todo = _mapper.Map<TodoVm, Todo>(todoVm); 
                    await _todoService.UpdateAsync(todo, GetLogonUser());
					return RedirectToIndexOrSubjectDetail(returnUrl);
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(todoVm);

                return View(todoVm);
            }
        }

		[ActionName("Delete")]
		[Route("Subject/{subjectId}/Todo/Delete/{id}")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var todo = await _todoService.GetByIdAsync(id.Value);
			if (todo == null)
            {
                return HttpNotFound();
            }

            var todoVm = _mapper.Map<Todo, TodoVm>(todo);

            return View(todoVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
		[Route("Subject/{subjectId}/Todo/Delete/{id}")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _todoService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(TodoVm todoVm)
        {
			todoVm.SubjectList = _subjectService.GetAll(GetLogonUser()).ToSelectList(s => s.Id.ToString(), s => $"{s.Lastname}, {s.Firstname}").ToList();
		    todoVm.TodoStatusList = _todoStatusService.GetAll().ToSelectList(s => s.Id.ToString(), s => s.Name).ToList();

        }

	}
}
