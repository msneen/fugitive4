﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Fugitive4.Services.Services;
using AutoMapper;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.SubjectForBondsman;
using Fugitive4.Web.ViewModels.BailBond;
using Microsoft.AspNet.Authorization;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace Fugitive4.Web.Controllers
{
    [Authorize(Roles = "BailBondsman")]
    public class SubjectForBondsmanController : ControllerBase
    {
        private readonly ISubjectService _subjectService;
        private readonly IMapper _mapper;

        public SubjectForBondsmanController(ISubjectService subjectService,
                                            IMapper mapper
            )
        {
            _subjectService = subjectService;
            _mapper = mapper;
        }
        // GET: /<controller>/
        public async Task<IActionResult> Index()
        {
            ViewBag.Title = "Index";
            ViewBag.TestValue = "Index";

            var subjects = await _subjectService.GetAllAsyncForBondsman(GetLogonUser());

            //limit this list by looking up the team of the login user.
            subjects = subjects
                .Where(s => s.BailBonds.Any())
                .OrderBy(s => s.BailBonds
                .OrderBy(t => t.DefaultDate)
                .First()
                .DefaultDate)
                .ToList();

            var subjectvms = _mapper.Map<IEnumerable<Subject>, IEnumerable<SubjectForBondsmanVm>>(subjects.ToList());
            foreach (var subject in subjectvms)
            {
                subject.CurrentBond = subject
                .BailBonds
                .OrderBy(t => t.DefaultDate)
                .Where(b => b.DefaultDate > DateTime.Now)
                .FirstOrDefault();
            }

            var subjectList = new SubjectForBondsmanListVm
            {
                Subjects = subjectvms
            };

            return View(subjectList);
        }
        //DetailsForBondsman
        public async Task<IActionResult> DetailsForBondsman(int? id)
        {
            ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            Subject subject = await _subjectService.GetByIdAllAsync(id.Value, GetLogonUser());
            if (subject == null)
            {
                return HttpNotFound();
            }

            var subjectForBondsmanVm = _mapper.Map<Subject, SubjectForBondsmanVm>(subject);

            var currentBond = subject
                                .BailBonds
                                .OrderByDescending(t => t.DefaultDate)
                                .Where(b => b.DefaultDate > DateTime.Now)
                                .FirstOrDefault();

            subjectForBondsmanVm.CurrentBond = _mapper.Map<BailBond, BailBondVm>(currentBond);

            return View(subjectForBondsmanVm);
        }
    }
}
