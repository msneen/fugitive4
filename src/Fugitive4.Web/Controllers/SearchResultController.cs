using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.SearchResult;
using Microsoft.AspNet.Authorization;
using Fugitive4.Web.ViewModels.SubjectSearchRecipe;
using Fugitive4.Web.ViewModels.SubjectSearchRecipeItem;

namespace Fugitive4.Web.Controllers
{
    [Authorize(Roles = "Member,Admin")]
    public class SearchResultController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly ISearchResultService _searchResultService;					
		private readonly ISubjectService _subjectService;					
		private readonly ISubjectSearchRecipeItemService _subjectSearchRecipeItemService;
	    private readonly ISubjectSearchRecipeService _subjectSearchRecipeService;
	    private readonly ISearchRecipeService _searchRecipeService;
	    private readonly ISearchRecipeItemService _searchRecipeItemService;

	    public SearchResultController(ISearchResultService searchResultService,
								ISubjectService subjectService,
								ISubjectSearchRecipeItemService subjectSearchRecipeItemService,
                                ISubjectSearchRecipeService subjectSearchRecipeService,
                                ISearchRecipeService searchRecipeService,
                                ISearchRecipeItemService searchRecipeItemService,
                                IMapper mapper		)
        {
            _searchResultService = searchResultService;
			_mapper = mapper;
			_subjectService = subjectService;
			_subjectSearchRecipeItemService = subjectSearchRecipeItemService;
	        _subjectSearchRecipeService = subjectSearchRecipeService;
	        _searchRecipeService = searchRecipeService;
	        _searchRecipeItemService = searchRecipeItemService;
        }

        [Route("Subject/{subjectId}/SearchResult/Background", Name = "BackgroundForSubject")]
        public async Task<IActionResult> Background(int subjectId, string returnUrl = "")
        {
            ViewBag.Title = "Background";
            ViewBag.TestValue = "Background";
            ViewBag.ReturnUrl = returnUrl;

            var subjectSearchRecipes = (await _subjectSearchRecipeService.GetAllAsync()).Where(s => s.SubjectId == subjectId);
            var subjectSearchRecipeList = new SubjectSearchRecipeListVm
            {
                SubjectSearchRecipes = _mapper.Map<IEnumerable<SubjectSearchRecipe>, IEnumerable<SubjectSearchRecipeVm>>(subjectSearchRecipes.ToList())
            };

            var allSubjectSearchRecipeItems = _subjectSearchRecipeItemService.GetBySubjectId(subjectId);
            var allSearchResults = _searchResultService.GetBySubjectId(subjectId);
            foreach (var subjectSearchRecipe in subjectSearchRecipeList.SubjectSearchRecipes)
            {
                var subjectSearchRecipeItems = allSubjectSearchRecipeItems.Where(i => i.SearchRecipeItem.SearchRecipeId == subjectSearchRecipe.SearchRecipeId);
                subjectSearchRecipe.SubjectSearchRecipeItems = _mapper.Map<IEnumerable<SubjectSearchRecipeItem>, IEnumerable<SubjectSearchRecipeItemVm>>(subjectSearchRecipeItems).ToList(); // subjectSearchRecipeItems
                foreach(var subjectSearchRecipeItemVm in subjectSearchRecipe.SubjectSearchRecipeItems)
                {
                    var searchResults = allSearchResults.Where(sr => sr.SubjectSearchRecipeItemId == subjectSearchRecipeItemVm.Id);
                    subjectSearchRecipeItemVm.SearchResults = _mapper.Map<IEnumerable<SearchResult>, IEnumerable<SearchResultVm>>(searchResults).ToList();
                }
            }

            //var searchResultVm = new SearchResultVm //not currently used
            //{
            //    SubjectId = subjectId,
            //    Username = GetLogonUser(),
            //};

            var backGroundVm = new BackgroundVm
            {
                SubjectId = subjectId,
                SubjectSearchRecipeListVm = subjectSearchRecipeList,
                SearchResultWithoutParentVms = _mapper.Map<IEnumerable<SearchResult>, IEnumerable<SearchResultVm>>(allSearchResults.Where(sr => !sr.SubjectSearchRecipeItemId.HasValue || (sr.SubjectSearchRecipeItemId.HasValue && sr.SubjectSearchRecipeItemId.Value == 0))).ToList()
            };

            //LoadLookups(searchResultVm);//not currently used

            return View(backGroundVm);
        }

        [Route("Subject/{subjectId}/SearchResult/Index")]
        public async Task<IActionResult> Index(int subjectId , string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";

            var searchResults = await _searchResultService.GetAllAsync();
            var searchResultList = new SearchResultListVm
            {
                SearchResults = _mapper.Map<IEnumerable<SearchResult>, IEnumerable<SearchResultVm>>(searchResults.ToList())
            };

            return View("Index", searchResultList);
        }

		public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            SearchResult searchResult = await _searchResultService.GetByIdAsync(id.Value);
            if (searchResult == null)
            {
                return HttpNotFound();
            }

            var searchResultVm = _mapper.Map<SearchResult, SearchResultVm>(searchResult);

            return View(searchResultVm);
        }


        [Route("Subject/{subjectId}/{subjectSearchRecipeItemId}/SearchResult/Create", Name = "SearchResultsForSubject")]
		public ActionResult Create(int subjectId , int? subjectSearchRecipeItemId = null, string returnUrl = "")
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";
			ViewBag.ReturnUrl = returnUrl;

            var searchResultVm = new SearchResultVm
            {
                SubjectId = subjectId,
                Username = GetLogonUser(),
            };

            if (subjectSearchRecipeItemId.HasValue)
            {
                searchResultVm.SubjectSearchRecipeItemId = subjectSearchRecipeItemId.Value;
            }

			LoadLookups(searchResultVm);

            return View(searchResultVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Subject/{subjectId}/{subjectSearchRecipeItemId}/SearchResult/Create", Name = "SearchResultsForSubject")]
        public async Task<IActionResult> Create(SearchResultVm searchResultVm, string returnUrl = "")
        {
					ViewBag.ReturnUrl = returnUrl;
            try
            {
                if (ModelState.IsValid)
                {
                    var searchResult = _mapper.Map<SearchResultVm, SearchResult>(searchResultVm);
                    if(searchResult.SubjectSearchRecipeItemId.HasValue && searchResult.SubjectSearchRecipeItemId.Value == 0)
                    {
                        searchResult.SubjectSearchRecipeItemId = null;
                    }
                    await _searchResultService.InsertAsync(searchResult, GetLogonUser());
					return RedirectToIndexOrSubjectDetail(returnUrl);
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(searchResultVm);

                return View(searchResultVm);
            }
        }

		[Route("Subject/{subjectId}/SearchResult/Edit/{id}", Name = "SearchResultsForSubjectEdit")]
		public async Task<IActionResult> Edit(int subjectId , int? id, string returnUrl = "")
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";
			ViewBag.ReturnUrl = returnUrl;

			if (id == null)
            {
                return HttpNotFound();
            }

            var searchResult = await _searchResultService.GetByIdAsync(id.Value);
            if (searchResult == null)
            {
                return HttpNotFound();
            }            
			
			var searchResultVm = _mapper.Map<SearchResult, SearchResultVm>(searchResult);

			LoadLookups(searchResultVm);

            return View(searchResultVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
		[Route("Subject/{subjectId}/SearchResult/Edit/{id}", Name = "SearchResultsForSubjectEdit")]
        public async Task<IActionResult> Edit(SearchResultVm searchResultVm, string returnUrl = "")
        {
			ViewBag.ReturnUrl = returnUrl;
            try
            {
                if (ModelState.IsValid)
                {
                    var searchResult = _mapper.Map<SearchResultVm, SearchResult>(searchResultVm); 
                    await _searchResultService.UpdateAsync(searchResult, GetLogonUser());
					return RedirectToIndexOrSubjectDetail(returnUrl);
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(searchResultVm);

                return View(searchResultVm);
            }
        }

		[ActionName("Delete")]
		[Route("Subject/{subjectId}/SearchResult/Delete/{id}")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var searchResult = await _searchResultService.GetByIdAsync(id.Value);
			if (searchResult == null)
            {
                return HttpNotFound();
            }

            var searchResultVm = _mapper.Map<SearchResult, SearchResultVm>(searchResult);

            return View(searchResultVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
		[Route("Subject/{subjectId}/SearchResult/Delete/{id}")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _searchResultService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(SearchResultVm searchResultVm)
        {
			searchResultVm.SubjectList = _subjectService.GetAll(GetLogonUser()).ToSelectList(s => s.Id.ToString(), s => s.EyeColor).ToList();
		}

	}
}
