using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Services.Enums;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.ActivityNote;
using Fugitive4.Web.ViewModels.BailBond;
using Fugitive4.Web.ViewModels.Subject;
using Microsoft.AspNet.Authorization;
using Fugitive4.Web.ViewModels.SubjectRelationship;
using Fugitive4.Web.ViewModels.Address;
using Fugitive4.Web.ViewModels.StoredFile;
using Fugitive4.Web.ViewModels._Interfaces;

namespace Fugitive4.Web.Controllers
{
    [Authorize(Roles = "Member,Admin")]
    public class SubjectController : ControllerBase
    {
	    private readonly IMapper _mapper; 

		private readonly ISubjectService _subjectService;					
		private readonly IReferrerService _referrerService;					
		private readonly IGenderService _genderService;					
		private readonly IRaceService _raceService;
	    private readonly ISubjectRelationshipService _subjectRelationshipService;
	    private readonly IRelationshipTypeService _relationshipTypeService;
	    private readonly IAddressService _addressService;
	    private readonly IStoredFileService _storedFileService;
	    private readonly IBookingChargeService _bookingChargeService;
	    private readonly IEmployerService _employerService;

	    public SubjectController(ISubjectService subjectService,
								IReferrerService referrerService,
								IGenderService genderService,
								IRaceService raceService,
                                ISubjectRelationshipService subjectRelationshipService,
                                IRelationshipTypeService relationshipTypeService,
                                IAddressService addressService,
                                IStoredFileService storedFileService,
                                IBookingChargeService bookingChargeService,
                                IEmployerService employerService,
								IMapper mapper		)
        {
            _subjectService = subjectService;
			_mapper = mapper;
			_referrerService = referrerService;
			_genderService = genderService;
			_raceService = raceService;
	        _subjectRelationshipService = subjectRelationshipService;
	        _relationshipTypeService = relationshipTypeService;
	        _addressService = addressService;
	        _storedFileService = storedFileService;
	        _bookingChargeService = bookingChargeService;
	        _employerService = employerService;
        }


        public async Task<IActionResult> Search()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Search(SubjectSearchVm subjectSearchVm)
        {
            return RedirectToAction(actionName: "Index", routeValues: new { searchCriteria = subjectSearchVm.SearchCriteria });
        }

        [Route("Subject/Index", Name="SubjectIndex")]
        public async Task<IActionResult> Index( string searchCriteria = null, int? bailBondId = null, int? employerId = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";

            var subjects = (await _subjectService.GetAllAsync(GetLogonUser())).ToList();


            if (!string.IsNullOrWhiteSpace(searchCriteria))
            {
                var search = searchCriteria.Split();
                var subjectFilter = subjects.Where(a => search.All(
                s => (a.Lastname ?? "").ToLower().Contains(s.ToLower()) ||
                (a.Firstname ?? "").ToLower().Contains(s.ToLower()) ||
                (a.Middlename ?? "").ToLower().Contains(s.ToLower())
                ));
                subjects = (subjectFilter == null ? new List<Subject>() : subjectFilter.ToList());
            }
            else if(bailBondId.HasValue && bailBondId.Value > 0)
            {
                subjects = subjects.Where(s => s.SubjectRelationships != null && s.SubjectRelationships.Any(sr => sr.BailBondId == bailBondId.Value)).ToList();
            }
            else if (employerId.HasValue && employerId.Value > 0)
            {
                subjects = subjects.Where(s => s.SubjectRelationships != null && s.SubjectRelationships.Any(sr => sr.EmployerId == employerId.Value)).ToList();
            }
            else
            {

                var newSubjects =
                    subjects
                        .Where(s => !s.BailBonds.Any())
                        .Where( s=> !s.SubjectRelationships.Any())
                        .Where(s=> !s.OtherSubjectRelationships.Any())
                        .ToList();


                subjects = subjects
                    .Where(s => 
                                (s.BailBonds.Any(b => b.DefaultDate >= DateTime.Now && (b.Done.HasValue == false || (b.Done.HasValue && b.Done.Value == false)))) 
                          )
                    .OrderBy(s=>s.BailBonds
                    .OrderBy(t => t.DefaultDate)
                    .First()
                    .DefaultDate)
                    .ToList();

                try
                {
                    if (newSubjects.Any())
                    {
                        subjects.AddRange(newSubjects.ToList());
                    }
                }
                catch (Exception ex)
                {
                    
                }

            }
            
            var subjectvms = _mapper.Map<IEnumerable<Subject>, IEnumerable<SubjectVm>>(subjects.ToList());
            foreach (var subject in subjectvms)
            {
                subject.CurrentBond = subject
                .BailBonds
                .OrderBy(t => t.DefaultDate)
                .FirstOrDefault(b => b.DefaultDate > DateTime.Now);
            }

            var subjectList = new SubjectListVm
            {
                Subjects = subjectvms
            };

            return View("Index", subjectList);
        }
        public async Task<IActionResult> Geomap(int? id)
        {
            //Add urls to each of the addresses and locations, to link to the item in the software
            if (id == null)
            {
                return HttpNotFound();
            }

            Subject subject = await _subjectService.GetByIdAllAsync(id.Value, GetLogonUser());
            if (subject == null)
            {
                return HttpNotFound();
            }

            var subjectRelationships = _subjectRelationshipService.GetBySubjectId(id.Value).ToList();

            var geoMapVm = new GeoMapVm
            {
                Addresses = new List<SubjectMapAddressVm>(),
                Locations = new List<SubjectMapLatLongVm>()
            };

            foreach (IAddress address in subject.Addresses)
            {
                var subjectMapAddressVm = new SubjectMapAddressVm();
                subjectMapAddressVm.Name = "Subject ";
                subjectMapAddressVm.Address = string.Format("{0},{1} {2} {3}", address.Address1, address.City, address.State, address.Zipcode);
                subjectMapAddressVm.Icon = "house";
                geoMapVm.Addresses.Add(subjectMapAddressVm);
            }

            foreach (var subjectRelationship in subjectRelationships)
            {
                var otherPerson = subjectRelationship.Subject.Id == subject.Id ? subjectRelationship.RelatedSubject : subjectRelationship.Subject;
                foreach (IAddress address in otherPerson.Addresses)
                {
                    var subjectMapAddressVm = new SubjectMapAddressVm();
                    subjectMapAddressVm.Name =String.Format("Related Subject - {0},{1}", otherPerson.Lastname,otherPerson.Firstname);
                    subjectMapAddressVm.Address = string.Format("{0},{1} {2} {3}", address.Address1, address.City, address.State, address.Zipcode);
                    subjectMapAddressVm.Icon = "friend";
                    geoMapVm.Addresses.Add(subjectMapAddressVm);
                }
            }

            foreach(var bailBond in subject.BailBonds)
            {
                foreach(var charge in bailBond.BookingCharges)
                {
                    IAddress chargeAddress = charge as IAddress;
                    var subjectMapAddressVm = new SubjectMapAddressVm();
                    subjectMapAddressVm.Name = String.Format("Bond - {0},{1}", charge.ChargeName, charge.CodeNumber);
                    subjectMapAddressVm.Address = string.Format("{0},{1} {2} {3}", chargeAddress.Address1, chargeAddress.City, chargeAddress.State, chargeAddress.Zipcode);
                    subjectMapAddressVm.Icon = "handcuffs";
                    geoMapVm.Addresses.Add(subjectMapAddressVm);
                }
            }

            foreach(var employer in subject.Employers)
            {
                IAddress employerAddress = employer as IAddress;
                var subjectMapAddressVm = new SubjectMapAddressVm();
                subjectMapAddressVm.Name = String.Format("Employer - {0}", employer.Name);
                subjectMapAddressVm.Address = string.Format("{0},{1} {2} {3}", employerAddress.Address1, employerAddress.City, employerAddress.State, employerAddress.Zipcode);
                subjectMapAddressVm.Icon = "office";
                geoMapVm.Addresses.Add(subjectMapAddressVm);
            }

            foreach (var location in subject.ActivityNotes)
            {
                var subjectMapLatLongVm = new SubjectMapLatLongVm();
                subjectMapLatLongVm.Name = "Activity Note" + location.ActivityDate.ToString("M/d/yy h:m tt");
                subjectMapLatLongVm.Latitude = location.Latitude;
                subjectMapLatLongVm.Longitude = location.Longitude;
                subjectMapLatLongVm.Icon = "car";
                geoMapVm.Locations.Add(subjectMapLatLongVm);
            }

            return View(geoMapVm);
        }

        public async Task<IActionResult> Mindmap(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Subject subject = await _subjectService.GetByIdAllAsync(id.Value, GetLogonUser());
            if (subject == null)
            {
                return HttpNotFound();
            }

            var subjectVm = _mapper.Map<Subject, SubjectVm>(subject);

            var subjectRelationships = _subjectRelationshipService.GetBySubjectId(id.Value).ToList();

            subjectVm.SubjectRelationships = _mapper.Map<IEnumerable<SubjectRelationship>, IEnumerable<ViewModels.SubjectRelationship.SubjectRelationshipVm>>(subjectRelationships);

            //map activity notes to Addresses
            AttachNotesToAddresses(subjectVm.Addresses, subjectVm);
            foreach(var subjectRelationship in subjectVm.SubjectRelationships)
            {
                var relatedSubject = subjectRelationship.RelatedSubject.Id == subjectVm.Id ? subjectRelationship.Subject : subjectRelationship.RelatedSubject;
                AttachNotesToAddresses(relatedSubject.Addresses, subjectVm);
            }
            var bookingCharges = subjectVm.BailBonds.SelectMany(b => b.BookingCharges);
            AttachNotesToAddresses(bookingCharges,subjectVm);


            return View(subjectVm);
        }

	    private static void AttachNotesToAddresses(IEnumerable<IHasActivity> addresses, SubjectVm subjectVm)
	    {
	        foreach (var addressVm in addresses)
	        {
	            var activityNoteVms = subjectVm
	                .ActivityNotes
	                .Where(a => a.RelatedAddressId == addressVm.Id)
	                .OrderByDescending(n => n.ActivityDate)
	                .ToList();

	            addressVm.ActivityNotes = new List<ActivityNoteVm>();
	            addressVm.ActivityNotes.AddRange(activityNoteVms);
	        }
	    }


	    public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            Subject subject = await _subjectService.GetByIdMediumAsync(id.Value, GetLogonUser());
            if (subject == null)
            {
                return HttpNotFound();
            }

            var subjectVm = _mapper.Map<Subject, SubjectVm>(subject);

            var currentBond = subject
                                .BailBonds
                                .OrderByDescending(t => t.DefaultDate)
                                .Where(b => b.DefaultDate > DateTime.Now)
                                .FirstOrDefault();

            subjectVm.CurrentBond = _mapper.Map<BailBond, BailBondVm>(currentBond);

            return View(subjectVm);
        }

        public async Task<IActionResult> SubjectPhotoAjax(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }
            var storedFile = _storedFileService
                                .GetBySubjectId(id.Value)
                                .OrderByDescending(t => t.CreatedDate)
                                .FirstOrDefault(f => f.IsSubject == true && f.ShowOnDetail == true);

            var storedFileVm = _mapper.Map<StoredFile, StoredFileVm>(storedFile);

            return PartialView("~/Views/Subject/_SubjectPhotoDetails.cshtml", storedFileVm);
        }

        public async Task<IActionResult> ContactInfoAjax(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Subject subject = await _subjectService.GetByIdMediumAsync(id.Value, GetLogonUser());
            if (subject == null)
            {
                return HttpNotFound();
            }

            subject.Addresses = subject.Addresses.OrderByDescending(x => x.AddressType.Name != "Bad Address").ToList();
            subject.PhoneNumbers = subject.PhoneNumbers.OrderByDescending(x => x.PhoneType.Name != "Bad Phone").ToList();

            var subjectVm = _mapper.Map<Subject, SubjectVm>(subject);
            ViewBag.ReturnUrl = "/Subject/Details/" + subjectVm.Id;

            return PartialView("~/Views/Subject/_ContactInfo.cshtml", subjectVm);
        }

        public async Task<IActionResult> DetailAjax(int? id)
        {
            try
            {
                ViewBag.TestValue = "Details";
                if (id == null)
                {
                    return HttpNotFound();
                }

                Subject subject = await _subjectService.GetByIdAllAsync(id.Value, GetLogonUser());
                if (subject == null)
                {
                    return HttpNotFound();
                }

                subject.Todos = subject.Todos.Where(x => x.Done == false).ToList();

                //var storedFiles = await _storedFileService.GetBySubjectIdAsync(subject.Id);
                //subject.StoredFiles = storedFiles.ToList();

                var subjectVm = _mapper.Map<Subject, SubjectVm>(subject);

                //var currentBond = subject
                //                    .BailBonds
                //                    .OrderByDescending(t => t.DefaultDate)
                //                    .FirstOrDefault(b => b.DefaultDate > DateTime.Now);

                //subjectVm.CurrentBond = _mapper.Map<BailBond, BailBondVm>(currentBond);

                var subjectRelationships = _subjectRelationshipService
                    .GetBySubjectId(id.Value)
                    .OrderBy(x=>x.RelationshipType.Name == "Not Useful")
                    .ThenBy(y=>y.SortOrder ?? 0)
                    .ToList();

                subjectVm.SubjectRelationships = _mapper.Map<IEnumerable<SubjectRelationship>, IEnumerable<ViewModels.SubjectRelationship.SubjectRelationshipVm>>(subjectRelationships);

                foreach (var activityNote in subjectVm.ActivityNotes)
                {
                    if (activityNote.RelatedSubjectId.HasValue && activityNote.RelatedSubjectId.Value > 0)
                    {
                        var relatedSubject = await _subjectService.GetByIdMediumAsync(activityNote.RelatedSubjectId.Value, GetLogonUser());
                        activityNote.RelatedSubject = _mapper.Map<Subject, SubjectVm>(relatedSubject);
                    }
                    if (activityNote.RelatedAddressId.HasValue && activityNote.RelatedAddressId.Value > 0)
                    {
                        try
                        {
                            IAddress address = null;
                            if (activityNote.RelatedAddressSource == AddressSource.SubjectAddress)
                            {
                                address = await _addressService.GetByIdAsync(activityNote.RelatedAddressId.Value);
                            }
                            else if (activityNote.RelatedAddressSource == AddressSource.BailBond)
                            {
                                address = await _bookingChargeService.GetByIdAsync(activityNote.RelatedAddressId.Value);
                            }
                            else if (activityNote.RelatedAddressSource == AddressSource.Employer)
                            {
                                address = await _employerService.GetByIdAsync(activityNote.RelatedAddressId.Value);
                            }

                            activityNote.RelatedAddress = _mapper.Map<IAddress, AddressVm>(address);
                        }
                        catch (Exception)
                        {
                            System.Diagnostics.Debugger.Break();
                        }
                    }
                }
                ViewBag.ReturnUrl = "/Subject/Details/" + subjectVm.Id;
                return PartialView("~/Views/Subject/_DetailAjax.cshtml", subjectVm);
            }
            catch(Exception ex)
            {
                System.Diagnostics.Debugger.Break();
            }

            return PartialView("~/Views/Subject/_DetailAjax.cshtml", new SubjectVm());

        }

        public async Task<IActionResult> FourthColumnAjax(int? id)
        {
            try
            {
                ViewBag.TestValue = "FourthColumn";
                if (id == null)
                {
                    return HttpNotFound();
                }

                Subject subject = await _subjectService.GetByIdFourthColumnAsync(id.Value, GetLogonUser());
                if (subject == null)
                {
                    return HttpNotFound();
                }

                //subject.Todos = subject.Todos.Where(x => x.Done == false).ToList();

                var storedFiles = await _storedFileService.GetBySubjectIdAsync(subject.Id);
                subject.StoredFiles = storedFiles.ToList();

                var subjectVm = _mapper.Map<Subject, SubjectVm>(subject);

                var currentBond = subject
                                    .BailBonds
                                    .OrderByDescending(t => t.DefaultDate)
                                    .FirstOrDefault(b => b.DefaultDate > DateTime.Now);

                subjectVm.CurrentBond = _mapper.Map<BailBond, BailBondVm>(currentBond);

                //var subjectRelationships = _subjectRelationshipService
                //    .GetBySubjectId(id.Value)
                //    .OrderBy(x => x.RelationshipType.Name == "Not Useful")
                //    .ThenBy(y => y.SortOrder ?? 0)
                //    .ToList();

                //subjectVm.SubjectRelationships = _mapper.Map<IEnumerable<SubjectRelationship>, IEnumerable<ViewModels.SubjectRelationship.SubjectRelationshipVm>>(subjectRelationships);

                //foreach (var activityNote in subjectVm.ActivityNotes)
                //{
                //    if (activityNote.RelatedSubjectId.HasValue && activityNote.RelatedSubjectId.Value > 0)
                //    {
                //        var relatedSubject = await _subjectService.GetByIdMediumAsync(activityNote.RelatedSubjectId.Value, GetLogonUser());
                //        activityNote.RelatedSubject = _mapper.Map<Subject, SubjectVm>(relatedSubject);
                //    }
                //    if (activityNote.RelatedAddressId.HasValue && activityNote.RelatedAddressId.Value > 0)
                //    {
                //        try
                //        {
                //            IAddress address = null;
                //            if (activityNote.RelatedAddressSource == AddressSource.SubjectAddress)
                //            {
                //                address = await _addressService.GetByIdAsync(activityNote.RelatedAddressId.Value);
                //            }
                //            else if (activityNote.RelatedAddressSource == AddressSource.BailBond)
                //            {
                //                address = await _bookingChargeService.GetByIdAsync(activityNote.RelatedAddressId.Value);
                //            }
                //            else if (activityNote.RelatedAddressSource == AddressSource.Employer)
                //            {
                //                address = await _employerService.GetByIdAsync(activityNote.RelatedAddressId.Value);
                //            }

                //            activityNote.RelatedAddress = _mapper.Map<IAddress, AddressVm>(address);
                //        }
                //        catch (Exception)
                //        {
                //            System.Diagnostics.Debugger.Break();
                //        }
                //    }
                //}
                ViewBag.ReturnUrl = "/Subject/Details/" + subjectVm.Id;
                return PartialView("~/Views/Subject/_FourthColumnAjax.cshtml", subjectVm);
            }
            catch (Exception ex)
            {
                System.Diagnostics.Debugger.Break();
            }

            return PartialView("~/Views/Subject/_DetailAjax.cshtml", new SubjectVm());

        }

        [Route("Subject/Create", Name = "SubjectCreate")]
        public ActionResult Create(int? relatedSubjectId = null, string returnUrl = "", int? bailBondId = null, int? employerId = null )
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";
            ViewBag.ReturnUrl = returnUrl;

            var subjectVm = new SubjectVm();

			LoadLookups(subjectVm);

            if (relatedSubjectId.HasValue)
            {
                
                subjectVm.RelatedSubject = new SubjectRelationshipVm();
                subjectVm.RelatedSubject.RelatedSubjectId = relatedSubjectId.Value;

                subjectVm.RelatedSubject.BailBondId = bailBondId.GetValueOrDefault();
                subjectVm.RelatedSubject.EmployerId = employerId.GetValueOrDefault();

                LoadRelatedLookups(subjectVm);
            }            

            return View(subjectVm);
        }

        private void LoadRelatedLookups(SubjectVm subjectVm)
        {

            subjectVm.RelatedSubject.RelationshipTypeList = _relationshipTypeService.GetAll().ToSelectList(s => s.Id.ToString(), s => s.Name).ToList();

            var subjectList = _subjectService
                .GetAll(GetLogonUser())
                .ToSelectList(s => s.Id.ToString(), s => s.Lastname + ", " + s.Firstname)
                .ToList();

            subjectVm.RelatedSubject.RelatedSubjectList = subjectList;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Subject/Create", Name = "SubjectCreate")]
        public async Task<IActionResult> Create(SubjectVm subjectVm, string returnUrl = "")
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var subject = _mapper.Map<SubjectVm, Subject>(subjectVm);
                    await _subjectService.InsertAsync(subject, GetLogonUser());

                    if(subjectVm.RelatedSubject != null)
                    {
                        var subjectRelationshipVm = subjectVm.RelatedSubject;
                        subjectRelationshipVm.SubjectId = subject.Id;
                        var subjectRelationship = _mapper.Map<SubjectRelationshipVm, SubjectRelationship>(subjectRelationshipVm);
                        await _subjectRelationshipService.InsertAsync(subjectRelationship, GetLogonUser());
                    }
					return RedirectToIndexOrSubjectDetail(returnUrl); //RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(subjectVm);

                return View(subjectVm);
            }
        }

        [Route("Subject/Edit", Name = "SubjectEdit")]
        public async Task<IActionResult> Edit( int? id, string returnUrl = "")
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";
            ViewBag.ReturnUrl = returnUrl;

            if (id == null)
            {
                return HttpNotFound();
            }

            var subject = await _subjectService.GetByIdMediumAsync(id.Value, GetLogonUser());
            if (subject == null)
            {
                return HttpNotFound();
            }            
			
			var subjectVm = _mapper.Map<Subject, SubjectVm>(subject);

			LoadLookups(subjectVm);

            return View(subjectVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Subject/Edit", Name = "SubjectEdit")]
        public async Task<IActionResult> Edit(SubjectVm subjectVm, string returnUrl = "")
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var subject = _mapper.Map<SubjectVm, Subject>(subjectVm); 
                    await _subjectService.UpdateAsync(subject, GetLogonUser());
                    return RedirectToIndexOrSubjectDetail(returnUrl);
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(subjectVm);

                return View(subjectVm);
            }
        }

		[ActionName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var subject = await _subjectService.GetByIdMediumAsync(id.Value, GetLogonUser());
			if (subject == null)
            {
                return HttpNotFound();
            }

            var subjectVm = _mapper.Map<Subject, SubjectVm>(subject);

            return View(subjectVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _subjectService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(SubjectVm subjectVm)
        {
			subjectVm.ReferrerList = _referrerService.GetAllByUser(GetLogonUser()).ToSelectList(s => s.Id.ToString(), s => s.Name).ToList();
			subjectVm.GenderList = _genderService.GetAll().ToSelectList(s => s.Id.ToString(), s => s.Name).ToList();
			subjectVm.RaceList = _raceService.GetAll().ToSelectList(s => s.Id.ToString(), s => s.Name).ToList();
		}

	}
}
