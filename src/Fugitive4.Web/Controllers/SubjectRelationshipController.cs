using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.Helpers;
using Fugitive4.Web.ViewModels.SubjectRelationship;
using Microsoft.AspNet.Authorization;
using Newtonsoft.Json;

namespace Fugitive4.Web.Controllers
{
    [Authorize(Roles = "Member,Admin")]
    public class SubjectRelationshipController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly ISubjectRelationshipService _subjectRelationshipService;					
		private readonly IRelationshipTypeService _relationshipTypeService;					
		private readonly ISubjectService _subjectService;					


		public SubjectRelationshipController(ISubjectRelationshipService subjectRelationshipService,                               
								             IRelationshipTypeService relationshipTypeService,
								             ISubjectService subjectService,
								             IMapper mapper		)
        {
            _subjectRelationshipService = subjectRelationshipService;
			_mapper = mapper;
			_relationshipTypeService = relationshipTypeService;
			_subjectService = subjectService;

        }

        [HttpPost]
        [Produces("application/jsonp")]
        public ActionResult Reorder([FromBody] dynamic reorderView)
        {
            var json = reorderView.ToString();
            ReorderVm reorderVm = JsonConvert.DeserializeObject<ReorderVm>(json);

            var todos = _subjectRelationshipService.GetBySubjectId(reorderVm.SubjectId);


            for (var i = 0; i < reorderVm.SubjectRelationshipIds.Length; i++)
            {
                var todo = todos.FirstOrDefault(t => t.Id == reorderVm.SubjectRelationshipIds[i]);
                if (todo != null)
                {
                    todo.SortOrder = i;
                    _subjectRelationshipService.Update(todo, GetLogonUser());
                }

            }

            return Json(new { success = true, responseText = "Items Reordered Successfully" });
        }

        private List<SubjectSummary> subjectList = new List<SubjectSummary>();
        private List<RelationshipVm> relationshipList = new List<RelationshipVm>();
        private List<SubjectRelationship> allSubjectRelationships;
        private List<Subject> allSubjects;
        private List<Address> allAddresses = new List<Address>();
        private List<PhoneNumber> allPhoneNumbers = new List<PhoneNumber>();
        private List<Employer> allEmployers = new List<Employer>();
        public async Task<IActionResult> RelationshipGraph(int subjectId)
        {

            allSubjects = (await _subjectService.GetAllAsync(GetLogonUser())).ToList();

            allSubjectRelationships = (await _subjectRelationshipService
                                            .GetAllAsync()
                                        ).ToList();


            GetRelationships(subjectId, 0);//*********************
            FindMatchingPhones();
            FindMatchingAddresses();
            FindMatchingEmployers();

            var subjectId1s = relationshipList.Select(s => s.subjectId1).ToList();
            subjectId1s.AddRange( relationshipList.Select(s => s.subjectId2));

            AddSummaries(subjectId, subjectId1s);


            var relationshipGraphvm = new RelationshipGraphVm
            {
                SubjectIds = subjectList,
                Relationships = relationshipList
            };

            return View(relationshipGraphvm);
        }



        private void AddSummaries(int subjectId, List<string> subjectId1s)
        {
            var distinctSubjects = subjectId1s.Distinct();
            foreach (var distinctSubjectId in distinctSubjects)
            {
                string text = "";
                bool isPrimary = false;
                if (distinctSubjectId.StartsWith("s"))
                {
                    var subject = allSubjects.FirstOrDefault(s => $"s{s.Id}" == distinctSubjectId);
                    text = $"{subject.Lastname}, {subject.Firstname}";
                    isPrimary = subject.Id == subjectId;
                }
                else if (distinctSubjectId.StartsWith("a"))
                {
                    var address = allAddresses.FirstOrDefault(a => $"a{a.Id}" == distinctSubjectId);
                    text = $"{address.Address1}";
                }
                else if (distinctSubjectId.StartsWith("p"))
                {
                    var phone = allPhoneNumbers.FirstOrDefault(p => $"p{p.Id}" == distinctSubjectId);
                    text = $"{phone.PhoneNumber1}";
                }
                else if (distinctSubjectId.StartsWith("e"))
                {
                    var employer = allEmployers.FirstOrDefault(p => $"e{p.Id}" == distinctSubjectId);
                    text = $"{employer.Name}";
                }
                subjectList.Add(new SubjectSummary
                {
                    NodeId = distinctSubjectId,
                    NodeName = text,
                    IsPrimaryNode = isPrimary
                });
            }
        }


        private void GetRelationships(int subjectId, int depth)
        {
            AddAddresses(subjectId);
            AddPhones(subjectId);
            AddEmployers(subjectId);

            var subjectRelationships1 = allSubjectRelationships
                .Where(s => s.SubjectId == subjectId)
                .ToList();
            AddRelationships(subjectRelationships1);
            if (depth < 5)
            {
                foreach (var relationship in subjectRelationships1)
                {
                    GetRelationships(relationship.RelatedSubjectId, depth + 1);
                }
            }

            var subjectRelationshipsR1 = allSubjectRelationships
                .Where(s => s.RelatedSubjectId == subjectId)
                .ToList();
            AddRelationships(subjectRelationshipsR1);
            if (depth < 5)
            {
                foreach (var relationship in subjectRelationshipsR1)
                {
                    GetRelationships(relationship.SubjectId, depth + 1);
                }
            }
        }

        private void AddEmployers(int subjectId)
        {
            var subject = allSubjects.FirstOrDefault(s => s.Id == subjectId);
            foreach (var employer in subject.Employers)
            {
                allEmployers.Add(employer);
                var id1 = subject.Id;
                var id2 = employer.Id;

                AddEdgeNode(id1, id2, "s", "e");
            }
        }

        private void AddAddresses(int subjectId)
        {
            
            var subject = allSubjects.FirstOrDefault(s => s.Id == subjectId);
            foreach (var address in subject.Addresses)
            {
                allAddresses.Add(address);
                var id1 = subject.Id;
                var id2 = address.Id;

                AddEdgeNode(id1, id2, "s", "a");
            }
        }

        private void AddPhones(int subjectId)
        {
            var subject = allSubjects.FirstOrDefault(s => s.Id == subjectId);
            foreach (var phone in subject.PhoneNumbers)
            {
                allPhoneNumbers.Add(phone);
                var id1 = subject.Id;
                var id2 = phone.Id;

                AddEdgeNode(id1, id2, "s", "p");
            }
        }

        private void AddRelationships(List<SubjectRelationship> subjectRelationships)
        {
            foreach (var relationship in subjectRelationships)
            {
                
                var id1 = relationship.SubjectId;
                var id2 = relationship.RelatedSubjectId;

                AddEdgeNode(id1, id2, "s", "s");
            }
        }

        private void FindMatchingAddresses()
        {
            foreach (var address in allAddresses)
            {
                var matchingAddresses = allAddresses.Where(a => a.Id != address.Id && a.IsRelated(address));
                foreach (var matchingAddress in matchingAddresses)
                {
                    AddEdgeNode(address.Id, matchingAddress.Id, "a", "a", 10);
                }
            }
        }

        private void FindMatchingPhones()
        {
            foreach (var phone in allPhoneNumbers)
            {
                var matchingPhones = allPhoneNumbers.Where(p => p.Id != phone.Id && p.IsRelated(phone));
                foreach (var matchingPhone in matchingPhones)
                {
                    AddEdgeNode(phone.Id, matchingPhone.Id, "p", "p", 10);
                }
            }    
        }

        private void FindMatchingEmployers()
        {
            foreach (var employer in allEmployers)
            {
                var matchingEmployers = allEmployers.Where(p => p.Id != employer.Id && p.IsRelated(employer));
                foreach (var matchingEmployer in matchingEmployers)
                {
                    AddEdgeNode(employer.Id, matchingEmployer.Id, "e", "e", 10);
                }

                var matchingAddresses = allAddresses.Where(a =>  a.IsRelated(employer));
                foreach (var matchingAddress in matchingAddresses)
                {
                    AddEdgeNode(employer.Id, matchingAddress.Id, "e", "a", 10);
                }
            }
        }


        private void AddEdgeNode(int id1, int id2, string nodeType1, string nodeType2, int weight = 1)
        {
            var relationshipVm = new RelationshipVm
            {
                Weight = weight.ToString()
            };
            if (id1 < id2)
            {
                relationshipVm.subjectId1 = $"{nodeType1}{id1}";
                relationshipVm.subjectId2 = $"{nodeType2}{id2}";
            }
            else
            {
                relationshipVm.subjectId1 = $"{nodeType2}{id2}";
                relationshipVm.subjectId2 = $"{nodeType1}{id1}";
            }
            if (
                !relationshipList.Any(
                    r => r.subjectId1 == relationshipVm.subjectId1 && r.subjectId2 == relationshipVm.subjectId2))
            {
                relationshipList.Add(relationshipVm);
            }
        }

        public async Task<IActionResult> Index( string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";

            var subjectRelationships = await _subjectRelationshipService.GetAllAsync();

            var subjectRelationshipVms = _mapper.Map<IEnumerable<SubjectRelationship>, IEnumerable<SubjectRelationshipVm>>(subjectRelationships.ToList());

            var subjectRelationshipList = new SubjectRelationshipListVm
            {
                SubjectRelationships = subjectRelationshipVms
            };

            return View("Index", subjectRelationshipList);
        }

		public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            SubjectRelationship subjectRelationship = await _subjectRelationshipService.GetByIdAsync(id.Value);
            if (subjectRelationship == null)
            {
                return HttpNotFound();
            }

            var subjectRelationshipVm = _mapper.Map<SubjectRelationship, SubjectRelationshipVm>(subjectRelationship);

            return View(subjectRelationshipVm);
        }


		public ActionResult Create(int? subjectId, string returnUrl = "")
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";
            ViewBag.ReturnUrl = returnUrl;

            var subjectRelationshipVm = new SubjectRelationshipVm();
            if (subjectId.HasValue)
            {
                subjectRelationshipVm.SubjectId = subjectId.Value;
            }

			LoadLookups(subjectRelationshipVm);
            if (subjectId.HasValue)
            {
                var currentSubject = subjectRelationshipVm.RelatedSubjectList.Where(s => Convert.ToInt32(s.Value) == subjectId.Value).FirstOrDefault();
                if (currentSubject != null)
                {
                    subjectRelationshipVm.RelatedSubjectList.Remove(currentSubject);
                }
                
            }
            return View(subjectRelationshipVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(SubjectRelationshipVm subjectRelationshipVm, string returnUrl = "")
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var subjectRelationship = _mapper.Map<SubjectRelationshipVm, SubjectRelationship>(subjectRelationshipVm);
                    await _subjectRelationshipService.InsertAsync(subjectRelationship, GetLogonUser());


                    return RedirectToIndexOrSubjectDetail(returnUrl); //RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(subjectRelationshipVm);

                return View(subjectRelationshipVm);
            }
        }

		public async Task<IActionResult> Edit( int? id, string returnUrl = "")
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";
            ViewBag.ReturnUrl = returnUrl;

            if (id == null)
            {
                return HttpNotFound();
            }

            var subjectRelationship = await _subjectRelationshipService.GetByIdAsync(id.Value);
            if (subjectRelationship == null)
            {
                return HttpNotFound();
            }            
			
			var subjectRelationshipVm = _mapper.Map<SubjectRelationship, SubjectRelationshipVm>(subjectRelationship);

			LoadLookups(subjectRelationshipVm);

            return View(subjectRelationshipVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(SubjectRelationshipVm subjectRelationshipVm, string returnUrl = "")
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var subjectRelationship = _mapper.Map<SubjectRelationshipVm, SubjectRelationship>(subjectRelationshipVm); 
                    await _subjectRelationshipService.UpdateAsync(subjectRelationship, GetLogonUser());
                    return RedirectToIndexOrSubjectDetail(returnUrl);
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(subjectRelationshipVm);

                return View(subjectRelationshipVm);
            }
        }

		[ActionName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var subjectRelationship = await _subjectRelationshipService.GetByIdAsync(id.Value);
			if (subjectRelationship == null)
            {
                return HttpNotFound();
            }

            var subjectRelationshipVm = _mapper.Map<SubjectRelationship, SubjectRelationshipVm>(subjectRelationship);

            return View(subjectRelationshipVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _subjectRelationshipService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(SubjectRelationshipVm subjectRelationshipVm)
        {
			subjectRelationshipVm.RelationshipTypeList = _relationshipTypeService.GetAll().ToSelectList(s => s.Id.ToString(), s => s.Name).ToList();

            
            var subjectList = _subjectService
                .GetAll(GetLogonUser())
                .ToSelectList(s => s.Id.ToString(), s => s.Lastname + ", " + s.Firstname)
                .ToList();

            subjectRelationshipVm.SubjectList = subjectList;
            subjectRelationshipVm.RelatedSubjectList = subjectList;

        }

	}
}
