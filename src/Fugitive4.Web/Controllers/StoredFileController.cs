using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.StoredFile;
using Microsoft.AspNet.Http;
using Microsoft.Net.Http.Headers;
using System.IO;
using System.Text;
using Fugitive4.Services.ModelsUnmapped;
using Microsoft.AspNet.Authorization;

namespace Fugitive4.Web.Controllers
{
    [Authorize(Roles = "Member,Admin")]
    public class StoredFileController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly IStoredFileService _storedFileService;					
		private readonly ISubjectService _subjectService;

		public StoredFileController(IStoredFileService storedFileService,
								ISubjectService subjectService
,
								IMapper mapper		)
        {
            _storedFileService = storedFileService;
			_mapper = mapper;
			_subjectService = subjectService;

        }

        [HttpGet]
        public FileContentResult Download(int id)
        {
            StoredFile storedFile = _storedFileService.GetById(id);
            return File(storedFile.FileContent, storedFile.ContentType, storedFile.FileName);
        }

        [Route("Subject/{subjectId}/StoredFile/Index", Name = "StoredFileIndexForSubject")]
        public async Task<IActionResult> Index(int subjectId , string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";

            var storedFiles = await _storedFileService.GetBySubjectIdAsync(subjectId);
            var storedFileList = new StoredFileListVm
            {
                StoredFiles = _mapper.Map<IEnumerable<StoredFileCondensed>, IEnumerable<StoredFileVm>>(storedFiles.Where(s=>s.SubjectId==subjectId).ToList())
            };

            return View("Index", storedFileList);
        }

		public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            StoredFile storedFile = await _storedFileService.GetByIdAsync(id.Value);
            if (storedFile == null)
            {
                return HttpNotFound();
            }

            var storedFileVm = _mapper.Map<StoredFile, StoredFileVm>(storedFile);

            return View(storedFileVm);
        }


[Route("Subject/{subjectId}/StoredFile/Create", Name = "StoredFileForSubject")]
		public ActionResult Create(int subjectId, string returnUrl = "")
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";
            ViewBag.ReturnUrl = returnUrl;

            var storedFileVm = new StoredFileVm {SubjectId = subjectId};

			LoadLookups(storedFileVm);

            return View(storedFileVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Subject/{subjectId}/StoredFile/Create", Name = "StoredFileForSubject")]
  
        public async Task<IActionResult> Create(StoredFileVm storedFileVm, IFormFile uploadFile, string returnUrl = "")
        {
            try
            {
                if (ModelState.IsValid)
                {
                    
                        //var storedFile = _mapper.Map<StoredFileVm, StoredFile>(storedFileVm);
                    var storedFile = new StoredFile();
                    storedFile.SubjectId = storedFileVm.SubjectId;
                    storedFile.FileName = ContentDispositionHeaderValue.Parse(uploadFile.ContentDisposition).FileName.Trim('"');
                    storedFile.ContentType = uploadFile.ContentType;
                    storedFile.FileType = storedFileVm.FileType;
                    storedFile.Notes = storedFileVm.Notes;

                    storedFile.IsSubject = storedFileVm.IsSubject;
                    storedFile.ShowOnDetail = storedFileVm.ShowOnDetail;

                    var input = uploadFile.OpenReadStream();
                    input.Position = 0;
                    using (var ms = new MemoryStream())
                    {
                        int length = System.Convert.ToInt32(input.Length);
                        input.CopyTo(ms, length);
                        storedFile.FileContent = ms.ToArray();
                    }

                    await _storedFileService.InsertAsync(storedFile, GetLogonUser());

                    return RedirectToIndexOrSubjectDetail(returnUrl); //RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(storedFileVm);

                return View(storedFileVm);
            }
        }

        [Route("Subject/{subjectId}/StoredFile/Edit/{id}", Name = "StoredFileForSubjectEdit")]
		public async Task<IActionResult> Edit(int subjectId , int? id, string returnUrl = "")
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";
            ViewBag.ReturnUrl = returnUrl;


            if (id == null)
            {
                return HttpNotFound();
            }

            var storedFile = await _storedFileService.GetByIdAsync(id.Value);
            if (storedFile == null)
            {
                return HttpNotFound();
            }            
			
			var storedFileVm = _mapper.Map<StoredFile, StoredFileVm>(storedFile);

			LoadLookups(storedFileVm);

            return View(storedFileVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Subject/{subjectId}/StoredFile/Edit/{id}", Name = "StoredFileForSubjectEdit")]
        public async Task<IActionResult> Edit(StoredFileVm storedFileVm, string returnUrl = "")
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var existingStoredFile = await _storedFileService.GetByIdAsync(storedFileVm.Id);
                    existingStoredFile.IsSubject = storedFileVm.IsSubject;
                    existingStoredFile.ShowOnDetail = storedFileVm.ShowOnDetail;
                    existingStoredFile.Notes = storedFileVm.Notes;
                    //var storedFile = _mapper.Map<StoredFileVm, StoredFile>(storedFileVm); 
                    await _storedFileService.UpdateAsync(existingStoredFile, GetLogonUser());
                    return RedirectToIndexOrSubjectDetail(returnUrl);
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(storedFileVm);

                return View(storedFileVm);
            }
        }

		[ActionName("Delete")]
[Route("Subject/{subjectId}/StoredFile/Delete/{id}")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var storedFile = await _storedFileService.GetByIdAsync(id.Value);
			if (storedFile == null)
            {
                return HttpNotFound();
            }

            var storedFileVm = _mapper.Map<StoredFile, StoredFileVm>(storedFile);

            return View(storedFileVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
[Route("Subject/{subjectId}/StoredFile/Delete/{id}")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _storedFileService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(StoredFileVm storedFileVm)
        {
			storedFileVm.SubjectList = _subjectService.GetAll(GetLogonUser()).ToSelectList(s => s.Id.ToString(), s => s.EyeColor).ToList();
		}

	}
}
