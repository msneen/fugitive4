using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.Address;
using Microsoft.AspNet.Authorization;

namespace Fugitive4.Web.Controllers
{
    [Authorize(Roles = "Member,Admin")]
    public class AddressController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly IAddressService _addressService;					
		private readonly ISubjectService _subjectService;					
		private readonly IAddressTypeService _addressTypeService;

		public AddressController(IAddressService addressService,
								ISubjectService subjectService,
								IAddressTypeService addressTypeService,
								IMapper mapper		)
        {
            _addressService = addressService;
			_mapper = mapper;
			_subjectService = subjectService;
			_addressTypeService = addressTypeService;

        }
[Route("Subject/{subjectId}/Address/Index")]
        public async Task<IActionResult> Index(int subjectId , string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";

            var addresses = await _addressService.GetAllAsync();
            var addressList = new AddressListVm
            {
                Addresses = _mapper.Map<IEnumerable<Address>, IEnumerable<AddressVm>>(addresses.Where(s=>s.SubjectId==subjectId).ToList())
            };

            return View("Index", addressList);
        }

		public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            Address address = await _addressService.GetByIdAsync(id.Value);
            if (address == null)
            {
                return HttpNotFound();
            }

            var addressVm = _mapper.Map<Address, AddressVm>(address);

            return View(addressVm);
        }


        [Route("Subject/{subjectId}/Address/Create", Name = "AddressForSubject")]
		public ActionResult Create(int subjectId, string returnUrl = "")
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";
            ViewBag.ReturnUrl = returnUrl;

            var addressVm = new AddressVm
            {
                SubjectId = subjectId,
                City = "San Diego",
                State ="Ca"
            };

			LoadLookups(addressVm);

            return View(addressVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Subject/{subjectId}/Address/Create", Name = "AddressForSubject")]
        public async Task<IActionResult> Create(AddressVm addressVm, string returnUrl = "")
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var address = _mapper.Map<AddressVm, Address>(addressVm);
                    await _addressService.InsertAsync(address, GetLogonUser());
                    return RedirectToIndexOrSubjectDetail(returnUrl); //RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(addressVm);

                return View(addressVm);
            }
        }

[Route("Subject/{subjectId}/Address/Edit/{id}", Name = "AddressForSubjectEdit")]
		public async Task<IActionResult> Edit(int subjectId , int? id, string returnUrl = "")
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";
            ViewBag.ReturnUrl = returnUrl;

            if (id == null)
            {
                return HttpNotFound();
            }

            var address = await _addressService.GetByIdAsync(id.Value);
            if (address == null)
            {
                return HttpNotFound();
            }            
			
			var addressVm = _mapper.Map<Address, AddressVm>(address);

			LoadLookups(addressVm);

            return View(addressVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Subject/{subjectId}/Address/Edit/{id}", Name = "AddressForSubjectEdit")]
        public async Task<IActionResult> Edit(AddressVm addressVm, string returnUrl = "")
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var address = _mapper.Map<AddressVm, Address>(addressVm); 
                    await _addressService.UpdateAsync(address, GetLogonUser());
                    return RedirectToIndexOrSubjectDetail(returnUrl);
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(addressVm);

                return View(addressVm);
            }
        }

		[ActionName("Delete")]
[Route("Subject/{subjectId}/Address/Delete/{id}")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var address = await _addressService.GetByIdAsync(id.Value);
			if (address == null)
            {
                return HttpNotFound();
            }

            var addressVm = _mapper.Map<Address, AddressVm>(address);

            return View(addressVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
[Route("Subject/{subjectId}/Address/Delete/{id}")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _addressService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(AddressVm addressVm)
        {
			addressVm.SubjectList = _subjectService.GetAll(GetLogonUser()).ToSelectList(s => s.Id.ToString(), s => s.EyeColor).ToList();
			addressVm.AddressTypeList = _addressTypeService.GetAll().ToSelectList(s => s.Id.ToString(), s => s.Name).ToList();
		}

	}
}
