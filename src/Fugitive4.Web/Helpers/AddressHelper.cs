﻿using Fugitive4.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fugitive4.Web.Helpers
{
    public class AddressHelper
    {
        public static Address ParseAddress(string[] addressLines)
        {
            var address = new Address
            {
                BelongsToSubject = false
            };

            if (addressLines.Count() == 1)
            {
                address.Address1 = addressLines[0].Trim();
            }


            if (addressLines.Count() == 2)
            {
                address.Address1 = addressLines[0].Trim();


                var stateZip = addressLines[1].Trim().Split((char[])null, StringSplitOptions.RemoveEmptyEntries);
                if (stateZip.Count() == 2)
                {
                    address.State = stateZip[0].Trim();
                    address.Zipcode = stateZip[1].Trim();
                }
                else if (stateZip.Count() == 1)
                {
                    if (IsNumeric(stateZip[0]))
                    {
                        address.Zipcode = stateZip[0].Trim();
                    }
                    else
                    {
                        address.State = stateZip[0].Trim();
                    }
                }
            }

            if (addressLines.Count() == 3)
            {
                address.Address1 = addressLines[0].Trim();
                address.City = addressLines[1].Trim();

                var stateZip = addressLines[2].Trim().Split((char[])null, StringSplitOptions.RemoveEmptyEntries);
                if (stateZip.Count() == 2)
                {
                    address.State = stateZip[0].Trim();
                    address.Zipcode = stateZip[1].Trim();
                }
                else if (stateZip.Count() == 1)
                {
                    if (IsNumeric(stateZip[0]))
                    {
                        address.Zipcode = stateZip[0].Trim();
                    }
                    else
                    {
                        address.State = stateZip[0].Trim();
                    }
                }
            }

            //add defaults
            if (string.IsNullOrWhiteSpace(address.City)){
                address.City = "San Diego";
            }
            if (string.IsNullOrWhiteSpace(address.State))
            {
                address.State = "CA";
            }
            return address;
        }

        public static bool IsNumeric(string input)
        {
            var zipTest = 0;
            if (int.TryParse(input, out zipTest))
            {
                return true;
            }
            return false;
        }
    }
}
