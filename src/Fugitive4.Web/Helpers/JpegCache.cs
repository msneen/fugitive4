﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Fugitive4.Web.ViewModels.JpegVideo;
using Microsoft.ApplicationInsights;
using Microsoft.ApplicationInsights.Extensibility;
using Microsoft.Extensions.Caching.Memory;

namespace Fugitive4.Web.Helpers
{
    public interface IJpegCache
    {
        void PutNextFrame(string cameraId, byte[] bytes, int frame);
        byte[] GetNextFrame(string cameraId);
        List<string> GetActiveCameraList();
    }
    public class JpegCache  :IJpegCache
    {
        private readonly IMemoryCache _jpegMemoryCache;
        private readonly object _cacheLock = new object();
        private TelemetryClient _telemetry;

        public JpegCache(IMemoryCache jpegMemoryCache)
        {
            _jpegMemoryCache = jpegMemoryCache;

            _telemetry = new TelemetryClient(new TelemetryConfiguration {DisableTelemetry = true});//Set to false to turn telemetry on
        }

        public void AddCamToList(string cameraId)
        {
            List<string> cameraList;
            if (!_jpegMemoryCache.TryGetValue<List<string>>("CameraList", out cameraList))
            {
                cameraList = new List<string>();
            }
            if (cameraList.Any(x => x == cameraId)) return;

            cameraList.Add(cameraId);
            _jpegMemoryCache.Set("CameraList", cameraList);
        }

        public List<string> GetActiveCameraList()
        {
            List<string> cameraList;
            if (!_jpegMemoryCache.TryGetValue<List<string>>("CameraList", out cameraList))
            {
                cameraList = new List<string>();
            }
            return cameraList;
        }


        public void PutNextFrame(string cameraId, byte[] bytes, int frame)
        {
            AddCamToList(cameraId);
            JpegUpload jpegUpload = new JpegUpload
            {
                Bytes = bytes,
                Frame = frame,
                Received = DateTime.Now
            };

            JpegUploads jpegUploads;

            if (!_jpegMemoryCache.TryGetValue<JpegUploads>(cameraId, out jpegUploads))
            {
                jpegUploads=new JpegUploads();
                jpegUploads.JpegUploadList = new List<JpegUpload>();
            }

            lock (_cacheLock)
            {
                //RemoveOlder than 3 seconds
                _telemetry.TrackTrace($"PutNextFrame, Locked Thread {cameraId}");
                jpegUploads.JpegUploadList.RemoveAll(x => DateTime.Now - x.Received > TimeSpan.FromSeconds(5));

                jpegUploads.JpegUploadList.Add(jpegUpload);

                _jpegMemoryCache.Set(cameraId, jpegUploads);
                _telemetry.TrackTrace($"PutNextFrame, Added image for {cameraId}");
            }
        }

        public byte[] GetNextFrame(string cameraId)
        {
            AddCamToList(cameraId);
            //_telemetry.TrackTrace($"In GetNextFrame({cameraId})");
            JpegUploads jpegUploads;
            if (_jpegMemoryCache.TryGetValue<JpegUploads>(cameraId, out jpegUploads))
            {
                lock (_cacheLock)
                {
                    _telemetry.TrackTrace($"GetNextFrame:  Found jpegCache for {cameraId}");
                    //RemoveOlder than 5 seconds
                    jpegUploads.JpegUploadList.RemoveAll(x => DateTime.Now - x.Received > TimeSpan.FromSeconds(5));

                    //There could be no frames waiting that are less than 5 seconds old
                    if (jpegUploads.JpegUploadList.Count == 0)
                    {
                        _telemetry.TrackTrace($"GetNextFrame:  No Uploads found for {cameraId}");
                        return jpegUploads.CurrentBytes;
                    }

                    _telemetry.TrackTrace($"GetNextFrame:  {jpegUploads.JpegUploadList.Count} Uploads found for {cameraId}");

                    var nextFrame = jpegUploads.CurrentFrame + 1;
                    if (nextFrame > 15) nextFrame = 0;

                    //There are some frames in the cache if we got here
                    var currentJpegUploads = jpegUploads.JpegUploadList.OrderBy(x => x.Frame);

                    var currentJpegUpload = currentJpegUploads.FirstOrDefault(x => x.Frame >= nextFrame);
                        //Happy Path, we're looking for the next frame
                    if (currentJpegUpload == null /*&&  DateTime.Now - jpegUploads.LastFrameChanged > TimeSpan.FromSeconds(2)*/)//Could turn this back on with || currrentBytes is null so it takes anything if there is no frame
                    {

                        _telemetry.TrackTrace($"GetNextFrame:  A greater frame or Equal in sequence not found for {cameraId}, frame{nextFrame}");
                        currentJpegUpload = currentJpegUploads.FirstOrDefault(x => x.Frame >= 0);
                            //There weren't any higher than, so maybe frames wrapped back to zero
                        if (currentJpegUpload == null)
                        {
                            _telemetry.TrackTrace($"GetNextFrame:  No Wraparound Frames found for {cameraId}, frame{nextFrame}");
                        }
                       
                    }

                    if (currentJpegUpload != null) //If we found one, use it
                    {
                        _telemetry.TrackTrace($"GetNextFrame:  Found Frame for {cameraId}, frame{nextFrame}");
                        currentJpegUpload.Viewed = DateTime.Now;
                        jpegUploads.CurrentFrame = nextFrame;
                        jpegUploads.CurrentBytes = currentJpegUpload.Bytes;
                        jpegUploads.LastFrameChanged = DateTime.Now;
                        jpegUploads.JpegUploadList.Remove(currentJpegUpload);
                    }

                    _telemetry.TrackTrace($"GetNextFrame:  Leaving Function for {cameraId}, frame{nextFrame}");
                }
            }
            return null;
        }


    }
}
