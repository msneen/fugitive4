﻿using Microsoft.AspNet.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fugitive4.Web.Helpers
{
    public class LinkHelper
    {

        public static string GetIndexOrReturnDetail(HttpContext httpContext, string returnUrl)
        {
            if (string.IsNullOrWhiteSpace(returnUrl))
            {
                return "Index";
            }
            else
            {
                var fullUrl = string.Format("{0}://{1}{2}", httpContext.Request.Scheme, httpContext.Request.Host.Value, returnUrl);
                return fullUrl;
            }
        }
    }
}
