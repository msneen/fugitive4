﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Fugitive4.Services.Models;
//using FuzzyString;

namespace Fugitive4.Web.Helpers
{
    public static class FuzzyExtensions
    {

        public static bool IsRelated(this Address address, Employer employer)
        {
            if (IsRelated(employer.Address1, address.Address1))
            {
                return true;
            }
            return false;
        }

        public static bool IsRelated(this Employer employer, Employer employer2)
        {
            if (IsRelated(employer.Name, employer2.Name))
            {
                return true;
            }
            if (IsRelated(employer.Address1, employer2.Address1))
            {
                return true;
            }
            return false;
        }

        public static bool IsRelated(this Address address, Address address2)
        {
            return IsRelated(address.Address1, address2.Address1);
        }

        public static bool IsRelated(string string1, string string2)
        {
            if (string.IsNullOrWhiteSpace(string1) || string.IsNullOrWhiteSpace(string2))
            {
                return false;
            }

            if (string1 == string2)
            {
                return true;
            }

            if (string1.Contains(string2) ||
                string2.Contains(string1))
            {
                return true;
            }

            //Start fuzzy matching
            var address1Pieces = string1.Split(' ');
            var address2Pieces = string2.Split(' ');
            var lowestBound = Math.Min(address1Pieces.GetUpperBound(0), address2Pieces.GetUpperBound(0));
            if (lowestBound >= 1)
            {
                var matches = 0;
                for (int i = 0; i < lowestBound; i++)
                {
                    if (address1Pieces[i].Contains(address2Pieces[i]) ||
                        address2Pieces[i].Contains(address1Pieces[i]))
                    {
                        matches += 1;
                    }
                }
                return matches >= 2;
            }


            return false;
        }

        public static string CleanPhoneNumber1(this PhoneNumber phoneNumber)
        {
            return phoneNumber.PhoneNumber1.Where(char.IsNumber).Aggregate("", (current, digit) => current + digit);
        }

        public static bool IsRelated(this PhoneNumber phoneNumber, PhoneNumber otherPhoneNumber)
        {
            var phone1 = phoneNumber.CleanPhoneNumber1();
            var phone2 = otherPhoneNumber.CleanPhoneNumber1();
            if (phone1 == phone2)
            {
                return true;
            }
            if (phone1.Contains(phone2) || phone2.Contains(phone1))
            {
                return true;
            }

            //test fuzzy matching here
            var phone1Beginning = FindBeginning(phone1);
            var phone1Last4 = FindLast4(phone1);

            var phone2Beginning = FindBeginning(phone2);
            var phone2Last4 = FindLast4(phone2);

            if (phone1Beginning.Contains(phone2Beginning) || phone2Beginning.Contains(phone1Beginning))
            {
                var matchesInLast4 = 0;
                for (var i = 0; i < 4; i++)
                {
                    if (phone1Last4[i] == phone2Last4[i])
                    {
                        matchesInLast4 += 1;
                    }
                }
                return matchesInLast4 > 3; //if 3 of the 4 last digits match, and thefirst digits match, it's problably a match
            }


            return false;
        }

        public static string FindBeginning(string number)
        {
            if (number.Length >= 4)
            {
                return number.Substring(0, number.Length - 4);
            }
            else
            {
                return number;
            }
        }

        public static string FindLast4(string number)
        {
            return number.Substring(Math.Max(0, number.Length - 4));
        }
    }
}
