using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.SubjectSearchRecipeItem;


namespace Fugitive4.Web.MapperProfiles
{
    public class SubjectSearchRecipeItemProfile : Profile
    {
        protected override void Configure()
        {
			CreateMap<SubjectSearchRecipeItem, SubjectSearchRecipeItemVm>().ReverseMap();
        }
    }
}
