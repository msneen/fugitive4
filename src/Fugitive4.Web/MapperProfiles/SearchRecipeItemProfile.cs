using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.SearchRecipeItem;


namespace Fugitive4.Web.MapperProfiles
{
    public class SearchRecipeItemProfile : Profile
    {
        protected override void Configure()
        {
			CreateMap<SearchRecipeItem, SearchRecipeItemVm>().ReverseMap();
        }
    }
}
