using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.AspNetUsers_Referrer;


namespace Fugitive4.Web.MapperProfiles
{
    public class AspNetUsers_ReferrerProfile : Profile
    {
        protected override void Configure()
        {
			CreateMap<AspNetUsers_Referrer, AspNetUsers_ReferrerVm>().ReverseMap();
        }
    }
}
