using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.Address;


namespace Fugitive4.Web.MapperProfiles
{
    public class AddressProfile : Profile
    {
        protected override void Configure()
        {
			CreateMap<Address, AddressVm>().ReverseMap();

            CreateMap<IAddress, AddressVm>()
                .ForMember(m => m.Id, opt => opt.MapFrom(f => f.Id))
                .ForMember(m => m.Address1, opt => opt.MapFrom(f => f.Address1))
                .ForMember(m => m.Address2, opt => opt.MapFrom(f => f.Address2))
                .ForMember(m => m.City, opt => opt.MapFrom(f => f.City))
                .ForMember(m => m.State, opt => opt.MapFrom(f => f.State))
                .ForMember(m => m.Zipcode, opt => opt.MapFrom(f => f.Zipcode))
                .ReverseMap();
        }
    }
}
