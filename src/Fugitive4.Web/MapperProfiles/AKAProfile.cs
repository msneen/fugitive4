using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.AKA;


namespace Fugitive4.Web.MapperProfiles
{
    public class AKAProfile : Profile
    {
        protected override void Configure()
        {
			CreateMap<AKA, AKAVm>().ReverseMap();
        }
    }
}
