using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Services.ModelsUnmapped;
using Fugitive4.Web.ViewModels.StoredFile;


namespace Fugitive4.Web.MapperProfiles
{
    public class StoredFileProfile : Profile
    {
        protected override void Configure()
        {
			CreateMap<StoredFile, StoredFileVm>().ReverseMap();

            CreateMap<StoredFileCondensed, StoredFileVm>()
                .ForMember(m=>m.Id, o=>o.MapFrom(c=>c.Id))
                .ForMember(m => m.FileName, o => o.MapFrom(c => c.FileName))
                .ForMember(m => m.Notes, o => o.MapFrom(c => c.Notes))
                .ForMember(m => m.IsSubject, o => o.MapFrom(c => c.IsSubject))
                .ForMember(m => m.SubjectId, o => o.MapFrom(c => c.SubjectId))
                .ReverseMap();
        }
    }
}
