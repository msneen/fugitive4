using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.PhoneNumber;


namespace Fugitive4.Web.MapperProfiles
{
    public class PhoneNumberProfile : Profile
    {
        protected override void Configure()
        {
			CreateMap<PhoneNumber, PhoneNumberVm>().ReverseMap();
        }
    }
}
