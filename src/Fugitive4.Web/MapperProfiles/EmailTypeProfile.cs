using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.EmailType;


namespace Fugitive4.Web.MapperProfiles
{
    public class EmailTypeProfile : Profile
    {
        protected override void Configure()
        {
			CreateMap<EmailType, EmailTypeVm>().ReverseMap();
        }
    }
}
