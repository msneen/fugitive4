using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.SearchRecipe;


namespace Fugitive4.Web.MapperProfiles
{
    public class SearchRecipeProfile : Profile
    {
        protected override void Configure()
        {
			CreateMap<SearchRecipe, SearchRecipeVm>().ReverseMap();
        }
    }
}
