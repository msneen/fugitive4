using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.RelationshipType;


namespace Fugitive4.Web.MapperProfiles
{
    public class RelationshipTypeProfile : Profile
    {
        protected override void Configure()
        {
			CreateMap<RelationshipType, RelationshipTypeVm>().ReverseMap();
        }
    }
}
