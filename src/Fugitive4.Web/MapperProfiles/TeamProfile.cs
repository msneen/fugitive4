using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.Team;


namespace Fugitive4.Web.MapperProfiles
{
    public class TeamProfile : Profile
    {
        protected override void Configure()
        {
			CreateMap<Team, TeamVm>().ReverseMap();
        }
    }
}
