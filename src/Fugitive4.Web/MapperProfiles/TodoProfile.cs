using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.Todo;


namespace Fugitive4.Web.MapperProfiles
{
    public class TodoProfile : Profile
    {
        protected override void Configure()
        {
			CreateMap<Todo, TodoVm>().ReverseMap();
        }
    }
}
