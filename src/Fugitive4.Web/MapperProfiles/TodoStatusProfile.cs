using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.TodoStatus;


namespace Fugitive4.Web.MapperProfiles
{
    public class TodoStatusProfile : Profile
    {
        protected override void Configure()
        {
			CreateMap<TodoStatus, TodoStatusVm>().ReverseMap();
        }
    }
}
