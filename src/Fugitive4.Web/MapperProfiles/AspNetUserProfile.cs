using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.AspNetUser;


namespace Fugitive4.Web.MapperProfiles
{
    public class AspNetUserProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<ApplicationUser, ApplicationUserVm>()
                .ForAllMembers(opt => opt.Ignore());

            CreateMap<ApplicationUser, ApplicationUserVm>()
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id));

            CreateMap<ApplicationUserVm, ApplicationUser>()
                .ForAllMembers(opt => opt.Ignore());

            CreateMap<ApplicationUserVm, ApplicationUser>()
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.UserName))
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id));
        }
    }
}
