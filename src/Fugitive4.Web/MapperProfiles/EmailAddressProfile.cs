using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.EmailAddress;


namespace Fugitive4.Web.MapperProfiles
{
    public class EmailAddressProfile : Profile
    {
        protected override void Configure()
        {
			CreateMap<EmailAddress, EmailAddressVm>().ReverseMap();
        }
    }
}
