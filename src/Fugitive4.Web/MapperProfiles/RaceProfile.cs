using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.Race;


namespace Fugitive4.Web.MapperProfiles
{
    public class RaceProfile : Profile
    {
        protected override void Configure()
        {
			CreateMap<Race, RaceVm>().ReverseMap();
        }
    }
}
