using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.Subject;
using Fugitive4.Web.ViewModels.SubjectForBondsman;

namespace Fugitive4.Web.MapperProfiles
{
    public class SubjectForBondsmanProfile : Profile
    {
        protected override void Configure()
        {
			CreateMap<Subject, SubjectForBondsmanVm>().ReverseMap();
        }
    }
}
