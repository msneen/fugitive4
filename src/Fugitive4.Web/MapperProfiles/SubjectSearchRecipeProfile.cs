using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.SubjectSearchRecipe;


namespace Fugitive4.Web.MapperProfiles
{
    public class SubjectSearchRecipeProfile : Profile
    {
        protected override void Configure()
        {
			CreateMap<SubjectSearchRecipe, SubjectSearchRecipeVm>().ReverseMap();
        }
    }
}
