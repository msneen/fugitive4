using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.Vehicle;


namespace Fugitive4.Web.MapperProfiles
{
    public class VehicleProfile : Profile
    {
        protected override void Configure()
        {
			CreateMap<Vehicle, VehicleVm>().ReverseMap();
        }
    }
}
