using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.Employer;


namespace Fugitive4.Web.MapperProfiles
{
    public class EmployerProfile : Profile
    {
        protected override void Configure()
        {
			CreateMap<Employer, EmployerVm>().ReverseMap();
        }
    }
}
