using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.Gender;


namespace Fugitive4.Web.MapperProfiles
{
    public class GenderProfile : Profile
    {
        protected override void Configure()
        {
			CreateMap<Gender, GenderVm>().ReverseMap();
        }
    }
}
