﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Data.Entity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.SampleData;
using Fugitive4.Services.Services;
using Fugitive4.Web.Services;
using Microsoft.Extensions.DependencyInjection.Extensions;
using AutoMapper;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Authorization;
using Fugitive4.Services.Enums;

namespace Fugitive4.Web
{
    public class Startup
    {
        private MapperConfiguration _mapperConfiguration { get; set; }
        private IHostingEnvironment _env { get; set; }

        public Startup(IHostingEnvironment env)
        {
            try
            {
                _env = env;
                // Set up configuration sources.

                var builder = new ConfigurationBuilder()
                    .AddJsonFile("appsettings.json")
                    .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

                if (env.IsDevelopment())
                {
                    // For more details on using the user secret store see http://go.microsoft.com/fwlink/?LinkID=532709
                    builder.AddUserSecrets();

                    // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                    builder.AddApplicationInsightsSettings(developerMode: true);
                }

                builder.AddEnvironmentVariables();
                Configuration = builder.Build();


                RegisterAutomapperProfiles();
            }
            catch (Exception) { }

        }

        private void RegisterAutomapperProfiles()
        {
            try
            {

                _mapperConfiguration = new MapperConfiguration(cfg =>
                {
                    cfg.AddProfile<MapperProfiles.SubjectProfile>();
                    cfg.AddProfile<MapperProfiles.ReferrerProfile>();
                    cfg.AddProfile<MapperProfiles.AddressProfile>();
                    cfg.AddProfile<MapperProfiles.AddressTypeProfile>();
                    cfg.AddProfile<MapperProfiles.GenderProfile>();
                    cfg.AddProfile<MapperProfiles.PhoneTypeProfile>();
                    cfg.AddProfile<MapperProfiles.PhoneNumberProfile>();           
                    cfg.AddProfile<MapperProfiles.EmailTypeProfile>();
                    cfg.AddProfile<MapperProfiles.EmailAddressProfile>();
                    cfg.AddProfile<MapperProfiles.BailBondProfile>();
                    cfg.AddProfile<MapperProfiles.VehicleProfile>();
                    cfg.AddProfile<MapperProfiles.ActivityNoteProfile>();
                    cfg.AddProfile<MapperProfiles.DriverLicenseProfile>();
                    cfg.AddProfile<MapperProfiles.StoredFileProfile>();
                    cfg.AddProfile<MapperProfiles.HyperlinkProfile>();
                    cfg.AddProfile<MapperProfiles.SearchResultProfile>();
                    cfg.AddProfile<MapperProfiles.VideoProfile>();
                    cfg.AddProfile<MapperProfiles.RaceProfile>();
                    cfg.AddProfile<MapperProfiles.RelationshipTypeProfile>();
                    cfg.AddProfile<MapperProfiles.SubjectRelationshipProfile>();
                    cfg.AddProfile<MapperProfiles.BookingChargeProfile>();
                    cfg.AddProfile<MapperProfiles.AKAProfile>();
                    cfg.AddProfile<MapperProfiles.SearchRecipeItemProfile>();
                    cfg.AddProfile<MapperProfiles.SearchRecipeProfile>();
                    cfg.AddProfile<MapperProfiles.SubjectSearchRecipeProfile>();
                    cfg.AddProfile<MapperProfiles.SubjectSearchRecipeItemProfile>();
                    cfg.AddProfile<MapperProfiles.EmployerProfile>();
                    cfg.AddProfile<MapperProfiles.TeamProfile>();
                    cfg.AddProfile<MapperProfiles.SubjectForBondsmanProfile>();
                    cfg.AddProfile<MapperProfiles.AspNetUsers_ReferrerProfile>();
                    cfg.AddProfile<MapperProfiles.TodoStatusProfile>();
                    cfg.AddProfile<MapperProfiles.TodoProfile>();
                });            }
            catch (Exception) { }


        }

        public IConfigurationRoot Configuration { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            try
            {
                // Add framework services.
                services.AddApplicationInsightsTelemetry(Configuration);

                services.AddEntityFramework()
                    .AddSqlServer()
                    .AddDbContext<ApplicationDbContext>(options =>
                        options.UseSqlServer(Configuration["Data:DefaultConnection:ConnectionString"]));

                services.AddIdentity<ApplicationUser, IdentityRole>()
                    .AddEntityFrameworkStores<ApplicationDbContext>()
                    .AddDefaultTokenProviders();

                services.AddMvc();

                if (!_env.IsDevelopment())
                {
                    services.Configure<MvcOptions>(options =>
                    {
                        options.Filters.Add(new RequireHttpsAttribute());
                    });
                }

                services.Configure<IdentityOptions>(options =>
                {
                    options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(10);
                    options.Lockout.MaxFailedAccessAttempts = 10;
                });

                services.Configure<AuthorizationOptions>(options =>
                {
                    options.AddPolicy("ManageTeams", policy => policy.RequireClaim("TeamAdmin", "TeamAdmin"));
                    options.AddPolicy("ManageSearchRecipes", policy => policy.RequireClaim("SearchRecipeAdmin", "SearchRecipeAdmin"));
                });

                // Add application services.
                services.AddTransient<IEmailSender, AuthMessageSender>();
                services.AddTransient<ISmsSender, AuthMessageSender>();            
                services.AddOptions();
                services.AddCaching();

                services.Configure<AuthMessageSMSSenderOptions>(options =>
                {
                    options.SID = Configuration["Twilio:AccountSid"];
                    options.AuthToken = Configuration["Twilio:AuthToken"];
                    options.SendNumber = Configuration["Twilio:From"];
                });

                DependencyRegistration.Register(services);
                services.AddSingleton<IMapper>(sp => _mapperConfiguration.CreateMapper());
            }
            catch (Exception) { }
           
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public async void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory,
            IServiceProvider serviceProvider, ApplicationDbContext context)
        {
            try
            {
                loggerFactory.AddConsole(Configuration.GetSection("Logging"));
                loggerFactory.AddDebug();

                app.UseApplicationInsightsRequestTelemetry();
                //app.UseDeveloperExceptionPage();
                //app.UseExceptionHandler("/Home/Error");
                app.UseStatusCodePagesWithRedirects("/Home/Error/{0}");
                if (env.IsDevelopment())
                {
                    app.UseBrowserLink();
                    //app.UseDeveloperExceptionPage();
                    app.UseDatabaseErrorPage();
                }
                else
                {
                    //app.UseDeveloperExceptionPage();
                    //app.UseExceptionHandler("/Home/Error");

                    // For more details on creating database during deployment see http://go.microsoft.com/fwlink/?LinkID=615859
                    try
                    {
                        using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>()
                            .CreateScope())
                        {
                            serviceScope.ServiceProvider.GetService<ApplicationDbContext>()
                                 .Database.Migrate();
                        }
                    }
                    catch { }
                }

                app.UseIISPlatformHandler(options => options.AuthenticationDescriptions.Clear());

                app.UseApplicationInsightsExceptionTelemetry();

                app.UseStaticFiles();

                app.UseIdentity();
                
            

                // To configure external authentication please see http://go.microsoft.com/fwlink/?LinkID=532715

                app.UseMvc(routes =>
                {
                    routes.MapRoute(
                        name: "default",
                        template: "{controller}/{action}/{id?}",
                        defaults: new { controller = "Subject", action = "Index" });

                });

                


                ReferrerSampleData.Initialize(app.ApplicationServices);
                AddressTypeSampleData.Initialize(app.ApplicationServices);
                GenderSampleData.Initialize(app.ApplicationServices);
                PhoneTypeSampleData.Initialize(app.ApplicationServices);
                EmailTypeSampleData.Initialize(app.ApplicationServices);
                RaceSampleData.Initialize(app.ApplicationServices);
                RelationshipTypeSampleData.Initialize(app.ApplicationServices);
                TeamSampleData.Initialize(app.ApplicationServices);
                TodoStatusSampleData.Initialize(app.ApplicationServices);
                SubjectSampleData.Initialize(app.ApplicationServices);

                // Method 2: Call to create user role
                //var userRolesCreated = await CreateRoles(context, serviceProvider);
            }
            catch (Exception) { }



        }

        //Method 2: Use RoleManager to create user role
        private async Task<bool> CreateRoles(ApplicationDbContext context, IServiceProvider serviceProvider)
        {

            var userManager = serviceProvider.GetRequiredService<UserManager<ApplicationUser>>();
            var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();

            var rolesCreated = await CreateRoles(context, roleManager);

            var roleClaimsCreated = await CreateAdminRoleClaims(roleManager);

            //var usersCreated = await CreateUser(context, userManager);

            return true;
        }

        private static async Task<bool> CreateRoles(ApplicationDbContext context, RoleManager<IdentityRole> roleManager)
        {
            // First, Creating User role as each role in User Manager  
            List<IdentityRole> roles = new List<IdentityRole>();
            roles.Add(new IdentityRole { Name = RoleName.Admin.Name(), NormalizedName = RoleName.Admin.Description() }); //roles.Add(new IdentityRole {Name = "Admin", NormalizedName = "ADMINISTRATOR"});
            roles.Add(new IdentityRole { Name = RoleName.Member.Name(), NormalizedName = RoleName.Member.Description() });//roles.Add(new IdentityRole {Name = "Member", NormalizedName = "MEMBER"});
            roles.Add(new IdentityRole { Name = RoleName.BailBondsman.Name(), NormalizedName = RoleName.BailBondsman.Description() }); //roles.Add(new IdentityRole {Name = "BailBondsman", NormalizedName = "BAILBONDSMAN"});

            //Then, the machine added Default User as the Admin user role
            foreach (var role in roles)
            {
                var roleExit = await roleManager.RoleExistsAsync(role.NormalizedName);
                if (!roleExit)
                {
                    context.Roles.Add(role);
                    var saved = context.SaveChanges();
                }
            }
            return true;
        }

        private static async Task<bool> CreateAdminRoleClaims(RoleManager<IdentityRole> roleManager)
        {
            var existingAdminRole =
                await roleManager.Roles.Where(r => r.NormalizedName == RoleName.Admin.Description()).FirstOrDefaultAsync();
            if (existingAdminRole != null)
            {
                var claims = await roleManager.GetClaimsAsync(existingAdminRole);
                if (!claims.Any(c => c.Value == ClaimName.TeamAdmin.Name()))
                {
                    var claimAdded = await roleManager.AddClaimAsync(existingAdminRole, new System.Security.Claims.Claim(ClaimName.TeamAdmin.Name(), ClaimName.TeamAdmin.Name()));
                }
                if (!claims.Any(c => c.Value == ClaimName.SearchRecipeAdmin.Name()))
                {
                    var claimAdded = await
                        roleManager.AddClaimAsync(existingAdminRole,
                            new System.Security.Claims.Claim(ClaimName.SearchRecipeAdmin.Name(), ClaimName.SearchRecipeAdmin.Name()));
                }
            }

            return true;
        }

        //Related to method 2
        private async Task<bool> CreateUser(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            var adminUser = await userManager.FindByEmailAsync("michael.sneen@gmail.com");
            if (adminUser != null)
            {
                var isAdmin = (await userManager.IsInRoleAsync(adminUser, "Admin"));
                if (!isAdmin)
                {
                    var adminAdded = await userManager.AddToRoleAsync(adminUser, "Admin");
                }
                //Add Claims Here using userManager
                var claims = await userManager.GetClaimsAsync(adminUser);
                if(!claims.Any(c=>c.Value == "TeamAdmin"))
                {
                    var teamAdmin = await userManager.AddClaimAsync(adminUser, new System.Security.Claims.Claim("TeamAdmin", "TeamAdmin"));
                }
                if (!claims.Any(c => c.Value == "SearchRecipeAdmin"))
                {
                    var searchRecipeAdmin = await userManager.AddClaimAsync(adminUser, new System.Security.Claims.Claim("SearchRecipeAdmin", "SearchRecipeAdmin"));
                }
            }
            return true;
        }

        // Entry point for the application.
        public static void Main(string[] args) => WebApplication.Run<Startup>(args);
    }
}
