﻿using Microsoft.AspNet.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fugitive4.Web.Extensions
{
    public static class ControllerExtensions
    {
        public static IEnumerable<SelectListItem> ToSelectList<T>(this IEnumerable<T> collection, Func<T, string> value, Func<T, string> text )
        {
            return collection.ToSelectList(value, text, x => false);
        }


        public static IEnumerable<SelectListItem> ToSelectList<T>(this IEnumerable<T> collection, Func<T, string> value, Func<T, string> text,  Func<T, bool> selected)
        {
            return (from item in collection
                    select new SelectListItem()
                    {
                        Text = text(item),
                        Value = value(item),
                        Selected = selected(item)
                    });
        }

        public static IEnumerable<SelectListItem> ToSelectList<T>(this IEnumerable<T> collection, Func<T, string> value, Func<T, string> text, string group)
        {
            var selectGroup = new SelectListGroup { Disabled = false, Name = group };

            return (from item in collection
                    select new SelectListItem()
                    {
                        Text = text(item),
                        Value = value(item),
                        Group = selectGroup
                    });
        }
    }
}
