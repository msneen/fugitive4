using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Mvc.Rendering;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Web.ViewModels.AspNetUser; //This is any parent or child vm
using Fugitive4.Web.ViewModels.Referrer; //This is any parent or child vm

namespace Fugitive4.Web.ViewModels.AspNetUsers_Referrer
{
	public class AspNetUsers_ReferrerVm : AuditableEntityBase
    {
		
		[Display(Name="Id")]	
		public int Id { get; set; }

		
		[Display(Name="Referrer Id")]	
		public int ReferrerId { get; set; }

		[Required]
		[StringLength(450, MinimumLength = 2, ErrorMessage = "field must be between 2 and 450 characters")]		
		[Display(Name="Asp Net Users Id")]	
		public string AspNetUsersId { get; set; }


		
        public ApplicationUserVm ApplicationUser { get; set; } //Parent
        public List<SelectListItem> AspNetUserList { get; set; } //Parent
		
        public ReferrerVm Referrer { get; set; } //Parent
        public List<SelectListItem> ReferrerList { get; set; } //Parent
    }

    public class AspNetUsers_ReferrerListVm
    {
        public IEnumerable<AspNetUsers_ReferrerVm> AspNetUsers_Referrers { get; set; }
				
		public int AspNetUserId { get; set; }
		public List<SelectListItem> AspNetUserList { get; set; }
		
		public int ReferrerId { get; set; }
		public List<SelectListItem> ReferrerList { get; set; }
    }
}
