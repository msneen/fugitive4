﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fugitive4.Web.ViewModels.SubjectRelationship
{
    public class SubjectSummary
    {
        public string NodeId { get; set; }
        public string NodeName { get; set; }
        public bool IsPrimaryNode { get; set; }
    }
}
