﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fugitive4.Web.ViewModels.SubjectRelationship
{
    public class RelationshipVm
    {
        public RelationshipVm()
        {
            Weight = "2";
        }
        public string subjectId1 { get; set; }
        public string subjectId2 { get; set; }
        public string Weight { get; set; }
    }
}
