﻿namespace Fugitive4.Web.ViewModels.SubjectRelationship
{
    public class ReorderVm
    {
        public int SubjectId { get; set; }
        public int[] SubjectRelationshipIds { get; set; }
    }
}
