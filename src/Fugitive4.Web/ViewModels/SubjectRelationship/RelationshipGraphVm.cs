﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fugitive4.Web.ViewModels.SubjectRelationship
{
    public class RelationshipGraphVm
    {
        public List<SubjectSummary> SubjectIds { get; set; }
        public List<RelationshipVm> Relationships { get; set; }
    }
}
