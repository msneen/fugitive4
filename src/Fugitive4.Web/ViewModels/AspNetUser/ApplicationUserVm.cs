using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Mvc.Rendering;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Web.ViewModels.Team;

namespace Fugitive4.Web.ViewModels.AspNetUser
{
	public class ApplicationUserVm : AuditableEntityBase
    {
		[Required]
		[StringLength(450, MinimumLength = 2, ErrorMessage = "field must be between 2 and 450 characters")]		
		[Display(Name="Id")]	
		public string Id { get; set; }

		
		[Display(Name="Access Failed Count")]	
		public int AccessFailedCount { get; set; }

		
		[Display(Name="Concurrency Stamp")]	
		public string ConcurrencyStamp { get; set; }

		[StringLength(256,  ErrorMessage = "field must be less than 256 characters")]		
		[Display(Name="Email")]	
		public string Email { get; set; }

		
		[Display(Name="Email Confirmed")]	
		public bool EmailConfirmed { get; set; }

		
		[Display(Name="Lockout Enabled")]	
		public bool LockoutEnabled { get; set; }

		
		[Display(Name="Lockout End")]	
		public Nullable<System.DateTimeOffset> LockoutEnd { get; set; }

		[StringLength(256,  ErrorMessage = "field must be less than 256 characters")]		
		[Display(Name="Normalized Email")]	
		public string NormalizedEmail { get; set; }

		[StringLength(256,  ErrorMessage = "field must be less than 256 characters")]		
		[Display(Name="Normalized User Name")]	
		public string NormalizedUserName { get; set; }

		
		[Display(Name="Password Hash")]	
		public string PasswordHash { get; set; }

		
		[Display(Name="Phone Number")]	
		public string PhoneNumber { get; set; }

		
		[Display(Name="Phone Number Confirmed")]	
		public bool PhoneNumberConfirmed { get; set; }

		
		[Display(Name="Security Stamp")]	
		public string SecurityStamp { get; set; }

		
		[Display(Name="Two Factor Enabled")]	
		public bool TwoFactorEnabled { get; set; }

		[StringLength(256,  ErrorMessage = "field must be less than 256 characters")]		
		[Display(Name="User Name")]	
		public string UserName { get; set; }


		public IEnumerable<TeamVm> Teams { get; set; }//Child
    }

    public class AspNetUserListVm
    {
        public IEnumerable<ApplicationUserVm> AspNetUsers { get; set; }
	}
}
