using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Mvc.Rendering;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Web.ViewModels.PhoneType; //This is any parent or child vm
using Fugitive4.Web.ViewModels.Subject; //This is any parent or child vm

namespace Fugitive4.Web.ViewModels.PhoneNumber
{
	public class PhoneNumberVm : AuditableEntityBase
    {
		
		[Display(Name="ID")]	
		public int Id { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="Phone Number")]	
		public string PhoneNumber1 { get; set; }

		[Required]
		[StringLength(50, MinimumLength = 2, ErrorMessage = "field must be between 2 and 50 characters")]		
		[Display(Name="Belongs To")]	
		public string BelongsTo { get; set; }

		
		[Display(Name="Subject")]	
		public int SubjectId { get; set; }

		
		[Display(Name="Phone Type")]	
		public int PhoneTypeId { get; set; }

		
		[Display(Name="Notes")]	
		public string Notes { get; set; }


		
        public PhoneTypeVm PhoneType { get; set; } //Parent
        public List<SelectListItem> PhoneTypeList { get; set; } //Parent
		
        public SubjectVm Subject { get; set; } //Parent
        public List<SelectListItem> SubjectList { get; set; } //Parent
    }

    public class PhoneNumberListVm
    {
        public IEnumerable<PhoneNumberVm> PhoneNumbers { get; set; }
				
		public int PhoneTypeId { get; set; }
		public List<SelectListItem> PhoneTypeList { get; set; }
		
		public int SubjectId { get; set; }
		public List<SelectListItem> SubjectList { get; set; }
    }
}
