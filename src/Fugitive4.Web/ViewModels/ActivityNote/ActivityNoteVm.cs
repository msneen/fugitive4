using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Fugitive4.Services.Enums;
using Microsoft.AspNet.Mvc.Rendering;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Web.ViewModels.Address;
using Fugitive4.Web.ViewModels.Subject; //This is any parent or child vm

namespace Fugitive4.Web.ViewModels.ActivityNote
{
	public class ActivityNoteVm : AuditableEntityBase
    {
		
		[Display(Name="Id")]	
		public int Id { get; set; }

		
		[Display(Name="Activity Date")]	
		public System.DateTime ActivityDate { get; set; }

		[StringLength(30,  ErrorMessage = "field must be less than 30 characters")]		
		[Display(Name="Latitude")]	
		public string Latitude { get; set; }

		[StringLength(30,  ErrorMessage = "field must be less than 30 characters")]		
		[Display(Name="Longitude")]	
		public string Longitude { get; set; }

		[StringLength(1000,  ErrorMessage = "field must be less than 10 characters")]		
		[Display(Name="Address")]	
		public string Address { get; set; }

		[StringLength(128,  ErrorMessage = "field must be less than 128 characters")]		
		[Display(Name="Performed By")]	
		public string PerformedBy { get; set; }

		[Required]		
		[Display(Name="Description")]	
		public string Description { get; set; }

		
		[Display(Name="Subject Id")]	
		public int SubjectId { get; set; }

        [Display(Name = "Related Subject")]
        public int? RelatedSubjectId { get; set; }

        [Display(Name = "Related Subject")]
        public SubjectVm RelatedSubject { get; set; }

        [Display(Name = "Related Address")]
        public int? RelatedAddressId { get; set; }

        [Display(Name = "Related Address")]
        public AddressVm RelatedAddress { get; set; }

        [Display(Name = "Related Address Source")]
        public AddressSource RelatedAddressSource { get; set; }

        [Display(Name = "Subject")]
        public SubjectVm Subject { get; set; } //Parent

        [Display(Name = "Subject List")]
        public List<SelectListItem> SubjectList { get; set; } //Parent

        [Display(Name = "Address List")]
        public List<SelectListItem> AddressList { get; set; }//Custom 

        [Display(Name = "Last Name")]
        public string Lastname { get; set; }//Custom 

        [Display(Name = "First Name")]
        public string Firstname { get; set; }//Custom

        [Display(Name = "Race")]
        public int RaceId { get; set; }

        [Display(Name = "Gender")]
        public int GenderId { get; set; }

        [Display(Name = "Gender List")]
        public List<SelectListItem> GenderList { get; set; }

        [Display(Name = "Race List")]
        public List<SelectListItem> RaceList { get; set; }

    }

    public class ActivityNoteListVm
    {
        public IEnumerable<ActivityNoteVm> ActivityNotes { get; set; }

        [Display(Name = "Subject ID")]
        public int SubjectId { get; set; }

        [Display(Name = "Subject List")]
        public List<SelectListItem> SubjectList { get; set; }
    }
}
