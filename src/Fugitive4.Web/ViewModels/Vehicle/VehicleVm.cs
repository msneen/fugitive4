using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Mvc.Rendering;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Web.ViewModels.Subject; //This is any parent or child vm

namespace Fugitive4.Web.ViewModels.Vehicle
{
	public class VehicleVm : AuditableEntityBase
    {
		
		[Display(Name="ID")]	
		public int Id { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="Make")]	
		public string Make { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="Model")]	
		public string Model { get; set; }

		[StringLength(10,  ErrorMessage = "field must be less than 10 characters")]		
		[Display(Name="Year")]	
		public string Year { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="Color")]	
		public string Color { get; set; }

		[StringLength(500,  ErrorMessage = "field must be less than 500 characters")]		
		[Display(Name="Description")]	
		public string Description { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="Belongs To")]	
		public string BelongsTo { get; set; }

		
		[Display(Name="Subject")]	
		public int SubjectId { get; set; }

		[StringLength(20,  ErrorMessage = "field must be less than 20 characters")]		
		[Display(Name="Plate Number")]	
		public string PlateNumber { get; set; }


		
        public SubjectVm Subject { get; set; } //Parent
        public List<SelectListItem> SubjectList { get; set; } //Parent
    }

    public class VehicleListVm
    {
        public IEnumerable<VehicleVm> Vehicles { get; set; }
				
		public int SubjectId { get; set; }
		public List<SelectListItem> SubjectList { get; set; }
    }
}
