using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Fugitive4.Services.Models;
using Microsoft.AspNet.Mvc.Rendering;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Web.ViewModels.Subject; //This is any parent or child vm

namespace Fugitive4.Web.ViewModels.DriverLicense
{
	public class DriverLicenseVm : AuditableEntityBase, IAddress
    {
		
		[Display(Name="Id")]	
		public int Id { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="DL Number")]	
		public string DLNumber { get; set; }

		[StringLength(100,  ErrorMessage = "field must be less than 100 characters")]		
		[Display(Name="Full Name")]	
		public string FullName { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="Issuing State")]	
		public string IssuingState { get; set; }

		
		[Display(Name="Issue Date")]	
		public Nullable<System.DateTime> IssueDate { get; set; }

		
		[Display(Name="Expiration Date")]	
		public Nullable<System.DateTime> ExpirationDate { get; set; }

		
		[Display(Name="Notes")]	
		public string Notes { get; set; }

		
		[Display(Name="Subject")]	
		public int SubjectId { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="Address 1")]	
		public string Address1 { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="Address 2")]	
		public string Address2 { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="City")]	
		public string City { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="State")]	
		public string State { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="Zip Code")]	
		public string Zipcode { get; set; }


		
        public SubjectVm Subject { get; set; } //Parent
        public List<SelectListItem> SubjectList { get; set; } //Parent
    }

    public class DriverLicenseListVm
    {
        public IEnumerable<DriverLicenseVm> DriverLicenses { get; set; }
				
		public int SubjectId { get; set; }
		public List<SelectListItem> SubjectList { get; set; }
    }
}
