using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Mvc.Rendering;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Web.ViewModels.Subject; //This is any parent or child vm
using Fugitive4.Web.ViewModels.BookingCharge; //This is any parent or child vm

namespace Fugitive4.Web.ViewModels.BailBond
{
	public class BailBondVm : AuditableEntityBase
    {
		
		[Display(Name="Id")]	
		public int Id { get; set; }

		
		[Display(Name="Original Arrest Date")]	
		public Nullable<System.DateTime> OriginalArrestDate { get; set; }

		
		[Display(Name="Issue Date")]	
		public Nullable<System.DateTime> IssueDate { get; set; }

		
		[Display(Name="Default Date")]	
		public Nullable<System.DateTime> DefaultDate { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="Issuing Bondsman")]	
		public string IssuingBondsman { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="Issuing Bond Company")]	
		public string IssuingBondCompany { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="Court")]	
		public string Court { get; set; }

		
		[Display(Name="Description")]	
		public string Description { get; set; }

		
		[Display(Name="Subject")]	
		public int SubjectId { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="Booking Number")]	
		public string BookingNumber { get; set; }

		
		[Display(Name="Bond Amount")]	
		public Nullable<decimal> BondAmount { get; set; }

		
		[Display(Name="Payment Amount")]	
		public Nullable<decimal> PaymentAmount { get; set; }

		
		[Display(Name="Payment Date")]	
		public Nullable<System.DateTime> PaymentDate { get; set; }

		
		[Display(Name="Done")]	
		public bool Done { get; set; }


		
        public SubjectVm Subject { get; set; } //Parent
        public List<SelectListItem> SubjectList { get; set; } //Parent
		public IEnumerable<BookingChargeVm> BookingCharges { get; set; }//Child
    }

    public class BailBondListVm
    {
        public IEnumerable<BailBondVm> BailBonds { get; set; }
				
		public int SubjectId { get; set; }
		public List<SelectListItem> SubjectList { get; set; }
    }
}
