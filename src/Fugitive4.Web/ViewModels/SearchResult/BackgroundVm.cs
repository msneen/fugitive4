﻿using Fugitive4.Web.ViewModels.SubjectSearchRecipe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fugitive4.Web.ViewModels.SearchResult
{
    public class BackgroundVm
    {
        public int SubjectId { get; set; }
        public SubjectSearchRecipeListVm SubjectSearchRecipeListVm { get;set;}

        public List<SearchResultVm> SearchResultWithoutParentVms { get; set; }
    }
}
