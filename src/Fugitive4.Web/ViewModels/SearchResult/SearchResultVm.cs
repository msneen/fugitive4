using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Mvc.Rendering;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Web.ViewModels.Subject; //This is any parent or child vm
using Fugitive4.Web.ViewModels.SubjectSearchRecipeItem; //This is any parent or child vm

namespace Fugitive4.Web.ViewModels.SearchResult
{
	public class SearchResultVm : AuditableEntityBase
    {
		
		[Display(Name="ID")]	
		public int Id { get; set; }

		[Required]
		[StringLength(250, MinimumLength = 2, ErrorMessage = "field must be between 2 and 250 characters")]		
		[Display(Name="Url Or Source")]	
		public string UrlOrSource { get; set; }

		
		[Display(Name="Result Text")]	
		public string ResultText { get; set; }

		[StringLength(128,  ErrorMessage = "field must be less than 128 characters")]		
		[Display(Name="Username")]	
		public string Username { get; set; }

		
		[Display(Name="Notes")]	
		public string Notes { get; set; }

		
		[Display(Name="Subject")]	
		public int SubjectId { get; set; }

		
		[Display(Name="Subject Search Recipe Item")]	
		public Nullable<int> SubjectSearchRecipeItemId { get; set; }

		
		[Display(Name="Cost")]	
		public Nullable<decimal> Cost { get; set; }


		
        public SubjectVm Subject { get; set; } //Parent
        public List<SelectListItem> SubjectList { get; set; } //Parent
		
        public SubjectSearchRecipeItemVm SubjectSearchRecipeItem { get; set; } //Parent
        public List<SelectListItem> SubjectSearchRecipeItemList { get; set; } //Parent
    }

    public class SearchResultListVm
    {
        public IEnumerable<SearchResultVm> SearchResults { get; set; }
				
		public int SubjectId { get; set; }
		public List<SelectListItem> SubjectList { get; set; }
		
		public int SubjectSearchRecipeItemId { get; set; }
		public List<SelectListItem> SubjectSearchRecipeItemList { get; set; }
    }
}
