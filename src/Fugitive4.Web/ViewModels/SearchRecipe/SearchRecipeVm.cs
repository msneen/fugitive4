using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Mvc.Rendering;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Web.ViewModels.SearchRecipeItem; //This is any parent or child vm
using Fugitive4.Web.ViewModels.SubjectSearchRecipe; //This is any parent or child vm

namespace Fugitive4.Web.ViewModels.SearchRecipe
{
	public class SearchRecipeVm : AuditableEntityBase
    {
		
		[Display(Name="ID")]	
		public int Id { get; set; }

		[Required]
		[StringLength(50, MinimumLength = 2, ErrorMessage = "field must be between 2 and 50 characters")]		
		[Display(Name="Name")]	
		public string Name { get; set; }

		
		[Display(Name="Description")]	
		public string Description { get; set; }

		
		[Display(Name="Disabled")]	
		public bool Disabled { get; set; }

		
		[Display(Name="Created By")]	
		public string CreatedBy { get; set; }

		
		[Display(Name="Created Date")]	
		public System.DateTime CreatedDate { get; set; }

		
		[Display(Name="Modified By")]	
		public string ModifiedBy { get; set; }

		
		[Display(Name="Modified Date")]	
		public Nullable<System.DateTime> ModifiedDate { get; set; }


		public IEnumerable<SearchRecipeItemVm> SearchRecipeItems { get; set; }//Child
		public IEnumerable<SubjectSearchRecipeVm> SubjectSearchRecipes { get; set; }//Child
    }

    public class SearchRecipeListVm
    {
        public IEnumerable<SearchRecipeVm> SearchRecipes { get; set; }
		    }
}
