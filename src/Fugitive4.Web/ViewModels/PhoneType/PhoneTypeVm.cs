using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Mvc.Rendering;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Web.ViewModels.PhoneNumber; //This is any parent or child vm

namespace Fugitive4.Web.ViewModels.PhoneType
{
	public class PhoneTypeVm : AuditableEntityBase
    {
		
		[Display(Name="ID")]	
		public int Id { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="Name")]	
		public string Name { get; set; }


		public IEnumerable<PhoneNumberVm> PhoneNumbers { get; set; }//Child
    }

    public class PhoneTypeListVm
    {
        public IEnumerable<PhoneTypeVm> PhoneTypes { get; set; }
	}
}
