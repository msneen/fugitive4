using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Mvc.Rendering;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Web.ViewModels.Subject; //This is any parent or child vm

namespace Fugitive4.Web.ViewModels.Video
{
	public class VideoVm : AuditableEntityBase
    {
		
		[Display(Name="ID")]	
		public int Id { get; set; }

		[StringLength(250,  ErrorMessage = "field must be less than 250 characters")]		
		[Display(Name="URL")]	
		public string Url { get; set; }

		
		[Display(Name="Notes")]	
		public string Notes { get; set; }

		
		[Display(Name="Subject")]	
		public int SubjectId { get; set; }


		
        public SubjectVm Subject { get; set; } //Parent
        public List<SelectListItem> SubjectList { get; set; } //Parent
    }

    public class VideoListVm
    {
        public IEnumerable<VideoVm> Videos { get; set; }
				
		public int SubjectId { get; set; }
		public List<SelectListItem> SubjectList { get; set; }
    }
}
