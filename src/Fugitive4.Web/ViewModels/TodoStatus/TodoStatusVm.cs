using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Mvc.Rendering;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Web.ViewModels.Todo; //This is any parent or child vm

namespace Fugitive4.Web.ViewModels.TodoStatus
{
	public class TodoStatusVm : AuditableEntityBase
    {
		
		[Display(Name="Id")]	
		public int Id { get; set; }

		[Required]
		[StringLength(250, MinimumLength = 2, ErrorMessage = "field must be between 2 and 250 characters")]		
		[Display(Name="Name")]	
		public string Name { get; set; }


		public IEnumerable<TodoVm> Todoes { get; set; }//Child
    }

    public class TodoStatusListVm
    {
        public IEnumerable<TodoStatusVm> TodoStatuses { get; set; }
		    }
}
