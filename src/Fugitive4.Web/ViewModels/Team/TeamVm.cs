using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Mvc.Rendering;
using Fugitive4.Services.Models.Bases;
//using Fugitive4.Web.ViewModels.AspNetUsers_Team; //This is any parent or child vm
using Fugitive4.Web.ViewModels.Team; //This is any parent or child vm
using Fugitive4.Web.ViewModels.AspNetUser;
using Fugitive4.Web.ViewModels.Referrer;

namespace Fugitive4.Web.ViewModels.Team
{
	public class TeamVm : AuditableEntityBase
    {
		
		[Display(Name="ID")]	
		public int Id { get; set; }

		[Required]
		[StringLength(50, MinimumLength = 2, ErrorMessage = "field must be between 2 and 50 characters")]		
		[Display(Name="Name")]	
		public string Name { get; set; }

		
		[Display(Name="Description")]	
		public string Description { get; set; }

		public IEnumerable<ApplicationUserVm> AspNetUserVms { get; set; }//Child
        [Display(Name = "User List")]
        public List<SelectListItem> UserList { get; set; } //Parent
        [Display(Name = "User")]
        public string[] UserIds { get; set; }

        public IEnumerable<ReferrerVm> ReferrerVms { get; set; }//Child
        [Display(Name = "Referrer List")]
        public List<SelectListItem> ReferrerList { get; set; } //Parent
        [Display(Name = "Referrer")]
        public int[] ReferrerIds { get; set; }
    }

    public class TeamListVm
    {
        public IEnumerable<TeamVm> Teams { get; set; }
	}
}
