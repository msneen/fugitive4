﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fugitive4.Web.ViewModels.Subject
{
    public class GeoMapVm
    {
        public List<SubjectMapAddressVm> Addresses { get; set; }
        public List<SubjectMapLatLongVm> Locations { get; set; }
    }
    public class SubjectMapLatLongVm: SubjectMapBase
    {
        public string Latitude { get; set; }
        public string Longitude { get; set; }

    }

    public class SubjectMapAddressVm: SubjectMapBase
    {
        public string Address { get; set; }
    }

    public class SubjectMapBase
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public string Icon { get; set; }
    }
}
