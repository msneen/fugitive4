﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Fugitive4.Web.ViewModels.Subject
{
    public class SubjectSearchVm
    {
        [Display(Name = "Search Criteria")]
        public string SearchCriteria { get; set; }
    }
}
