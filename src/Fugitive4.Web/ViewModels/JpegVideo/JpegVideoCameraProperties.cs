﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Fugitive4.Web.ViewModels.JpegVideo
{
    public class JpegVideoCameraProperties
    {
        public string CameraId { get; set; }

        public bool preview { get; set; }//not currently used

        [Display(Name="Diff%")]
        public int differenceThreshold { get; set; }//percent difference to cause image transmission 5% seems good

        [Display(Name = "Pixel%")]
        public int pixelThreshold { get; set; }//difference between rgb in pixel. 50 seems good

        [Display(Name = "Tail")]
        public int transmitSeconds { get; set; }//number of seconds  to continue transmitting after motion detected and lost

        [Display(Name = "Focus")]
        public int focus { get; set; }//0=autofocus.  10 is minimup

        [Display(Name="ZoomM")]
        public int zoom { get; set; } //based on max zoom of the camera.  S7 is 4

        [Display(Name="ZoomX")]
        public int zoomXOffset { get; set; }// -3 to 3.  0 is centered.  -3 is full left.  3 is full right

        [Display(Name = "ZoomY")]
        public int zoomYOffset { get; set; }// -3 to 3.  0 is centered.  -3 is full top.  3 is full bottom

        [Display(Name = "Max Temp")]
        public int maxTemp { get; set; }//the highest temp the battery can get.  any higher and app stops transmitting

        [Display(Name = "Min Bat%")]
        public int minBatteryPercent { get; set; }

        [Display(Name = "Stop")]
        public int afterDarkHour { get; set; }//24 hour time.  The time the app stops monitoring for the night.  default 20

        [Display(Name = "Start")]
        public int beforeSunriseHour { get; set; }//the time the app wakes up in the morning.  default 6

        public int quality { get; set; }

        public Coordinates movement { get; set; } //The area to watch for motion detection
        public Coordinates view { get; set; } //The area to crop and transmit to the server
    }

    public class Coordinates
    {
        public int x { get; set; }
        public int y { get; set; }
        public int w { get; set; }
        public int h { get; set; }
    }
}
