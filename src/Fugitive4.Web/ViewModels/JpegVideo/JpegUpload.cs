﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fugitive4.Web.ViewModels.JpegVideo
{
    public class JpegUpload
    {
        public byte[] Bytes { get; set; }
        public int Frame { get; set; }
        public DateTime Received { get; set; }
        public DateTime? Viewed { get; set; }
    }

    public class JpegUploads
    {
        public JpegUploads()
        {
            CurrentFrame = -1;
        }
        public List<JpegUpload> JpegUploadList { get; set; }
        public int CurrentFrame { get; set; }
        public byte[] CurrentBytes { get; set; }
        public DateTime? LastFrameChanged { get; set; }
    }
}
