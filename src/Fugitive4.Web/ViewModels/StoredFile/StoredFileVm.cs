using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Mvc.Rendering;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Web.ViewModels.Subject; //This is any parent or child vm

namespace Fugitive4.Web.ViewModels.StoredFile
{
	public class StoredFileVm : AuditableEntityBase
    {
		
		[Display(Name="ID")]	
		public int Id { get; set; }

		[Required]
		[StringLength(255, MinimumLength = 2, ErrorMessage = "field must be between 2 and 255 characters")]		
		[Display(Name="File Name")]	
		public string FileName { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="Content Type")]	
		public string ContentType { get; set; }

		
		[Display(Name="File Content")]	
		public byte[] FileContent { get; set; }

		[StringLength(10,  ErrorMessage = "field must be less than 10 characters")]		
		[Display(Name="File Type")]	
		public string FileType { get; set; }

        [Required]
        [Display(Name="Notes")]	
		public string Notes { get; set; }

		
		[Display(Name="Subject")]	
		public int SubjectId { get; set; }

		
		[Display(Name="Is Subject")]	
		public bool IsSubject { get; set; }

		
		[Display(Name="Show On Detail")]	
		public bool ShowOnDetail { get; set; }


		
        public SubjectVm Subject { get; set; } //Parent
        public List<SelectListItem> SubjectList { get; set; } //Parent
    }

    public class StoredFileListVm
    {
        public IEnumerable<StoredFileVm> StoredFiles { get; set; }
				
		public int SubjectId { get; set; }
		public List<SelectListItem> SubjectList { get; set; }
    }
}
