using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Mvc.Rendering;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Web.ViewModels.SearchRecipe; //This is any parent or child vm
using Fugitive4.Web.ViewModels.Subject;
using Fugitive4.Web.ViewModels.SubjectSearchRecipeItem;

//This is any parent or child vm

namespace Fugitive4.Web.ViewModels.SubjectSearchRecipe
{
	public class SubjectSearchRecipeVm : AuditableEntityBase
    {
		
		[Display(Name="ID")]	
		public int Id { get; set; }

		
		[Display(Name="Subject")]	
		public int SubjectId { get; set; }

		
		[Display(Name="Search Recipe")]	
		public int SearchRecipeId { get; set; }

		
		[Display(Name="Description")]	
		public string Description { get; set; }

		
		[Display(Name="Notes")]	
		public string Notes { get; set; }

		
		[Display(Name="Disabled")]	
		public bool Disabled { get; set; }

		
		[Display(Name="Created By")]	
		public string CreatedBy { get; set; }

		
		[Display(Name="Created Date")]	
		public System.DateTime CreatedDate { get; set; }

		
		[Display(Name="Modified By")]	
		public string ModifiedBy { get; set; }

		
		[Display(Name="Modified Date")]	
		public Nullable<System.DateTime> ModifiedDate { get; set; }


		
        public SearchRecipeVm SearchRecipe { get; set; } //Parent
        public List<SelectListItem> SearchRecipeList { get; set; } //Parent
		
        public SubjectVm Subject { get; set; } //Parent
        public List<SelectListItem> SubjectList { get; set; } //Parent

        public List<SubjectSearchRecipeItemVm> SubjectSearchRecipeItems { get; set; }
    }

    public class SubjectSearchRecipeListVm
    {
        public IEnumerable<SubjectSearchRecipeVm> SubjectSearchRecipes { get; set; }
				
		public int SearchRecipeId { get; set; }
		public List<SelectListItem> SearchRecipeList { get; set; }
		
		public int SubjectId { get; set; }
		public List<SelectListItem> SubjectList { get; set; }
    }
}
