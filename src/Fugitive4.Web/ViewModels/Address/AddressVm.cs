using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Fugitive4.Services.Models;
using Microsoft.AspNet.Mvc.Rendering;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Web.ViewModels.Subject; //This is any parent or child vm
using Fugitive4.Web.ViewModels.AddressType;
using Fugitive4.Web.ViewModels._Interfaces;

//This is any parent or child vm

namespace Fugitive4.Web.ViewModels.Address
{
	public class AddressVm : AuditableEntityBase, IAddress, IHasActivity
    {
		
		[Display(Name="Id")]	
		public int Id { get; set; }

		[StringLength(150,  ErrorMessage = "field must be less than 150 characters")]		
		[Display(Name="Address 1")]	
		public string Address1 { get; set; }

		[StringLength(150,  ErrorMessage = "field must be less than 150 characters")]		
		[Display(Name="Address 2")]	
		public string Address2 { get; set; }

		[StringLength(100,  ErrorMessage = "field must be less than 100 characters")]		
		[Display(Name="City")]	
		public string City { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="State")]	
		public string State { get; set; }

		[StringLength(20,  ErrorMessage = "field must be less than 20 characters")]		
		[Display(Name="Zip Code")]	
		public string Zipcode { get; set; }

		
		[Display(Name="Belongs To Subject")]	
		public bool BelongsToSubject { get; set; }

		
		[Display(Name="Subject")]	
		public int SubjectId { get; set; }

		
		[Display(Name="Address Type")]	
		public int AddressTypeId { get; set; }

		
		[Display(Name="Notes")]	
		public string Notes { get; set; }


        [Display(Name = "Subject")]
        public SubjectVm Subject { get; set; } //Parent
        [Display(Name = "Subject List")]
        public List<SelectListItem> SubjectList { get; set; } //Parent

        [Display(Name = "Address Type")]
        public AddressTypeVm AddressType { get; set; } //Parent
        [Display(Name = "Address Type List")]
        public List<SelectListItem> AddressTypeList { get; set; } //Parent

        [Display(Name = "Activity Notes")]
        public List<ActivityNote.ActivityNoteVm> ActivityNotes { get; set; }//custom
    }

    public class AddressListVm
    {
        public IEnumerable<AddressVm> Addresses { get; set; }
				
		public int SubjectId { get; set; }
		public List<SelectListItem> SubjectList { get; set; }
		
		public int AddressTypeId { get; set; }
		public List<SelectListItem> AddressTypeList { get; set; }
    }
}
