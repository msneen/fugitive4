﻿namespace Fugitive4.Web.ViewModels.Todo
{
    public class ReorderVm
    {
        public int SubjectId { get; set; }
        public int[] TodoIds { get; set; }
    }
}
