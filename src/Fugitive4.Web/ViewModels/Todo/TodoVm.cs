using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Mvc.Rendering;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Web.ViewModels.Subject; //This is any parent or child vm
using Fugitive4.Web.ViewModels.TodoStatus; //This is any parent or child vm

namespace Fugitive4.Web.ViewModels.Todo
{
	public class TodoVm : AuditableEntityBase
    {
		
		[Display(Name="Id")]	
		public int Id { get; set; }

		[Required]
		[StringLength(250, MinimumLength = 2, ErrorMessage = "field must be between 2 and 250 characters")]		
		[Display(Name="Synopsis")]	
		public string Synopsis { get; set; }

		
		[Display(Name="Description")]	
		public string Description { get; set; }

		
		[Display(Name="Notes")]	
		public string Notes { get; set; }

		[Required]
		[StringLength(450, MinimumLength = 2, ErrorMessage = "field must be between 2 and 450 characters")]		
		[Display(Name="Assigned To")]	
		public string AssignedTo { get; set; }

		
		[Display(Name="Done")]	
		public bool Done { get; set; }

		
		[Display(Name="Todo Status Id")]	
		public int TodoStatusId { get; set; }

		
		[Display(Name="Subject Id")]	
		public Nullable<int> SubjectId { get; set; }

		
		[Display(Name="Sort Order")]	
		public Nullable<int> SortOrder { get; set; }


		
        public SubjectVm Subject { get; set; } //Parent
        public List<SelectListItem> SubjectList { get; set; } //Parent
		
        public TodoStatusVm TodoStatus { get; set; } //Parent
        public List<SelectListItem> TodoStatusList { get; set; } //Parent
    }

    public class TodoListVm
    {
        public IEnumerable<TodoVm> Todos { get; set; }
				
		public int SubjectId { get; set; }
		public List<SelectListItem> SubjectList { get; set; }
		
		public int TodoStatusId { get; set; }
		public List<SelectListItem> TodoStatusList { get; set; }
    }
}
