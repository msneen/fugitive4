using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Fugitive4.Services.Models;
using Microsoft.AspNet.Mvc.Rendering;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Web.ViewModels.BailBond; //This is any parent or child vm
using Fugitive4.Web.ViewModels._Interfaces;

namespace Fugitive4.Web.ViewModels.BookingCharge
{
	public class BookingChargeVm : AuditableEntityBase, IAddress, IHasActivity
    {
		
		[Display(Name="Id")]	
		public int Id { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="Booking Number")]	
		public string BookingNumber { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="Charge Name")]	
		public string ChargeName { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="Code Number")]	
		public string CodeNumber { get; set; }

		
		[Display(Name="Felony")]	
		public bool Felony { get; set; }

		
		[Display(Name="Date Occurred")]	
		public Nullable<System.DateTime> DateOccurred { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="Jurisdiction")]	
		public string Jurisdiction { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="Police Agency")]	
		public string PoliceAgency { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="Address 1")]	
		public string Address1 { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="Address 2")]	
		public string Address2 { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="City")]	
		public string City { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="State")]	
		public string State { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="Zip Code")]	
		public string Zipcode { get; set; }

		
		[Display(Name="Detectives")]	
		public string Detectives { get; set; }

		
		[Display(Name="Victim")]	
		public string Victim { get; set; }

		
		[Display(Name="Details")]	
		public string Details { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="Report Number")]	
		public string ReportNumber { get; set; }

		
		[Display(Name="Description")]	
		public string Description { get; set; }

		
		[Display(Name="Notes")]	
		public string Notes { get; set; }

		
		[Display(Name="Bail Bond")]	
		public int BailBondId { get; set; }


		
        public BailBondVm BailBond { get; set; } //Parent
        public List<SelectListItem> BailBondList { get; set; } //Parent

        public List<ActivityNote.ActivityNoteVm> ActivityNotes { get; set; }//custom
    }

    public class BookingChargeListVm
    {
        public IEnumerable<BookingChargeVm> BookingCharges { get; set; }
				
		public int BailBondId { get; set; }
		public List<SelectListItem> BailBondList { get; set; }
    }
}
