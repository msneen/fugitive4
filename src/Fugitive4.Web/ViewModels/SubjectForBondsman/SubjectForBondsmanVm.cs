﻿using Fugitive4.Services.Models.Bases;
using Fugitive4.Web.ViewModels.ActivityNote;
using Fugitive4.Web.ViewModels.BailBond;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Fugitive4.Web.ViewModels.SubjectForBondsman
{
    public class SubjectForBondsmanVm : AuditableEntityBase
    {
        [Display(Name = "ID")]
        public int Id { get; set; }

        [Display(Name = "First Name")]
        public string Firstname { get; set; }


        [Display(Name = "Last Name")]
        public string Lastname { get; set; }


        [Display(Name = "Middle Name")]
        public string Middlename { get; set; }

        [Display(Name = "Notes")]
        public string Notes { get; set; }

        public BailBondVm CurrentBond { get; set; }

        public IEnumerable<BailBondVm> BailBonds { get; set; }//Child

        public IEnumerable<ActivityNoteVm> ActivityNotes { get; set; }//Child

    }
    public class SubjectForBondsmanListVm
    {
        public IEnumerable<SubjectForBondsmanVm> Subjects { get; set; }

    }
}
