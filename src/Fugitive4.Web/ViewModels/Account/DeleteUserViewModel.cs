﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Fugitive4.Services.Models;
using Microsoft.AspNet.Mvc.Rendering;

namespace Fugitive4.Web.ViewModels.Account
{
    public class DeleteUserViewModel
    {
        public string UserId { get; set; }
        public ApplicationUser User { get; set; }

        public List<SelectListItem> Users { get; set; }
    }
}
