﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fugitive4.Web.ViewModels._Interfaces
{
    public interface IHasActivity
    {
        int Id { get; set; }
        List<ActivityNote.ActivityNoteVm> ActivityNotes { get; set; }//custom
    }
}
