using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Mvc.Rendering;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Web.ViewModels.SearchRecipe; //This is any parent or child vm

namespace Fugitive4.Web.ViewModels.SearchRecipeItem
{
	public class SearchRecipeItemVm : AuditableEntityBase
    {
		
		[Display(Name="ID")]	
		public int Id { get; set; }

		[Required]
		[StringLength(50, MinimumLength = 2, ErrorMessage = "field must be between 2 and 50 characters")]		
		[Display(Name="Name")]	
		public string Name { get; set; }

		[StringLength(500,  ErrorMessage = "field must be less than 500 characters")]		
		[Display(Name="Url Or Source")]	
		public string UrlOrSource { get; set; }

		
		[Display(Name="Description")]	
		public string Description { get; set; }

		
		[Display(Name="Search Recipe")]	
		public int SearchRecipeId { get; set; }

		
		[Display(Name="Disabled")]	
		public bool Disabled { get; set; }

		
		[Display(Name="Created By")]	
		public string CreatedBy { get; set; }

		
		[Display(Name="Created Date")]	
		public System.DateTime CreatedDate { get; set; }

		
		[Display(Name="Modified By")]	
		public string ModifiedBy { get; set; }

		
		[Display(Name="Modified Date")]	
		public Nullable<System.DateTime> ModifiedDate { get; set; }

		
		[Display(Name="Sort Order")]	
		public int SortOrder { get; set; }


		
        public SearchRecipeVm SearchRecipe { get; set; } //Parent
        public List<SelectListItem> SearchRecipeList { get; set; } //Parent
    }

    public class SearchRecipeItemListVm
    {
        public string SearchRecipeName { get; set; }

        public IEnumerable<SearchRecipeItemVm> SearchRecipeItems { get; set; }
				
		public int SearchRecipeId { get; set; }
		public List<SelectListItem> SearchRecipeList { get; set; }
    }
}
