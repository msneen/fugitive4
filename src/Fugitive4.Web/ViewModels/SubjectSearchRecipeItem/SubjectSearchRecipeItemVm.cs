using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Mvc.Rendering;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Web.ViewModels.SearchRecipeItem; //This is any parent or child vm
using Fugitive4.Web.ViewModels.SearchResult; //This is any parent or child vm
using Fugitive4.Web.ViewModels.Subject; //This is any parent or child vm

namespace Fugitive4.Web.ViewModels.SubjectSearchRecipeItem
{
	public class SubjectSearchRecipeItemVm : AuditableEntityBase
    {
		
		[Display(Name="ID")]	
		public int Id { get; set; }

		
		[Display(Name="Search Recipe Item")]	
		public int SearchRecipeItemId { get; set; }

		
		[Display(Name="Subject")]	
		public int SubjectId { get; set; }


		
        public SearchRecipeItemVm SearchRecipeItem { get; set; } //Parent
        public List<SelectListItem> SearchRecipeItemList { get; set; } //Parent
		public IEnumerable<SearchResultVm> SearchResults { get; set; }//Child
		
        public SubjectVm Subject { get; set; } //Parent
        public List<SelectListItem> SubjectList { get; set; } //Parent
    }

    public class SubjectSearchRecipeItemListVm
    {
        public IEnumerable<SubjectSearchRecipeItemVm> SubjectSearchRecipeItems { get; set; }
				
		public int SearchRecipeItemId { get; set; }
		public List<SelectListItem> SearchRecipeItemList { get; set; }
		
		public int SubjectId { get; set; }
		public List<SelectListItem> SubjectList { get; set; }
    }
}
