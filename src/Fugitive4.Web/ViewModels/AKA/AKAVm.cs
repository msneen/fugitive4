using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Mvc.Rendering;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Web.ViewModels.Subject; //This is any parent or child vm

namespace Fugitive4.Web.ViewModels.AKA
{
	public class AKAVm : AuditableEntityBase
    {
		
		[Display(Name="Id")]	
		public int Id { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="First Name")]	
		public string Firstname { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="Middle Name")]	
		public string Middlename { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="Last Name")]	
		public string Lastname { get; set; }

		
		[Display(Name="Subject")]	
		public int SubjectId { get; set; }

		
		[Display(Name="Disabled")]	
		public bool Disabled { get; set; }

		
		[Display(Name="Created By")]	
		public string CreatedBy { get; set; }

		
		[Display(Name="Created Date")]	
		public System.DateTime CreatedDate { get; set; }

		
		[Display(Name="Modified By")]	
		public string ModifiedBy { get; set; }

		
		[Display(Name="Modified Date")]	
		public Nullable<System.DateTime> ModifiedDate { get; set; }


        [Display(Name = "Subject")]
        public SubjectVm Subject { get; set; } //Parent
        [Display(Name = "Subject List")]
        public List<SelectListItem> SubjectList { get; set; } //Parent
    }

    public class AKAListVm
    {
        [Display(Name = "AKA")]
        public IEnumerable<AKAVm> AKAS { get; set; }

        [Display(Name = "Subject")]
        public int SubjectId { get; set; }
        [Display(Name = "Subject List")]
        public List<SelectListItem> SubjectList { get; set; }
    }
}
