using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Fugitive4.Services.Models;
using Microsoft.AspNet.Mvc.Rendering;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Web.ViewModels.Subject; //This is any parent or child vm
using Fugitive4.Web.ViewModels.SubjectRelationship; //This is any parent or child vm

namespace Fugitive4.Web.ViewModels.Employer
{
	public class EmployerVm : AuditableEntityBase, IAddress
    {
		
		[Display(Name="Id")]	
		public int Id { get; set; }


		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="Name")]	
		public string Name { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="Job Title")]	
		public string JobTitle { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="Phone Number")]	
		public string PhoneNumber { get; set; }

		[StringLength(150,  ErrorMessage = "field must be less than 150 characters")]		
		[Display(Name="Address 1")]	
		public string Address1 { get; set; }

		[StringLength(150,  ErrorMessage = "field must be less than 150 characters")]		
		[Display(Name="Address 2")]	
		public string Address2 { get; set; }

		[StringLength(100,  ErrorMessage = "field must be less than 100 characters")]		
		[Display(Name="City")]	
		public string City { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="State")]	
		public string State { get; set; }

		[StringLength(20,  ErrorMessage = "field must be less than 20 characters")]		
		[Display(Name="Zip Code")]	
		public string Zipcode { get; set; }

		
		[Display(Name="Subject")]	
		public int SubjectId { get; set; }

		
		[Display(Name="Notes")]	
		public string Notes { get; set; }

		
		[Display(Name="Start Date")]	
		public Nullable<System.DateTime> StartDate { get; set; }

		
		[Display(Name="End Date")]	
		public Nullable<System.DateTime> EndDate { get; set; }


		
        public SubjectVm Subject { get; set; } //Parent
        public List<SelectListItem> SubjectList { get; set; } //Parent
		public IEnumerable<SubjectRelationshipVm> SubjectRelationships { get; set; }//Child
    }

    public class EmployerListVm
    {
        public IEnumerable<EmployerVm> Employers { get; set; }
				
		public int SubjectId { get; set; }
		public List<SelectListItem> SubjectList { get; set; }
    }
}
