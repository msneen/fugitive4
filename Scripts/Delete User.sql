USE [Fugitive4]
GO

SELECT [Id]
      ,[AccessFailedCount]
      ,[ConcurrencyStamp]
      ,[Email]
      ,[EmailConfirmed]
      ,[LockoutEnabled]
      ,[LockoutEnd]
      ,[NormalizedEmail]
      ,[NormalizedUserName]
      ,[PasswordHash]
      ,[PhoneNumber]
      ,[PhoneNumberConfirmed]
      ,[SecurityStamp]
      ,[TwoFactorEnabled]
      ,[UserName]
  FROM [dbo].[AspNetUsers]
GO

/*
declare @userId varchar(430) = '7a168bbc-19ab-40be-b29e-a9efe0dda4a0'

Delete From [dbo].[AspNetUserRoles]
Where UserId = @userId

Delete From [dbo].[AspNetUsers_Team]
Where ApplicationUserId=@userId

delete from  [dbo].[AspNetUsers]
 Where Id = @userId
*/