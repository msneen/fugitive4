﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace Fugitive4.Services.Models.SampleData
{
    public class ReferrerSampleData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetService<ApplicationDbContext>();
            if (!context.Referrers.Any())
            {
                context.Referrers.AddRange(
                    new Referrer
                    {
                        Name = "Happy Bail Bonds",
                        Description = "Test Bail Bond Company",
                        AdditionalInfo = "Test Company"
                    }
                );
                context.SaveChanges();
            }
        }
    }
}
