using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface ISubjectRelationshipService 	{
		IEnumerable<SubjectRelationship> GetAll(bool disabled = false);
		Task<ICollection<SubjectRelationship>> GetAllAsync(bool disabled = false);
		SubjectRelationship GetById(int id);
		Task<SubjectRelationship> GetByIdAsync(int id);
		Task InsertAsync(SubjectRelationship subjectRelationship, string userId);
        void Update(SubjectRelationship subjectRelationship, string userId);
        Task UpdateAsync(SubjectRelationship subjectRelationship, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);
		List<SubjectRelationship> GetByRelationshipTypeId(int relationshipTypeId);
		List<SubjectRelationship> GetBySubjectId(int subjectId);
	}														

	public partial class SubjectRelationshipService : AuditableServiceBase, ISubjectRelationshipService, IDisposable  
	{
		private readonly ApplicationDbContext _context;

		public SubjectRelationshipService(ApplicationDbContext context)
		{
			_context = context;
		}

		public IEnumerable<SubjectRelationship> GetAll(bool disabled = false) 
		{
			return _context
                .SubjectRelationships
                .Where(x => x.Disabled == false || x.Disabled == disabled)
                .Include(s => s.Subject)
                .Include(s => s.RelatedSubject)
                .ToList();
		}

		public async Task<ICollection<SubjectRelationship>> GetAllAsync(bool disabled = false)
        {
            return await _context
                .SubjectRelationships
                .Where(x => x.Disabled == false || x.Disabled == disabled)
                .Include(s=>s.Subject)
                .Include(s=>s.RelatedSubject)
                .ToListAsync();
        }

		public SubjectRelationship GetById(int id) 
		{	
			return _context
                .SubjectRelationships
                .Include(s => s.Subject)
                .Include(s => s.RelatedSubject)
                .SingleOrDefault(e => e.Id == id);
		}

		public async Task<SubjectRelationship> GetByIdAsync(int id)
        {
            return await _context
                .SubjectRelationships
                .Include(s => s.Subject)
                .Include(s => s.RelatedSubject)
                .SingleAsync(m => m.Id == id);
        }

		public void Insert(SubjectRelationship subjectRelationship, string userId)
        {
			SetCreateModify(subjectRelationship, userId);
            _context.SubjectRelationships.Add(subjectRelationship);
            _context.SaveChanges();
        }

		public async Task InsertAsync(SubjectRelationship subjectRelationship, string userId)
        {
			SetCreateModify(subjectRelationship, userId);
            _context.SubjectRelationships.Add(subjectRelationship);
            await _context.SaveChangesAsync();
        }

		public void Update(SubjectRelationship subjectRelationship, string userId)
		{
			SetCreateModify(subjectRelationship, userId);

			_context.Entry(subjectRelationship).State = subjectRelationship.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(SubjectRelationship subjectRelationship, string userId)
        {
            SetCreateModify(subjectRelationship, userId);

			_context.Entry(subjectRelationship).State = subjectRelationship.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			SubjectRelationship subjectRelationship = _context.SubjectRelationships.SingleOrDefault(e => e.Id == id);
			            if (subjectRelationship == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(subjectRelationship, userId);

            subjectRelationship.Disabled = true;
            Update(subjectRelationship, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var subjectRelationship = await GetByIdAsync(id);
            SetCreateModify(subjectRelationship, userId);

            subjectRelationship.Disabled = true;

            await UpdateAsync(subjectRelationship, userId);
        }

		private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }



        public List<SubjectRelationship> GetByRelationshipTypeId(int relationshipTypeId)
        {
            return _context
                .SubjectRelationships
                .Where(x => x.RelationshipTypeId == relationshipTypeId)
                .Include(s => s.Subject)
                .Include(s => s.RelatedSubject)
                .ToList();
        }

        public List<SubjectRelationship> GetBySubjectId(int subjectId)
        {
            var subjects = _context
                .SubjectRelationships
                .Where(x => x.SubjectId == subjectId)
                .Include(s => s.Subject).ThenInclude(r=>r.Addresses).ThenInclude(t=>t.AddressType)
                .Include(s => s.RelatedSubject).ThenInclude(r => r.Addresses).ThenInclude(t => t.AddressType)
                .Include(s=>s.RelationshipType)
                .ToList();

            var otherSubjects = _context
                .SubjectRelationships
                .Where(x => x.RelatedSubjectId == subjectId)
                .Include(s => s.Subject).ThenInclude(r => r.Addresses).ThenInclude(t => t.AddressType)
                .Include(s => s.RelatedSubject).ThenInclude(r => r.Addresses).ThenInclude(t => t.AddressType)
                .Include(s => s.RelationshipType)
                .ToList();
            if (otherSubjects != null)
            {
                subjects.AddRange(otherSubjects);
            }
            return subjects;
        }

	}
}
