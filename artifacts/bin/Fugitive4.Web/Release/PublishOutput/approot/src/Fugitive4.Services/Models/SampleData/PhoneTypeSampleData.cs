﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace Fugitive4.Services.Models.SampleData
{
    public class PhoneTypeSampleData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetService<ApplicationDbContext>();
            if (!context.PhoneTypes.Any())
            {
                context.PhoneTypes.AddRange(
                    new PhoneType
                    {
                        Name = "Home"
                    },
                    new PhoneType
                    {
                        Name = "Work"
                    },
                    new PhoneType
                    {
                        Name = "Mobile"
                    },
                    new PhoneType
                    {
                        Name = "Parents"
                    },
                    new PhoneType
                    {
                        Name = "GirlFriends"
                    },
                    new PhoneType
                    {
                        Name = "Friends"
                    },
                    new PhoneType
                    {
                        Name = "Unknown"
                    }
                );
                context.SaveChanges();
            }
        }
    }
}


