using System;
using System.Collections.Generic;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Services.Migrations;


namespace Fugitive4.Services.Models
{
	public class Vehicle : AuditableEntityBase
    {
		public int Id { get; set; }
		public string Make { get; set; }
		public string Model { get; set; }
		public string Year { get; set; }
		public string Color { get; set; }
		public string Description { get; set; }
		public string BelongsTo { get; set; }
		public int SubjectId { get; set; }
		public string PlateNumber { get; set; }

		
        public Subject Subject { get; set; } //Parent
    }

}
