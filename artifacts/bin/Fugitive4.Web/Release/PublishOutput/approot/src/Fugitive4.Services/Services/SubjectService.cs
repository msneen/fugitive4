using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface ISubjectService 	{
		IEnumerable<Subject> GetAll(bool disabled = false);
		Task<ICollection<Subject>> GetAllAsync(bool disabled = false);
		Subject GetById(int id);
		Task<Subject> GetByIdAsync(int id);
		Task InsertAsync(Subject subject, string userId);
        void Update(Subject subject, string userId);
        Task UpdateAsync(Subject subject, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);
		List<Subject> GetByReferrerId(int referrerId);
		List<Subject> GetByGenderId(int genderId);
		List<Subject> GetByRaceId(int raceId);
	}														

	public partial class SubjectService : AuditableServiceBase, ISubjectService, IDisposable  
	{
		private readonly ApplicationDbContext _context;

		public SubjectService(ApplicationDbContext context)
		{
			_context = context;
		}

		public IEnumerable<Subject> GetAll(bool disabled = false) 
		{
			return _context
                .Subjects
                .Where(x => x.Disabled == false || x.Disabled == disabled)
                .Include(s => s.BailBonds)
                .ToList();
		}

		public async Task<ICollection<Subject>> GetAllAsync(bool disabled = false)
        {
            return await _context
                .Subjects
                .Where(x => x.Disabled == false || x.Disabled == disabled)
                .Include(s => s.BailBonds)
                .ToListAsync();
        }

		public Subject GetById(int id) 
		{	
			return _context
                .Subjects
                .Include(s=>s.Race)
                .Include(s=>s.Gender)
                .Include(s=>s.StoredFiles)
                .Include(s=>s.Addresses).ThenInclude(a=>a.AddressType)
                .Include(s => s.PhoneNumbers).ThenInclude(a => a.PhoneType)
                .Include(s=>s.BailBonds).ThenInclude(b=>b.BookingCharges)
                .Include(s=>s.ActivityNotes).OrderByDescending(t=>t.CreatedDate)
                .Include(s=>s.Vehicles)
                .Include(s => s.DriverLicenses)
                .Include(s => s.EmailAddresses)
                .Include(s => s.Hyperlinks)
                .Include(s => s.SearchResults)
                .Include(s => s.Videos)
                .SingleOrDefault(e => e.Id == id);
		}

		public async Task<Subject> GetByIdAsync(int id)
        {
            return await _context
                .Subjects
                .Include(s => s.Race)
                .Include(s => s.Gender)
                .Include(s => s.StoredFiles)
                .Include(s => s.Addresses).ThenInclude(a => a.AddressType)
                .Include(s=>s.PhoneNumbers).ThenInclude(a=>a.PhoneType)
                .Include(s => s.BailBonds).ThenInclude(b => b.BookingCharges)
                .Include(s => s.ActivityNotes).OrderByDescending(t => t.CreatedDate)
                .Include(s => s.Vehicles)
                .Include(s => s.DriverLicenses)
                .Include(s => s.EmailAddresses).ThenInclude(a => a.EmailType)
                .Include(s => s.Hyperlinks)
                .Include(s => s.SearchResults)
                .Include(s => s.Videos)
                .SingleAsync(m => m.Id == id);
        }

		public void Insert(Subject subject, string userId)
        {
			SetCreateModify(subject, userId);
            _context.Subjects.Add(subject);
            _context.SaveChanges();
        }

		public async Task InsertAsync(Subject subject, string userId)
        {
			SetCreateModify(subject, userId);
            _context.Subjects.Add(subject);
            await _context.SaveChangesAsync();
        }

		public void Update(Subject subject, string userId)
		{
			SetCreateModify(subject, userId);

			_context.Entry(subject).State = subject.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(Subject subject, string userId)
        {
            SetCreateModify(subject, userId);

			_context.Entry(subject).State = subject.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			Subject subject = _context.Subjects.SingleOrDefault(e => e.Id == id);
			            if (subject == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(subject, userId);

            subject.Disabled = true;
            Update(subject, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var subject = await GetByIdAsync(id);
            SetCreateModify(subject, userId);

            subject.Disabled = true;

            await UpdateAsync(subject, userId);
        }

		private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }



        public List<Subject> GetByReferrerId(int referrerId)
        {
            return _context.Subjects.Where(x => x.ReferrerId == referrerId).ToList();
        }
    //
    //
    //

        public List<Subject> GetByGenderId(int genderId)
        {
            return _context.Subjects.Where(x => x.GenderId == genderId).ToList();
        }
    //
    //
    //
    //
    //
    //
    //
    //

        public List<Subject> GetByRaceId(int raceId)
        {
            return _context.Subjects.Where(x => x.RaceId == raceId).ToList();
        }
	}
}
