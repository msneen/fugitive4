﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace Fugitive4.Services.Models.SampleData
{
    public class GenderSampleData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetService<ApplicationDbContext>();
            if (!context.Genders.Any())
            {
                context.Genders.AddRange(
                    new Gender
                    {
                        Name = "Male"
                    },
                    new Gender
                    {
                        Name = "Female"
                    },
                    new Gender
                    {
                        Name = "TransGender"
                    },
                    new Gender
                    {
                        Name = "CrossDresser"
                    },
                    new Gender
                    {
                        Name = "Male to Female"
                    },
                    new Gender
                    {
                        Name = "Female to Male"
                    },
                    new Gender
                    {
                        Name = "Unknown"
                    }
                );
                context.SaveChanges();
            }
        }
    }
}

