using System;
using System.Collections.Generic;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Services.Migrations;


namespace Fugitive4.Services.Models
{
	public class PhoneType : AuditableEntityBase
    {
		public int Id { get; set; }
		public string Name { get; set; }

		public ICollection<PhoneNumber> PhoneNumbers { get; set; }//Child
    }

}
