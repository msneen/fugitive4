using System;
using System.Collections.Generic;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Services.Migrations;


namespace Fugitive4.Services.Models
{
	public class SearchResult : AuditableEntityBase
    {
		public int Id { get; set; }
		public string UrlOrSource { get; set; }
		public string ResultText { get; set; }
		public string Username { get; set; }
		public string Notes { get; set; }
		public int SubjectId { get; set; }

		
        public Subject Subject { get; set; } //Parent
    }

}
