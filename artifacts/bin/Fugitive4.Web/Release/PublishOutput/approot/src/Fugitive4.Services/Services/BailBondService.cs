using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface IBailBondService 	{
		IEnumerable<BailBond> GetAll(bool disabled = false);
		Task<ICollection<BailBond>> GetAllAsync(bool disabled = false);
		BailBond GetById(int id);
		Task<BailBond> GetByIdAsync(int id);
		Task InsertAsync(BailBond bailBond, string userId);
        void Update(BailBond bailBond, string userId);
        Task UpdateAsync(BailBond bailBond, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);
		List<BailBond> GetBySubjectId(int subjectId);
	}														

	public partial class BailBondService : AuditableServiceBase, IBailBondService, IDisposable  
	{
		private readonly ApplicationDbContext _context;

		public BailBondService(ApplicationDbContext context)
		{
			_context = context;
		}

		public IEnumerable<BailBond> GetAll(bool disabled = false) 
		{
			return _context.BailBonds.Where(x => x.Disabled == false || x.Disabled == disabled).ToList();
		}

		public async Task<ICollection<BailBond>> GetAllAsync(bool disabled = false)
        {
            return await _context.BailBonds.Where(x => x.Disabled == false || x.Disabled == disabled).ToListAsync();
        }

		public BailBond GetById(int id) 
		{	
			return _context.BailBonds.SingleOrDefault(e => e.Id == id);
		}

		public async Task<BailBond> GetByIdAsync(int id)
        {
            return await _context.BailBonds.SingleAsync(m => m.Id == id);
        }

		public void Insert(BailBond bailBond, string userId)
        {
			SetCreateModify(bailBond, userId);
            _context.BailBonds.Add(bailBond);
            _context.SaveChanges();
        }

		public async Task InsertAsync(BailBond bailBond, string userId)
        {
			SetCreateModify(bailBond, userId);
            _context.BailBonds.Add(bailBond);
            await _context.SaveChangesAsync();
        }

		public void Update(BailBond bailBond, string userId)
		{
			SetCreateModify(bailBond, userId);

			_context.Entry(bailBond).State = bailBond.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(BailBond bailBond, string userId)
        {
            SetCreateModify(bailBond, userId);

			_context.Entry(bailBond).State = bailBond.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			BailBond bailBond = _context.BailBonds.SingleOrDefault(e => e.Id == id);
			            if (bailBond == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(bailBond, userId);

            bailBond.Disabled = true;
            Update(bailBond, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var bailBond = await GetByIdAsync(id);
            SetCreateModify(bailBond, userId);

            bailBond.Disabled = true;

            await UpdateAsync(bailBond, userId);
        }

		private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }



        public List<BailBond> GetBySubjectId(int subjectId)
        {
            return _context.BailBonds.Where(x => x.SubjectId == subjectId).ToList();
        }
	}
}
