using System;
using System.Collections.Generic;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Services.Migrations;


namespace Fugitive4.Services.Models
{
    public interface IAddress
    {
        int Id { get; set; }
        string Address1 { get; set; }
        string Address2 { get; set; }
        string City { get; set; }
        string State { get; set; }
        string Zipcode { get; set; }
    }
	public class Address : AuditableEntityBase, IAddress
    {
		public int Id { get; set; }
		public string Address1 { get; set; }
		public string Address2 { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string Zipcode { get; set; }
		public bool BelongsToSubject { get; set; }
		public int SubjectId { get; set; }
		public int AddressTypeId { get; set; }
		public string Notes { get; set; }

		
        public Subject Subject { get; set; } //Parent
		
        public AddressType AddressType { get; set; } //Parent
    }

}
