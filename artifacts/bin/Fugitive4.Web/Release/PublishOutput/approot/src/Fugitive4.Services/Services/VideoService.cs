using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface IVideoService 	{
		IEnumerable<Video> GetAll(bool disabled = false);
		Task<ICollection<Video>> GetAllAsync(bool disabled = false);
		Video GetById(int id);
		Task<Video> GetByIdAsync(int id);
		Task InsertAsync(Video video, string userId);
        void Update(Video video, string userId);
        Task UpdateAsync(Video video, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);
		List<Video> GetBySubjectId(int subjectId);
	}														

	public partial class VideoService : AuditableServiceBase, IVideoService, IDisposable  
	{
		private readonly ApplicationDbContext _context;

		public VideoService(ApplicationDbContext context)
		{
			_context = context;
		}

		public IEnumerable<Video> GetAll(bool disabled = false) 
		{
			return _context.Videos.Where(x => x.Disabled == false || x.Disabled == disabled).ToList();
		}

		public async Task<ICollection<Video>> GetAllAsync(bool disabled = false)
        {
            return await _context.Videos.Where(x => x.Disabled == false || x.Disabled == disabled).ToListAsync();
        }

		public Video GetById(int id) 
		{	
			return _context.Videos.SingleOrDefault(e => e.Id == id);
		}

		public async Task<Video> GetByIdAsync(int id)
        {
            return await _context.Videos.SingleAsync(m => m.Id == id);
        }

		public void Insert(Video video, string userId)
        {
			SetCreateModify(video, userId);
            _context.Videos.Add(video);
            _context.SaveChanges();
        }

		public async Task InsertAsync(Video video, string userId)
        {
			SetCreateModify(video, userId);
            _context.Videos.Add(video);
            await _context.SaveChangesAsync();
        }

		public void Update(Video video, string userId)
		{
			SetCreateModify(video, userId);

			_context.Entry(video).State = video.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(Video video, string userId)
        {
            SetCreateModify(video, userId);

			_context.Entry(video).State = video.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			Video video = _context.Videos.SingleOrDefault(e => e.Id == id);
			            if (video == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(video, userId);

            video.Disabled = true;
            Update(video, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var video = await GetByIdAsync(id);
            SetCreateModify(video, userId);

            video.Disabled = true;

            await UpdateAsync(video, userId);
        }

		private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }



        public List<Video> GetBySubjectId(int subjectId)
        {
            return _context.Videos.Where(x => x.SubjectId == subjectId).ToList();
        }
	}
}
