using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface IEmailTypeService 	{
		IEnumerable<EmailType> GetAll(bool disabled = false);
		Task<ICollection<EmailType>> GetAllAsync(bool disabled = false);
		EmailType GetById(int id);
		Task<EmailType> GetByIdAsync(int id);
		Task InsertAsync(EmailType emailType, string userId);
        void Update(EmailType emailType, string userId);
        Task UpdateAsync(EmailType emailType, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);
	}														

	public partial class EmailTypeService : AuditableServiceBase, IEmailTypeService, IDisposable  
	{
		private readonly ApplicationDbContext _context;

		public EmailTypeService(ApplicationDbContext context)
		{
			_context = context;
		}

		public IEnumerable<EmailType> GetAll(bool disabled = false) 
		{
			return _context.EmailTypes.Where(x => x.Disabled == false || x.Disabled == disabled).ToList();
		}

		public async Task<ICollection<EmailType>> GetAllAsync(bool disabled = false)
        {
            return await _context.EmailTypes.Where(x => x.Disabled == false || x.Disabled == disabled).ToListAsync();
        }

		public EmailType GetById(int id) 
		{	
			return _context.EmailTypes.SingleOrDefault(e => e.Id == id);
		}

		public async Task<EmailType> GetByIdAsync(int id)
        {
            return await _context.EmailTypes.SingleAsync(m => m.Id == id);
        }

		public void Insert(EmailType emailType, string userId)
        {
			SetCreateModify(emailType, userId);
            _context.EmailTypes.Add(emailType);
            _context.SaveChanges();
        }

		public async Task InsertAsync(EmailType emailType, string userId)
        {
			SetCreateModify(emailType, userId);
            _context.EmailTypes.Add(emailType);
            await _context.SaveChangesAsync();
        }

		public void Update(EmailType emailType, string userId)
		{
			SetCreateModify(emailType, userId);

			_context.Entry(emailType).State = emailType.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(EmailType emailType, string userId)
        {
            SetCreateModify(emailType, userId);

			_context.Entry(emailType).State = emailType.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			EmailType emailType = _context.EmailTypes.SingleOrDefault(e => e.Id == id);
			            if (emailType == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(emailType, userId);

            emailType.Disabled = true;
            Update(emailType, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var emailType = await GetByIdAsync(id);
            SetCreateModify(emailType, userId);

            emailType.Disabled = true;

            await UpdateAsync(emailType, userId);
        }

		private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


    //
	}
}
