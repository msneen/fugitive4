using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface IGenderService 	{
		IEnumerable<Gender> GetAll(bool disabled = false);
		Task<ICollection<Gender>> GetAllAsync(bool disabled = false);
		Gender GetById(int id);
		Task<Gender> GetByIdAsync(int id);
		Task InsertAsync(Gender gender, string userId);
        void Update(Gender gender, string userId);
        Task UpdateAsync(Gender gender, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);
	}														

	public partial class GenderService : AuditableServiceBase, IGenderService, IDisposable  
	{
		private readonly ApplicationDbContext _context;

		public GenderService(ApplicationDbContext context)
		{
			_context = context;
		}

		public IEnumerable<Gender> GetAll(bool disabled = false) 
		{
			return _context.Genders.Where(x => x.Disabled == false || x.Disabled == disabled).ToList();
		}

		public async Task<ICollection<Gender>> GetAllAsync(bool disabled = false)
        {
            return await _context.Genders.Where(x => x.Disabled == false || x.Disabled == disabled).ToListAsync();
        }

		public Gender GetById(int id) 
		{	
			return _context.Genders.SingleOrDefault(e => e.Id == id);
		}

		public async Task<Gender> GetByIdAsync(int id)
        {
            return await _context.Genders.SingleAsync(m => m.Id == id);
        }

		public void Insert(Gender gender, string userId)
        {
			SetCreateModify(gender, userId);
            _context.Genders.Add(gender);
            _context.SaveChanges();
        }

		public async Task InsertAsync(Gender gender, string userId)
        {
			SetCreateModify(gender, userId);
            _context.Genders.Add(gender);
            await _context.SaveChangesAsync();
        }

		public void Update(Gender gender, string userId)
		{
			SetCreateModify(gender, userId);

			_context.Entry(gender).State = gender.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(Gender gender, string userId)
        {
            SetCreateModify(gender, userId);

			_context.Entry(gender).State = gender.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			Gender gender = _context.Genders.SingleOrDefault(e => e.Id == id);
			            if (gender == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(gender, userId);

            gender.Disabled = true;
            Update(gender, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var gender = await GetByIdAsync(id);
            SetCreateModify(gender, userId);

            gender.Disabled = true;

            await UpdateAsync(gender, userId);
        }

		private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }


    //
	}
}
