﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Microsoft.Extensions.DependencyInjection;

namespace Fugitive4.Services.Models.SampleData
{
    public static class SubjectSampleData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetService<ApplicationDbContext>();
            //var existingSubjects = context.Subjects;
            //context.RemoveRange(existingSubjects);
            //context.SaveChanges();

            if ( !context.Subjects.Any())
            {
                context.Subjects.AddRange(
                    new Subject
                    {
                        Firstname = "Harry",
                        Lastname = "Harris",
                        DOB = new DateTime(1992, 11, 1),
                        HairColor = "Brown",
                        EyeColor = "Blue",
                        Height = "6'4",
                        Weight = 130,
                        ReferrerId = context.Referrers.FirstOrDefault().Id,
                        GenderId = context.Genders.FirstOrDefault().Id,
                        RaceId = context.Races.FirstOrDefault().Id
                    }
                );
                context.SaveChanges();
            }
        }
    }
}
