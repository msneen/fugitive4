﻿using System.Collections.Generic;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Models
{
    public class Referrer : AuditableEntityBase
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string AdditionalInfo { get; set; }

        public ICollection<Subject> Subjects { get; set; }
    }
}
