using System;
using System.Collections.Generic;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Services.Migrations;


namespace Fugitive4.Services.Models
{
	public class ActivityNote : AuditableEntityBase
    {
		public int Id { get; set; }
		public System.DateTime ActivityDate { get; set; }
		public string Latitude { get; set; }
		public string Longitude { get; set; }
		public string Address { get; set; }
		public string PerformedBy { get; set; }
		public string Description { get; set; }
		public int SubjectId { get; set; }

		
        public Subject Subject { get; set; } //Parent
    }

}
