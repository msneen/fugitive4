﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Data.Entity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Data.Entity.Metadata;

namespace Fugitive4.Services.Models
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public IConfigurationRoot Configuration { get; set; }

        public DbSet<Subject> Subjects { get; set; }
        public DbSet<Referrer> Referrers { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<AddressType> AddressTypes { get; set; }
        public DbSet<Gender> Genders { get; set; }
        public DbSet<PhoneType> PhoneTypes { get; set; }
        public DbSet<PhoneNumber> PhoneNumbers { get; set; }
        public DbSet<EmailType> EmailTypes { get; set; }
        public DbSet<EmailAddress> EmailAddresses { get; set; }
        public DbSet<BailBond> BailBonds { get; set; }
        public DbSet<Vehicle> Vehicles { get; set; }
        public DbSet<ActivityNote> ActivityNotes { get; set; }
        public DbSet<DriverLicense> DriverLicenses { get; set; }
        public DbSet<StoredFile> StoredFiles { get; set; }
        public DbSet<Hyperlink> Hyperlinks { get; set; }
        public DbSet<SearchResult> SearchResults { get; set; }
        public DbSet<Video> Videos { get; set; }
        public DbSet<Race> Races { get; set; }
        public DbSet<RelationshipType> RelationshipTypes { get; set; }
        public DbSet<SubjectRelationship> SubjectRelationships { get; set; }
        public DbSet<BookingCharge> BookingCharges { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            
            builder.Entity<Subject>().HasKey(q => q.Id);
            builder.Entity<SubjectRelationship>().HasKey(q => q.Id);

            builder.Entity<SubjectRelationship>()
                .HasOne(q => q.Subject)
                .WithMany(r=>r.SubjectRelationships)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<SubjectRelationship>()
                .HasOne(q => q.RelatedSubject)
                .WithMany(r => r.OtherSubjectRelationships)
                .OnDelete(DeleteBehavior.Restrict);

            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json");
            Configuration = builder.Build();

            optionsBuilder.UseSqlServer(Configuration["Data:DefaultConnection:ConnectionString"]);
            

        }
    }
}
