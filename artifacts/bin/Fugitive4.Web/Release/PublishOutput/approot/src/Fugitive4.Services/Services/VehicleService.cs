using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface IVehicleService 	{
		IEnumerable<Vehicle> GetAll(bool disabled = false);
		Task<ICollection<Vehicle>> GetAllAsync(bool disabled = false);
		Vehicle GetById(int id);
		Task<Vehicle> GetByIdAsync(int id);
		Task InsertAsync(Vehicle vehicle, string userId);
        void Update(Vehicle vehicle, string userId);
        Task UpdateAsync(Vehicle vehicle, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);
		List<Vehicle> GetBySubjectId(int subjectId);
	}														

	public partial class VehicleService : AuditableServiceBase, IVehicleService, IDisposable  
	{
		private readonly ApplicationDbContext _context;

		public VehicleService(ApplicationDbContext context)
		{
			_context = context;
		}

		public IEnumerable<Vehicle> GetAll(bool disabled = false) 
		{
			return _context.Vehicles.Where(x => x.Disabled == false || x.Disabled == disabled).ToList();
		}

		public async Task<ICollection<Vehicle>> GetAllAsync(bool disabled = false)
        {
            return await _context.Vehicles.Where(x => x.Disabled == false || x.Disabled == disabled).ToListAsync();
        }

		public Vehicle GetById(int id) 
		{	
			return _context.Vehicles.SingleOrDefault(e => e.Id == id);
		}

		public async Task<Vehicle> GetByIdAsync(int id)
        {
            return await _context.Vehicles.SingleAsync(m => m.Id == id);
        }

		public void Insert(Vehicle vehicle, string userId)
        {
			SetCreateModify(vehicle, userId);
            _context.Vehicles.Add(vehicle);
            _context.SaveChanges();
        }

		public async Task InsertAsync(Vehicle vehicle, string userId)
        {
			SetCreateModify(vehicle, userId);
            _context.Vehicles.Add(vehicle);
            await _context.SaveChangesAsync();
        }

		public void Update(Vehicle vehicle, string userId)
		{
			SetCreateModify(vehicle, userId);

			_context.Entry(vehicle).State = vehicle.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(Vehicle vehicle, string userId)
        {
            SetCreateModify(vehicle, userId);

			_context.Entry(vehicle).State = vehicle.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			Vehicle vehicle = _context.Vehicles.SingleOrDefault(e => e.Id == id);
			            if (vehicle == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(vehicle, userId);

            vehicle.Disabled = true;
            Update(vehicle, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var vehicle = await GetByIdAsync(id);
            SetCreateModify(vehicle, userId);

            vehicle.Disabled = true;

            await UpdateAsync(vehicle, userId);
        }

		private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }



        public List<Vehicle> GetBySubjectId(int subjectId)
        {
            return _context.Vehicles.Where(x => x.SubjectId == subjectId).ToList();
        }
	}
}
