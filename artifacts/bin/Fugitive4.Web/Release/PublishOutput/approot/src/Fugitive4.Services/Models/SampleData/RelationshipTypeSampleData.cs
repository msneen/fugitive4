﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace Fugitive4.Services.Models.SampleData
{
    public class RelationshipTypeSampleData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetService<ApplicationDbContext>();
            if (!context.RelationshipTypes.Any())
            {
                context.RelationshipTypes.AddRange(
                    new RelationshipType
                    {
                        Name = "Signor"
                    },
                    new RelationshipType
                    {
                        Name = "Friend"
                    },
                    new RelationshipType
                    {
                        Name = "Family"
                    },
                    new RelationshipType
                    {
                        Name = "Girlfriend"
                    },
                    new RelationshipType
                    {
                        Name = "Work Acquaintance"
                    },
                    new RelationshipType
                    {
                        Name = "Other"
                    }
                );
                context.SaveChanges();
            }
        }
    }
}

