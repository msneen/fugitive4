using System;
using System.Collections.Generic;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Services.Migrations;


namespace Fugitive4.Services.Models
{
	public class Gender : AuditableEntityBase
    {
		public int Id { get; set; }
		public string Name { get; set; }

		public ICollection<Subject> Subjects { get; set; }//Child
    }

}
