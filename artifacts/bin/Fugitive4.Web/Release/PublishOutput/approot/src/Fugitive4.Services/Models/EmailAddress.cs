using System;
using System.Collections.Generic;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Services.Migrations;


namespace Fugitive4.Services.Models
{
	public class EmailAddress : AuditableEntityBase
    {
		public int Id { get; set; }
		public string EmailAddress1 { get; set; }
		public string BelongsTo { get; set; }
		public int SubjectId { get; set; }
		public int EmailTypeId { get; set; }
		public string Notes { get; set; }

		
        public EmailType EmailType { get; set; } //Parent
		
        public Subject Subject { get; set; } //Parent
    }

}
