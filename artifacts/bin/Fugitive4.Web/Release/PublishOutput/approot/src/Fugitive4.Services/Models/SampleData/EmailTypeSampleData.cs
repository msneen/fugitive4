﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace Fugitive4.Services.Models.SampleData
{
    public class EmailTypeSampleData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            var context = serviceProvider.GetService<ApplicationDbContext>();
            if (!context.EmailTypes.Any())
            {
                context.EmailTypes.AddRange(
                    new EmailType
                    {
                        Name = "Home"
                    },
                    new EmailType
                    {
                        Name = "Work"
                    },

                    new EmailType
                    {
                        Name = "Parents"
                    },
                    new EmailType
                    {
                        Name = "GirlFriends"
                    },
                    new EmailType
                    {
                        Name = "Friends"
                    },
                    new EmailType
                    {
                        Name = "Unknown"
                    }
                );
                context.SaveChanges();
            }
        }
    }
}



