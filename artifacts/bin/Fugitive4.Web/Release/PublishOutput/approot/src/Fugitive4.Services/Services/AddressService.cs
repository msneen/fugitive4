using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.Bases;

namespace Fugitive4.Services.Services
{
	public interface IAddressService 	{
		IEnumerable<Address> GetAll(bool disabled = false);
		Task<ICollection<Address>> GetAllAsync(bool disabled = false);
		Address GetById(int id);
		Task<Address> GetByIdAsync(int id);
		Task InsertAsync(Address address, string userId);
        void Update(Address address, string userId);
        Task UpdateAsync(Address address, string userId);
        void Delete(int id, string userId);
        Task DeleteAsync(int id, string userId);
		List<Address> GetBySubjectId(int subjectId);
		List<Address> GetByAddressTypeId(int addressTypeId);
	}														

	public partial class AddressService : AuditableServiceBase, IAddressService, IDisposable  
	{
		private readonly ApplicationDbContext _context;

		public AddressService(ApplicationDbContext context)
		{
			_context = context;
		}

		public IEnumerable<Address> GetAll(bool disabled = false) 
		{
			return _context.Addresses.Where(x => x.Disabled == false || x.Disabled == disabled).ToList();
		}

		public async Task<ICollection<Address>> GetAllAsync(bool disabled = false)
        {
            return await _context.Addresses.Where(x => x.Disabled == false || x.Disabled == disabled).ToListAsync();
        }

		public Address GetById(int id) 
		{	
			return _context.Addresses.SingleOrDefault(e => e.Id == id);
		}

		public async Task<Address> GetByIdAsync(int id)
        {
            return await _context.Addresses.SingleAsync(m => m.Id == id);
        }

		public void Insert(Address address, string userId)
        {
			SetCreateModify(address, userId);
            _context.Addresses.Add(address);
            _context.SaveChanges();
        }

		public async Task InsertAsync(Address address, string userId)
        {
			SetCreateModify(address, userId);
            _context.Addresses.Add(address);
            await _context.SaveChangesAsync();
        }

		public void Update(Address address, string userId)
		{
			SetCreateModify(address, userId);

			_context.Entry(address).State = address.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;

			_context.SaveChanges();           
		}

		public async Task UpdateAsync(Address address, string userId)
        {
            SetCreateModify(address, userId);

			_context.Entry(address).State = address.Id == 0 ?
											EntityState.Added :
											EntityState.Modified;
            await _context.SaveChangesAsync();
        }

		public void Delete(int id, string userId) 
		{
			Address address = _context.Addresses.SingleOrDefault(e => e.Id == id);
			            if (address == null)
                throw new ArgumentOutOfRangeException();

			SetCreateModify(address, userId);

            address.Disabled = true;
            Update(address, userId);
		}

		public async Task DeleteAsync(int id, string userId)
        {
            var address = await GetByIdAsync(id);
            SetCreateModify(address, userId);

            address.Disabled = true;

            await UpdateAsync(address, userId);
        }

		private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }



        public List<Address> GetBySubjectId(int subjectId)
        {
            return _context.Addresses.Where(x => x.SubjectId == subjectId).ToList();
        }

        public List<Address> GetByAddressTypeId(int addressTypeId)
        {
            return _context.Addresses.Where(x => x.AddressTypeId == addressTypeId).ToList();
        }
	}
}
