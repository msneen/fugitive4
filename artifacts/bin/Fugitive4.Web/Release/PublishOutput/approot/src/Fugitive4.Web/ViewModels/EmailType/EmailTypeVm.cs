using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Mvc.Rendering;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Web.ViewModels.EmailAddress; //This is any parent or child vm

namespace Fugitive4.Web.ViewModels.EmailType
{
	public class EmailTypeVm : AuditableEntityBase
    {
		
		[Display(Name="Id")]	
		public int Id { get; set; }

		[StringLength(10,  ErrorMessage = "field must be less than 10 characters")]		
		[Display(Name="Name")]	
		public string Name { get; set; }


		public IEnumerable<EmailAddressVm> EmailAddresses { get; set; }//Child
    }

    public class EmailTypeListVm
    {
        public IEnumerable<EmailTypeVm> EmailTypes { get; set; }
		    }
}
