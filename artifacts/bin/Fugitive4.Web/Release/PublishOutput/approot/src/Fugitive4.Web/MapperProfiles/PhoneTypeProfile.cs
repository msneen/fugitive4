using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.PhoneType;


namespace Fugitive4.Web.MapperProfiles
{
    public class PhoneTypeProfile : Profile
    {
        protected override void Configure()
        {
			CreateMap<PhoneType, PhoneTypeVm>().ReverseMap();
        }
    }
}
