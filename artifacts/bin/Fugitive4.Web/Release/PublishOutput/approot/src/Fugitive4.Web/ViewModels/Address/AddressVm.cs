using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Fugitive4.Services.Models;
using Microsoft.AspNet.Mvc.Rendering;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Web.ViewModels.Subject; //This is any parent or child vm
using Fugitive4.Web.ViewModels.AddressType; //This is any parent or child vm

namespace Fugitive4.Web.ViewModels.Address
{
	public class AddressVm : AuditableEntityBase, IAddress
    {
		
		[Display(Name="Id")]	
		public int Id { get; set; }

		[StringLength(150,  ErrorMessage = "field must be less than 150 characters")]		
		[Display(Name="Address 1")]	
		public string Address1 { get; set; }

		[StringLength(150,  ErrorMessage = "field must be less than 150 characters")]		
		[Display(Name="Address 2")]	
		public string Address2 { get; set; }

		[StringLength(100,  ErrorMessage = "field must be less than 100 characters")]		
		[Display(Name="City")]	
		public string City { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="State")]	
		public string State { get; set; }

		[StringLength(20,  ErrorMessage = "field must be less than 20 characters")]		
		[Display(Name="Zipcode")]	
		public string Zipcode { get; set; }

		
		[Display(Name="Belongs To Subject")]	
		public bool BelongsToSubject { get; set; }

		
		[Display(Name="Subject Id")]	
		public int SubjectId { get; set; }

		
		[Display(Name="Address Type Id")]	
		public int AddressTypeId { get; set; }

		
		[Display(Name="Notes")]	
		public string Notes { get; set; }


		
        public SubjectVm Subject { get; set; } //Parent
        public List<SelectListItem> SubjectList { get; set; } //Parent
		
        public AddressTypeVm AddressType { get; set; } //Parent
        public List<SelectListItem> AddressTypeList { get; set; } //Parent
    }

    public class AddressListVm
    {
        public IEnumerable<AddressVm> Addresses { get; set; }
				
		public int SubjectId { get; set; }
		public List<SelectListItem> SubjectList { get; set; }
		
		public int AddressTypeId { get; set; }
		public List<SelectListItem> AddressTypeList { get; set; }
    }
}
