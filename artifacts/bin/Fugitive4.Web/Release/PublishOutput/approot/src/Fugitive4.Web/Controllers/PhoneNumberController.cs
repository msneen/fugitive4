using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.PhoneNumber;
using Microsoft.AspNet.Authorization;

namespace Fugitive4.Web.Controllers
{
    [Authorize]
    public class PhoneNumberController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly IPhoneNumberService _phoneNumberService;					
		private readonly IPhoneTypeService _phoneTypeService;					
		private readonly ISubjectService _subjectService;

		public PhoneNumberController(IPhoneNumberService phoneNumberService,
								IPhoneTypeService phoneTypeService,
								ISubjectService subjectService
,
								IMapper mapper		)
        {
            _phoneNumberService = phoneNumberService;
			_mapper = mapper;
			_phoneTypeService = phoneTypeService;
			_subjectService = subjectService;

        }

        [Route("Subject/{subjectId}/PhoneNumber/Index")]
        public async Task<IActionResult> Index(int subjectId, string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";

            var phoneNumbers = await _phoneNumberService.GetAllAsync();
            var phoneNumberList = new PhoneNumberListVm
            {
                PhoneNumbers = _mapper.Map<IEnumerable<PhoneNumber>, IEnumerable<PhoneNumberVm>>(phoneNumbers.Where(s=>s.SubjectId==subjectId).ToList())
            };

            return View("Index", phoneNumberList);
        }

		public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            PhoneNumber phoneNumber = await _phoneNumberService.GetByIdAsync(id.Value);
            if (phoneNumber == null)
            {
                return HttpNotFound();
            }

            var phoneNumberVm = _mapper.Map<PhoneNumber, PhoneNumberVm>(phoneNumber);

            return View(phoneNumberVm);
        }

        [Route("Subject/{subjectId}/PhoneNumber/Create", Name = "PhoneNumberForSubject")]
        public ActionResult Create(int subjectId, string returnUrl = "")
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";
            ViewBag.ReturnUrl = returnUrl;

            var phoneNumberVm = new PhoneNumberVm {SubjectId = subjectId};
            phoneNumberVm.SubjectId = subjectId;

            LoadLookups(phoneNumberVm);

            return View(phoneNumberVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Subject/{subjectId}/PhoneNumber/Create", Name = "PhoneNumberForSubject")]
        public async Task<IActionResult> Create(PhoneNumberVm phoneNumberVm, string returnUrl = "")
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var phoneNumber = _mapper.Map<PhoneNumberVm, PhoneNumber>(phoneNumberVm);
                    await _phoneNumberService.InsertAsync(phoneNumber, GetLogonUser());
                    return RedirectToIndexOrSubjectDetail(returnUrl); //RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(phoneNumberVm);

                return View(phoneNumberVm);
            }
        }

		public async Task<IActionResult> Edit( int? id)
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";

			if (id == null)
            {
                return HttpNotFound();
            }

            var phoneNumber = await _phoneNumberService.GetByIdAsync(id.Value);
            if (phoneNumber == null)
            {
                return HttpNotFound();
            }            
			
			var phoneNumberVm = _mapper.Map<PhoneNumber, PhoneNumberVm>(phoneNumber);

			LoadLookups(phoneNumberVm);

            return View(phoneNumberVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(PhoneNumberVm phoneNumberVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var phoneNumber = _mapper.Map<PhoneNumberVm, PhoneNumber>(phoneNumberVm); 
                    await _phoneNumberService.UpdateAsync(phoneNumber, GetLogonUser());
					return RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(phoneNumberVm);

                return View(phoneNumberVm);
            }
        }

		[ActionName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var phoneNumber = await _phoneNumberService.GetByIdAsync(id.Value);
			if (phoneNumber == null)
            {
                return HttpNotFound();
            }

            var phoneNumberVm = _mapper.Map<PhoneNumber, PhoneNumberVm>(phoneNumber);

            return View(phoneNumberVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _phoneNumberService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(PhoneNumberVm phoneNumberVm)
        {
			phoneNumberVm.PhoneTypeList = _phoneTypeService.GetAll().ToSelectList(s => s.Id.ToString(), s => s.Name).ToList();
			phoneNumberVm.SubjectList = _subjectService.GetAll().ToSelectList(s => s.Id.ToString(), s => s.EyeColor).ToList();
		}

	}
}
