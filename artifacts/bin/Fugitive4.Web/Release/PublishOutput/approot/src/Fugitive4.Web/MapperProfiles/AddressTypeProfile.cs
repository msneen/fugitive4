using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.AddressType;


namespace Fugitive4.Web.MapperProfiles
{
    public class AddressTypeProfile : Profile
    {
        protected override void Configure()
        {
			CreateMap<AddressType, AddressTypeVm>().ReverseMap();
        }
    }
}
