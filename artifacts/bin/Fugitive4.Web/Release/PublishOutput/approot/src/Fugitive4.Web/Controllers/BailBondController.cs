using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.BailBond;
using Microsoft.AspNet.Authorization;

namespace Fugitive4.Web.Controllers
{
    [Authorize]
    public class BailBondController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly IBailBondService _bailBondService;					
		private readonly ISubjectService _subjectService;

		public BailBondController(IBailBondService bailBondService,
								ISubjectService subjectService
,
								IMapper mapper		)
        {
            _bailBondService = bailBondService;
			_mapper = mapper;
			_subjectService = subjectService;

        }
[Route("Subject/{subjectId}/BailBond/Index")]
        public async Task<IActionResult> Index(int subjectId , string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";

            var bailBonds = await _bailBondService.GetAllAsync();
            var bailBondList = new BailBondListVm
            {
                BailBonds = _mapper.Map<IEnumerable<BailBond>, IEnumerable<BailBondVm>>(bailBonds.Where(s=>s.SubjectId==subjectId).ToList())
            };

            return View("Index", bailBondList);
        }

		public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            BailBond bailBond = await _bailBondService.GetByIdAsync(id.Value);
            if (bailBond == null)
            {
                return HttpNotFound();
            }

            var bailBondVm = _mapper.Map<BailBond, BailBondVm>(bailBond);

            return View(bailBondVm);
        }


[Route("Subject/{subjectId}/BailBond/Create", Name= "BailBondForSubject")]
		public ActionResult Create(int subjectId )
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";

            var bailBondVm = new BailBondVm {SubjectId = subjectId};

			LoadLookups(bailBondVm);

            return View(bailBondVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
[Route("Subject/{subjectId}/BailBond/Create", Name= "BailBondForSubject")]
        public async Task<IActionResult> Create(BailBondVm bailBondVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var bailBond = _mapper.Map<BailBondVm, BailBond>(bailBondVm);
                    await _bailBondService.InsertAsync(bailBond, GetLogonUser());
					return RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(bailBondVm);

                return View(bailBondVm);
            }
        }

[Route("Subject/{subjectId}/BailBond/Edit/{id}")]
		public async Task<IActionResult> Edit(int subjectId , int? id)
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";

			if (id == null)
            {
                return HttpNotFound();
            }

            var bailBond = await _bailBondService.GetByIdAsync(id.Value);
            if (bailBond == null)
            {
                return HttpNotFound();
            }            
			
			var bailBondVm = _mapper.Map<BailBond, BailBondVm>(bailBond);

			LoadLookups(bailBondVm);

            return View(bailBondVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
[Route("Subject/{subjectId}/BailBond/Edit/{id}")]
        public async Task<IActionResult> Edit(BailBondVm bailBondVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var bailBond = _mapper.Map<BailBondVm, BailBond>(bailBondVm); 
                    await _bailBondService.UpdateAsync(bailBond, GetLogonUser());
					return RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(bailBondVm);

                return View(bailBondVm);
            }
        }

		[ActionName("Delete")]
[Route("Subject/{subjectId}/BailBond/Delete/{id}")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var bailBond = await _bailBondService.GetByIdAsync(id.Value);
			if (bailBond == null)
            {
                return HttpNotFound();
            }

            var bailBondVm = _mapper.Map<BailBond, BailBondVm>(bailBond);

            return View(bailBondVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
[Route("Subject/{subjectId}/BailBond/Delete/{id}")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _bailBondService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(BailBondVm bailBondVm)
        {
			bailBondVm.SubjectList = _subjectService.GetAll().ToSelectList(s => s.Id.ToString(), s => s.EyeColor).ToList();
		}

	}
}
