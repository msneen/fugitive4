﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fugitive4.Web.ViewModels.Subject
{
    public class SubjectSearchVm
    {
        public string SearchCriteria { get; set; }
    }
}
