using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.BookingCharge;


namespace Fugitive4.Web.MapperProfiles
{
    public class BookingChargeProfile : Profile
    {
        protected override void Configure()
        {
			CreateMap<BookingCharge, BookingChargeVm>().ReverseMap();
        }
    }
}
