using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Mvc.Rendering;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Web.ViewModels.Referrer; //This is any parent or child vm
using Fugitive4.Web.ViewModels.Address; //This is any parent or child vm
using Fugitive4.Web.ViewModels.BailBond; //This is any parent or child vm
using Fugitive4.Web.ViewModels.EmailAddress; //This is any parent or child vm
using Fugitive4.Web.ViewModels.Gender; //This is any parent or child vm
using Fugitive4.Web.ViewModels.PhoneNumber; //This is any parent or child vm
using Fugitive4.Web.ViewModels.Vehicle; //This is any parent or child vm
using Fugitive4.Web.ViewModels.ActivityNote; //This is any parent or child vm
using Fugitive4.Web.ViewModels.DriverLicense; //This is any parent or child vm
using Fugitive4.Web.ViewModels.StoredFile; //This is any parent or child vm
using Fugitive4.Web.ViewModels.Hyperlink; //This is any parent or child vm
using Fugitive4.Web.ViewModels.SearchResult; //This is any parent or child vm
using Fugitive4.Web.ViewModels.Video; //This is any parent or child vm
using Fugitive4.Web.ViewModels.Race; //This is any parent or child vm
using Fugitive4.Web.ViewModels.SubjectRelationship; //This is any parent or child vm


namespace Fugitive4.Web.ViewModels.Subject
{
	public class SubjectVm : AuditableEntityBase
    {
		
		[Display(Name="Id")]	
		public int Id { get; set; }

		
		[Display(Name="DOB")]	
		public Nullable<System.DateTime> DOB { get; set; }

		
		[Display(Name="Eye Color")]	
		public string EyeColor { get; set; }

		
		[Display(Name="Firstname")]	
		public string Firstname { get; set; }

		
		[Display(Name="Hair Color")]	
		public string HairColor { get; set; }

		
		[Display(Name="Height")]	
		public string Height { get; set; }

		
		[Display(Name="Lastname")]	
		public string Lastname { get; set; }

		
		[Display(Name="Middlename")]	
		public string Middlename { get; set; }

		
		[Display(Name="SSN")]	
		public string SSN { get; set; }

		
		[Display(Name="Weight")]	
		public Nullable<int> Weight { get; set; }

		
		[Display(Name="Referrer Id")]	
		public int ReferrerId { get; set; }

		
		[Display(Name="Gender Id")]	
		public int GenderId { get; set; }

		
		[Display(Name="Notes")]	
		public string Notes { get; set; }

		
		[Display(Name="Race Id")]	
		public int RaceId { get; set; }

        public BailBondVm CurrentBond { get; set; }

        public ReferrerVm Referrer { get; set; } //Parent
        public List<SelectListItem> ReferrerList { get; set; } //Parent
		public IEnumerable<AddressVm> Addresses { get; set; }//Child
		public IEnumerable<BailBondVm> BailBonds { get; set; }//Child
		public IEnumerable<EmailAddressVm> EmailAddresses { get; set; }//Child
		
        public GenderVm Gender { get; set; } //Parent
        public List<SelectListItem> GenderList { get; set; } //Parent
		public IEnumerable<PhoneNumberVm> PhoneNumbers { get; set; }//Child
		public IEnumerable<VehicleVm> Vehicles { get; set; }//Child
		public IEnumerable<ActivityNoteVm> ActivityNotes { get; set; }//Child
		public IEnumerable<DriverLicenseVm> DriverLicenses { get; set; }//Child
		public IEnumerable<StoredFileVm> StoredFiles { get; set; }//Child
		public IEnumerable<HyperlinkVm> Hyperlinks { get; set; }//Child
		public IEnumerable<SearchResultVm> SearchResults { get; set; }//Child
		public IEnumerable<VideoVm> Videos { get; set; }//Child
		
        public RaceVm Race { get; set; } //Parent
        public List<SelectListItem> RaceList { get; set; } //Parent
		public IEnumerable<SubjectRelationshipVm> SubjectRelationships { get; set; }//Child
		public IEnumerable<SubjectRelationshipVm> OtherSubjectRelationships { get; set; }//Child

        //This is custom, for adding an new subject and a relationship at the same time
        public SubjectRelationshipVm RelatedSubject { get; set; }
    }

    public class SubjectListVm
    {
        public IEnumerable<SubjectVm> Subjects { get; set; }
				
		public int ReferrerId { get; set; }
		public List<SelectListItem> ReferrerList { get; set; }
		
		public int GenderId { get; set; }
		public List<SelectListItem> GenderList { get; set; }
		
		public int RaceId { get; set; }
		public List<SelectListItem> RaceList { get; set; }
    }
}
