using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.SearchResult;
using Microsoft.AspNet.Authorization;

namespace Fugitive4.Web.Controllers
{
	[Authorize]
    public class SearchResultController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly ISearchResultService _searchResultService;					
		private readonly ISubjectService _subjectService;

		public SearchResultController(ISearchResultService searchResultService,
								ISubjectService subjectService
,
								IMapper mapper		)
        {
            _searchResultService = searchResultService;
			_mapper = mapper;
			_subjectService = subjectService;

        }
[Route("Subject/{subjectId}/SearchResult/Index")]
        public async Task<IActionResult> Index(int subjectId , string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";

            var searchResults = await _searchResultService.GetAllAsync();
            var searchResultList = new SearchResultListVm
            {
                SearchResults = _mapper.Map<IEnumerable<SearchResult>, IEnumerable<SearchResultVm>>(searchResults.Where(s=>s.SubjectId==subjectId).ToList())
            };

            return View("Index", searchResultList);
        }

		public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            SearchResult searchResult = await _searchResultService.GetByIdAsync(id.Value);
            if (searchResult == null)
            {
                return HttpNotFound();
            }

            var searchResultVm = _mapper.Map<SearchResult, SearchResultVm>(searchResult);

            return View(searchResultVm);
        }


[Route("Subject/{subjectId}/SearchResult/Create", Name = "SearchResultsForSubject")]
		public ActionResult Create(int subjectId )
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";

    var searchResultVm = new SearchResultVm {SubjectId = subjectId};

    LoadLookups(searchResultVm);

            return View(searchResultVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
[Route("Subject/{subjectId}/SearchResult/Create", Name = "SearchResultsForSubject")]
        public async Task<IActionResult> Create(SearchResultVm searchResultVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var searchResult = _mapper.Map<SearchResultVm, SearchResult>(searchResultVm);
                    await _searchResultService.InsertAsync(searchResult, GetLogonUser());
					return RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(searchResultVm);

                return View(searchResultVm);
            }
        }

[Route("Subject/{subjectId}/SearchResult/Edit/{id}")]
		public async Task<IActionResult> Edit(int subjectId , int? id)
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";

			if (id == null)
            {
                return HttpNotFound();
            }

            var searchResult = await _searchResultService.GetByIdAsync(id.Value);
            if (searchResult == null)
            {
                return HttpNotFound();
            }            
			
			var searchResultVm = _mapper.Map<SearchResult, SearchResultVm>(searchResult);

			LoadLookups(searchResultVm);

            return View(searchResultVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
[Route("Subject/{subjectId}/SearchResult/Edit/{id}")]
        public async Task<IActionResult> Edit(SearchResultVm searchResultVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var searchResult = _mapper.Map<SearchResultVm, SearchResult>(searchResultVm); 
                    await _searchResultService.UpdateAsync(searchResult, GetLogonUser());
					return RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(searchResultVm);

                return View(searchResultVm);
            }
        }

		[ActionName("Delete")]
[Route("Subject/{subjectId}/SearchResult/Delete/{id}")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var searchResult = await _searchResultService.GetByIdAsync(id.Value);
			if (searchResult == null)
            {
                return HttpNotFound();
            }

            var searchResultVm = _mapper.Map<SearchResult, SearchResultVm>(searchResult);

            return View(searchResultVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
[Route("Subject/{subjectId}/SearchResult/Delete/{id}")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _searchResultService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(SearchResultVm searchResultVm)
        {
			searchResultVm.SubjectList = _subjectService.GetAll().ToSelectList(s => s.Id.ToString(), s => s.EyeColor).ToList();
		}

	}
}
