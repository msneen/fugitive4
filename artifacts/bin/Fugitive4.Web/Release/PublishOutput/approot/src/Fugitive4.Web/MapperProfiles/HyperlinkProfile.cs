using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.Hyperlink;


namespace Fugitive4.Web.MapperProfiles
{
    public class HyperlinkProfile : Profile
    {
        protected override void Configure()
        {
			CreateMap<Hyperlink, HyperlinkVm>().ReverseMap();
        }
    }
}
