using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.DriverLicense;


namespace Fugitive4.Web.MapperProfiles
{
    public class DriverLicenseProfile : Profile
    {
        protected override void Configure()
        {
			CreateMap<DriverLicense, DriverLicenseVm>().ReverseMap();
        }
    }
}
