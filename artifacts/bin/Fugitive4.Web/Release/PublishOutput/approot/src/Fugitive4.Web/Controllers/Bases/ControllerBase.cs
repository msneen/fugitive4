﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;

namespace Fugitive4.Web.Controllers.Bases
{
    public class ControllerBase : Controller
    {
        internal string GetLogonUser()
        {
            //Put me in a base class
            //This should be get userId
            var user = HttpContext.User.Identity.Name;
            return user;
        }

        internal ActionResult RedirectToIndexOrSubjectDetail(string returnUrl)
        {
            if(string.IsNullOrWhiteSpace(returnUrl))
            {
                return RedirectToAction("Index");
            }
            else
            {
                var fullUrl = string.Format("{0}://{1}{2}", HttpContext.Request.Scheme, HttpContext.Request.Host.Value, returnUrl);
                return Redirect(fullUrl);
            }
        }

        internal void LogError(Exception ex)
        {
            
        }
    }
}
