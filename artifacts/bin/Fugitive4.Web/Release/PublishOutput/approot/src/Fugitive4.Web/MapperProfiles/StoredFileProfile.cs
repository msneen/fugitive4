using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.StoredFile;


namespace Fugitive4.Web.MapperProfiles
{
    public class StoredFileProfile : Profile
    {
        protected override void Configure()
        {
			CreateMap<StoredFile, StoredFileVm>().ReverseMap();
        }
    }
}
