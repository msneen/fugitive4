using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.Video;


namespace Fugitive4.Web.MapperProfiles
{
    public class VideoProfile : Profile
    {
        protected override void Configure()
        {
			CreateMap<Video, VideoVm>().ReverseMap();
        }
    }
}
