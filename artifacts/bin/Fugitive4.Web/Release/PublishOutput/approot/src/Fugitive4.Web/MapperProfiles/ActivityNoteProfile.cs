using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.ActivityNote;


namespace Fugitive4.Web.MapperProfiles
{
    public class ActivityNoteProfile : Profile
    {
        protected override void Configure()
        {
			CreateMap<ActivityNote, ActivityNoteVm>().ReverseMap();
        }
    }
}
