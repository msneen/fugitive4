using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.SubjectRelationship;


namespace Fugitive4.Web.MapperProfiles
{
    public class SubjectRelationshipProfile : Profile
    {
        protected override void Configure()
        {
			CreateMap<SubjectRelationship, SubjectRelationshipVm>().ReverseMap();
        }
    }
}
