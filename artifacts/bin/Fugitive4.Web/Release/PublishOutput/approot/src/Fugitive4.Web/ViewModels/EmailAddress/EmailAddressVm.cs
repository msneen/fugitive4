using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Mvc.Rendering;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Web.ViewModels.EmailType; //This is any parent or child vm
using Fugitive4.Web.ViewModels.Subject; //This is any parent or child vm

namespace Fugitive4.Web.ViewModels.EmailAddress
{
	public class EmailAddressVm : AuditableEntityBase
    {
		
		[Display(Name="Id")]	
		public int Id { get; set; }

		[Required]
		[StringLength(50, MinimumLength = 2, ErrorMessage = "field must be between 2 and 50 characters")]		
		[Display(Name="Email Address 1")]	
		public string EmailAddress1 { get; set; }

		[StringLength(50,  ErrorMessage = "field must be less than 50 characters")]		
		[Display(Name="Belongs To")]	
		public string BelongsTo { get; set; }

		
		[Display(Name="Subject Id")]	
		public int SubjectId { get; set; }

		
		[Display(Name="Email Type Id")]	
		public int EmailTypeId { get; set; }

		
		[Display(Name="Notes")]	
		public string Notes { get; set; }


		
        public EmailTypeVm EmailType { get; set; } //Parent
        public List<SelectListItem> EmailTypeList { get; set; } //Parent
		
        public SubjectVm Subject { get; set; } //Parent
        public List<SelectListItem> SubjectList { get; set; } //Parent
    }

    public class EmailAddressListVm
    {
        public IEnumerable<EmailAddressVm> EmailAddresses { get; set; }
				
		public int EmailTypeId { get; set; }
		public List<SelectListItem> EmailTypeList { get; set; }
		
		public int SubjectId { get; set; }
		public List<SelectListItem> SubjectList { get; set; }
    }
}
