using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.Vehicle;
using Microsoft.AspNet.Authorization;

namespace Fugitive4.Web.Controllers
{
    [Authorize]
    public class VehicleController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly IVehicleService _vehicleService;					
		private readonly ISubjectService _subjectService;

		public VehicleController(IVehicleService vehicleService,
								ISubjectService subjectService
,
								IMapper mapper		)
        {
            _vehicleService = vehicleService;
			_mapper = mapper;
			_subjectService = subjectService;

        }
[Route("Subject/{subjectId}/Vehicle/Index")]
        public async Task<IActionResult> Index(int subjectId , string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";

            var vehicles = await _vehicleService.GetAllAsync();
            var vehicleList = new VehicleListVm
            {
                Vehicles = _mapper.Map<IEnumerable<Vehicle>, IEnumerable<VehicleVm>>(vehicles.Where(s=>subjectId==subjectId).ToList())
            };

            return View("Index", vehicleList);
        }

		public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            Vehicle vehicle = await _vehicleService.GetByIdAsync(id.Value);
            if (vehicle == null)
            {
                return HttpNotFound();
            }

            var vehicleVm = _mapper.Map<Vehicle, VehicleVm>(vehicle);

            return View(vehicleVm);
        }


[Route("Subject/{subjectId}/Vehicle/Create", Name="VehicleForSubject")]
		public ActionResult Create(int subjectId )
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";

            var vehicleVm = new VehicleVm {SubjectId = subjectId};

			LoadLookups(vehicleVm);

            return View(vehicleVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
[Route("Subject/{subjectId}/Vehicle/Create")]
        public async Task<IActionResult> Create(VehicleVm vehicleVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var vehicle = _mapper.Map<VehicleVm, Vehicle>(vehicleVm);
                    await _vehicleService.InsertAsync(vehicle, GetLogonUser());
					return RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(vehicleVm);

                return View(vehicleVm);
            }
        }

[Route("Subject/{subjectId}/Vehicle/Edit/{id}")]
		public async Task<IActionResult> Edit(int subjectId , int? id)
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";

			if (id == null)
            {
                return HttpNotFound();
            }

            var vehicle = await _vehicleService.GetByIdAsync(id.Value);
            if (vehicle == null)
            {
                return HttpNotFound();
            }            
			
			var vehicleVm = _mapper.Map<Vehicle, VehicleVm>(vehicle);

			LoadLookups(vehicleVm);

            return View(vehicleVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
[Route("Subject/{subjectId}/Vehicle/Edit/{id}")]
        public async Task<IActionResult> Edit(VehicleVm vehicleVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var vehicle = _mapper.Map<VehicleVm, Vehicle>(vehicleVm); 
                    await _vehicleService.UpdateAsync(vehicle, GetLogonUser());
					return RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(vehicleVm);

                return View(vehicleVm);
            }
        }

		[ActionName("Delete")]
[Route("Subject/{subjectId}/Vehicle/Delete/{id}")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var vehicle = await _vehicleService.GetByIdAsync(id.Value);
			if (vehicle == null)
            {
                return HttpNotFound();
            }

            var vehicleVm = _mapper.Map<Vehicle, VehicleVm>(vehicle);

            return View(vehicleVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
[Route("Subject/{subjectId}/Vehicle/Delete/{id}")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _vehicleService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(VehicleVm vehicleVm)
        {
			vehicleVm.SubjectList = _subjectService.GetAll().ToSelectList(s => s.Id.ToString(), s => s.EyeColor).ToList();
		}

	}
}
