using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Mvc.Rendering;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Web.ViewModels.SubjectRelationship; //This is any parent or child vm

namespace Fugitive4.Web.ViewModels.RelationshipType
{
	public class RelationshipTypeVm : AuditableEntityBase
    {
		
		[Display(Name="Id")]	
		public int Id { get; set; }

		[Required]
		[StringLength(50, MinimumLength = 2, ErrorMessage = "field must be between 2 and 50 characters")]		
		[Display(Name="Name")]	
		public string Name { get; set; }


		public IEnumerable<SubjectRelationshipVm> SubjectRelationships { get; set; }//Child
    }

    public class RelationshipTypeListVm
    {
        public IEnumerable<RelationshipTypeVm> RelationshipTypes { get; set; }
		    }
}
