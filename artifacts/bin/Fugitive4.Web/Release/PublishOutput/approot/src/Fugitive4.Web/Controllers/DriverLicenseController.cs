using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.DriverLicense;
using Microsoft.AspNet.Authorization;

namespace Fugitive4.Web.Controllers
{
    [Authorize]
    public class DriverLicenseController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly IDriverLicenseService _driverLicenseService;					
		private readonly ISubjectService _subjectService;

		public DriverLicenseController(IDriverLicenseService driverLicenseService,
								ISubjectService subjectService
,
								IMapper mapper		)
        {
            _driverLicenseService = driverLicenseService;
			_mapper = mapper;
			_subjectService = subjectService;

        }
[Route("Subject/{subjectId}/DriverLicense/Index")]
        public async Task<IActionResult> Index(int subjectId , string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";

            var driverLicenses = await _driverLicenseService.GetAllAsync();
            var driverLicenseList = new DriverLicenseListVm
            {
                DriverLicenses = _mapper.Map<IEnumerable<DriverLicense>, IEnumerable<DriverLicenseVm>>(driverLicenses.Where(s=>s.SubjectId==subjectId).ToList())
            };

            return View("Index", driverLicenseList);
        }

		public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            DriverLicense driverLicense = await _driverLicenseService.GetByIdAsync(id.Value);
            if (driverLicense == null)
            {
                return HttpNotFound();
            }

            var driverLicenseVm = _mapper.Map<DriverLicense, DriverLicenseVm>(driverLicense);

            return View(driverLicenseVm);
        }


[Route("Subject/{subjectId}/DriverLicense/Create", Name= "DriverLicenseForSubject")]
		public ActionResult Create(int subjectId )
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";

            var driverLicenseVm = new DriverLicenseVm {SubjectId = subjectId};

			LoadLookups(driverLicenseVm);

            return View(driverLicenseVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
[Route("Subject/{subjectId}/DriverLicense/Create")]
        public async Task<IActionResult> Create(DriverLicenseVm driverLicenseVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var driverLicense = _mapper.Map<DriverLicenseVm, DriverLicense>(driverLicenseVm);
                    await _driverLicenseService.InsertAsync(driverLicense, GetLogonUser());
					return RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(driverLicenseVm);

                return View(driverLicenseVm);
            }
        }

[Route("Subject/{subjectId}/DriverLicense/Edit/{id}")]
		public async Task<IActionResult> Edit(int subjectId , int? id)
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";

			if (id == null)
            {
                return HttpNotFound();
            }

            var driverLicense = await _driverLicenseService.GetByIdAsync(id.Value);
            if (driverLicense == null)
            {
                return HttpNotFound();
            }            
			
			var driverLicenseVm = _mapper.Map<DriverLicense, DriverLicenseVm>(driverLicense);

			LoadLookups(driverLicenseVm);

            return View(driverLicenseVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
[Route("Subject/{subjectId}/DriverLicense/Edit/{id}")]
        public async Task<IActionResult> Edit(DriverLicenseVm driverLicenseVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var driverLicense = _mapper.Map<DriverLicenseVm, DriverLicense>(driverLicenseVm); 
                    await _driverLicenseService.UpdateAsync(driverLicense, GetLogonUser());
					return RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(driverLicenseVm);

                return View(driverLicenseVm);
            }
        }

		[ActionName("Delete")]
[Route("Subject/{subjectId}/DriverLicense/Delete/{id}")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var driverLicense = await _driverLicenseService.GetByIdAsync(id.Value);
			if (driverLicense == null)
            {
                return HttpNotFound();
            }

            var driverLicenseVm = _mapper.Map<DriverLicense, DriverLicenseVm>(driverLicense);

            return View(driverLicenseVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
[Route("Subject/{subjectId}/DriverLicense/Delete/{id}")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _driverLicenseService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(DriverLicenseVm driverLicenseVm)
        {
			driverLicenseVm.SubjectList = _subjectService.GetAll().ToSelectList(s => s.Id.ToString(), s => s.EyeColor).ToList();
		}

	}
}
