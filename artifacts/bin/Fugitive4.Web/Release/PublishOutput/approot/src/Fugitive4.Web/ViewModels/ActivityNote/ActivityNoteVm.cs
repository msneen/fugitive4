using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Mvc.Rendering;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Web.ViewModels.Subject; //This is any parent or child vm

namespace Fugitive4.Web.ViewModels.ActivityNote
{
	public class ActivityNoteVm : AuditableEntityBase
    {
		
		[Display(Name="Id")]	
		public int Id { get; set; }

		
		[Display(Name="Activity Date")]	
		public System.DateTime ActivityDate { get; set; }

		[StringLength(30,  ErrorMessage = "field must be less than 30 characters")]		
		[Display(Name="Latitude")]	
		public string Latitude { get; set; }

		[StringLength(30,  ErrorMessage = "field must be less than 30 characters")]		
		[Display(Name="Longitude")]	
		public string Longitude { get; set; }

		[StringLength(1000,  ErrorMessage = "field must be less than 10 characters")]		
		[Display(Name="Address")]	
		public string Address { get; set; }

		[StringLength(128,  ErrorMessage = "field must be less than 128 characters")]		
		[Display(Name="Performed By")]	
		public string PerformedBy { get; set; }

		[Required]		
		[Display(Name="Description")]	
		public string Description { get; set; }

		
		[Display(Name="Subject Id")]	
		public int SubjectId { get; set; }


		
        public SubjectVm Subject { get; set; } //Parent
        public List<SelectListItem> SubjectList { get; set; } //Parent
    }

    public class ActivityNoteListVm
    {
        public IEnumerable<ActivityNoteVm> ActivityNotes { get; set; }
				
		public int SubjectId { get; set; }
		public List<SelectListItem> SubjectList { get; set; }
    }
}
