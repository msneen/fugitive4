using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.BailBond;


namespace Fugitive4.Web.MapperProfiles
{
    public class BailBondProfile : Profile
    {
        protected override void Configure()
        {
			CreateMap<BailBond, BailBondVm>().ReverseMap();
        }
    }
}
