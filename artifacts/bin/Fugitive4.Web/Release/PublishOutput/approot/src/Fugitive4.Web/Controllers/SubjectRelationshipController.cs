using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.SubjectRelationship;
using Microsoft.AspNet.Authorization;

namespace Fugitive4.Web.Controllers
{
	[Authorize]
    public class SubjectRelationshipController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly ISubjectRelationshipService _subjectRelationshipService;					
		private readonly IRelationshipTypeService _relationshipTypeService;					
		private readonly ISubjectService _subjectService;					


		public SubjectRelationshipController(ISubjectRelationshipService subjectRelationshipService,
								IRelationshipTypeService relationshipTypeService,
								ISubjectService subjectService,
								IMapper mapper		)
        {
            _subjectRelationshipService = subjectRelationshipService;
			_mapper = mapper;
			_relationshipTypeService = relationshipTypeService;
			_subjectService = subjectService;

        }
        public async Task<IActionResult> Index( string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";

            var subjectRelationships = await _subjectRelationshipService.GetAllAsync();
            var subjectRelationshipList = new SubjectRelationshipListVm
            {
                SubjectRelationships = _mapper.Map<IEnumerable<SubjectRelationship>, IEnumerable<SubjectRelationshipVm>>(subjectRelationships.ToList())
            };

            return View("Index", subjectRelationshipList);
        }

		public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            SubjectRelationship subjectRelationship = await _subjectRelationshipService.GetByIdAsync(id.Value);
            if (subjectRelationship == null)
            {
                return HttpNotFound();
            }

            var subjectRelationshipVm = _mapper.Map<SubjectRelationship, SubjectRelationshipVm>(subjectRelationship);

            return View(subjectRelationshipVm);
        }


		public ActionResult Create()
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";

            var subjectRelationshipVm = new SubjectRelationshipVm();

			LoadLookups(subjectRelationshipVm);

            return View(subjectRelationshipVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(SubjectRelationshipVm subjectRelationshipVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var subjectRelationship = _mapper.Map<SubjectRelationshipVm, SubjectRelationship>(subjectRelationshipVm);
                    await _subjectRelationshipService.InsertAsync(subjectRelationship, GetLogonUser());
					return RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(subjectRelationshipVm);

                return View(subjectRelationshipVm);
            }
        }

		public async Task<IActionResult> Edit( int? id)
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";

			if (id == null)
            {
                return HttpNotFound();
            }

            var subjectRelationship = await _subjectRelationshipService.GetByIdAsync(id.Value);
            if (subjectRelationship == null)
            {
                return HttpNotFound();
            }            
			
			var subjectRelationshipVm = _mapper.Map<SubjectRelationship, SubjectRelationshipVm>(subjectRelationship);

			LoadLookups(subjectRelationshipVm);

            return View(subjectRelationshipVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(SubjectRelationshipVm subjectRelationshipVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var subjectRelationship = _mapper.Map<SubjectRelationshipVm, SubjectRelationship>(subjectRelationshipVm); 
                    await _subjectRelationshipService.UpdateAsync(subjectRelationship, GetLogonUser());
					return RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(subjectRelationshipVm);

                return View(subjectRelationshipVm);
            }
        }

		[ActionName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var subjectRelationship = await _subjectRelationshipService.GetByIdAsync(id.Value);
			if (subjectRelationship == null)
            {
                return HttpNotFound();
            }

            var subjectRelationshipVm = _mapper.Map<SubjectRelationship, SubjectRelationshipVm>(subjectRelationship);

            return View(subjectRelationshipVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _subjectRelationshipService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(SubjectRelationshipVm subjectRelationshipVm)
        {
			subjectRelationshipVm.RelationshipTypeList = _relationshipTypeService.GetAll().ToSelectList(s => s.Id.ToString(), s => s.Name).ToList();
            var subjectList = _subjectService.GetAll().ToSelectList(s => s.Id.ToString(), s => s.Lastname + ", " + s.Firstname).ToList();
            subjectRelationshipVm.SubjectList = subjectList;
            subjectRelationshipVm.RelatedSubjectList = subjectList;

        }

	}
}
