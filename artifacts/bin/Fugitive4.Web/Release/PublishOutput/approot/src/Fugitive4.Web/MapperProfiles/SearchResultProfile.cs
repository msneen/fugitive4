using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.SearchResult;


namespace Fugitive4.Web.MapperProfiles
{
    public class SearchResultProfile : Profile
    {
        protected override void Configure()
        {
			CreateMap<SearchResult, SearchResultVm>().ReverseMap();
        }
    }
}
