﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Data.Entity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Fugitive4.Services.Models;
using Fugitive4.Services.Models.SampleData;
using Fugitive4.Services.Services;
using Fugitive4.Web.Services;
using Microsoft.Extensions.DependencyInjection.Extensions;
using AutoMapper;

namespace Fugitive4.Web
{
    public class Startup
    {
        private MapperConfiguration _mapperConfiguration { get; set; }

        public Startup(IHostingEnvironment env)
        {
            // Set up configuration sources.

            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);

            if (env.IsDevelopment())
            {
                // For more details on using the user secret store see http://go.microsoft.com/fwlink/?LinkID=532709
                builder.AddUserSecrets();

                // This will push telemetry data through Application Insights pipeline faster, allowing you to view results immediately.
                builder.AddApplicationInsightsSettings(developerMode: true);
            }

            builder.AddEnvironmentVariables();
            Configuration = builder.Build();

            RegisterAutomapperProfiles();
        }

        private void RegisterAutomapperProfiles()
        {
            _mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<MapperProfiles.SubjectProfile>();
                cfg.AddProfile<MapperProfiles.ReferrerProfile>();
                cfg.AddProfile<MapperProfiles.AddressProfile>();
                cfg.AddProfile<MapperProfiles.AddressTypeProfile>();
                cfg.AddProfile<MapperProfiles.GenderProfile>();
                cfg.AddProfile<MapperProfiles.PhoneTypeProfile>();
                cfg.AddProfile<MapperProfiles.PhoneNumberProfile>();           
                cfg.AddProfile<MapperProfiles.EmailTypeProfile>();
                cfg.AddProfile<MapperProfiles.EmailAddressProfile>();
                cfg.AddProfile<MapperProfiles.BailBondProfile>();
                cfg.AddProfile<MapperProfiles.VehicleProfile>();
                cfg.AddProfile<MapperProfiles.ActivityNoteProfile>();
                cfg.AddProfile<MapperProfiles.DriverLicenseProfile>();
                cfg.AddProfile<MapperProfiles.StoredFileProfile>();
                cfg.AddProfile<MapperProfiles.HyperlinkProfile>();
                cfg.AddProfile<MapperProfiles.SearchResultProfile>();
                cfg.AddProfile<MapperProfiles.VideoProfile>();
                cfg.AddProfile<MapperProfiles.RaceProfile>();
                cfg.AddProfile<MapperProfiles.RelationshipTypeProfile>();
                cfg.AddProfile<MapperProfiles.SubjectRelationshipProfile>();
                cfg.AddProfile<MapperProfiles.BookingChargeProfile>();
            });
        }

        public IConfigurationRoot Configuration { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddApplicationInsightsTelemetry(Configuration);

            services.AddEntityFramework()
                .AddSqlServer()
                .AddDbContext<ApplicationDbContext>(options =>
                    options.UseSqlServer(Configuration["Data:DefaultConnection:ConnectionString"]));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>()
                .AddDefaultTokenProviders();

            services.AddMvc();

            // Add application services.
            services.AddTransient<IEmailSender, AuthMessageSender>();
            services.AddTransient<ISmsSender, AuthMessageSender>();

            DependencyRegistration.Register(services);
            services.AddSingleton<IMapper>(sp => _mapperConfiguration.CreateMapper());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseApplicationInsightsRequestTelemetry();

            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");

                // For more details on creating database during deployment see http://go.microsoft.com/fwlink/?LinkID=615859
                try
                {
                    using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>()
                        .CreateScope())
                    {
                        serviceScope.ServiceProvider.GetService<ApplicationDbContext>()
                             .Database.Migrate();
                    }
                }
                catch { }
            }

            app.UseIISPlatformHandler(options => options.AuthenticationDescriptions.Clear());

            app.UseApplicationInsightsExceptionTelemetry();

            app.UseStaticFiles();

            app.UseIdentity();

            // To configure external authentication please see http://go.microsoft.com/fwlink/?LinkID=532715

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });


            ReferrerSampleData.Initialize(app.ApplicationServices);            
            AddressTypeSampleData.Initialize(app.ApplicationServices);
            GenderSampleData.Initialize(app.ApplicationServices);
            PhoneTypeSampleData.Initialize(app.ApplicationServices);
            EmailTypeSampleData.Initialize(app.ApplicationServices);
            RaceSampleData.Initialize(app.ApplicationServices);
            RelationshipTypeSampleData.Initialize(app.ApplicationServices);

            SubjectSampleData.Initialize(app.ApplicationServices);
        }

        // Entry point for the application.
        public static void Main(string[] args) => WebApplication.Run<Startup>(args);
    }
}
