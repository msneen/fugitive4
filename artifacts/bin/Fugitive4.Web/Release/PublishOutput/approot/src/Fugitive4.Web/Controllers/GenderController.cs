using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.Gender;
using Microsoft.AspNet.Authorization;

namespace Fugitive4.Web.Controllers
{
    [Authorize]
    public class GenderController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly IGenderService _genderService;

		public GenderController(IGenderService genderService,
								IMapper mapper		)
        {
            _genderService = genderService;
			_mapper = mapper;

        }
        public async Task<IActionResult> Index( string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";

            var genders = await _genderService.GetAllAsync();
            var genderList = new GenderListVm
            {
                Genders = _mapper.Map<IEnumerable<Gender>, IEnumerable<GenderVm>>(genders.ToList())
            };

            return View("Index", genderList);
        }

		public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            Gender gender = await _genderService.GetByIdAsync(id.Value);
            if (gender == null)
            {
                return HttpNotFound();
            }

            var genderVm = _mapper.Map<Gender, GenderVm>(gender);

            return View(genderVm);
        }


		public ActionResult Create()
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";

            var genderVm = new GenderVm();

			LoadLookups(genderVm);

            return View(genderVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(GenderVm genderVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var gender = _mapper.Map<GenderVm, Gender>(genderVm);
                    await _genderService.InsertAsync(gender, GetLogonUser());
					return RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(genderVm);

                return View(genderVm);
            }
        }

		public async Task<IActionResult> Edit( int? id)
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";

			if (id == null)
            {
                return HttpNotFound();
            }

            var gender = await _genderService.GetByIdAsync(id.Value);
            if (gender == null)
            {
                return HttpNotFound();
            }            
			
			var genderVm = _mapper.Map<Gender, GenderVm>(gender);

			LoadLookups(genderVm);

            return View(genderVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(GenderVm genderVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var gender = _mapper.Map<GenderVm, Gender>(genderVm); 
                    await _genderService.UpdateAsync(gender, GetLogonUser());
					return RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(genderVm);

                return View(genderVm);
            }
        }

		[ActionName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var gender = await _genderService.GetByIdAsync(id.Value);
			if (gender == null)
            {
                return HttpNotFound();
            }

            var genderVm = _mapper.Map<Gender, GenderVm>(gender);

            return View(genderVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _genderService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(GenderVm genderVm)
        {
		}

	}
}
