using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.BailBond;
using Fugitive4.Web.ViewModels.Subject;
using Microsoft.AspNet.Authorization;
using Fugitive4.Web.ViewModels.SubjectRelationship;

namespace Fugitive4.Web.Controllers
{
	[Authorize]
    public class SubjectController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly ISubjectService _subjectService;					
		private readonly IReferrerService _referrerService;					
		private readonly IGenderService _genderService;					
		private readonly IRaceService _raceService;
	    private readonly ISubjectRelationshipService _subjectRelationshipService;
	    private readonly IRelationshipTypeService _relationshipTypeService;

	    public SubjectController(ISubjectService subjectService,
								IReferrerService referrerService,
								IGenderService genderService,
								IRaceService raceService,
                                ISubjectRelationshipService subjectRelationshipService,
                                IRelationshipTypeService relationshipTypeService,
								IMapper mapper		)
        {
            _subjectService = subjectService;
			_mapper = mapper;
			_referrerService = referrerService;
			_genderService = genderService;
			_raceService = raceService;
	        _subjectRelationshipService = subjectRelationshipService;
	        _relationshipTypeService = relationshipTypeService;
        }


        public async Task<IActionResult> Search()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Search(SubjectSearchVm subjectSearchVm)
        {
            return RedirectToAction(actionName: "Index", routeValues: new { searchCriteria = subjectSearchVm.SearchCriteria });
        }

        public async Task<IActionResult> Index( string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";

            var subjects = await _subjectService.GetAllAsync();


            if (!string.IsNullOrWhiteSpace(searchCriteria))
            {
                var search = searchCriteria.Split();
                var subjectFilter = subjects.Where(a => search.All(
                s => (a.Lastname ?? "").ToLower().Contains(s.ToLower()) ||
                (a.Firstname ?? "").ToLower().Contains(s.ToLower()) ||
                (a.Middlename ?? "").ToLower().Contains(s.ToLower())
                ));
                subjects = (subjectFilter == null ? new List<Subject>() : subjectFilter.ToList());
            }
            else
            {
                subjects = subjects
                    .Where(s => s.BailBonds.Any(b => b.DefaultDate >= DateTime.Now))
                    .OrderByDescending(s=>s.BailBonds.First().DefaultDate)
                    .ToList();
            }
            var subjectvms = _mapper.Map<IEnumerable<Subject>, IEnumerable<SubjectVm>>(subjects.ToList());
            foreach (var subject in subjectvms)
            {
                subject.CurrentBond = subject
                .BailBonds
                .OrderByDescending(t => t.DefaultDate)
                .Where(b => b.DefaultDate > DateTime.Now)
                .FirstOrDefault();
            }

            var subjectList = new SubjectListVm
            {
                Subjects = subjectvms
            };

            return View("Index", subjectList);
        }

		public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            Subject subject = await _subjectService.GetByIdAsync(id.Value);
            if (subject == null)
            {
                return HttpNotFound();
            }

            var subjectVm = _mapper.Map<Subject, SubjectVm>(subject);

            var currentBond = subject
                                .BailBonds
                                .OrderByDescending(t => t.DefaultDate)
                                .Where(b => b.DefaultDate > DateTime.Now)
                                .FirstOrDefault();

            subjectVm.CurrentBond = _mapper.Map<BailBond, BailBondVm>(currentBond);

            var subjectRelationships = _subjectRelationshipService.GetBySubjectId(id.Value).ToList();

            subjectVm.SubjectRelationships = _mapper.Map<IEnumerable<SubjectRelationship>, IEnumerable<ViewModels.SubjectRelationship.SubjectRelationshipVm>>(subjectRelationships);

            return View(subjectVm);
        }


		public ActionResult Create(int? relatedSubjectId = null, string returnUrl = "" )
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";
            ViewBag.ReturnUrl = returnUrl;

            var subjectVm = new SubjectVm();

			LoadLookups(subjectVm);

            if (relatedSubjectId.HasValue)
            {
                
                subjectVm.RelatedSubject = new SubjectRelationshipVm();
                subjectVm.RelatedSubject.RelatedSubjectId = relatedSubjectId.Value;
                LoadRelatedLookups(subjectVm);
            }            

            return View(subjectVm);
        }

        private void LoadRelatedLookups(SubjectVm subjectVm)
        {

            subjectVm.RelatedSubject.RelationshipTypeList = _relationshipTypeService.GetAll().ToSelectList(s => s.Id.ToString(), s => s.Name).ToList();
            var subjectList = _subjectService.GetAll().ToSelectList(s => s.Id.ToString(), s => s.Lastname + ", " + s.Firstname).ToList();

            subjectVm.RelatedSubject.RelatedSubjectList = subjectList;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(SubjectVm subjectVm, string returnUrl = "")
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var subject = _mapper.Map<SubjectVm, Subject>(subjectVm);
                    await _subjectService.InsertAsync(subject, GetLogonUser());

                    if(subjectVm.RelatedSubject != null)
                    {
                        var subjectRelationshipVm = subjectVm.RelatedSubject;
                        subjectRelationshipVm.SubjectId = subject.Id;
                        var subjectRelationship = _mapper.Map<SubjectRelationshipVm, SubjectRelationship>(subjectRelationshipVm);
                        await _subjectRelationshipService.InsertAsync(subjectRelationship, GetLogonUser());
                    }
					return RedirectToIndexOrSubjectDetail(returnUrl); //RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(subjectVm);

                return View(subjectVm);
            }
        }

		public async Task<IActionResult> Edit( int? id)
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";

			if (id == null)
            {
                return HttpNotFound();
            }

            var subject = await _subjectService.GetByIdAsync(id.Value);
            if (subject == null)
            {
                return HttpNotFound();
            }            
			
			var subjectVm = _mapper.Map<Subject, SubjectVm>(subject);

			LoadLookups(subjectVm);

            return View(subjectVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(SubjectVm subjectVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var subject = _mapper.Map<SubjectVm, Subject>(subjectVm); 
                    await _subjectService.UpdateAsync(subject, GetLogonUser());
					return RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(subjectVm);

                return View(subjectVm);
            }
        }

		[ActionName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var subject = await _subjectService.GetByIdAsync(id.Value);
			if (subject == null)
            {
                return HttpNotFound();
            }

            var subjectVm = _mapper.Map<Subject, SubjectVm>(subject);

            return View(subjectVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _subjectService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(SubjectVm subjectVm)
        {
			subjectVm.ReferrerList = _referrerService.GetAll().ToSelectList(s => s.Id.ToString(), s => s.AdditionalInfo).ToList();
			subjectVm.GenderList = _genderService.GetAll().ToSelectList(s => s.Id.ToString(), s => s.Name).ToList();
			subjectVm.RaceList = _raceService.GetAll().ToSelectList(s => s.Id.ToString(), s => s.Name).ToList();
		}

	}
}
