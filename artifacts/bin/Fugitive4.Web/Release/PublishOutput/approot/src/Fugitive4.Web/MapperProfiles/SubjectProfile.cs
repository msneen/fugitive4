using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.Subject;


namespace Fugitive4.Web.MapperProfiles
{
    public class SubjectProfile : Profile
    {
        protected override void Configure()
        {
			CreateMap<Subject, SubjectVm>().ReverseMap();
        }
    }
}
