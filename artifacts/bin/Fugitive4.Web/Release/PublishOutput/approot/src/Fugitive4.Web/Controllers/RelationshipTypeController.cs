using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.RelationshipType;
using Microsoft.AspNet.Authorization;

namespace Fugitive4.Web.Controllers
{
	[Authorize]
    public class RelationshipTypeController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly IRelationshipTypeService _relationshipTypeService;

		public RelationshipTypeController(IRelationshipTypeService relationshipTypeService,
								IMapper mapper		)
        {
            _relationshipTypeService = relationshipTypeService;
			_mapper = mapper;

        }
        public async Task<IActionResult> Index( string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";

            var relationshipTypes = await _relationshipTypeService.GetAllAsync();
            var relationshipTypeList = new RelationshipTypeListVm
            {
                RelationshipTypes = _mapper.Map<IEnumerable<RelationshipType>, IEnumerable<RelationshipTypeVm>>(relationshipTypes.ToList())
            };

            return View("Index", relationshipTypeList);
        }

		public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            RelationshipType relationshipType = await _relationshipTypeService.GetByIdAsync(id.Value);
            if (relationshipType == null)
            {
                return HttpNotFound();
            }

            var relationshipTypeVm = _mapper.Map<RelationshipType, RelationshipTypeVm>(relationshipType);

            return View(relationshipTypeVm);
        }


		public ActionResult Create()
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";

            var relationshipTypeVm = new RelationshipTypeVm();

			LoadLookups(relationshipTypeVm);

            return View(relationshipTypeVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(RelationshipTypeVm relationshipTypeVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var relationshipType = _mapper.Map<RelationshipTypeVm, RelationshipType>(relationshipTypeVm);
                    await _relationshipTypeService.InsertAsync(relationshipType, GetLogonUser());
					return RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(relationshipTypeVm);

                return View(relationshipTypeVm);
            }
        }

		public async Task<IActionResult> Edit( int? id)
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";

			if (id == null)
            {
                return HttpNotFound();
            }

            var relationshipType = await _relationshipTypeService.GetByIdAsync(id.Value);
            if (relationshipType == null)
            {
                return HttpNotFound();
            }            
			
			var relationshipTypeVm = _mapper.Map<RelationshipType, RelationshipTypeVm>(relationshipType);

			LoadLookups(relationshipTypeVm);

            return View(relationshipTypeVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(RelationshipTypeVm relationshipTypeVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var relationshipType = _mapper.Map<RelationshipTypeVm, RelationshipType>(relationshipTypeVm); 
                    await _relationshipTypeService.UpdateAsync(relationshipType, GetLogonUser());
					return RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(relationshipTypeVm);

                return View(relationshipTypeVm);
            }
        }

		[ActionName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var relationshipType = await _relationshipTypeService.GetByIdAsync(id.Value);
			if (relationshipType == null)
            {
                return HttpNotFound();
            }

            var relationshipTypeVm = _mapper.Map<RelationshipType, RelationshipTypeVm>(relationshipType);

            return View(relationshipTypeVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _relationshipTypeService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(RelationshipTypeVm relationshipTypeVm)
        {
		}

	}
}
