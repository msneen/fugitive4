﻿using AutoMapper;
using Fugitive4.Services.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Fugitive4.Web
{
    public class DependencyRegistration
    {
        public static void Register(IServiceCollection services)
        {
            services.TryAdd(ServiceDescriptor.Scoped<ISubjectService, SubjectService>());
            services.TryAdd(ServiceDescriptor.Scoped<IReferrerService, ReferrerService>());
            services.TryAdd(ServiceDescriptor.Scoped<IAddressService, AddressService>());
            services.TryAdd(ServiceDescriptor.Scoped<IAddressTypeService, AddressTypeService>());
            services.TryAdd(ServiceDescriptor.Scoped<IGenderService, GenderService>());
            services.TryAdd(ServiceDescriptor.Scoped<IPhoneTypeService, PhoneTypeService>());
            services.TryAdd(ServiceDescriptor.Scoped<IPhoneNumberService, PhoneNumberService>());
            services.TryAdd(ServiceDescriptor.Scoped<IEmailTypeService, EmailTypeService>());
            services.TryAdd(ServiceDescriptor.Scoped<IEmailAddressService, EmailAddressService>());
            services.TryAdd(ServiceDescriptor.Scoped<IBailBondService, BailBondService>());
            services.TryAdd(ServiceDescriptor.Scoped<IVehicleService, VehicleService>());
            services.TryAdd(ServiceDescriptor.Scoped<IActivityNoteService, ActivityNoteService>());
            services.TryAdd(ServiceDescriptor.Scoped<IDriverLicenseService, DriverLicenseService>());
            services.TryAdd(ServiceDescriptor.Scoped<IStoredFileService, StoredFileService>());
            services.TryAdd(ServiceDescriptor.Scoped<IHyperlinkService, HyperlinkService>());
            services.TryAdd(ServiceDescriptor.Scoped<ISearchResultService, SearchResultService>());
            services.TryAdd(ServiceDescriptor.Scoped<IVideoService, VideoService>());
            services.TryAdd(ServiceDescriptor.Scoped<IRaceService, RaceService>());
            services.TryAdd(ServiceDescriptor.Scoped<IRelationshipTypeService, RelationshipTypeService>());
            services.TryAdd(ServiceDescriptor.Scoped<ISubjectRelationshipService, SubjectRelationshipService>());
            services.TryAdd(ServiceDescriptor.Scoped<IBookingChargeService, BookingChargeService>());
        }
    }
}