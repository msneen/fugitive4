using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.Address;


namespace Fugitive4.Web.MapperProfiles
{
    public class AddressProfile : Profile
    {
        protected override void Configure()
        {
			CreateMap<Address, AddressVm>().ReverseMap();
        }
    }
}
