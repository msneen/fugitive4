using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNet.Mvc.Rendering;
using Fugitive4.Services.Models.Bases;
using Fugitive4.Web.ViewModels.RelationshipType; //This is any parent or child vm
using Fugitive4.Web.ViewModels.Subject; //This is any parent or child vm


namespace Fugitive4.Web.ViewModels.SubjectRelationship
{
	public class SubjectRelationshipVm : AuditableEntityBase
    {
		
		[Display(Name="Id")]	
		public int Id { get; set; }

		
		[Display(Name="Subject Id")]	
		public int SubjectId { get; set; }

		
		[Display(Name="Related Subject Id")]	
		public int RelatedSubjectId { get; set; }

		
		[Display(Name="Relationship Type Id")]	
		public int RelationshipTypeId { get; set; }

		
		[Display(Name="Notes")]	
		public string Notes { get; set; }

		
		[Display(Name="Description")]	
		public string Description { get; set; }


		
        public RelationshipTypeVm RelationshipType { get; set; } //Parent
        public List<SelectListItem> RelationshipTypeList { get; set; } //Parent
		
        public SubjectVm Subject { get; set; } //Parent
        public List<SelectListItem> SubjectList { get; set; } //Parent
		
        public SubjectVm RelatedSubject { get; set; } //Parent
        public List<SelectListItem> RelatedSubjectList { get; set; } //Parent
    }

    public class SubjectRelationshipListVm
    {
        public IEnumerable<SubjectRelationshipVm> SubjectRelationships { get; set; }
				
		public int RelationshipTypeId { get; set; }
		public List<SelectListItem> RelationshipTypeList { get; set; }
		
		public int SubjectId { get; set; }
		public List<SelectListItem> SubjectList { get; set; }
		
		public int Subject1Id { get; set; }
		public List<SelectListItem> Subject1List { get; set; }
    }
}
