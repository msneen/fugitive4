﻿using AutoMapper;
using Fugitive4.Services.Models;
using Fugitive4.Web.ViewModels.Referrer;

namespace Fugitive4.Web.MapperProfiles
{
    public class ReferrerProfile : Profile
    {
        protected override void Configure()
        {
            CreateMap<Referrer, ReferrerVm>().ReverseMap();
        }
    }
}
