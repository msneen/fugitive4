using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.ActivityNote;
using Microsoft.AspNet.Authorization;

namespace Fugitive4.Web.Controllers
{
    [Authorize]
    public class ActivityNoteController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly IActivityNoteService _activityNoteService;					
		private readonly ISubjectService _subjectService;

		public ActivityNoteController(IActivityNoteService activityNoteService,
								ISubjectService subjectService
,
								IMapper mapper		)
        {
            _activityNoteService = activityNoteService;
			_mapper = mapper;
			_subjectService = subjectService;

        }
[Route("Subject/{subjectId}/ActivityNote/Index")]
        public async Task<IActionResult> Index(int subjectId , string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";

            var activityNotes = await _activityNoteService.GetAllAsync();
            var activityNoteList = new ActivityNoteListVm
            {
                ActivityNotes = _mapper.Map<IEnumerable<ActivityNote>, IEnumerable<ActivityNoteVm>>(activityNotes.Where(s=>s.SubjectId==subjectId).ToList())
            };

            return View("Index", activityNoteList);
        }

		public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            ActivityNote activityNote = await _activityNoteService.GetByIdAsync(id.Value);
            if (activityNote == null)
            {
                return HttpNotFound();
            }

            var activityNoteVm = _mapper.Map<ActivityNote, ActivityNoteVm>(activityNote);

            return View(activityNoteVm);
        }


[Route("Subject/{subjectId}/ActivityNote/Create", Name= "ActivityNoteForSubject")]
		public ActionResult Create(int subjectId, string returnUrl = "")
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";
            ViewBag.ReturnUrl = returnUrl;

            var activityNoteVm = new ActivityNoteVm
            {
                SubjectId = subjectId,
                ActivityDate = DateTime.Now,
                PerformedBy = GetLogonUser(),                
            };

			LoadLookups(activityNoteVm);

            return View(activityNoteVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
[Route("Subject/{subjectId}/ActivityNote/Create", Name = "ActivityNoteForSubject")]
        public async Task<IActionResult> Create(ActivityNoteVm activityNoteVm, string returnUrl = "")
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var activityNote = _mapper.Map<ActivityNoteVm, ActivityNote>(activityNoteVm);
                    await _activityNoteService.InsertAsync(activityNote, GetLogonUser());
                    return RedirectToIndexOrSubjectDetail(returnUrl);
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(activityNoteVm);

                return View(activityNoteVm);
            }
        }

[Route("Subject/{subjectId}/ActivityNote/Edit/{id}")]
		public async Task<IActionResult> Edit(int subjectId , int? id)
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";

			if (id == null)
            {
                return HttpNotFound();
            }

            var activityNote = await _activityNoteService.GetByIdAsync(id.Value);
            if (activityNote == null)
            {
                return HttpNotFound();
            }            
			
			var activityNoteVm = _mapper.Map<ActivityNote, ActivityNoteVm>(activityNote);

			LoadLookups(activityNoteVm);

            return View(activityNoteVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
[Route("Subject/{subjectId}/ActivityNote/Edit/{id}")]
        public async Task<IActionResult> Edit(ActivityNoteVm activityNoteVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var activityNote = _mapper.Map<ActivityNoteVm, ActivityNote>(activityNoteVm); 
                    await _activityNoteService.UpdateAsync(activityNote, GetLogonUser());
					return RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(activityNoteVm);

                return View(activityNoteVm);
            }
        }

		[ActionName("Delete")]
[Route("Subject/{subjectId}/ActivityNote/Delete/{id}")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var activityNote = await _activityNoteService.GetByIdAsync(id.Value);
			if (activityNote == null)
            {
                return HttpNotFound();
            }

            var activityNoteVm = _mapper.Map<ActivityNote, ActivityNoteVm>(activityNote);

            return View(activityNoteVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
[Route("Subject/{subjectId}/ActivityNote/Delete/{id}")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _activityNoteService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(ActivityNoteVm activityNoteVm)
        {
			activityNoteVm.SubjectList = _subjectService.GetAll().ToSelectList(s => s.Id.ToString(), s => s.EyeColor).ToList();
		}

	}
}
