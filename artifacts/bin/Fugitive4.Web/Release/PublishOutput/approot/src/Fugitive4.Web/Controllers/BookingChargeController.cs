using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.BookingCharge;
using Microsoft.AspNet.Authorization;

namespace Fugitive4.Web.Controllers
{
	[Authorize]
    public class BookingChargeController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly IBookingChargeService _bookingChargeService;					
		private readonly IBailBondService _bailBondService;

		public BookingChargeController(IBookingChargeService bookingChargeService,
								IBailBondService bailBondService
,
								IMapper mapper		)
        {
            _bookingChargeService = bookingChargeService;
			_mapper = mapper;
			_bailBondService = bailBondService;

        }
[Route("BailBond/{bailBondId}/BookingCharge/Index")]
        public async Task<IActionResult> Index(int bailBondId , string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";

            var bookingCharges = await _bookingChargeService.GetAllAsync();
            var bookingChargeList = new BookingChargeListVm
            {
                BailBondId = bailBondId,
                BookingCharges = _mapper.Map<IEnumerable<BookingCharge>, IEnumerable<BookingChargeVm>>(bookingCharges.ToList())
            };

            return View("Index", bookingChargeList);
        }

		public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            BookingCharge bookingCharge = await _bookingChargeService.GetByIdAsync(id.Value);
            if (bookingCharge == null)
            {
                return HttpNotFound();
            }

            var bookingChargeVm = _mapper.Map<BookingCharge, BookingChargeVm>(bookingCharge);

            return View(bookingChargeVm);
        }


[Route("BailBond/{bailBondId}/BookingCharge/Create")]
		public ActionResult Create(int bailBondId )
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";

            var bookingChargeVm = new BookingChargeVm {BailBondId = bailBondId};

			LoadLookups(bookingChargeVm);

            return View(bookingChargeVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
[Route("BailBond/{bailBondId}/BookingCharge/Create")]
        public async Task<IActionResult> Create(BookingChargeVm bookingChargeVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var bookingCharge = _mapper.Map<BookingChargeVm, BookingCharge>(bookingChargeVm);
                    await _bookingChargeService.InsertAsync(bookingCharge, GetLogonUser());
					return RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(bookingChargeVm);

                return View(bookingChargeVm);
            }
        }

[Route("BailBond/{bailBondId}/BookingCharge/Edit/{id}")]
		public async Task<IActionResult> Edit(int bailBondId , int? id)
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";

			if (id == null)
            {
                return HttpNotFound();
            }

            var bookingCharge = await _bookingChargeService.GetByIdAsync(id.Value);
            if (bookingCharge == null)
            {
                return HttpNotFound();
            }            
			
			var bookingChargeVm = _mapper.Map<BookingCharge, BookingChargeVm>(bookingCharge);

			LoadLookups(bookingChargeVm);

            return View(bookingChargeVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
[Route("BailBond/{bailBondId}/BookingCharge/Edit/{id}")]
        public async Task<IActionResult> Edit(BookingChargeVm bookingChargeVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var bookingCharge = _mapper.Map<BookingChargeVm, BookingCharge>(bookingChargeVm); 
                    await _bookingChargeService.UpdateAsync(bookingCharge, GetLogonUser());
					return RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(bookingChargeVm);

                return View(bookingChargeVm);
            }
        }

		[ActionName("Delete")]
[Route("BailBond/{bailBondId}/BookingCharge/Delete/{id}")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var bookingCharge = await _bookingChargeService.GetByIdAsync(id.Value);
			if (bookingCharge == null)
            {
                return HttpNotFound();
            }

            var bookingChargeVm = _mapper.Map<BookingCharge, BookingChargeVm>(bookingCharge);

            return View(bookingChargeVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
[Route("BailBond/{bailBondId}/BookingCharge/Delete/{id}")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _bookingChargeService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(BookingChargeVm bookingChargeVm)
        {
			bookingChargeVm.BailBondList = _bailBondService.GetAll().ToSelectList(s => s.Id.ToString(), s => s.IssuingBondsman).ToList();
		}

	}
}
