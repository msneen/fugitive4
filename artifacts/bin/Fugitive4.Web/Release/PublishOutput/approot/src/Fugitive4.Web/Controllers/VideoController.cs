using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.Video;
using Microsoft.AspNet.Authorization;

namespace Fugitive4.Web.Controllers
{
	[Authorize]
    public class VideoController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly IVideoService _videoService;					
		private readonly ISubjectService _subjectService;

		public VideoController(IVideoService videoService,
								ISubjectService subjectService,
								IMapper mapper		)
        {
            _videoService = videoService;
			_mapper = mapper;
			_subjectService = subjectService;

        }
[Route("Subject/{subjectId}/Video/Index")]
        public async Task<IActionResult> Index(int subjectId , string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";

            var videos = await _videoService.GetAllAsync();
            var videoList = new VideoListVm
            {
                Videos = _mapper.Map<IEnumerable<Video>, IEnumerable<VideoVm>>(videos.Where(s=>s.SubjectId==subjectId).ToList())
            };

            return View("Index", videoList);
        }

		public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            Video video = await _videoService.GetByIdAsync(id.Value);
            if (video == null)
            {
                return HttpNotFound();
            }

            var videoVm = _mapper.Map<Video, VideoVm>(video);

            return View(videoVm);
        }


[Route("Subject/{subjectId}/Video/Create", Name = "VideosForSubject")]
		public ActionResult Create(int subjectId )
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";

            var videoVm = new VideoVm {SubjectId = subjectId};

			LoadLookups(videoVm);

            return View(videoVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
[Route("Subject/{subjectId}/Video/Create", Name = "VideosForSubject")]
        public async Task<IActionResult> Create(VideoVm videoVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var video = _mapper.Map<VideoVm, Video>(videoVm);
                    await _videoService.InsertAsync(video, GetLogonUser());
					return RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(videoVm);

                return View(videoVm);
            }
        }

[Route("Subject/{subjectId}/Video/Edit/{id}")]
		public async Task<IActionResult> Edit(int subjectId , int? id)
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";

			if (id == null)
            {
                return HttpNotFound();
            }

            var video = await _videoService.GetByIdAsync(id.Value);
            if (video == null)
            {
                return HttpNotFound();
            }            
			
			var videoVm = _mapper.Map<Video, VideoVm>(video);

			LoadLookups(videoVm);

            return View(videoVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
[Route("Subject/{subjectId}/Video/Edit/{id}")]
        public async Task<IActionResult> Edit(VideoVm videoVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var video = _mapper.Map<VideoVm, Video>(videoVm); 
                    await _videoService.UpdateAsync(video, GetLogonUser());
					return RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(videoVm);

                return View(videoVm);
            }
        }

		[ActionName("Delete")]
[Route("Subject/{subjectId}/Video/Delete/{id}")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var video = await _videoService.GetByIdAsync(id.Value);
			if (video == null)
            {
                return HttpNotFound();
            }

            var videoVm = _mapper.Map<Video, VideoVm>(video);

            return View(videoVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
[Route("Subject/{subjectId}/Video/Delete/{id}")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _videoService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(VideoVm videoVm)
        {
			videoVm.SubjectList = _subjectService.GetAll().ToSelectList(s => s.Id.ToString(), s => s.EyeColor).ToList();
		}

	}
}
