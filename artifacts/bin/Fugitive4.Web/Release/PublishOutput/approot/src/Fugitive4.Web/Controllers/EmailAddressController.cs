using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.EmailAddress;
using Microsoft.AspNet.Authorization;

namespace Fugitive4.Web.Controllers
{
    [Authorize]
    public class EmailAddressController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly IEmailAddressService _emailAddressService;					
		private readonly IEmailTypeService _emailTypeService;					
		private readonly ISubjectService _subjectService;

		public EmailAddressController(IEmailAddressService emailAddressService,
								IEmailTypeService emailTypeService,
								ISubjectService subjectService
,
								IMapper mapper		)
        {
            _emailAddressService = emailAddressService;
			_mapper = mapper;
			_emailTypeService = emailTypeService;
			_subjectService = subjectService;

        }
        [Route("Subject/{subjectId}/EmailAddress/Index")]
        public async Task<IActionResult> Index(int subjectId, string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";

            var emailAddresses = await _emailAddressService.GetAllAsync();
            var emailAddressList = new EmailAddressListVm
            {
                EmailAddresses = _mapper.Map<IEnumerable<EmailAddress>, IEnumerable<EmailAddressVm>>(emailAddresses.Where(s=>s.SubjectId==subjectId).ToList())
            };

            return View("Index", emailAddressList);
        }

		public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            EmailAddress emailAddress = await _emailAddressService.GetByIdAsync(id.Value);
            if (emailAddress == null)
            {
                return HttpNotFound();
            }

            var emailAddressVm = _mapper.Map<EmailAddress, EmailAddressVm>(emailAddress);

            return View(emailAddressVm);
        }

        [Route("Subject/{subjectId}/EmailAddress/Create", Name= "EmailAddressForSubject")]
        public ActionResult Create(int subjectId)
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";

            var emailAddressVm = new EmailAddressVm {SubjectId = subjectId};

			LoadLookups(emailAddressVm);

            return View(emailAddressVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Subject/{subjectId}/EmailAddress/Create", Name = "EmailAddressForSubject")]
        public async Task<IActionResult> Create(EmailAddressVm emailAddressVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var emailAddress = _mapper.Map<EmailAddressVm, EmailAddress>(emailAddressVm);
                    await _emailAddressService.InsertAsync(emailAddress, GetLogonUser());
					return RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(emailAddressVm);

                return View(emailAddressVm);
            }
        }
        [Route("Subject/{subjectId}/EmailAddress/Edit/{id}")]
        public async Task<IActionResult> Edit( int? id)
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";

			if (id == null)
            {
                return HttpNotFound();
            }

            var emailAddress = await _emailAddressService.GetByIdAsync(id.Value);
            if (emailAddress == null)
            {
                return HttpNotFound();
            }            
			
			var emailAddressVm = _mapper.Map<EmailAddress, EmailAddressVm>(emailAddress);

			LoadLookups(emailAddressVm);

            return View(emailAddressVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Route("Subject/{subjectId}/EmailAddress/Edit/{id}")]
        public async Task<IActionResult> Edit(EmailAddressVm emailAddressVm)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    var emailAddress = _mapper.Map<EmailAddressVm, EmailAddress>(emailAddressVm); 
                    await _emailAddressService.UpdateAsync(emailAddress, GetLogonUser());
					return RedirectToAction("Index");
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(emailAddressVm);

                return View(emailAddressVm);
            }
        }

		[ActionName("Delete")]
        [Route("Subject/{subjectId}/EmailAddress/Delete/{id}")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var emailAddress = await _emailAddressService.GetByIdAsync(id.Value);
			if (emailAddress == null)
            {
                return HttpNotFound();
            }

            var emailAddressVm = _mapper.Map<EmailAddress, EmailAddressVm>(emailAddress);

            return View(emailAddressVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Route("Subject/{subjectId}/EmailAddress/Delete/{id}")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _emailAddressService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(EmailAddressVm emailAddressVm)
        {
			emailAddressVm.EmailTypeList = _emailTypeService.GetAll().ToSelectList(s => s.Id.ToString(), s => s.Name).ToList();
			emailAddressVm.SubjectList = _subjectService.GetAll().ToSelectList(s => s.Id.ToString(), s => s.EyeColor).ToList();
		}

	}
}
