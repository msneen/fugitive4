﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Fugitive4.Services.Models.Bases;
using Microsoft.AspNet.Mvc.Rendering;
using Fugitive4.Web.ViewModels.Subject; //This is any parent or child vm

namespace Fugitive4.Web.ViewModels.Referrer
{
	public class ReferrerVm : AuditableEntityBase
    {
		
		[Display(Name="Id")]	
		public int Id { get; set; }

		
		[Display(Name="Additional Info")]	
		public string AdditionalInfo { get; set; }

		
		[Display(Name="Description")]	
		public string Description { get; set; }

		
		[Display(Name="Name")]	
		public string Name { get; set; }


		public IEnumerable<SubjectVm> Subjects { get; set; }//Child
    }

    public class ReferrerListVm
    {
        public IEnumerable<ReferrerVm> Referrers { get; set; }
	}
}
