using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.SessionState;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Tws.AfFirst2.Services.Services;

namespace Tws.AfFirst2.Services.Tests
{

    [TestClass]
    public class StoredFileTests
    {
        [TestMethod]
        public void StoredFile_Service_GetAll()
        {
            var service = GetService();
            var storedFiles = service.GetAll();

            Assert.AreEqual(4, storedFiles.Count);

			//Programmer, add asserts for specific data here.  The mock data to match is defined at
			//The end of this file
            //Assert.AreEqual("Mike", storedFiles[0].Firstname);
            //Assert.AreEqual("Kirk", storedFiles[1].Firstname);
            //Assert.AreEqual("Brian", storedFiles[2].Firstname); 
        }

        [TestMethod]
        public void StoredFile_Service_GetById()
        {
            var service = GetService();
            var storedFile = service.GetById(2);

            Assert.AreEqual(2, storedFile.Id);
        }

        [TestMethod]
        public void StoredFile_Service_Add()
        {
            var mockSet = new Mock<DbSet<StoredFile>>();

            var mockContext = new Mock<AFFirst2Entities>();
            mockContext.Setup(m => m.Set<StoredFile>()).Returns(mockSet.Object);

            var service = GetService(mockContext);

            var storedFile = service.CreateNew();

            storedFile.FileName = "Fix Me";
            storedFile.ContentType = "Fix Me";
            storedFile.FileContent = "Fix Me";
            storedFile.FileType = "Fix Me";
            storedFile.Notes = "Fix Me";
            storedFile.SubjectId = 1;
            storedFile.IsSubject = false;
            storedFile.ShowOnDetail = false;

            service.Update(storedFile);

            //mockSet.Verify(svc => svc.Add(It.IsAny<StoredFile>()), Times.Once);


            mockContext.Verify(m => m.SaveChanges(), Times.Once());
        }

        [TestMethod]
        public void StoredFile_Service_Update()
        {
            var mockContext = MakeMockContextWithData();
            var service = GetService(mockContext);
            var storedFile = service.GetById(2);

            storedFile.FileName = "Fix Me";
            storedFile.ContentType = "Fix Me";
            storedFile.FileContent = "Fix Me";
            storedFile.FileType = "Fix Me";
            storedFile.Notes = "Fix Me";
            storedFile.SubjectId = 1;
            storedFile.IsSubject = false;
            storedFile.ShowOnDetail = false;

            service.Update(storedFile);

            mockContext.Verify(m => m.SaveChanges(), Times.Once());           
        }

		        [TestMethod]
        public void StoredFile_Service_Delete()
        {
            var mockContext = MakeMockContextWithData();
            var service = GetService(mockContext);
            var storedFile = service.GetById(4);


            service.Delete(storedFile.Id);

            mockContext.Verify(m => m.SaveChanges(), Times.Once());           
        }


        private static IStoredFileSvc GetService(Mock<AFFirst2Entities> mockContext = null, HttpContext mockHttpContext = null)
        {

            if (mockContext == null)
            {
                mockContext = MakeMockContextWithData();
            }
			if (mockHttpContext == null)
            {
                mockHttpContext = FakeHttpContext("http://localhost/StoredFile/");
            }

            var service = new StoredFileSvc(mockContext.Object, mockHttpContext);
            return service;
        }

		public static HttpContext FakeHttpContext(string url)
        {
            var uri = new Uri(url);
            var httpRequest = new HttpRequest(string.Empty, uri.ToString(),
                                                uri.Query.TrimStart('?'));
            var stringWriter = new StringWriter();
            var httpResponse = new HttpResponse(stringWriter);
            var httpContext = new HttpContext(httpRequest, httpResponse);

            var sessionContainer = new HttpSessionStateContainer("id",
                                            new SessionStateItemCollection(),
                                            new HttpStaticObjectsCollection(),
                                            10, true, HttpCookieMode.AutoDetect,
                                            SessionStateMode.InProc, false);

            SessionStateUtility.AddHttpSessionStateToContext(
                                                 httpContext, sessionContainer);

            httpContext.User = new GenericPrincipal(
                new GenericIdentity("username"),
                new string[0]
                );

            return httpContext;
        }

        private static Mock<AFFirst2Entities> MakeMockContextWithData()
        {
            var data = new List<StoredFile>
            {
                new StoredFile 
				{
					Id = 1,
					FileName = "Fix Me", 
					ContentType = "Fix Me", 
					FileContent = "Fix Me", 
					FileType = "Fix Me", 
					Notes = "Fix Me", 
					SubjectId = 1, 
					IsSubject = false, 
					ShowOnDetail = false, 
				},
                new StoredFile 
				{
					Id = 2,
					FileName = "Fix Me", 
					ContentType = "Fix Me", 
					FileContent = "Fix Me", 
					FileType = "Fix Me", 
					Notes = "Fix Me", 
					SubjectId = 1, 
					IsSubject = false, 
					ShowOnDetail = false, 
				},
                new StoredFile 
				{
					Id = 3,
					FileName = "Fix Me", 
					ContentType = "Fix Me", 
					FileContent = "Fix Me", 
					FileType = "Fix Me", 
					Notes = "Fix Me", 
					SubjectId = 1, 
					IsSubject = false, 
					ShowOnDetail = false, 
				},
                new StoredFile 
				{
					Id = 4,
					FileName = "Fix Me", 
					ContentType = "Fix Me", 
					FileContent = "Fix Me", 
					FileType = "Fix Me", 
					Notes = "Fix Me", 
					SubjectId = 1, 
					IsSubject = false, 
					ShowOnDetail = false, 
				},

            }.AsQueryable();

            var mockSet = new Mock<DbSet<StoredFile>>();
            mockSet.As<IQueryable<StoredFile>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<StoredFile>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<StoredFile>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<StoredFile>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            var mockContext = new Mock<AFFirst2Entities>();
            mockContext.Setup(c => c.StoredFiles).Returns(mockSet.Object);
            return mockContext;
        }
    }
}

