using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.SessionState;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Tws.AfFirst2.Services.Services;

namespace Tws.AfFirst2.Services.Tests
{

    [TestClass]
    public class Team_ReferrerTests
    {
        [TestMethod]
        public void Team_Referrer_Service_GetAll()
        {
            var service = GetService();
            var team_Referrers = service.GetAll();

            Assert.AreEqual(4, team_Referrers.Count);

			//Programmer, add asserts for specific data here.  The mock data to match is defined at
			//The end of this file
            //Assert.AreEqual("Mike", team_Referrers[0].Firstname);
            //Assert.AreEqual("Kirk", team_Referrers[1].Firstname);
            //Assert.AreEqual("Brian", team_Referrers[2].Firstname); 
        }

        [TestMethod]
        public void Team_Referrer_Service_GetById()
        {
            var service = GetService();
            var team_Referrer = service.GetById(2);

            Assert.AreEqual(2, team_Referrer.Id);
        }

        [TestMethod]
        public void Team_Referrer_Service_Add()
        {
            var mockSet = new Mock<DbSet<Team_Referrer>>();

            var mockContext = new Mock<AFFirst2Entities>();
            mockContext.Setup(m => m.Set<Team_Referrer>()).Returns(mockSet.Object);

            var service = GetService(mockContext);

            var team_Referrer = service.CreateNew();

            team_Referrer.ReferrerId = 1;
            team_Referrer.TeamId = 1;
            team_Referrer.Disabled = false;

            service.Update(team_Referrer);

            //mockSet.Verify(svc => svc.Add(It.IsAny<Team_Referrer>()), Times.Once);


            mockContext.Verify(m => m.SaveChanges(), Times.Once());
        }

        [TestMethod]
        public void Team_Referrer_Service_Update()
        {
            var mockContext = MakeMockContextWithData();
            var service = GetService(mockContext);
            var team_Referrer = service.GetById(2);

            team_Referrer.ReferrerId = 1;
            team_Referrer.TeamId = 1;
            team_Referrer.Disabled = false;

            service.Update(team_Referrer);

            mockContext.Verify(m => m.SaveChanges(), Times.Once());           
        }

		        [TestMethod]
        public void Team_Referrer_Service_Delete()
        {
            var mockContext = MakeMockContextWithData();
            var service = GetService(mockContext);
            var team_Referrer = service.GetById(4);


            service.Delete(team_Referrer.Id);

            mockContext.Verify(m => m.SaveChanges(), Times.Once());           
        }


        private static ITeam_ReferrerSvc GetService(Mock<AFFirst2Entities> mockContext = null, HttpContext mockHttpContext = null)
        {

            if (mockContext == null)
            {
                mockContext = MakeMockContextWithData();
            }
			if (mockHttpContext == null)
            {
                mockHttpContext = FakeHttpContext("http://localhost/Team_Referrer/");
            }

            var service = new Team_ReferrerSvc(mockContext.Object, mockHttpContext);
            return service;
        }

		public static HttpContext FakeHttpContext(string url)
        {
            var uri = new Uri(url);
            var httpRequest = new HttpRequest(string.Empty, uri.ToString(),
                                                uri.Query.TrimStart('?'));
            var stringWriter = new StringWriter();
            var httpResponse = new HttpResponse(stringWriter);
            var httpContext = new HttpContext(httpRequest, httpResponse);

            var sessionContainer = new HttpSessionStateContainer("id",
                                            new SessionStateItemCollection(),
                                            new HttpStaticObjectsCollection(),
                                            10, true, HttpCookieMode.AutoDetect,
                                            SessionStateMode.InProc, false);

            SessionStateUtility.AddHttpSessionStateToContext(
                                                 httpContext, sessionContainer);

            httpContext.User = new GenericPrincipal(
                new GenericIdentity("username"),
                new string[0]
                );

            return httpContext;
        }

        private static Mock<AFFirst2Entities> MakeMockContextWithData()
        {
            var data = new List<Team_Referrer>
            {
                new Team_Referrer 
				{
					Id = 1,
					ReferrerId = 1, 
					TeamId = 1, 
					Disabled = false, 
					CreatedBy = "6fbad129-31da-49fb-9b22-9e052d72850b", 
					CreatedDate = DateTime.Now, 
					ModifiedBy = "6fbad129-31da-49fb-9b22-9e052d72850b", 
					ModifiedDate = DateTime.Now, 
				},
                new Team_Referrer 
				{
					Id = 2,
					ReferrerId = 1, 
					TeamId = 1, 
					Disabled = false, 
					CreatedBy = "6fbad129-31da-49fb-9b22-9e052d72850b", 
					CreatedDate = DateTime.Now, 
					ModifiedBy = "6fbad129-31da-49fb-9b22-9e052d72850b", 
					ModifiedDate = DateTime.Now, 
				},
                new Team_Referrer 
				{
					Id = 3,
					ReferrerId = 1, 
					TeamId = 1, 
					Disabled = false, 
					CreatedBy = "6fbad129-31da-49fb-9b22-9e052d72850b", 
					CreatedDate = DateTime.Now, 
					ModifiedBy = "6fbad129-31da-49fb-9b22-9e052d72850b", 
					ModifiedDate = DateTime.Now, 
				},
                new Team_Referrer 
				{
					Id = 4,
					ReferrerId = 1, 
					TeamId = 1, 
					Disabled = false, 
					CreatedBy = "6fbad129-31da-49fb-9b22-9e052d72850b", 
					CreatedDate = DateTime.Now, 
					ModifiedBy = "6fbad129-31da-49fb-9b22-9e052d72850b", 
					ModifiedDate = DateTime.Now, 
				},

            }.AsQueryable();

            var mockSet = new Mock<DbSet<Team_Referrer>>();
            mockSet.As<IQueryable<Team_Referrer>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<Team_Referrer>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<Team_Referrer>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<Team_Referrer>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            var mockContext = new Mock<AFFirst2Entities>();
            mockContext.Setup(c => c.Team_Referrers).Returns(mockSet.Object);
            return mockContext;
        }
    }
}

