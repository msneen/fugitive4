using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.SessionState;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Tws.AfFirst2.Services.Services;

namespace Tws.AfFirst2.Services.Tests
{

    [TestClass]
    public class GenderTests
    {
        [TestMethod]
        public void Gender_Service_GetAll()
        {
            var service = GetService();
            var genders = service.GetAll();

            Assert.AreEqual(4, genders.Count);

			//Programmer, add asserts for specific data here.  The mock data to match is defined at
			//The end of this file
            //Assert.AreEqual("Mike", genders[0].Firstname);
            //Assert.AreEqual("Kirk", genders[1].Firstname);
            //Assert.AreEqual("Brian", genders[2].Firstname); 
        }

        [TestMethod]
        public void Gender_Service_GetById()
        {
            var service = GetService();
            var gender = service.GetById(2);

            Assert.AreEqual(2, gender.Id);
        }

        [TestMethod]
        public void Gender_Service_Add()
        {
            var mockSet = new Mock<DbSet<Gender>>();

            var mockContext = new Mock<AFFirst2Entities>();
            mockContext.Setup(m => m.Set<Gender>()).Returns(mockSet.Object);

            var service = GetService(mockContext);

            var gender = service.CreateNew();

            gender.Name = "Fix Me";

            service.Update(gender);

            //mockSet.Verify(svc => svc.Add(It.IsAny<Gender>()), Times.Once);


            mockContext.Verify(m => m.SaveChanges(), Times.Once());
        }

        [TestMethod]
        public void Gender_Service_Update()
        {
            var mockContext = MakeMockContextWithData();
            var service = GetService(mockContext);
            var gender = service.GetById(2);

            gender.Name = "Fix Me";

            service.Update(gender);

            mockContext.Verify(m => m.SaveChanges(), Times.Once());           
        }

		        [TestMethod]
        public void Gender_Service_Delete()
        {
            var mockContext = MakeMockContextWithData();
            var service = GetService(mockContext);
            var gender = service.GetById(4);


            service.Delete(gender.Id);

            mockContext.Verify(m => m.SaveChanges(), Times.Once());           
        }


        private static IGenderSvc GetService(Mock<AFFirst2Entities> mockContext = null, HttpContext mockHttpContext = null)
        {

            if (mockContext == null)
            {
                mockContext = MakeMockContextWithData();
            }
			if (mockHttpContext == null)
            {
                mockHttpContext = FakeHttpContext("http://localhost/Gender/");
            }

            var service = new GenderSvc(mockContext.Object, mockHttpContext);
            return service;
        }

		public static HttpContext FakeHttpContext(string url)
        {
            var uri = new Uri(url);
            var httpRequest = new HttpRequest(string.Empty, uri.ToString(),
                                                uri.Query.TrimStart('?'));
            var stringWriter = new StringWriter();
            var httpResponse = new HttpResponse(stringWriter);
            var httpContext = new HttpContext(httpRequest, httpResponse);

            var sessionContainer = new HttpSessionStateContainer("id",
                                            new SessionStateItemCollection(),
                                            new HttpStaticObjectsCollection(),
                                            10, true, HttpCookieMode.AutoDetect,
                                            SessionStateMode.InProc, false);

            SessionStateUtility.AddHttpSessionStateToContext(
                                                 httpContext, sessionContainer);

            httpContext.User = new GenericPrincipal(
                new GenericIdentity("username"),
                new string[0]
                );

            return httpContext;
        }

        private static Mock<AFFirst2Entities> MakeMockContextWithData()
        {
            var data = new List<Gender>
            {
                new Gender 
				{
					Id = 1,
					Name = "Fix Me", 
				},
                new Gender 
				{
					Id = 2,
					Name = "Fix Me", 
				},
                new Gender 
				{
					Id = 3,
					Name = "Fix Me", 
				},
                new Gender 
				{
					Id = 4,
					Name = "Fix Me", 
				},

            }.AsQueryable();

            var mockSet = new Mock<DbSet<Gender>>();
            mockSet.As<IQueryable<Gender>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<Gender>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<Gender>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<Gender>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            var mockContext = new Mock<AFFirst2Entities>();
            mockContext.Setup(c => c.Genders).Returns(mockSet.Object);
            return mockContext;
        }
    }
}

