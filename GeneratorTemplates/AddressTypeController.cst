using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using AutoMapper;
using Fugitive4.Web.Extensions;
using Fugitive4.Services.Models;
using Fugitive4.Services.Services;
using Fugitive4.Web.Controllers.Bases;
using Fugitive4.Web.ViewModels.AddressType;
using Microsoft.AspNet.Authorization;

namespace Fugitive4.Web.Controllers
{
	[Authorize]
    public class AddressTypeController : ControllerBase
    {
	    private readonly IMapper _mapper;
		private readonly IAddressTypeService _addressTypeService;

		public AddressTypeController(IAddressTypeService addressTypeService,
								IMapper mapper		)
        {
            _addressTypeService = addressTypeService;
			_mapper = mapper;

        }
        public async Task<IActionResult> Index( string searchCriteria = null)
        {
			ViewBag.Title = "Index";
			ViewBag.TestValue = "Index";

            var addressTypes = await _addressTypeService.GetAllAsync();
            var addressTypeList = new AddressTypeListVm
            {
                AddressTypes = _mapper.Map<IEnumerable<AddressType>, IEnumerable<AddressTypeVm>>(addressTypes.ToList())
            };

            return View("Index", addressTypeList);
        }

		public async Task<IActionResult> Details(int? id)
        {
			ViewBag.TestValue = "Details";
            if (id == null)
            {
                return HttpNotFound();
            }

            AddressType addressType = await _addressTypeService.GetByIdAsync(id.Value);
            if (addressType == null)
            {
                return HttpNotFound();
            }

            var addressTypeVm = _mapper.Map<AddressType, AddressTypeVm>(addressType);

            return View(addressTypeVm);
        }


		public ActionResult Create( string returnUrl = "")
        {
			ViewBag.Title = "Create"; 
			ViewBag.TestValue = "Create";
			ViewBag.ReturnUrl = returnUrl;

            var addressTypeVm = new AddressTypeVm();

			LoadLookups(addressTypeVm);

            return View(addressTypeVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AddressTypeVm addressTypeVm, string returnUrl = "")
        {
					ViewBag.ReturnUrl = returnUrl;
            try
            {
                if (ModelState.IsValid)
                {
                    var addressType = _mapper.Map<AddressTypeVm, AddressType>(addressTypeVm);
                    await _addressTypeService.InsertAsync(addressType, GetLogonUser());
					return RedirectToIndexOrSubjectDetail(returnUrl);
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }               
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(addressTypeVm);

                return View(addressTypeVm);
            }
        }

		public async Task<IActionResult> Edit( int? id, string returnUrl = "")
        {
			ViewBag.Title = "Edit"; 
			ViewBag.TestValue = "Edit";
			ViewBag.ReturnUrl = returnUrl;

			if (id == null)
            {
                return HttpNotFound();
            }

            var addressType = await _addressTypeService.GetByIdAsync(id.Value);
            if (addressType == null)
            {
                return HttpNotFound();
            }            
			
			var addressTypeVm = _mapper.Map<AddressType, AddressTypeVm>(addressType);

			LoadLookups(addressTypeVm);

            return View(addressTypeVm);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(AddressTypeVm addressTypeVm, string returnUrl = "")
        {
			ViewBag.ReturnUrl = returnUrl;
            try
            {
                if (ModelState.IsValid)
                {
                    var addressType = _mapper.Map<AddressTypeVm, AddressType>(addressTypeVm); 
                    await _addressTypeService.UpdateAsync(addressType, GetLogonUser());
					return RedirectToIndexOrSubjectDetail(returnUrl);
                }
				else
				{
                    var message = string.Join(" | ", ModelState.Values
                        .SelectMany(v => v.Errors)
                        .Select(e => e.ErrorMessage));
                    throw new Exception(message);
                }                 
            }
            catch(Exception ex)
            {
				LogError(ex);
				//Add any dropdown values back to the VM Here
				LoadLookups(addressTypeVm);

                return View(addressTypeVm);
            }
        }

		[ActionName("Delete")]
        public async Task<IActionResult> Delete(int? id)
        {
			ViewBag.Title = "Delete";
			ViewBag.TestValue = "Delete"; 

			if (id == null)
            {
                return HttpNotFound();
            }

            var addressType = await _addressTypeService.GetByIdAsync(id.Value);
			if (addressType == null)
            {
                return HttpNotFound();
            }

            var addressTypeVm = _mapper.Map<AddressType, AddressTypeVm>(addressType);

            return View(addressTypeVm);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    await _addressTypeService.DeleteAsync(id, GetLogonUser());
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

		private void LoadLookups(AddressTypeVm addressTypeVm)
        {
		}

	}
}
