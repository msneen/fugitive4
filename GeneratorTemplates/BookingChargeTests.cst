using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.SessionState;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Tws.AfFirst2.Services.Services;

namespace Tws.AfFirst2.Services.Tests
{

    [TestClass]
    public class BookingChargeTests
    {
        [TestMethod]
        public void BookingCharge_Service_GetAll()
        {
            var service = GetService();
            var bookingCharges = service.GetAll();

            Assert.AreEqual(4, bookingCharges.Count);

			//Programmer, add asserts for specific data here.  The mock data to match is defined at
			//The end of this file
            //Assert.AreEqual("Mike", bookingCharges[0].Firstname);
            //Assert.AreEqual("Kirk", bookingCharges[1].Firstname);
            //Assert.AreEqual("Brian", bookingCharges[2].Firstname); 
        }

        [TestMethod]
        public void BookingCharge_Service_GetById()
        {
            var service = GetService();
            var bookingCharge = service.GetById(2);

            Assert.AreEqual(2, bookingCharge.Id);
        }

        [TestMethod]
        public void BookingCharge_Service_Add()
        {
            var mockSet = new Mock<DbSet<BookingCharge>>();

            var mockContext = new Mock<AFFirst2Entities>();
            mockContext.Setup(m => m.Set<BookingCharge>()).Returns(mockSet.Object);

            var service = GetService(mockContext);

            var bookingCharge = service.CreateNew();

            bookingCharge.BookingNumber = "Fix Me";
            bookingCharge.ChargeName = "Fix Me";
            bookingCharge.CodeNumber = "Fix Me";
            bookingCharge.Felony = false;
            bookingCharge.DateOccurred = DateTime.Now;
            bookingCharge.Jurisdiction = "Fix Me";
            bookingCharge.PoliceAgency = "Fix Me";
            bookingCharge.Address1 = "Fix Me";
            bookingCharge.Address2 = "Fix Me";
            bookingCharge.City = "Fix Me";
            bookingCharge.State = "Fix Me";
            bookingCharge.Zipcode = "Fix Me";
            bookingCharge.Detectives = "Fix Me";
            bookingCharge.Victim = "Fix Me";
            bookingCharge.Details = "Fix Me";
            bookingCharge.ReportNumber = "Fix Me";
            bookingCharge.Description = "Fix Me";
            bookingCharge.Notes = "Fix Me";
            bookingCharge.BailBondId = 1;

            service.Update(bookingCharge);

            //mockSet.Verify(svc => svc.Add(It.IsAny<BookingCharge>()), Times.Once);


            mockContext.Verify(m => m.SaveChanges(), Times.Once());
        }

        [TestMethod]
        public void BookingCharge_Service_Update()
        {
            var mockContext = MakeMockContextWithData();
            var service = GetService(mockContext);
            var bookingCharge = service.GetById(2);

            bookingCharge.BookingNumber = "Fix Me";
            bookingCharge.ChargeName = "Fix Me";
            bookingCharge.CodeNumber = "Fix Me";
            bookingCharge.Felony = false;
            bookingCharge.DateOccurred = DateTime.Now;
            bookingCharge.Jurisdiction = "Fix Me";
            bookingCharge.PoliceAgency = "Fix Me";
            bookingCharge.Address1 = "Fix Me";
            bookingCharge.Address2 = "Fix Me";
            bookingCharge.City = "Fix Me";
            bookingCharge.State = "Fix Me";
            bookingCharge.Zipcode = "Fix Me";
            bookingCharge.Detectives = "Fix Me";
            bookingCharge.Victim = "Fix Me";
            bookingCharge.Details = "Fix Me";
            bookingCharge.ReportNumber = "Fix Me";
            bookingCharge.Description = "Fix Me";
            bookingCharge.Notes = "Fix Me";
            bookingCharge.BailBondId = 1;

            service.Update(bookingCharge);

            mockContext.Verify(m => m.SaveChanges(), Times.Once());           
        }

		        [TestMethod]
        public void BookingCharge_Service_Delete()
        {
            var mockContext = MakeMockContextWithData();
            var service = GetService(mockContext);
            var bookingCharge = service.GetById(4);


            service.Delete(bookingCharge.Id);

            mockContext.Verify(m => m.SaveChanges(), Times.Once());           
        }


        private static IBookingChargeSvc GetService(Mock<AFFirst2Entities> mockContext = null, HttpContext mockHttpContext = null)
        {

            if (mockContext == null)
            {
                mockContext = MakeMockContextWithData();
            }
			if (mockHttpContext == null)
            {
                mockHttpContext = FakeHttpContext("http://localhost/BookingCharge/");
            }

            var service = new BookingChargeSvc(mockContext.Object, mockHttpContext);
            return service;
        }

		public static HttpContext FakeHttpContext(string url)
        {
            var uri = new Uri(url);
            var httpRequest = new HttpRequest(string.Empty, uri.ToString(),
                                                uri.Query.TrimStart('?'));
            var stringWriter = new StringWriter();
            var httpResponse = new HttpResponse(stringWriter);
            var httpContext = new HttpContext(httpRequest, httpResponse);

            var sessionContainer = new HttpSessionStateContainer("id",
                                            new SessionStateItemCollection(),
                                            new HttpStaticObjectsCollection(),
                                            10, true, HttpCookieMode.AutoDetect,
                                            SessionStateMode.InProc, false);

            SessionStateUtility.AddHttpSessionStateToContext(
                                                 httpContext, sessionContainer);

            httpContext.User = new GenericPrincipal(
                new GenericIdentity("username"),
                new string[0]
                );

            return httpContext;
        }

        private static Mock<AFFirst2Entities> MakeMockContextWithData()
        {
            var data = new List<BookingCharge>
            {
                new BookingCharge 
				{
					Id = 1,
					BookingNumber = "Fix Me", 
					ChargeName = "Fix Me", 
					CodeNumber = "Fix Me", 
					Felony = false, 
					DateOccurred = DateTime.Now, 
					Jurisdiction = "Fix Me", 
					PoliceAgency = "Fix Me", 
					Address1 = "Fix Me", 
					Address2 = "Fix Me", 
					City = "Fix Me", 
					State = "Fix Me", 
					Zipcode = "Fix Me", 
					Detectives = "Fix Me", 
					Victim = "Fix Me", 
					Details = "Fix Me", 
					ReportNumber = "Fix Me", 
					Description = "Fix Me", 
					Notes = "Fix Me", 
					BailBondId = 1, 
				},
                new BookingCharge 
				{
					Id = 2,
					BookingNumber = "Fix Me", 
					ChargeName = "Fix Me", 
					CodeNumber = "Fix Me", 
					Felony = false, 
					DateOccurred = DateTime.Now, 
					Jurisdiction = "Fix Me", 
					PoliceAgency = "Fix Me", 
					Address1 = "Fix Me", 
					Address2 = "Fix Me", 
					City = "Fix Me", 
					State = "Fix Me", 
					Zipcode = "Fix Me", 
					Detectives = "Fix Me", 
					Victim = "Fix Me", 
					Details = "Fix Me", 
					ReportNumber = "Fix Me", 
					Description = "Fix Me", 
					Notes = "Fix Me", 
					BailBondId = 1, 
				},
                new BookingCharge 
				{
					Id = 3,
					BookingNumber = "Fix Me", 
					ChargeName = "Fix Me", 
					CodeNumber = "Fix Me", 
					Felony = false, 
					DateOccurred = DateTime.Now, 
					Jurisdiction = "Fix Me", 
					PoliceAgency = "Fix Me", 
					Address1 = "Fix Me", 
					Address2 = "Fix Me", 
					City = "Fix Me", 
					State = "Fix Me", 
					Zipcode = "Fix Me", 
					Detectives = "Fix Me", 
					Victim = "Fix Me", 
					Details = "Fix Me", 
					ReportNumber = "Fix Me", 
					Description = "Fix Me", 
					Notes = "Fix Me", 
					BailBondId = 1, 
				},
                new BookingCharge 
				{
					Id = 4,
					BookingNumber = "Fix Me", 
					ChargeName = "Fix Me", 
					CodeNumber = "Fix Me", 
					Felony = false, 
					DateOccurred = DateTime.Now, 
					Jurisdiction = "Fix Me", 
					PoliceAgency = "Fix Me", 
					Address1 = "Fix Me", 
					Address2 = "Fix Me", 
					City = "Fix Me", 
					State = "Fix Me", 
					Zipcode = "Fix Me", 
					Detectives = "Fix Me", 
					Victim = "Fix Me", 
					Details = "Fix Me", 
					ReportNumber = "Fix Me", 
					Description = "Fix Me", 
					Notes = "Fix Me", 
					BailBondId = 1, 
				},

            }.AsQueryable();

            var mockSet = new Mock<DbSet<BookingCharge>>();
            mockSet.As<IQueryable<BookingCharge>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<BookingCharge>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<BookingCharge>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<BookingCharge>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            var mockContext = new Mock<AFFirst2Entities>();
            mockContext.Setup(c => c.BookingCharges).Returns(mockSet.Object);
            return mockContext;
        }
    }
}

