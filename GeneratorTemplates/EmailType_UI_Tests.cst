using System;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.SessionState;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using Tws.AfFirst2.Services;
using Tws.AfFirst2.Services.Services;

namespace Tws.AfFirst2.Web.UiTests
{
#if(!DEBUG)
    [Ignore]
#endif
    [TestClass]
    public class EmailTypeUiTests
    {
        IWebDriver _driver;
        private string _baseUrl;

        private void InitializeDriver()
        {
            _driver = new FirefoxDriver();
            _baseUrl = "http://localhost/";
        }


        [TestMethod]
        public void EmailType_UI_Create()
        {
            //Launch the Web Browser
            InitializeDriver();

			AccountTests.DoLogin(_driver, _baseUrl);
            

            _driver.Navigate().GoToUrl(_baseUrl + "/Tws.AfFirst2.Web/EmailType/Create");
            Thread.Sleep(5000);

            _driver.FindElement(By.Id("Name")).Clear();
            _driver.FindElement(By.Id("Name")).SendKeys("Fix Me");

            _driver.FindElement(By.CssSelector("input[type=\"submit\"]")).Click();

            Thread.Sleep(2000);
            Assert.AreEqual(_baseUrl + "Tws.AfFirst2.Web/EmailType", _driver.Url);


            //Cleanup
            try
            {
                var emailTypeSvc = GetEmailTypeSvc();
                var emailTypeRecords = emailTypeSvc.GetAll().Where(l => l.Name == "Fix Me");
                foreach (var emailType in emailTypeRecords)
                {
                    emailTypeSvc.Delete(emailType.Id);
                }
            }
            catch
            {
                // ignored
            }
        }


        [TestMethod]
        public void EmailType_UI_Edit()
        {
            //Setup
            var emailTypeSvc = GetEmailTypeSvc();
            var newDeletion = emailTypeSvc.CreateNew();

            newDeletion.Name = "Fix Me";


            emailTypeSvc.Update(newDeletion);
            var id = newDeletion.Id;
            //Launch the Web Browser
            InitializeDriver();

			AccountTests.DoLogin(_driver, _baseUrl);

            Random random = new Random();
            int randomNumber = random.Next(0, 10000);

            _driver.Navigate().GoToUrl(_baseUrl +string.Format( "Tws.AfFirst2.Web/EmailType/Edit/{0}", id));
            Thread.Sleep(5000);

			
            var randomName = "FixMe" + randomNumber;


            _driver.FindElement(By.Id("Name")).Clear();
            _driver.FindElement(By.Id("Name")).SendKeys(randomName);


            _driver.FindElement(By.CssSelector("input[type=\"submit\"]")).Click();

            Thread.Sleep(2000);
            Assert.AreEqual(_baseUrl + "Tws.AfFirst2.Web/EmailType", _driver.Url);
			Thread.Sleep(2000);

            emailTypeSvc = GetEmailTypeSvc();
            var updatedEmailType = emailTypeSvc.GetById(id);
            Assert.AreEqual(randomName, updatedEmailType.Name);

        }

        [TestMethod]
        public void EmailType_UI_Delete()
        {
            //Setup
            var emailTypeSvc = GetEmailTypeSvc();
            var newDeletion = emailTypeSvc.CreateNew();

            newDeletion.Name = "Fix Me";


            emailTypeSvc.Update(newDeletion);
            var id = newDeletion.Id;

            //Test
            //Launch the Web Browser
            InitializeDriver();

			AccountTests.DoLogin(_driver, _baseUrl);

            _driver.Navigate().GoToUrl(_baseUrl + string.Format( "/Tws.AfFirst2.Web/EmailType/Delete/{0}", id));
            Thread.Sleep(5000);

            _driver.FindElement(By.CssSelector("input[type=\"submit\"]")).Click();

            //Verify
            Thread.Sleep(2000);
            Assert.AreEqual(_baseUrl + "Tws.AfFirst2.Web/EmailType", _driver.Url);
			Thread.Sleep(2000);

            emailTypeSvc = GetEmailTypeSvc();
            var deletionFound = emailTypeSvc.GetById(id);
            Assert.IsTrue(deletionFound.Disabled);

        }

        [TestCleanup]
        public void CleanupTest()
        {
            //Close the browser
            _driver.Close();

            //End the test
            _driver.Quit();

			//Cleanup
            var emailTypeSvc = GetEmailTypeSvc();
            var fixMeList = emailTypeSvc.GetAll().Where(a => a.Name.Contains( "Fix Me") || a.Name.Contains( "FixMe") );
            foreach (var fixMe in fixMeList)
            {
                emailTypeSvc.Delete(fixMe.Id);
            }
        }

        private static IEmailTypeSvc GetEmailTypeSvc()
        {

            AFFirst2Entities afFirst2Entities = new AFFirst2Entities();
            IEmailTypeSvc emailTypeSvc = new EmailTypeSvc(afFirst2Entities, FakeHttpContext("http://localhost/EmailType"));
            return emailTypeSvc;
        }

		public static HttpContext FakeHttpContext(string url)
        {
            var uri = new Uri(url);
            var httpRequest = new HttpRequest(string.Empty, uri.ToString(),
                                                uri.Query.TrimStart('?'));
            var stringWriter = new StringWriter();
            var httpResponse = new HttpResponse(stringWriter);
            var httpContext = new HttpContext(httpRequest, httpResponse);

            var sessionContainer = new HttpSessionStateContainer("id",
                                            new SessionStateItemCollection(),
                                            new HttpStaticObjectsCollection(),
                                            10, true, HttpCookieMode.AutoDetect,
                                            SessionStateMode.InProc, false);

            SessionStateUtility.AddHttpSessionStateToContext(
                                                 httpContext, sessionContainer);

            httpContext.User = new GenericPrincipal(
                new GenericIdentity("username"),
                new string[0]
                );

            return httpContext;
        }
    }
}
