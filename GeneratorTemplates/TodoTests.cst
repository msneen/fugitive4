using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.SessionState;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Tws.AfFirst2.Services.Services;

namespace Tws.AfFirst2.Services.Tests
{

    [TestClass]
    public class TodoTests
    {
        [TestMethod]
        public void Todo_Service_GetAll()
        {
            var service = GetService();
            var todos = service.GetAll();

            Assert.AreEqual(4, todos.Count);

			//Programmer, add asserts for specific data here.  The mock data to match is defined at
			//The end of this file
            //Assert.AreEqual("Mike", todos[0].Firstname);
            //Assert.AreEqual("Kirk", todos[1].Firstname);
            //Assert.AreEqual("Brian", todos[2].Firstname); 
        }

        [TestMethod]
        public void Todo_Service_GetById()
        {
            var service = GetService();
            var todo = service.GetById(2);

            Assert.AreEqual(2, todo.Id);
        }

        [TestMethod]
        public void Todo_Service_Add()
        {
            var mockSet = new Mock<DbSet<Todo>>();

            var mockContext = new Mock<AFFirst2Entities>();
            mockContext.Setup(m => m.Set<Todo>()).Returns(mockSet.Object);

            var service = GetService(mockContext);

            var todo = service.CreateNew();

            todo.Synopsis = "Fix Me";
            todo.Description = "Fix Me";
            todo.Notes = "Fix Me";
            todo.AssignedTo = "Fix Me";
            todo.Done = false;
            todo.TodoStatusId = 1;
            todo.SubjectId = 1;
            todo.SortOrder = 1;

            service.Update(todo);

            //mockSet.Verify(svc => svc.Add(It.IsAny<Todo>()), Times.Once);


            mockContext.Verify(m => m.SaveChanges(), Times.Once());
        }

        [TestMethod]
        public void Todo_Service_Update()
        {
            var mockContext = MakeMockContextWithData();
            var service = GetService(mockContext);
            var todo = service.GetById(2);

            todo.Synopsis = "Fix Me";
            todo.Description = "Fix Me";
            todo.Notes = "Fix Me";
            todo.AssignedTo = "Fix Me";
            todo.Done = false;
            todo.TodoStatusId = 1;
            todo.SubjectId = 1;
            todo.SortOrder = 1;

            service.Update(todo);

            mockContext.Verify(m => m.SaveChanges(), Times.Once());           
        }

		        [TestMethod]
        public void Todo_Service_Delete()
        {
            var mockContext = MakeMockContextWithData();
            var service = GetService(mockContext);
            var todo = service.GetById(4);


            service.Delete(todo.Id);

            mockContext.Verify(m => m.SaveChanges(), Times.Once());           
        }


        private static ITodoSvc GetService(Mock<AFFirst2Entities> mockContext = null, HttpContext mockHttpContext = null)
        {

            if (mockContext == null)
            {
                mockContext = MakeMockContextWithData();
            }
			if (mockHttpContext == null)
            {
                mockHttpContext = FakeHttpContext("http://localhost/Todo/");
            }

            var service = new TodoSvc(mockContext.Object, mockHttpContext);
            return service;
        }

		public static HttpContext FakeHttpContext(string url)
        {
            var uri = new Uri(url);
            var httpRequest = new HttpRequest(string.Empty, uri.ToString(),
                                                uri.Query.TrimStart('?'));
            var stringWriter = new StringWriter();
            var httpResponse = new HttpResponse(stringWriter);
            var httpContext = new HttpContext(httpRequest, httpResponse);

            var sessionContainer = new HttpSessionStateContainer("id",
                                            new SessionStateItemCollection(),
                                            new HttpStaticObjectsCollection(),
                                            10, true, HttpCookieMode.AutoDetect,
                                            SessionStateMode.InProc, false);

            SessionStateUtility.AddHttpSessionStateToContext(
                                                 httpContext, sessionContainer);

            httpContext.User = new GenericPrincipal(
                new GenericIdentity("username"),
                new string[0]
                );

            return httpContext;
        }

        private static Mock<AFFirst2Entities> MakeMockContextWithData()
        {
            var data = new List<Todo>
            {
                new Todo 
				{
					Id = 1,
					Synopsis = "Fix Me", 
					Description = "Fix Me", 
					Notes = "Fix Me", 
					AssignedTo = "Fix Me", 
					Done = false, 
					TodoStatusId = 1, 
					SubjectId = 1, 
					SortOrder = 1, 
				},
                new Todo 
				{
					Id = 2,
					Synopsis = "Fix Me", 
					Description = "Fix Me", 
					Notes = "Fix Me", 
					AssignedTo = "Fix Me", 
					Done = false, 
					TodoStatusId = 1, 
					SubjectId = 1, 
					SortOrder = 1, 
				},
                new Todo 
				{
					Id = 3,
					Synopsis = "Fix Me", 
					Description = "Fix Me", 
					Notes = "Fix Me", 
					AssignedTo = "Fix Me", 
					Done = false, 
					TodoStatusId = 1, 
					SubjectId = 1, 
					SortOrder = 1, 
				},
                new Todo 
				{
					Id = 4,
					Synopsis = "Fix Me", 
					Description = "Fix Me", 
					Notes = "Fix Me", 
					AssignedTo = "Fix Me", 
					Done = false, 
					TodoStatusId = 1, 
					SubjectId = 1, 
					SortOrder = 1, 
				},

            }.AsQueryable();

            var mockSet = new Mock<DbSet<Todo>>();
            mockSet.As<IQueryable<Todo>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<Todo>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<Todo>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<Todo>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            var mockContext = new Mock<AFFirst2Entities>();
            mockContext.Setup(c => c.Todos).Returns(mockSet.Object);
            return mockContext;
        }
    }
}

