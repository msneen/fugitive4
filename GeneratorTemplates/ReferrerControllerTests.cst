using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Tws.AfFirst2.Services;
using Tws.AfFirst2.Services.Services;
using Tws.AfFirst2.Web.Controllers;

namespace Tws.AfFirst2.Web.Tests.Controllers
{
    [TestClass]
    public class ReferrerControllerTest
    {
        [TestInitialize]
        public void Initialize()
        {
            App_Start.AutoMapperProfileRegister.Start();
        }

        [TestMethod]
        public void Referrer_Controller_Index()
        {
            // Arrange
            ReferrerController controller = new ReferrerController(new ReferrerSvc(new AFFirst2Entities(), HttpContext.Current)		);

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.AreEqual("Index", result.ViewBag.Title);
        }

        [TestMethod]
        public void Referrer_Controller_Create()
        {
            // Arrange
            ReferrerController controller = new ReferrerController(new ReferrerSvc(new AFFirst2Entities(), HttpContext.Current)		);

            // Act
            ViewResult result = controller.Create() as ViewResult;

            // Assert
            Assert.AreEqual("Create", result.ViewBag.Title);
        }

        [TestMethod]
        public void Referrer_Controller_Edit()
        {
            // Arrange
			var mockContext = MakeMockContextWithData();
			var referrerSvc =  GetService(mockContext);
            ReferrerController controller = new ReferrerController(referrerSvc		);
            var id = referrerSvc.GetAll().FirstOrDefault().Id;

            // Act
            ViewResult result = controller.Edit(id) as ViewResult;

            // Assert
            Assert.AreEqual("Edit", result.ViewBag.Title);
        }

		[TestMethod]
        public void Referrer_Controller_Delete()
        {
            // Arrange
            ReferrerSvc referrerSvc = new ReferrerSvc(new AFFirst2Entities(), HttpContext.Current);
            ReferrerController controller = new ReferrerController(referrerSvc		);
            //var id = referrerSvc.GetAll().FirstOrDefault().Id;

            // Act
            ViewResult result = controller.Delete(0) as ViewResult;

            // Assert
            Assert.AreEqual("Delete", result.ViewBag.Title);
        }

		        private static IReferrerSvc GetService(Mock<AFFirst2Entities> mockContext = null, HttpContext mockHttpContext = null)
        {

            if (mockContext == null)
            {
                mockContext = MakeMockContextWithData();
            }
			if (mockHttpContext == null)
            {
                mockHttpContext = FakeHttpContext("http://localhost/Referrer/");
            }

            var service = new ReferrerSvc(mockContext.Object, mockHttpContext);
            return service;
        }

        public static HttpContext FakeHttpContext(string url)
        {
            var uri = new Uri(url);
            var httpRequest = new HttpRequest(string.Empty, uri.ToString(),
                                                uri.Query.TrimStart('?'));
            var stringWriter = new StringWriter();
            var httpResponse = new HttpResponse(stringWriter);
            var httpContext = new HttpContext(httpRequest, httpResponse);

            var sessionContainer = new HttpSessionStateContainer("id",
                                            new SessionStateItemCollection(),
                                            new HttpStaticObjectsCollection(),
                                            10, true, HttpCookieMode.AutoDetect,
                                            SessionStateMode.InProc, false);

            SessionStateUtility.AddHttpSessionStateToContext(
                                                 httpContext, sessionContainer);

            httpContext.User = new GenericPrincipal(
                new GenericIdentity("username"),
                new string[0]
                );

            return httpContext;
        }

		        private static Mock<AFFirst2Entities> MakeMockContextWithData()
        {
            var data = new List<Referrer>
            {
                new Referrer 
				{
					Id = 1,
					AdditionalInfo = "Fix Me", 
					Description = "Fix Me", 
					Name = "Fix Me", 
				},
                new Referrer 
				{
					Id = 2,
					AdditionalInfo = "Fix Me", 
					Description = "Fix Me", 
					Name = "Fix Me", 
				},
                new Referrer 
				{
					Id = 3,
					AdditionalInfo = "Fix Me", 
					Description = "Fix Me", 
					Name = "Fix Me", 
				},
                new Referrer 
				{
					Id = 4,
					AdditionalInfo = "Fix Me", 
					Description = "Fix Me", 
					Name = "Fix Me", 
				},

            }.AsQueryable();

            var mockSet = new Mock<DbSet<Referrer>>();
            mockSet.As<IQueryable<Referrer>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<Referrer>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<Referrer>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<Referrer>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            var mockContext = new Mock<AFFirst2Entities>();
            mockContext.Setup(c => c.Referrers).Returns(mockSet.Object);
            return mockContext;
        }

    }
}
