
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 05/08/2016 17:46:11
-- Generated from EDMX file: F:\Projects\fugitive4\GeneratorTemplates\DataModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [Fugitive4Model];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_ActivityNotes_Subject]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ActivityNotes] DROP CONSTRAINT [FK_ActivityNotes_Subject];
GO
IF OBJECT_ID(N'[dbo].[FK_Address_AddressType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Address] DROP CONSTRAINT [FK_Address_AddressType];
GO
IF OBJECT_ID(N'[dbo].[FK_Address_Subject]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Address] DROP CONSTRAINT [FK_Address_Subject];
GO
IF OBJECT_ID(N'[dbo].[FK_AKA_Subject]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AKA] DROP CONSTRAINT [FK_AKA_Subject];
GO
IF OBJECT_ID(N'[dbo].[FK_AspNetUsers_Referrer_AspNetUsers]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AspNetUsers_Referrer] DROP CONSTRAINT [FK_AspNetUsers_Referrer_AspNetUsers];
GO
IF OBJECT_ID(N'[dbo].[FK_AspNetUsers_Referrer_Referrer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AspNetUsers_Referrer] DROP CONSTRAINT [FK_AspNetUsers_Referrer_Referrer];
GO
IF OBJECT_ID(N'[dbo].[FK_AspNetUsers_Team_AspNetUsers]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AspNetUsers_Team] DROP CONSTRAINT [FK_AspNetUsers_Team_AspNetUsers];
GO
IF OBJECT_ID(N'[dbo].[FK_AspNetUsers_Team_Team]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[AspNetUsers_Team] DROP CONSTRAINT [FK_AspNetUsers_Team_Team];
GO
IF OBJECT_ID(N'[dbo].[FK_BailBond_Subject]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[BailBond] DROP CONSTRAINT [FK_BailBond_Subject];
GO
IF OBJECT_ID(N'[dbo].[FK_BookingCharge_BailBond]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[BookingCharge] DROP CONSTRAINT [FK_BookingCharge_BailBond];
GO
IF OBJECT_ID(N'[dbo].[FK_DriverLicense_Subject]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[DriverLicense] DROP CONSTRAINT [FK_DriverLicense_Subject];
GO
IF OBJECT_ID(N'[dbo].[FK_EmailAddress_EmailType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[EmailAddress] DROP CONSTRAINT [FK_EmailAddress_EmailType];
GO
IF OBJECT_ID(N'[dbo].[FK_EmailAddress_Subject]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[EmailAddress] DROP CONSTRAINT [FK_EmailAddress_Subject];
GO
IF OBJECT_ID(N'[dbo].[FK_Employer_Subject]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Employer] DROP CONSTRAINT [FK_Employer_Subject];
GO
IF OBJECT_ID(N'[dbo].[FK_Hyperlink_Subject]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Hyperlink] DROP CONSTRAINT [FK_Hyperlink_Subject];
GO
IF OBJECT_ID(N'[dbo].[FK_PhoneNumber_PhoneType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PhoneNumber] DROP CONSTRAINT [FK_PhoneNumber_PhoneType];
GO
IF OBJECT_ID(N'[dbo].[FK_PhoneNumber_Subject]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[PhoneNumber] DROP CONSTRAINT [FK_PhoneNumber_Subject];
GO
IF OBJECT_ID(N'[dbo].[FK_SearchRecipeItem_SearchRecipe]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SearchRecipeItem] DROP CONSTRAINT [FK_SearchRecipeItem_SearchRecipe];
GO
IF OBJECT_ID(N'[dbo].[FK_SearchResult_Subject]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SearchResult] DROP CONSTRAINT [FK_SearchResult_Subject];
GO
IF OBJECT_ID(N'[dbo].[FK_SearchResult_SubjectSearchRecipeItem]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SearchResult] DROP CONSTRAINT [FK_SearchResult_SubjectSearchRecipeItem];
GO
IF OBJECT_ID(N'[dbo].[FK_StoredFile_Subject]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[StoredFile] DROP CONSTRAINT [FK_StoredFile_Subject];
GO
IF OBJECT_ID(N'[dbo].[FK_Subject_Gender]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Subject] DROP CONSTRAINT [FK_Subject_Gender];
GO
IF OBJECT_ID(N'[dbo].[FK_Subject_Race]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Subject] DROP CONSTRAINT [FK_Subject_Race];
GO
IF OBJECT_ID(N'[dbo].[FK_Subject_Referrer_ReferrerId]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Subject] DROP CONSTRAINT [FK_Subject_Referrer_ReferrerId];
GO
IF OBJECT_ID(N'[dbo].[FK_Subject_SearchRecipe_SearchRecipe]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SubjectSearchRecipe] DROP CONSTRAINT [FK_Subject_SearchRecipe_SearchRecipe];
GO
IF OBJECT_ID(N'[dbo].[FK_Subject_SearchRecipe_Subject]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SubjectSearchRecipe] DROP CONSTRAINT [FK_Subject_SearchRecipe_Subject];
GO
IF OBJECT_ID(N'[dbo].[FK_SubjectRelationship_BailBond]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SubjectRelationship] DROP CONSTRAINT [FK_SubjectRelationship_BailBond];
GO
IF OBJECT_ID(N'[dbo].[FK_SubjectRelationship_RelationshipType]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SubjectRelationship] DROP CONSTRAINT [FK_SubjectRelationship_RelationshipType];
GO
IF OBJECT_ID(N'[dbo].[FK_SubjectRelationship_Subject]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SubjectRelationship] DROP CONSTRAINT [FK_SubjectRelationship_Subject];
GO
IF OBJECT_ID(N'[dbo].[FK_SubjectRelationship_Subject1]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SubjectRelationship] DROP CONSTRAINT [FK_SubjectRelationship_Subject1];
GO
IF OBJECT_ID(N'[dbo].[FK_SubjectRelationship_SubjectRelationship]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SubjectRelationship] DROP CONSTRAINT [FK_SubjectRelationship_SubjectRelationship];
GO
IF OBJECT_ID(N'[dbo].[FK_SubjectSearchRecipeItem_SearchRecipeItem]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SubjectSearchRecipeItem] DROP CONSTRAINT [FK_SubjectSearchRecipeItem_SearchRecipeItem];
GO
IF OBJECT_ID(N'[dbo].[FK_SubjectSearchRecipeItem_Subject]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[SubjectSearchRecipeItem] DROP CONSTRAINT [FK_SubjectSearchRecipeItem_Subject];
GO
IF OBJECT_ID(N'[dbo].[FK_Team_Referrer_Referrer]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Team_Referrer] DROP CONSTRAINT [FK_Team_Referrer_Referrer];
GO
IF OBJECT_ID(N'[dbo].[FK_Team_Referrer_Team]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Team_Referrer] DROP CONSTRAINT [FK_Team_Referrer_Team];
GO
IF OBJECT_ID(N'[dbo].[FK_Vehicle_Subject]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Vehicle] DROP CONSTRAINT [FK_Vehicle_Subject];
GO
IF OBJECT_ID(N'[dbo].[FK_Video_Subject]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Video] DROP CONSTRAINT [FK_Video_Subject];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[ActivityNotes]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ActivityNotes];
GO
IF OBJECT_ID(N'[dbo].[Address]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Address];
GO
IF OBJECT_ID(N'[dbo].[AddressType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AddressType];
GO
IF OBJECT_ID(N'[dbo].[AKA]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AKA];
GO
IF OBJECT_ID(N'[dbo].[AspNetUsers]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AspNetUsers];
GO
IF OBJECT_ID(N'[dbo].[AspNetUsers_Referrer]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AspNetUsers_Referrer];
GO
IF OBJECT_ID(N'[dbo].[AspNetUsers_Team]', 'U') IS NOT NULL
    DROP TABLE [dbo].[AspNetUsers_Team];
GO
IF OBJECT_ID(N'[dbo].[BailBond]', 'U') IS NOT NULL
    DROP TABLE [dbo].[BailBond];
GO
IF OBJECT_ID(N'[dbo].[BookingCharge]', 'U') IS NOT NULL
    DROP TABLE [dbo].[BookingCharge];
GO
IF OBJECT_ID(N'[dbo].[DriverLicense]', 'U') IS NOT NULL
    DROP TABLE [dbo].[DriverLicense];
GO
IF OBJECT_ID(N'[dbo].[EmailAddress]', 'U') IS NOT NULL
    DROP TABLE [dbo].[EmailAddress];
GO
IF OBJECT_ID(N'[dbo].[EmailType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[EmailType];
GO
IF OBJECT_ID(N'[dbo].[Employer]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Employer];
GO
IF OBJECT_ID(N'[dbo].[Gender]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Gender];
GO
IF OBJECT_ID(N'[dbo].[Hyperlink]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Hyperlink];
GO
IF OBJECT_ID(N'[dbo].[PhoneNumber]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PhoneNumber];
GO
IF OBJECT_ID(N'[dbo].[PhoneType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[PhoneType];
GO
IF OBJECT_ID(N'[dbo].[Race]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Race];
GO
IF OBJECT_ID(N'[dbo].[Referrer]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Referrer];
GO
IF OBJECT_ID(N'[dbo].[RelationshipType]', 'U') IS NOT NULL
    DROP TABLE [dbo].[RelationshipType];
GO
IF OBJECT_ID(N'[dbo].[SearchRecipe]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SearchRecipe];
GO
IF OBJECT_ID(N'[dbo].[SearchRecipeItem]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SearchRecipeItem];
GO
IF OBJECT_ID(N'[dbo].[SearchResult]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SearchResult];
GO
IF OBJECT_ID(N'[dbo].[StoredFile]', 'U') IS NOT NULL
    DROP TABLE [dbo].[StoredFile];
GO
IF OBJECT_ID(N'[dbo].[Subject]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Subject];
GO
IF OBJECT_ID(N'[dbo].[SubjectRelationship]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SubjectRelationship];
GO
IF OBJECT_ID(N'[dbo].[SubjectSearchRecipe]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SubjectSearchRecipe];
GO
IF OBJECT_ID(N'[dbo].[SubjectSearchRecipeItem]', 'U') IS NOT NULL
    DROP TABLE [dbo].[SubjectSearchRecipeItem];
GO
IF OBJECT_ID(N'[dbo].[Team]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Team];
GO
IF OBJECT_ID(N'[dbo].[Team_Referrer]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Team_Referrer];
GO
IF OBJECT_ID(N'[dbo].[Vehicle]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Vehicle];
GO
IF OBJECT_ID(N'[dbo].[Video]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Video];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Referrers'
CREATE TABLE [dbo].[Referrers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [AdditionalInfo] nvarchar(max)  NULL,
    [Description] nvarchar(max)  NULL,
    [Name] nvarchar(max)  NULL
);
GO

-- Creating table 'Subjects'
CREATE TABLE [dbo].[Subjects] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [DOB] datetime  NULL,
    [EyeColor] nvarchar(max)  NULL,
    [Firstname] nvarchar(max)  NULL,
    [HairColor] nvarchar(max)  NULL,
    [Height] nvarchar(max)  NULL,
    [Lastname] nvarchar(max)  NULL,
    [Middlename] nvarchar(max)  NULL,
    [SSN] nvarchar(max)  NULL,
    [Weight] int  NULL,
    [ReferrerId] int  NOT NULL,
    [GenderId] int  NOT NULL,
    [Notes] nvarchar(max)  NULL,
    [RaceId] int  NOT NULL,
    [PerceivedThreatLevel] int  NOT NULL
);
GO

-- Creating table 'Addresses'
CREATE TABLE [dbo].[Addresses] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Address1] nvarchar(150)  NULL,
    [Address2] nvarchar(150)  NULL,
    [City] nvarchar(100)  NULL,
    [State] nvarchar(50)  NULL,
    [Zipcode] nvarchar(20)  NULL,
    [BelongsToSubject] bit  NOT NULL,
    [SubjectId] int  NOT NULL,
    [AddressTypeId] int  NOT NULL,
    [Notes] varchar(max)  NULL
);
GO

-- Creating table 'AddressTypes'
CREATE TABLE [dbo].[AddressTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] varchar(50)  NOT NULL
);
GO

-- Creating table 'BailBonds'
CREATE TABLE [dbo].[BailBonds] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [OriginalArrestDate] datetime  NULL,
    [IssueDate] datetime  NULL,
    [DefaultDate] datetime  NULL,
    [IssuingBondsman] nvarchar(50)  NULL,
    [IssuingBondCompany] nvarchar(50)  NULL,
    [Court] nvarchar(50)  NULL,
    [Description] nvarchar(max)  NULL,
    [SubjectId] int  NOT NULL,
    [BookingNumber] nvarchar(50)  NULL,
    [BondAmount] decimal(18,0)  NULL
);
GO

-- Creating table 'EmailAddresses'
CREATE TABLE [dbo].[EmailAddresses] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [EmailAddress1] nvarchar(50)  NOT NULL,
    [BelongsTo] nvarchar(50)  NULL,
    [SubjectId] int  NOT NULL,
    [EmailTypeId] int  NOT NULL,
    [Notes] varchar(max)  NULL
);
GO

-- Creating table 'EmailTypes'
CREATE TABLE [dbo].[EmailTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nchar(10)  NULL
);
GO

-- Creating table 'Genders'
CREATE TABLE [dbo].[Genders] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'PhoneNumbers'
CREATE TABLE [dbo].[PhoneNumbers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [PhoneNumber1] varchar(50)  NULL,
    [BelongsTo] nvarchar(50)  NOT NULL,
    [SubjectId] int  NOT NULL,
    [PhoneTypeId] int  NOT NULL,
    [Notes] varchar(max)  NULL
);
GO

-- Creating table 'PhoneTypes'
CREATE TABLE [dbo].[PhoneTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] varchar(50)  NULL
);
GO

-- Creating table 'Vehicles'
CREATE TABLE [dbo].[Vehicles] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Make] nvarchar(50)  NULL,
    [Model] nvarchar(50)  NULL,
    [Year] nvarchar(10)  NULL,
    [Color] nvarchar(50)  NULL,
    [Description] nvarchar(500)  NULL,
    [BelongsTo] nvarchar(50)  NULL,
    [SubjectId] int  NOT NULL,
    [PlateNumber] nvarchar(20)  NULL
);
GO

-- Creating table 'ActivityNotes'
CREATE TABLE [dbo].[ActivityNotes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ActivityDate] datetime  NOT NULL,
    [Latitude] nvarchar(30)  NULL,
    [Longitude] nvarchar(30)  NULL,
    [Address] nchar(10)  NULL,
    [PerformedBy] nvarchar(128)  NULL,
    [Description] nvarchar(max)  NOT NULL,
    [SubjectId] int  NOT NULL
);
GO

-- Creating table 'DriverLicenses'
CREATE TABLE [dbo].[DriverLicenses] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [DLNumber] nvarchar(50)  NULL,
    [FullName] nvarchar(100)  NULL,
    [IssuingState] nvarchar(50)  NULL,
    [IssueDate] datetime  NULL,
    [ExpirationDate] datetime  NULL,
    [Notes] nvarchar(max)  NULL,
    [SubjectId] int  NOT NULL,
    [Address1] nvarchar(50)  NULL,
    [Address2] nvarchar(50)  NULL,
    [City] nvarchar(50)  NULL,
    [State] nvarchar(50)  NULL,
    [Zipcode] nvarchar(50)  NULL
);
GO

-- Creating table 'StoredFiles'
CREATE TABLE [dbo].[StoredFiles] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FileName] nvarchar(255)  NOT NULL,
    [ContentType] nvarchar(50)  NULL,
    [FileContent] varbinary(max)  NULL,
    [FileType] nchar(10)  NULL,
    [Notes] nvarchar(max)  NULL,
    [SubjectId] int  NOT NULL,
    [IsSubject] bit  NOT NULL,
    [ShowOnDetail] bit  NOT NULL
);
GO

-- Creating table 'Hyperlinks'
CREATE TABLE [dbo].[Hyperlinks] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Url] nchar(250)  NULL,
    [Notes] nvarchar(max)  NULL,
    [SubjectId] int  NOT NULL
);
GO

-- Creating table 'SearchResults'
CREATE TABLE [dbo].[SearchResults] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [UrlOrSource] nvarchar(250)  NOT NULL,
    [ResultText] nvarchar(max)  NULL,
    [Username] nvarchar(128)  NULL,
    [Notes] nvarchar(max)  NULL,
    [SubjectId] int  NOT NULL,
    [SubjectSearchRecipeItemId] int  NULL
);
GO

-- Creating table 'Videos'
CREATE TABLE [dbo].[Videos] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Url] nvarchar(250)  NULL,
    [Notes] nvarchar(max)  NULL,
    [SubjectId] int  NOT NULL
);
GO

-- Creating table 'Races'
CREATE TABLE [dbo].[Races] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'RelationshipTypes'
CREATE TABLE [dbo].[RelationshipTypes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL
);
GO

-- Creating table 'SubjectRelationships'
CREATE TABLE [dbo].[SubjectRelationships] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [SubjectId] int  NOT NULL,
    [RelatedSubjectId] int  NOT NULL,
    [RelationshipTypeId] int  NOT NULL,
    [Notes] nvarchar(max)  NULL,
    [Description] nvarchar(max)  NULL,
    [EmployerId] int  NULL,
    [BailBondId] int  NULL
);
GO

-- Creating table 'BookingCharges'
CREATE TABLE [dbo].[BookingCharges] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [BookingNumber] nvarchar(50)  NULL,
    [ChargeName] nvarchar(50)  NULL,
    [CodeNumber] nvarchar(50)  NULL,
    [Felony] bit  NOT NULL,
    [DateOccurred] datetime  NULL,
    [Jurisdiction] nvarchar(50)  NULL,
    [PoliceAgency] nvarchar(50)  NULL,
    [Address1] nvarchar(50)  NULL,
    [Address2] nvarchar(50)  NULL,
    [City] nvarchar(50)  NULL,
    [State] nvarchar(50)  NULL,
    [Zipcode] nvarchar(50)  NULL,
    [Detectives] nvarchar(max)  NULL,
    [Victim] nvarchar(max)  NULL,
    [Details] nvarchar(max)  NULL,
    [ReportNumber] nvarchar(50)  NULL,
    [Description] nvarchar(max)  NULL,
    [Notes] nvarchar(max)  NULL,
    [BailBondId] int  NOT NULL
);
GO

-- Creating table 'AKAs'
CREATE TABLE [dbo].[AKAs] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Firstname] nvarchar(50)  NULL,
    [Middlename] nvarchar(50)  NULL,
    [Lastname] nvarchar(50)  NULL,
    [SubjectId] int  NOT NULL
);
GO

-- Creating table 'SearchRecipes'
CREATE TABLE [dbo].[SearchRecipes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [Description] nvarchar(max)  NULL
);
GO

-- Creating table 'SearchRecipeItems'
CREATE TABLE [dbo].[SearchRecipeItems] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [UrlOrSource] nvarchar(500)  NULL,
    [Description] nvarchar(max)  NULL,
    [SearchRecipeId] int  NOT NULL,
    [SortOrder] int  NOT NULL
);
GO

-- Creating table 'SubjectSearchRecipes'
CREATE TABLE [dbo].[SubjectSearchRecipes] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [SubjectId] int  NOT NULL,
    [SearchRecipeId] int  NOT NULL,
    [Description] nvarchar(max)  NULL,
    [Notes] nvarchar(max)  NULL
);
GO

-- Creating table 'SubjectSearchRecipeItems'
CREATE TABLE [dbo].[SubjectSearchRecipeItems] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [SearchRecipeItemId] int  NOT NULL,
    [SubjectId] int  NOT NULL
);
GO

-- Creating table 'Employers'
CREATE TABLE [dbo].[Employers] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Disabled] bit  NOT NULL,
    [CreatedBy] nvarchar(128)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(128)  NULL,
    [ModifiedDate] datetime  NULL,
    [Name] nvarchar(50)  NULL,
    [JobTitle] nvarchar(50)  NULL,
    [PhoneNumber] nvarchar(50)  NULL,
    [Address1] nvarchar(150)  NULL,
    [Address2] nvarchar(150)  NULL,
    [City] nvarchar(100)  NULL,
    [State] nvarchar(50)  NULL,
    [Zipcode] nvarchar(20)  NULL,
    [SubjectId] int  NOT NULL,
    [Notes] nvarchar(max)  NULL,
    [StartDate] datetime  NULL,
    [EndDate] datetime  NULL
);
GO

-- Creating table 'AspNetUsers'
CREATE TABLE [dbo].[AspNetUsers] (
    [Id] nvarchar(450)  NOT NULL,
    [AccessFailedCount] int  NOT NULL,
    [ConcurrencyStamp] nvarchar(max)  NULL,
    [Email] nvarchar(256)  NULL,
    [EmailConfirmed] bit  NOT NULL,
    [LockoutEnabled] bit  NOT NULL,
    [LockoutEnd] datetimeoffset  NULL,
    [NormalizedEmail] nvarchar(256)  NULL,
    [NormalizedUserName] nvarchar(256)  NULL,
    [PasswordHash] nvarchar(max)  NULL,
    [PhoneNumber] nvarchar(max)  NULL,
    [PhoneNumberConfirmed] bit  NOT NULL,
    [SecurityStamp] nvarchar(max)  NULL,
    [TwoFactorEnabled] bit  NOT NULL,
    [UserName] nvarchar(256)  NULL
);
GO

-- Creating table 'AspNetUsers_Team'
CREATE TABLE [dbo].[AspNetUsers_Team] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [AspNetUsersId] nvarchar(450)  NOT NULL,
    [TeamId] int  NOT NULL,
    [Disabled] bit  NOT NULL,
    [CreatedBy] nvarchar(128)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(128)  NULL,
    [ModifiedDate] datetime  NULL
);
GO

-- Creating table 'Teams'
CREATE TABLE [dbo].[Teams] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(50)  NOT NULL,
    [Description] nvarchar(max)  NULL,
    [Disabled] bit  NOT NULL,
    [CreatedBy] nvarchar(128)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(128)  NULL,
    [ModifiedDate] datetime  NULL
);
GO

-- Creating table 'Team_Referrer'
CREATE TABLE [dbo].[Team_Referrer] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ReferrerId] int  NOT NULL,
    [TeamId] int  NOT NULL,
    [Disabled] bit  NOT NULL,
    [CreatedBy] nvarchar(128)  NOT NULL,
    [CreatedDate] datetime  NOT NULL,
    [ModifiedBy] nvarchar(128)  NULL,
    [ModifiedDate] datetime  NULL
);
GO

-- Creating table 'AspNetUsers_Referrer'
CREATE TABLE [dbo].[AspNetUsers_Referrer] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [ReferrerId] int  NOT NULL,
    [AspNetUsersId] nvarchar(450)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Referrers'
ALTER TABLE [dbo].[Referrers]
ADD CONSTRAINT [PK_Referrers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Subjects'
ALTER TABLE [dbo].[Subjects]
ADD CONSTRAINT [PK_Subjects]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Addresses'
ALTER TABLE [dbo].[Addresses]
ADD CONSTRAINT [PK_Addresses]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AddressTypes'
ALTER TABLE [dbo].[AddressTypes]
ADD CONSTRAINT [PK_AddressTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'BailBonds'
ALTER TABLE [dbo].[BailBonds]
ADD CONSTRAINT [PK_BailBonds]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EmailAddresses'
ALTER TABLE [dbo].[EmailAddresses]
ADD CONSTRAINT [PK_EmailAddresses]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'EmailTypes'
ALTER TABLE [dbo].[EmailTypes]
ADD CONSTRAINT [PK_EmailTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Genders'
ALTER TABLE [dbo].[Genders]
ADD CONSTRAINT [PK_Genders]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PhoneNumbers'
ALTER TABLE [dbo].[PhoneNumbers]
ADD CONSTRAINT [PK_PhoneNumbers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'PhoneTypes'
ALTER TABLE [dbo].[PhoneTypes]
ADD CONSTRAINT [PK_PhoneTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Vehicles'
ALTER TABLE [dbo].[Vehicles]
ADD CONSTRAINT [PK_Vehicles]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ActivityNotes'
ALTER TABLE [dbo].[ActivityNotes]
ADD CONSTRAINT [PK_ActivityNotes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'DriverLicenses'
ALTER TABLE [dbo].[DriverLicenses]
ADD CONSTRAINT [PK_DriverLicenses]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'StoredFiles'
ALTER TABLE [dbo].[StoredFiles]
ADD CONSTRAINT [PK_StoredFiles]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Hyperlinks'
ALTER TABLE [dbo].[Hyperlinks]
ADD CONSTRAINT [PK_Hyperlinks]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SearchResults'
ALTER TABLE [dbo].[SearchResults]
ADD CONSTRAINT [PK_SearchResults]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Videos'
ALTER TABLE [dbo].[Videos]
ADD CONSTRAINT [PK_Videos]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Races'
ALTER TABLE [dbo].[Races]
ADD CONSTRAINT [PK_Races]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'RelationshipTypes'
ALTER TABLE [dbo].[RelationshipTypes]
ADD CONSTRAINT [PK_RelationshipTypes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SubjectRelationships'
ALTER TABLE [dbo].[SubjectRelationships]
ADD CONSTRAINT [PK_SubjectRelationships]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'BookingCharges'
ALTER TABLE [dbo].[BookingCharges]
ADD CONSTRAINT [PK_BookingCharges]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AKAs'
ALTER TABLE [dbo].[AKAs]
ADD CONSTRAINT [PK_AKAs]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SearchRecipes'
ALTER TABLE [dbo].[SearchRecipes]
ADD CONSTRAINT [PK_SearchRecipes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SearchRecipeItems'
ALTER TABLE [dbo].[SearchRecipeItems]
ADD CONSTRAINT [PK_SearchRecipeItems]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SubjectSearchRecipes'
ALTER TABLE [dbo].[SubjectSearchRecipes]
ADD CONSTRAINT [PK_SubjectSearchRecipes]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'SubjectSearchRecipeItems'
ALTER TABLE [dbo].[SubjectSearchRecipeItems]
ADD CONSTRAINT [PK_SubjectSearchRecipeItems]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Employers'
ALTER TABLE [dbo].[Employers]
ADD CONSTRAINT [PK_Employers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AspNetUsers'
ALTER TABLE [dbo].[AspNetUsers]
ADD CONSTRAINT [PK_AspNetUsers]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AspNetUsers_Team'
ALTER TABLE [dbo].[AspNetUsers_Team]
ADD CONSTRAINT [PK_AspNetUsers_Team]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Teams'
ALTER TABLE [dbo].[Teams]
ADD CONSTRAINT [PK_Teams]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Team_Referrer'
ALTER TABLE [dbo].[Team_Referrer]
ADD CONSTRAINT [PK_Team_Referrer]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'AspNetUsers_Referrer'
ALTER TABLE [dbo].[AspNetUsers_Referrer]
ADD CONSTRAINT [PK_AspNetUsers_Referrer]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [ReferrerId] in table 'Subjects'
ALTER TABLE [dbo].[Subjects]
ADD CONSTRAINT [FK_Subject_Referrer_ReferrerId]
    FOREIGN KEY ([ReferrerId])
    REFERENCES [dbo].[Referrers]
        ([Id])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Subject_Referrer_ReferrerId'
CREATE INDEX [IX_FK_Subject_Referrer_ReferrerId]
ON [dbo].[Subjects]
    ([ReferrerId]);
GO

-- Creating foreign key on [SubjectId] in table 'Addresses'
ALTER TABLE [dbo].[Addresses]
ADD CONSTRAINT [FK_Address_Subject]
    FOREIGN KEY ([SubjectId])
    REFERENCES [dbo].[Subjects]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Address_Subject'
CREATE INDEX [IX_FK_Address_Subject]
ON [dbo].[Addresses]
    ([SubjectId]);
GO

-- Creating foreign key on [AddressTypeId] in table 'Addresses'
ALTER TABLE [dbo].[Addresses]
ADD CONSTRAINT [FK_Address_AddressType]
    FOREIGN KEY ([AddressTypeId])
    REFERENCES [dbo].[AddressTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Address_AddressType'
CREATE INDEX [IX_FK_Address_AddressType]
ON [dbo].[Addresses]
    ([AddressTypeId]);
GO

-- Creating foreign key on [SubjectId] in table 'BailBonds'
ALTER TABLE [dbo].[BailBonds]
ADD CONSTRAINT [FK_BailBond_Subject]
    FOREIGN KEY ([SubjectId])
    REFERENCES [dbo].[Subjects]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_BailBond_Subject'
CREATE INDEX [IX_FK_BailBond_Subject]
ON [dbo].[BailBonds]
    ([SubjectId]);
GO

-- Creating foreign key on [EmailTypeId] in table 'EmailAddresses'
ALTER TABLE [dbo].[EmailAddresses]
ADD CONSTRAINT [FK_EmailAddress_EmailType]
    FOREIGN KEY ([EmailTypeId])
    REFERENCES [dbo].[EmailTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EmailAddress_EmailType'
CREATE INDEX [IX_FK_EmailAddress_EmailType]
ON [dbo].[EmailAddresses]
    ([EmailTypeId]);
GO

-- Creating foreign key on [SubjectId] in table 'EmailAddresses'
ALTER TABLE [dbo].[EmailAddresses]
ADD CONSTRAINT [FK_EmailAddress_Subject]
    FOREIGN KEY ([SubjectId])
    REFERENCES [dbo].[Subjects]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_EmailAddress_Subject'
CREATE INDEX [IX_FK_EmailAddress_Subject]
ON [dbo].[EmailAddresses]
    ([SubjectId]);
GO

-- Creating foreign key on [GenderId] in table 'Subjects'
ALTER TABLE [dbo].[Subjects]
ADD CONSTRAINT [FK_Subject_Gender]
    FOREIGN KEY ([GenderId])
    REFERENCES [dbo].[Genders]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Subject_Gender'
CREATE INDEX [IX_FK_Subject_Gender]
ON [dbo].[Subjects]
    ([GenderId]);
GO

-- Creating foreign key on [PhoneTypeId] in table 'PhoneNumbers'
ALTER TABLE [dbo].[PhoneNumbers]
ADD CONSTRAINT [FK_PhoneNumber_PhoneType]
    FOREIGN KEY ([PhoneTypeId])
    REFERENCES [dbo].[PhoneTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PhoneNumber_PhoneType'
CREATE INDEX [IX_FK_PhoneNumber_PhoneType]
ON [dbo].[PhoneNumbers]
    ([PhoneTypeId]);
GO

-- Creating foreign key on [SubjectId] in table 'PhoneNumbers'
ALTER TABLE [dbo].[PhoneNumbers]
ADD CONSTRAINT [FK_PhoneNumber_Subject]
    FOREIGN KEY ([SubjectId])
    REFERENCES [dbo].[Subjects]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_PhoneNumber_Subject'
CREATE INDEX [IX_FK_PhoneNumber_Subject]
ON [dbo].[PhoneNumbers]
    ([SubjectId]);
GO

-- Creating foreign key on [SubjectId] in table 'Vehicles'
ALTER TABLE [dbo].[Vehicles]
ADD CONSTRAINT [FK_Vehicle_Subject]
    FOREIGN KEY ([SubjectId])
    REFERENCES [dbo].[Subjects]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Vehicle_Subject'
CREATE INDEX [IX_FK_Vehicle_Subject]
ON [dbo].[Vehicles]
    ([SubjectId]);
GO

-- Creating foreign key on [SubjectId] in table 'ActivityNotes'
ALTER TABLE [dbo].[ActivityNotes]
ADD CONSTRAINT [FK_ActivityNotes_Subject]
    FOREIGN KEY ([SubjectId])
    REFERENCES [dbo].[Subjects]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ActivityNotes_Subject'
CREATE INDEX [IX_FK_ActivityNotes_Subject]
ON [dbo].[ActivityNotes]
    ([SubjectId]);
GO

-- Creating foreign key on [SubjectId] in table 'DriverLicenses'
ALTER TABLE [dbo].[DriverLicenses]
ADD CONSTRAINT [FK_DriverLicense_Subject]
    FOREIGN KEY ([SubjectId])
    REFERENCES [dbo].[Subjects]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_DriverLicense_Subject'
CREATE INDEX [IX_FK_DriverLicense_Subject]
ON [dbo].[DriverLicenses]
    ([SubjectId]);
GO

-- Creating foreign key on [SubjectId] in table 'StoredFiles'
ALTER TABLE [dbo].[StoredFiles]
ADD CONSTRAINT [FK_StoredFile_Subject]
    FOREIGN KEY ([SubjectId])
    REFERENCES [dbo].[Subjects]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_StoredFile_Subject'
CREATE INDEX [IX_FK_StoredFile_Subject]
ON [dbo].[StoredFiles]
    ([SubjectId]);
GO

-- Creating foreign key on [SubjectId] in table 'Hyperlinks'
ALTER TABLE [dbo].[Hyperlinks]
ADD CONSTRAINT [FK_Hyperlink_Subject]
    FOREIGN KEY ([SubjectId])
    REFERENCES [dbo].[Subjects]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Hyperlink_Subject'
CREATE INDEX [IX_FK_Hyperlink_Subject]
ON [dbo].[Hyperlinks]
    ([SubjectId]);
GO

-- Creating foreign key on [SubjectId] in table 'SearchResults'
ALTER TABLE [dbo].[SearchResults]
ADD CONSTRAINT [FK_SearchResult_Subject]
    FOREIGN KEY ([SubjectId])
    REFERENCES [dbo].[Subjects]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SearchResult_Subject'
CREATE INDEX [IX_FK_SearchResult_Subject]
ON [dbo].[SearchResults]
    ([SubjectId]);
GO

-- Creating foreign key on [SubjectId] in table 'Videos'
ALTER TABLE [dbo].[Videos]
ADD CONSTRAINT [FK_Video_Subject]
    FOREIGN KEY ([SubjectId])
    REFERENCES [dbo].[Subjects]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Video_Subject'
CREATE INDEX [IX_FK_Video_Subject]
ON [dbo].[Videos]
    ([SubjectId]);
GO

-- Creating foreign key on [RaceId] in table 'Subjects'
ALTER TABLE [dbo].[Subjects]
ADD CONSTRAINT [FK_Subject_Race]
    FOREIGN KEY ([RaceId])
    REFERENCES [dbo].[Races]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Subject_Race'
CREATE INDEX [IX_FK_Subject_Race]
ON [dbo].[Subjects]
    ([RaceId]);
GO

-- Creating foreign key on [RelationshipTypeId] in table 'SubjectRelationships'
ALTER TABLE [dbo].[SubjectRelationships]
ADD CONSTRAINT [FK_SubjectRelationship_RelationshipType]
    FOREIGN KEY ([RelationshipTypeId])
    REFERENCES [dbo].[RelationshipTypes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SubjectRelationship_RelationshipType'
CREATE INDEX [IX_FK_SubjectRelationship_RelationshipType]
ON [dbo].[SubjectRelationships]
    ([RelationshipTypeId]);
GO

-- Creating foreign key on [SubjectId] in table 'SubjectRelationships'
ALTER TABLE [dbo].[SubjectRelationships]
ADD CONSTRAINT [FK_SubjectRelationship_Subject]
    FOREIGN KEY ([SubjectId])
    REFERENCES [dbo].[Subjects]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SubjectRelationship_Subject'
CREATE INDEX [IX_FK_SubjectRelationship_Subject]
ON [dbo].[SubjectRelationships]
    ([SubjectId]);
GO

-- Creating foreign key on [RelatedSubjectId] in table 'SubjectRelationships'
ALTER TABLE [dbo].[SubjectRelationships]
ADD CONSTRAINT [FK_SubjectRelationship_Subject1]
    FOREIGN KEY ([RelatedSubjectId])
    REFERENCES [dbo].[Subjects]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SubjectRelationship_Subject1'
CREATE INDEX [IX_FK_SubjectRelationship_Subject1]
ON [dbo].[SubjectRelationships]
    ([RelatedSubjectId]);
GO

-- Creating foreign key on [BailBondId] in table 'BookingCharges'
ALTER TABLE [dbo].[BookingCharges]
ADD CONSTRAINT [FK_BookingCharge_BailBond]
    FOREIGN KEY ([BailBondId])
    REFERENCES [dbo].[BailBonds]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_BookingCharge_BailBond'
CREATE INDEX [IX_FK_BookingCharge_BailBond]
ON [dbo].[BookingCharges]
    ([BailBondId]);
GO

-- Creating foreign key on [SubjectId] in table 'AKAs'
ALTER TABLE [dbo].[AKAs]
ADD CONSTRAINT [FK_AKA_Subject]
    FOREIGN KEY ([SubjectId])
    REFERENCES [dbo].[Subjects]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AKA_Subject'
CREATE INDEX [IX_FK_AKA_Subject]
ON [dbo].[AKAs]
    ([SubjectId]);
GO

-- Creating foreign key on [SearchRecipeId] in table 'SearchRecipeItems'
ALTER TABLE [dbo].[SearchRecipeItems]
ADD CONSTRAINT [FK_SearchRecipeItem_SearchRecipe]
    FOREIGN KEY ([SearchRecipeId])
    REFERENCES [dbo].[SearchRecipes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SearchRecipeItem_SearchRecipe'
CREATE INDEX [IX_FK_SearchRecipeItem_SearchRecipe]
ON [dbo].[SearchRecipeItems]
    ([SearchRecipeId]);
GO

-- Creating foreign key on [SearchRecipeId] in table 'SubjectSearchRecipes'
ALTER TABLE [dbo].[SubjectSearchRecipes]
ADD CONSTRAINT [FK_Subject_SearchRecipe_SearchRecipe]
    FOREIGN KEY ([SearchRecipeId])
    REFERENCES [dbo].[SearchRecipes]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Subject_SearchRecipe_SearchRecipe'
CREATE INDEX [IX_FK_Subject_SearchRecipe_SearchRecipe]
ON [dbo].[SubjectSearchRecipes]
    ([SearchRecipeId]);
GO

-- Creating foreign key on [SubjectId] in table 'SubjectSearchRecipes'
ALTER TABLE [dbo].[SubjectSearchRecipes]
ADD CONSTRAINT [FK_Subject_SearchRecipe_Subject]
    FOREIGN KEY ([SubjectId])
    REFERENCES [dbo].[Subjects]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Subject_SearchRecipe_Subject'
CREATE INDEX [IX_FK_Subject_SearchRecipe_Subject]
ON [dbo].[SubjectSearchRecipes]
    ([SubjectId]);
GO

-- Creating foreign key on [SearchRecipeItemId] in table 'SubjectSearchRecipeItems'
ALTER TABLE [dbo].[SubjectSearchRecipeItems]
ADD CONSTRAINT [FK_SubjectSearchRecipeItem_SearchRecipeItem]
    FOREIGN KEY ([SearchRecipeItemId])
    REFERENCES [dbo].[SearchRecipeItems]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SubjectSearchRecipeItem_SearchRecipeItem'
CREATE INDEX [IX_FK_SubjectSearchRecipeItem_SearchRecipeItem]
ON [dbo].[SubjectSearchRecipeItems]
    ([SearchRecipeItemId]);
GO

-- Creating foreign key on [SubjectSearchRecipeItemId] in table 'SearchResults'
ALTER TABLE [dbo].[SearchResults]
ADD CONSTRAINT [FK_SearchResult_SubjectSearchRecipeItem]
    FOREIGN KEY ([SubjectSearchRecipeItemId])
    REFERENCES [dbo].[SubjectSearchRecipeItems]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SearchResult_SubjectSearchRecipeItem'
CREATE INDEX [IX_FK_SearchResult_SubjectSearchRecipeItem]
ON [dbo].[SearchResults]
    ([SubjectSearchRecipeItemId]);
GO

-- Creating foreign key on [SubjectId] in table 'SubjectSearchRecipeItems'
ALTER TABLE [dbo].[SubjectSearchRecipeItems]
ADD CONSTRAINT [FK_SubjectSearchRecipeItem_Subject]
    FOREIGN KEY ([SubjectId])
    REFERENCES [dbo].[Subjects]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SubjectSearchRecipeItem_Subject'
CREATE INDEX [IX_FK_SubjectSearchRecipeItem_Subject]
ON [dbo].[SubjectSearchRecipeItems]
    ([SubjectId]);
GO

-- Creating foreign key on [SubjectId] in table 'Employers'
ALTER TABLE [dbo].[Employers]
ADD CONSTRAINT [FK_Employer_Subject]
    FOREIGN KEY ([SubjectId])
    REFERENCES [dbo].[Subjects]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Employer_Subject'
CREATE INDEX [IX_FK_Employer_Subject]
ON [dbo].[Employers]
    ([SubjectId]);
GO

-- Creating foreign key on [BailBondId] in table 'SubjectRelationships'
ALTER TABLE [dbo].[SubjectRelationships]
ADD CONSTRAINT [FK_SubjectRelationship_BailBond]
    FOREIGN KEY ([BailBondId])
    REFERENCES [dbo].[BailBonds]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SubjectRelationship_BailBond'
CREATE INDEX [IX_FK_SubjectRelationship_BailBond]
ON [dbo].[SubjectRelationships]
    ([BailBondId]);
GO

-- Creating foreign key on [EmployerId] in table 'SubjectRelationships'
ALTER TABLE [dbo].[SubjectRelationships]
ADD CONSTRAINT [FK_SubjectRelationship_SubjectRelationship]
    FOREIGN KEY ([EmployerId])
    REFERENCES [dbo].[Employers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_SubjectRelationship_SubjectRelationship'
CREATE INDEX [IX_FK_SubjectRelationship_SubjectRelationship]
ON [dbo].[SubjectRelationships]
    ([EmployerId]);
GO

-- Creating foreign key on [AspNetUsersId] in table 'AspNetUsers_Team'
ALTER TABLE [dbo].[AspNetUsers_Team]
ADD CONSTRAINT [FK_AspNetUsers_Team_AspNetUsers]
    FOREIGN KEY ([AspNetUsersId])
    REFERENCES [dbo].[AspNetUsers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AspNetUsers_Team_AspNetUsers'
CREATE INDEX [IX_FK_AspNetUsers_Team_AspNetUsers]
ON [dbo].[AspNetUsers_Team]
    ([AspNetUsersId]);
GO

-- Creating foreign key on [TeamId] in table 'AspNetUsers_Team'
ALTER TABLE [dbo].[AspNetUsers_Team]
ADD CONSTRAINT [FK_AspNetUsers_Team_Team]
    FOREIGN KEY ([TeamId])
    REFERENCES [dbo].[Teams]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AspNetUsers_Team_Team'
CREATE INDEX [IX_FK_AspNetUsers_Team_Team]
ON [dbo].[AspNetUsers_Team]
    ([TeamId]);
GO

-- Creating foreign key on [ReferrerId] in table 'Team_Referrer'
ALTER TABLE [dbo].[Team_Referrer]
ADD CONSTRAINT [FK_Team_Referrer_Referrer]
    FOREIGN KEY ([ReferrerId])
    REFERENCES [dbo].[Referrers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Team_Referrer_Referrer'
CREATE INDEX [IX_FK_Team_Referrer_Referrer]
ON [dbo].[Team_Referrer]
    ([ReferrerId]);
GO

-- Creating foreign key on [TeamId] in table 'Team_Referrer'
ALTER TABLE [dbo].[Team_Referrer]
ADD CONSTRAINT [FK_Team_Referrer_Team]
    FOREIGN KEY ([TeamId])
    REFERENCES [dbo].[Teams]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_Team_Referrer_Team'
CREATE INDEX [IX_FK_Team_Referrer_Team]
ON [dbo].[Team_Referrer]
    ([TeamId]);
GO

-- Creating foreign key on [AspNetUsersId] in table 'AspNetUsers_Referrer'
ALTER TABLE [dbo].[AspNetUsers_Referrer]
ADD CONSTRAINT [FK_AspNetUsers_Referrer_AspNetUsers]
    FOREIGN KEY ([AspNetUsersId])
    REFERENCES [dbo].[AspNetUsers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AspNetUsers_Referrer_AspNetUsers'
CREATE INDEX [IX_FK_AspNetUsers_Referrer_AspNetUsers]
ON [dbo].[AspNetUsers_Referrer]
    ([AspNetUsersId]);
GO

-- Creating foreign key on [ReferrerId] in table 'AspNetUsers_Referrer'
ALTER TABLE [dbo].[AspNetUsers_Referrer]
ADD CONSTRAINT [FK_AspNetUsers_Referrer_Referrer]
    FOREIGN KEY ([ReferrerId])
    REFERENCES [dbo].[Referrers]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_AspNetUsers_Referrer_Referrer'
CREATE INDEX [IX_FK_AspNetUsers_Referrer_Referrer]
ON [dbo].[AspNetUsers_Referrer]
    ([ReferrerId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------