using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AutoMapper;
using MvcContrib.EnumerableExtensions;
using Tws.AfFirst2.Services;
using Tws.AfFirst2.Services.Services;
using Tws.AfFirst2.Web.ViewModels.AspNetUsers_Referrer;

namespace Tws.AfFirst2.Web.Controllers.Api
{
    [RoutePrefix("~/Api/AspNetUsers_Referrer")]
    public class AspNetUsers_ReferrerController : ApiController
    {
        private readonly IAspNetUsers_ReferrerSvc _aspNetUsers_ReferrerSvc;					
		private readonly IAspNetUserService _aspNetUserService;					
		private readonly IReferrerService _referrerService;


			public AspNetUsers_ReferrerController(IAspNetUsers_ReferrerSvc aspNetUsers_ReferrerSvc,
								IAspNetUserService aspNetUserService,
								IReferrerService referrerService
	)
        {
            _aspNetUsers_ReferrerSvc = aspNetUsers_ReferrerSvc;
			_aspNetUserService = aspNetUserService;
			_referrerService = referrerService;

        }


			
        // GET: api/AspNetUsers_Referrer
        [HttpGet]
        [Route("~/Api/AspNetUser/{aspNetUserId}/AspNetUsers_Referrer/GetByAspNetUser")]
        public AspNetUsers_ReferrerListVm GetByAspNetUser(int aspNetUserId)
        {
            var aspNetUsers_Referrers = _aspNetUsers_ReferrerSvc.GetAll().Where(a=>a.AspNetUserId==aspNetUserId && a.Disabled == false).ToList();
			var referrerItems = _referrerSvc.GetAll().ToSelectList(s => s.Id, s => s.AdditionalInfo).ToList();
            
            var aspNetUsers_ReferrerList = new AspNetUsers_ReferrerListVm
            {
                AspNetUsers_Referrers = Mapper.Map<IEnumerable<AspNetUsers_Referrer>, IEnumerable<AspNetUsers_ReferrerVm>>(aspNetUsers_Referrers),
                AspNetUserId = aspNetUserId,
				Referrers = referrerItems,
                 
            };
           
            return aspNetUsers_ReferrerList;
        }
			
        // GET: api/AspNetUsers_Referrer
        [HttpGet]
        [Route("~/Api/Referrer/{referrerId}/AspNetUsers_Referrer/GetByReferrer")]
        public AspNetUsers_ReferrerListVm GetByReferrer(int referrerId)
        {
            var aspNetUsers_Referrers = _aspNetUsers_ReferrerSvc.GetAll().Where(a=>a.ReferrerId==referrerId && a.Disabled == false).ToList();
			var aspNetUserItems = _aspNetUserSvc.GetAll().ToSelectList(s => s.Id, s => s.Id).ToList();
            
            var aspNetUsers_ReferrerList = new AspNetUsers_ReferrerListVm
            {
                AspNetUsers_Referrers = Mapper.Map<IEnumerable<AspNetUsers_Referrer>, IEnumerable<AspNetUsers_ReferrerVm>>(aspNetUsers_Referrers),
                ReferrerId = referrerId,
				AspNetUsers = aspNetUserItems,
                 
            };
           
            return aspNetUsers_ReferrerList;
        }


        //// GET: api/AspNetUsers_Referrer/5
        //public AspNetUsers_Referrer Get(int id)
        //{
        //    return _aspNetUsers_ReferrerSvc.GetById(id);
        //}

        // POST: api/AspNetUsers_Referrer
        public HttpResponseMessage Post(AspNetUsers_Referrer aspNetUsers_Referrer)
        {
            if (ModelState.IsValid)
            {
                _aspNetUsers_ReferrerSvc.Update(aspNetUsers_Referrer);

                HttpResponseMessage response = Request.CreateResponse(HttpStatusCode.Created, aspNetUsers_Referrer);
                response.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = aspNetUsers_Referrer.Id }));
                return response;
            }
            else
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            } 
        }

        // DELETE: api/AspNetUsers_Referrer/5
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                _aspNetUsers_ReferrerSvc.Delete(id);
            }
            catch (Exception ex)
            {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            }

            return Request.CreateResponse(HttpStatusCode.OK);  
        }
    }
}
