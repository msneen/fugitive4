using System;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Threading;
using System.Web;
using System.Web.SessionState;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using Tws.AfFirst2.Services;
using Tws.AfFirst2.Services.Services;

namespace Tws.AfFirst2.Web.UiTests
{
#if(!DEBUG)
    [Ignore]
#endif
    [TestClass]
    public class BailBondUiTests
    {
        IWebDriver _driver;
        private string _baseUrl;

        private void InitializeDriver()
        {
            _driver = new FirefoxDriver();
            _baseUrl = "http://localhost/";
        }


        [TestMethod]
        public void BailBond_UI_Create()
        {
            //Launch the Web Browser
            InitializeDriver();

			AccountTests.DoLogin(_driver, _baseUrl);
            

            _driver.Navigate().GoToUrl(_baseUrl + "/Tws.AfFirst2.Web/BailBond/Create");
            Thread.Sleep(5000);

            _driver.FindElement(By.Id("IssuingBondsman")).Clear();
            _driver.FindElement(By.Id("IssuingBondsman")).SendKeys("Fix Me");
            _driver.FindElement(By.Id("IssuingBondCompany")).Clear();
            _driver.FindElement(By.Id("IssuingBondCompany")).SendKeys("Fix Me");
            _driver.FindElement(By.Id("Court")).Clear();
            _driver.FindElement(By.Id("Court")).SendKeys("Fix Me");
            _driver.FindElement(By.Id("Description")).Clear();
            _driver.FindElement(By.Id("Description")).SendKeys("Fix Me");
            _driver.FindElement(By.Id("BookingNumber")).Clear();
            _driver.FindElement(By.Id("BookingNumber")).SendKeys("Fix Me");
			new SelectElement(_driver.FindElement(By.Id("SubjectId"))).SelectByText("Fix Me");

            _driver.FindElement(By.CssSelector("input[type=\"submit\"]")).Click();

            Thread.Sleep(2000);
            Assert.AreEqual(_baseUrl + "Tws.AfFirst2.Web/BailBond", _driver.Url);


            //Cleanup
            try
            {
                var bailBondSvc = GetBailBondSvc();
                var bailBondRecords = bailBondSvc.GetAll().Where(l => l.IssuingBondsman == "Fix Me");
                foreach (var bailBond in bailBondRecords)
                {
                    bailBondSvc.Delete(bailBond.Id);
                }
            }
            catch
            {
                // ignored
            }
        }


        [TestMethod]
        public void BailBond_UI_Edit()
        {
            //Setup
            var bailBondSvc = GetBailBondSvc();
            var newDeletion = bailBondSvc.CreateNew();

            newDeletion.OriginalArrestDate = DateTime.Now;

            newDeletion.IssueDate = DateTime.Now;

            newDeletion.DefaultDate = DateTime.Now;

            newDeletion.IssuingBondsman = "Fix Me";

            newDeletion.IssuingBondCompany = "Fix Me";

            newDeletion.Court = "Fix Me";

            newDeletion.Description = "Fix Me";

            newDeletion.SubjectId = 1;

            newDeletion.BookingNumber = "Fix Me";

            newDeletion.BondAmount = "Fix Me";

            newDeletion.PaymentAmount = "Fix Me";

            newDeletion.PaymentDate = DateTime.Now;

            newDeletion.Done = false;


            bailBondSvc.Update(newDeletion);
            var id = newDeletion.Id;
            //Launch the Web Browser
            InitializeDriver();

			AccountTests.DoLogin(_driver, _baseUrl);

            Random random = new Random();
            int randomNumber = random.Next(0, 10000);

            _driver.Navigate().GoToUrl(_baseUrl +string.Format( "Tws.AfFirst2.Web/BailBond/Edit/{0}", id));
            Thread.Sleep(5000);

			
            var randomOriginalArrestDate = "FixMe" + randomNumber;


            _driver.FindElement(By.Id("OriginalArrestDate")).Clear();
            _driver.FindElement(By.Id("OriginalArrestDate")).SendKeys(randomOriginalArrestDate);


            _driver.FindElement(By.CssSelector("input[type=\"submit\"]")).Click();

            Thread.Sleep(2000);
            Assert.AreEqual(_baseUrl + "Tws.AfFirst2.Web/BailBond", _driver.Url);
			Thread.Sleep(2000);

            bailBondSvc = GetBailBondSvc();
            var updatedBailBond = bailBondSvc.GetById(id);
            Assert.AreEqual(randomOriginalArrestDate, updatedBailBond.OriginalArrestDate);

        }

        [TestMethod]
        public void BailBond_UI_Delete()
        {
            //Setup
            var bailBondSvc = GetBailBondSvc();
            var newDeletion = bailBondSvc.CreateNew();

            newDeletion.OriginalArrestDate = DateTime.Now;

            newDeletion.IssueDate = DateTime.Now;

            newDeletion.DefaultDate = DateTime.Now;

            newDeletion.IssuingBondsman = "Fix Me";

            newDeletion.IssuingBondCompany = "Fix Me";

            newDeletion.Court = "Fix Me";

            newDeletion.Description = "Fix Me";

            newDeletion.SubjectId = 1;

            newDeletion.BookingNumber = "Fix Me";

            newDeletion.BondAmount = "Fix Me";

            newDeletion.PaymentAmount = "Fix Me";

            newDeletion.PaymentDate = DateTime.Now;

            newDeletion.Done = false;


            bailBondSvc.Update(newDeletion);
            var id = newDeletion.Id;

            //Test
            //Launch the Web Browser
            InitializeDriver();

			AccountTests.DoLogin(_driver, _baseUrl);

            _driver.Navigate().GoToUrl(_baseUrl + string.Format( "/Tws.AfFirst2.Web/BailBond/Delete/{0}", id));
            Thread.Sleep(5000);

            _driver.FindElement(By.CssSelector("input[type=\"submit\"]")).Click();

            //Verify
            Thread.Sleep(2000);
            Assert.AreEqual(_baseUrl + "Tws.AfFirst2.Web/BailBond", _driver.Url);
			Thread.Sleep(2000);

            bailBondSvc = GetBailBondSvc();
            var deletionFound = bailBondSvc.GetById(id);
            Assert.IsTrue(deletionFound.Disabled);

        }

        [TestCleanup]
        public void CleanupTest()
        {
            //Close the browser
            _driver.Close();

            //End the test
            _driver.Quit();

			//Cleanup
            var bailBondSvc = GetBailBondSvc();
            var fixMeList = bailBondSvc.GetAll().Where(a => a.IssuingBondsman.Contains( "Fix Me") || a.IssuingBondsman.Contains( "FixMe") );
            foreach (var fixMe in fixMeList)
            {
                bailBondSvc.Delete(fixMe.Id);
            }
        }

        private static IBailBondSvc GetBailBondSvc()
        {

            AFFirst2Entities afFirst2Entities = new AFFirst2Entities();
            IBailBondSvc bailBondSvc = new BailBondSvc(afFirst2Entities, FakeHttpContext("http://localhost/BailBond"));
            return bailBondSvc;
        }

		public static HttpContext FakeHttpContext(string url)
        {
            var uri = new Uri(url);
            var httpRequest = new HttpRequest(string.Empty, uri.ToString(),
                                                uri.Query.TrimStart('?'));
            var stringWriter = new StringWriter();
            var httpResponse = new HttpResponse(stringWriter);
            var httpContext = new HttpContext(httpRequest, httpResponse);

            var sessionContainer = new HttpSessionStateContainer("id",
                                            new SessionStateItemCollection(),
                                            new HttpStaticObjectsCollection(),
                                            10, true, HttpCookieMode.AutoDetect,
                                            SessionStateMode.InProc, false);

            SessionStateUtility.AddHttpSessionStateToContext(
                                                 httpContext, sessionContainer);

            httpContext.User = new GenericPrincipal(
                new GenericIdentity("username"),
                new string[0]
                );

            return httpContext;
        }
    }
}
