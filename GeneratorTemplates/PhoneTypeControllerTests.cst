using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.SessionState;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Tws.AfFirst2.Services;
using Tws.AfFirst2.Services.Services;
using Tws.AfFirst2.Web.Controllers;

namespace Tws.AfFirst2.Web.Tests.Controllers
{
    [TestClass]
    public class PhoneTypeControllerTest
    {
        [TestInitialize]
        public void Initialize()
        {
            App_Start.AutoMapperProfileRegister.Start();
        }

        [TestMethod]
        public void PhoneType_Controller_Index()
        {
            // Arrange
            PhoneTypeController controller = new PhoneTypeController(new PhoneTypeSvc(new AFFirst2Entities(), HttpContext.Current)		);

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.AreEqual("Index", result.ViewBag.Title);
        }

        [TestMethod]
        public void PhoneType_Controller_Create()
        {
            // Arrange
            PhoneTypeController controller = new PhoneTypeController(new PhoneTypeSvc(new AFFirst2Entities(), HttpContext.Current)		);

            // Act
            ViewResult result = controller.Create() as ViewResult;

            // Assert
            Assert.AreEqual("Create", result.ViewBag.Title);
        }

        [TestMethod]
        public void PhoneType_Controller_Edit()
        {
            // Arrange
			var mockContext = MakeMockContextWithData();
			var phoneTypeSvc =  GetService(mockContext);
            PhoneTypeController controller = new PhoneTypeController(phoneTypeSvc		);
            var id = phoneTypeSvc.GetAll().FirstOrDefault().Id;

            // Act
            ViewResult result = controller.Edit(id) as ViewResult;

            // Assert
            Assert.AreEqual("Edit", result.ViewBag.Title);
        }

		[TestMethod]
        public void PhoneType_Controller_Delete()
        {
            // Arrange
            PhoneTypeSvc phoneTypeSvc = new PhoneTypeSvc(new AFFirst2Entities(), HttpContext.Current);
            PhoneTypeController controller = new PhoneTypeController(phoneTypeSvc		);
            //var id = phoneTypeSvc.GetAll().FirstOrDefault().Id;

            // Act
            ViewResult result = controller.Delete(0) as ViewResult;

            // Assert
            Assert.AreEqual("Delete", result.ViewBag.Title);
        }

		        private static IPhoneTypeSvc GetService(Mock<AFFirst2Entities> mockContext = null, HttpContext mockHttpContext = null)
        {

            if (mockContext == null)
            {
                mockContext = MakeMockContextWithData();
            }
			if (mockHttpContext == null)
            {
                mockHttpContext = FakeHttpContext("http://localhost/PhoneType/");
            }

            var service = new PhoneTypeSvc(mockContext.Object, mockHttpContext);
            return service;
        }

        public static HttpContext FakeHttpContext(string url)
        {
            var uri = new Uri(url);
            var httpRequest = new HttpRequest(string.Empty, uri.ToString(),
                                                uri.Query.TrimStart('?'));
            var stringWriter = new StringWriter();
            var httpResponse = new HttpResponse(stringWriter);
            var httpContext = new HttpContext(httpRequest, httpResponse);

            var sessionContainer = new HttpSessionStateContainer("id",
                                            new SessionStateItemCollection(),
                                            new HttpStaticObjectsCollection(),
                                            10, true, HttpCookieMode.AutoDetect,
                                            SessionStateMode.InProc, false);

            SessionStateUtility.AddHttpSessionStateToContext(
                                                 httpContext, sessionContainer);

            httpContext.User = new GenericPrincipal(
                new GenericIdentity("username"),
                new string[0]
                );

            return httpContext;
        }

		        private static Mock<AFFirst2Entities> MakeMockContextWithData()
        {
            var data = new List<PhoneType>
            {
                new PhoneType 
				{
					Id = 1,
					Name = "Fix Me", 
				},
                new PhoneType 
				{
					Id = 2,
					Name = "Fix Me", 
				},
                new PhoneType 
				{
					Id = 3,
					Name = "Fix Me", 
				},
                new PhoneType 
				{
					Id = 4,
					Name = "Fix Me", 
				},

            }.AsQueryable();

            var mockSet = new Mock<DbSet<PhoneType>>();
            mockSet.As<IQueryable<PhoneType>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<PhoneType>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<PhoneType>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<PhoneType>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            var mockContext = new Mock<AFFirst2Entities>();
            mockContext.Setup(c => c.PhoneTypes).Returns(mockSet.Object);
            return mockContext;
        }

    }
}
