using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.SessionState;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Tws.AfFirst2.Services.Services;

namespace Tws.AfFirst2.Services.Tests
{

    [TestClass]
    public class ActivityNoteTests
    {
        [TestMethod]
        public void ActivityNote_Service_GetAll()
        {
            var service = GetService();
            var activityNotes = service.GetAll();

            Assert.AreEqual(4, activityNotes.Count);

			//Programmer, add asserts for specific data here.  The mock data to match is defined at
			//The end of this file
            //Assert.AreEqual("Mike", activityNotes[0].Firstname);
            //Assert.AreEqual("Kirk", activityNotes[1].Firstname);
            //Assert.AreEqual("Brian", activityNotes[2].Firstname); 
        }

        [TestMethod]
        public void ActivityNote_Service_GetById()
        {
            var service = GetService();
            var activityNote = service.GetById(2);

            Assert.AreEqual(2, activityNote.Id);
        }

        [TestMethod]
        public void ActivityNote_Service_Add()
        {
            var mockSet = new Mock<DbSet<ActivityNote>>();

            var mockContext = new Mock<AFFirst2Entities>();
            mockContext.Setup(m => m.Set<ActivityNote>()).Returns(mockSet.Object);

            var service = GetService(mockContext);

            var activityNote = service.CreateNew();

            activityNote.ActivityDate = DateTime.Now;
            activityNote.Latitude = "Fix Me";
            activityNote.Longitude = "Fix Me";
            activityNote.Address = "Fix Me";
            activityNote.PerformedBy = "Fix Me";
            activityNote.Description = "Fix Me";
            activityNote.SubjectId = 1;

            service.Update(activityNote);

            //mockSet.Verify(svc => svc.Add(It.IsAny<ActivityNote>()), Times.Once);


            mockContext.Verify(m => m.SaveChanges(), Times.Once());
        }

        [TestMethod]
        public void ActivityNote_Service_Update()
        {
            var mockContext = MakeMockContextWithData();
            var service = GetService(mockContext);
            var activityNote = service.GetById(2);

            activityNote.ActivityDate = DateTime.Now;
            activityNote.Latitude = "Fix Me";
            activityNote.Longitude = "Fix Me";
            activityNote.Address = "Fix Me";
            activityNote.PerformedBy = "Fix Me";
            activityNote.Description = "Fix Me";
            activityNote.SubjectId = 1;

            service.Update(activityNote);

            mockContext.Verify(m => m.SaveChanges(), Times.Once());           
        }

		        [TestMethod]
        public void ActivityNote_Service_Delete()
        {
            var mockContext = MakeMockContextWithData();
            var service = GetService(mockContext);
            var activityNote = service.GetById(4);


            service.Delete(activityNote.Id);

            mockContext.Verify(m => m.SaveChanges(), Times.Once());           
        }


        private static IActivityNoteSvc GetService(Mock<AFFirst2Entities> mockContext = null, HttpContext mockHttpContext = null)
        {

            if (mockContext == null)
            {
                mockContext = MakeMockContextWithData();
            }
			if (mockHttpContext == null)
            {
                mockHttpContext = FakeHttpContext("http://localhost/ActivityNote/");
            }

            var service = new ActivityNoteSvc(mockContext.Object, mockHttpContext);
            return service;
        }

		public static HttpContext FakeHttpContext(string url)
        {
            var uri = new Uri(url);
            var httpRequest = new HttpRequest(string.Empty, uri.ToString(),
                                                uri.Query.TrimStart('?'));
            var stringWriter = new StringWriter();
            var httpResponse = new HttpResponse(stringWriter);
            var httpContext = new HttpContext(httpRequest, httpResponse);

            var sessionContainer = new HttpSessionStateContainer("id",
                                            new SessionStateItemCollection(),
                                            new HttpStaticObjectsCollection(),
                                            10, true, HttpCookieMode.AutoDetect,
                                            SessionStateMode.InProc, false);

            SessionStateUtility.AddHttpSessionStateToContext(
                                                 httpContext, sessionContainer);

            httpContext.User = new GenericPrincipal(
                new GenericIdentity("username"),
                new string[0]
                );

            return httpContext;
        }

        private static Mock<AFFirst2Entities> MakeMockContextWithData()
        {
            var data = new List<ActivityNote>
            {
                new ActivityNote 
				{
					Id = 1,
					ActivityDate = DateTime.Now, 
					Latitude = "Fix Me", 
					Longitude = "Fix Me", 
					Address = "Fix Me", 
					PerformedBy = "Fix Me", 
					Description = "Fix Me", 
					SubjectId = 1, 
				},
                new ActivityNote 
				{
					Id = 2,
					ActivityDate = DateTime.Now, 
					Latitude = "Fix Me", 
					Longitude = "Fix Me", 
					Address = "Fix Me", 
					PerformedBy = "Fix Me", 
					Description = "Fix Me", 
					SubjectId = 1, 
				},
                new ActivityNote 
				{
					Id = 3,
					ActivityDate = DateTime.Now, 
					Latitude = "Fix Me", 
					Longitude = "Fix Me", 
					Address = "Fix Me", 
					PerformedBy = "Fix Me", 
					Description = "Fix Me", 
					SubjectId = 1, 
				},
                new ActivityNote 
				{
					Id = 4,
					ActivityDate = DateTime.Now, 
					Latitude = "Fix Me", 
					Longitude = "Fix Me", 
					Address = "Fix Me", 
					PerformedBy = "Fix Me", 
					Description = "Fix Me", 
					SubjectId = 1, 
				},

            }.AsQueryable();

            var mockSet = new Mock<DbSet<ActivityNote>>();
            mockSet.As<IQueryable<ActivityNote>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<ActivityNote>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<ActivityNote>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<ActivityNote>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator());

            var mockContext = new Mock<AFFirst2Entities>();
            mockContext.Setup(c => c.ActivityNotes).Returns(mockSet.Object);
            return mockContext;
        }
    }
}

